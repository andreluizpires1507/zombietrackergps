#-----------------------------------------------------------------------
# Copyright 2019-2022 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

QT           += core widgets gui charts Marble svg xml concurrent
TARGET        = zombietrackergps
TEMPLATE      = app
PKGVERSION    = $$cat(VERSION)    # package version
PKGVERSION    = $$replace(PKGVERSION, "\"", "")
MOC_DIR       = build/moc
OBJECTS_DIR   = build/obj
UI_DIR        = build/ui

DEFINES      += QT_DEPRECATED_WARNINGS
DEFINES      += QT_DISABLE_DEPRECATED_BEFORE=0x060000  # disables all the APIs deprecated before Qt 6.00.0

SOURCES      += $$files(src/*.cpp, true)
HEADERS      += $$files(src/*.h, true)
FORMS        += $$files(src/*.ui, true)
RESOURCES    += # no internal resources
TRANSLATIONS += bg ca cs da de en es fi fr he hu it ja ko lv pl ru sk uk

# expand to full translation files
TRANSLATIONS ~= s:^:translations/ztgps.:g
TRANSLATIONS ~= s:$:.ts:g

unix {
    TARGET_MULTIARCH = $$system(gcc -print-multiarch)

    INSTALL_ROOT=$$(INSTALL_ROOT) # use env variable if set
    isEmpty(INSTALL_ROOT) {
       INSTALL_ROOT = /usr/local
    }
}

# update build date
system("touch -c $${PWD}/src/core/builddate.cpp 2>/dev/null")

# update translations
system("lupdate 2>/dev/null -silent $${PWD}/*.pro")

# manpage
mangz.target   = $${TARGET}.1.gz
mangz.depends  = $$PWD/man/$${TARGET}.1
mangz.commands = gzip --stdout "$$mangz.depends" > "$$mangz.target"

QMAKE_CLEAN += $${TARGET}.1.gz

QMAKE_EXTRA_TARGETS += mangz tests
PRE_TARGETDEPS      += $${mangz.target}

unix {
    # disable -Wno-odr (One Definition Rule) for link, otherwise qmetatype.h has warnings under gcc 12.x
    QMAKE_LFLAGS="-Wno-odr"
    QMAKE_LFLAGS_RELEASE='-Wl,--strip-all,--dynamic-list-cpp-typeinfo'
    QMAKE_LFLAGS_DEBUG='-Wl,--dynamic-list-cpp-typeinfo'
    QMAKE_CXXFLAGS += -std=c++17 -Wall -pedantic
    QMAKE_CXXFLAGS_RELEASE += -DNDEBUG
# CONFIG += c++17    # TODO: not supported yet in qmake, so use QMAKE_CXXFLAGS as just above.

    # static linkage of system libraries if requested
    # TODO: static libc linkage causes R_X86_64_PC32 relocation error.  Haven't found any pie/pic fix yet.
    STATIC=$$(STATIC)
    !isEmpty(STATIC) {
        QMAKE_LFLAGS += -l:libm.a -l:libgcc.a -l:libstdc++.a -l:libpthread.a # -l:libc.a
    }

    MAKEFILE="Makefile.$$basename(_PRO_FILE_)"
    MAKEFILE=$$replace(MAKEFILE, ".pro", "")

    # for "make deb"
    system("which dpkg-architecture >/dev/null 2>&1") {
        deb.target   = deb
        deb.commands = INSTALL_ROOT=$${OUT_PWD}/debbuild/usr qmake -o $${MAKEFILE} $${_PRO_FILE_}; \
                       make -f $${MAKEFILE} install;

        QMAKE_EXTRA_TARGETS += deb
    }

    # for "make rpm"
    system("which rpmbuild >/dev/null 2>&1") {
        rpm.target = rpm
        rpm.commands = INSTALL_ROOT=$${OUT_PWD}/rpmbuild/usr qmake -o $${MAKEFILE} $${_PRO_FILE_}; \
                       make -f $${MAKEFILE} install;

        QMAKE_EXTRA_TARGETS += rpm
    }

    INCLUDEPATH += $$clean_path($$_PRO_FILE_PWD_/../libldutils)
    INCLUDEPATH += /usr/local/include/libldutils
    INCLUDEPATH += /usr/include/libldutils

    # ensure we can find ldutils headers someplace in the INCLUDEPATH
    foundldutils = 
    for (ipath, INCLUDEPATH) {
        exists($${ipath}/src/core/appbase.h) {
           foundldutils = $${ipath}
        }
    }

    !contains(foundldutils, ".*/libldutils") {
	message("This project requires ldutils to be built and installed.  Please see the BUILDING file in that project for details.")
        error("libldutils headers not found.")
    }

    LIBS += -L$$clean_path($$_PRO_FILE_PWD_/../libldutils/lib/$${TARGET_MULTIARCH})
    LIBS += -L/usr/lib/$${TARGET_MULTIARCH}
    LIBS += -L/usr/local/lib
    LIBS += -L/app/lib
    LIBS += -ldl

    CONFIG(debug, debug|release) {
        LIBS += -lldutilsd
    } else {
        LIBS += -lldutils
    }

    # This should already be covered by the Qt += Marble entry near the top, but for some reason that
    # isn't working on Arch Linux.  This adds that library in explicitly, and should be harmless on
    # other distros.
    LIBS += -lmarblewidget-qt5

    # manpage install target
    manpage.path     = $${INSTALL_ROOT}/share/man/man1
    manpage.files    = $$files($${OUT_PWD}/*.1.gz)

    # shared files install target
    sharedoc.path    = $${INSTALL_ROOT}/share/$${TARGET}
    sharedoc.files   = data/sample data/pubkey.asc art/ui/docs art/text/docs \
                       $$files($${OUT_PWD}/*.rcc) \
                       art/logos/projects/$${TARGET}.png

    # combine geoloc file parts
    geoLoc.path = $${INSTALL_ROOT}/share/$${TARGET}/geoloc
    geoLoc.commands = cat $$files($${PWD}/art/geoloc/locData.dat.*) > $${geoLoc.path}/locData.dat; \
                      cat $$files($${PWD}/art/geoloc/locName.dat.*) > $${geoLoc.path}/locName.dat; \
                      cp $${PWD}/art/geoloc/locTz.dat $${geoLoc.path}/locTz.dat;

    # documentation install target
    docs.path    = $${INSTALL_ROOT}/share/doc/$${TARGET}
    docs.files   = debian/copyright

    # point the desktop Icon: to the installed icon
    finalroot        = $${INSTALL_ROOT}
    finalroot        ~= s:.*build/:/:
    desktop.path     = $${INSTALL_ROOT}/share/applications
    desktop.commands = sed -e "s:Icon=.*:Icon=$${finalroot}/share/$${TARGET}/$${TARGET}.png:" $${PWD}/data/desktop/$${TARGET}.desktop > $${desktop.path}/$${TARGET}.desktop

    target.path   = $${INSTALL_ROOT}/bin
    INSTALLS += target sharedoc manpage desktop docs geoLoc
}
