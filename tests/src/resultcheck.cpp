/*
    Copyright 2020-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/util.h>
#include "resultcheck.h"

const ResultCheck ResultCheck::isGpx("<gpx[^>]+version[ ]*=[ ]*[\"0-9.]+.*</gpx>", 1);
const ResultCheck ResultCheck::isKml("<kml[^>]+xmlns[ ]*=.*<Document>.*</Document>.*</kml>", 1);
const ResultCheck ResultCheck::isTcx("<TrainingCenterDatabase.*</TrainingCenterDatabase>", 1);

std::optional<QString> ResultCheck::operator()(const QString& data) const
{
    int count = 0;
    for (auto m = m_regex.globalMatch(data); m.hasNext(); m.next() )
        ++count;

    if (m_min <= count && count <= m_max)
        return std::nullopt;

    return QString("Actual: %1  Min: %2  Max: %3  Pattern: %4")
            .arg(count).arg(m_min).arg(m_max).arg(m_regex.pattern());
}


std::optional<QString> ResultCheck::operator()() const
{
    return (*this)(Util::ReadFile<QString>(m_file, 1<<24));
}

std::optional<QString> ResultCheckAll::operator()() const
{
    // If we were given a file, use it for everything
    for (const auto& check : *this) {
        if (!m_file.isEmpty()) {
            const auto data = Util::ReadFile<QString>(m_file, 1<<24);
            if (const auto result = check(data); result)
                return result;
        } else {
            if (const auto result = check(); result)
                return result;
        }
    }

    return std::nullopt;
}

std::optional<QString> ResultCheckAll::operator()(const QString& data) const
{
    for (const auto& check : *this)
        if (const auto result = check(data); result)
            return result;

    return std::nullopt;
}
