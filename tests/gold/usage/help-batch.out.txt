Batch File Processing Options:
   --input, -i FILE...    One or more GPS input files to process. '-' for stdin. 
   --output, -o FILE...   One per input, or omit to auto-generate names. '-' for stdout.
   --dir, -d DIR          Write outputs to this directory. $PWD if unset.
   --clobber              Do not complain about existing output files.
   --concat, --merge      Merge multiple input files to a single output file.
   --pretty               Generate human readable XML files.
   --indent NUM           Indent XML files with NUM spaces.
   --indent-tabs          Use tabs instead of space to indent XML files.
   --type trk|wpt|all     Write tracks, waypoints, or all.
   --filter-trk QUERY     Process tracks matching QUERY.  --help-filter for syntax.
   --filter-wpt QUERY     Process waypoints matching QUERY. --help-filter for syntax.
   --match-case           Use case sensitive queries.
   --verbose,-v           Be more verbose.
   --stat                 Print statistics about conversions.

