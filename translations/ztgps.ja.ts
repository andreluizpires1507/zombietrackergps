<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="14"/>
        <location filename="../src/ui/forms/aboutdialog.ui" line="84"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="24"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-style:italic;&quot;&gt;ZombieTracker GPS&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Brraaaaaiiinnnnns!&lt;br/&gt;&lt;/span&gt;&lt;br/&gt;(Or for physicist zombies: &lt;span style=&quot; font-style:italic;&quot;&gt;Braaanes!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="40"/>
        <source>Brraaaaaiiinnnnns!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="92"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:18pt; font-style:italic;&quot;&gt;ZombieTracker GPS 0.0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="107"/>
        <source>build:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="117"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/zombietrackergps.png&quot; /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ZTGPS is a GPS track manager for Linux, using the Qt widget set. It provides rich sorting, filtering, and visualization abilities for gpx format track files.  It has a customizable, pane based interface and can manage large collections of GPS tracks.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ZTGPS can display basemaps from OpenStreetMap, OpenTopoMap, OpenCycleMap, or other map providers.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Note! ZTGPS can also be used to track non-zombies!&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When a Zombie Apocalypse hits, closed sourced applications leave you in the lurch!  Use your braaiiiins! Choose well fleshed out Open Source Software!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Program authors and art creators&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="139"/>
        <source>Program authors and art creators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="161"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Library information&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="169"/>
        <source>Library information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="172"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;This program uses the following open source libraries:&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/Qt_logo-32x32.png&quot; /&gt; &lt;a href=&quot;http://qt.io/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;The Qt Application Framework&lt;br /&gt;&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/Marble_Logo-32x26.png&quot; /&gt; &lt;a href=&quot;https://marble.kde.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;libMarble&lt;/span&gt;&lt;/a&gt;, from the &lt;img src=&quot;:art/logos/projects/KDE_Logo-TrBlue-32x32.png&quot; /&gt; &lt;a href=&quot;https://kde.org&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;KDE project&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="187"/>
        <source>Libraries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="192"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Program license&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="195"/>
        <source>Program license</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="361"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;To Support ZombieTrackerGPS&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;The &lt;a href=&quot;${ZTGPSWWW}&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;ZombieTrackerGPS&lt;/span&gt;&lt;/a&gt; web site now has a Donations tab and options for donations through Liberapay and Stripe.&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; text-decoration: underline;&quot;&gt;Please consider donating to these projects&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Please consider supporting the OpenStreetMap.  Without open data sets like this, projects like ZombieTrackerGPS would not be possible.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/OSM_Logo-32x32.png&quot; /&gt; &lt;a href=&quot;https://donate.openstreetmap.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;OpenStreetMap&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These projects are invaluable for content creation. Please consider donating to keep them running.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;&quot; cellspacing=&quot;2&quot; cellpadding=&quot;0&quot;&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/Inkscape_Logo-36x36.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://inkscape.org/support-us/donate/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Inkscape&lt;/span&gt;&lt;/a&gt; - Vector graphics editor for Linux.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/GIMP_Logo-32x32.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://www.gimp.org/donating/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;GIMP&lt;/span&gt;&lt;/a&gt; - Bitmap image editor for Linux.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;These projects defend digital and internet rights.  They can use your support.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px;&quot; cellspacing=&quot;2&quot; cellpadding=&quot;0&quot;&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/EFF_Logo-32x40.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://supporters.eff.org/donate&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Electronic Frontier Foundation&lt;/span&gt;&lt;/a&gt; - A 501(c)(3) nonprofit defending internet civil liberties.   &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/distros/Debian_Logo-32x32.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://www.debian.org/donations&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Debian&lt;/span&gt;&lt;/a&gt; - The root of many popular Linux distributions.    &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/KDE_Logo-TrBlue-32x32.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://www.kde.org/community/donations/index.php&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;KDE Desktop Environment&lt;/span&gt;&lt;/a&gt; - Advanced OSS graphical desktop.  Good stuff.    &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/GnuPG_Logo-24x32.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://gnupg.org/donate/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;GnuPG&lt;/span&gt;&lt;/a&gt; - Encryption which protects journalists, human rights activists, and more.&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/gcc_Logo-31x36.png&quot; /&gt;&lt;/p&gt;&lt;/td&gt;
&lt;td&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://my.fsf.org/civicrm/contribute/transact?reset=1&amp;amp;id=57&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;GCC&lt;/span&gt;&lt;/a&gt; - The Gnu Compiler Collection&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;  &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="142"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic; text-decoration: underline;&quot;&gt;Primary development:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    © 2019-2022 LoopDawg Software&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600; font-style:italic; text-decoration: underline;&quot;&gt;Icons:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-weight:600; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The included Breeze icon set is Copyright © 2014 Uri Herrera &amp;lt;uri_herrera@nitrux.in&amp;gt; and others, and is distributed here under the terms of the LGPL.  See the included file COPYING-ICONS for the distribution terms.  This is an excellent icon set, currently the standard one for KDE Plasma 5.  It also supplies the built-in Weather tag icons.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Train activity icon was modified from a version by the Austrian National Railway.&lt;br /&gt;&lt;br /&gt;The Misc icons are from FreeSVG.org, under a Creative Commons CC0 license.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="198"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;img src=&quot;:art/logos/projects/gplv3-127x51.png&quot; /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;GNU GENERAL PUBLIC LICENSE&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Version 3, 29 June 2007 &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright © 2007 Free Software Foundation, Inc. &amp;lt;&lt;a href=&quot;https://fsf.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;https://fsf.org/&lt;/span&gt;&lt;/a&gt;&amp;gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed. &lt;/p&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;preamble&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;P&lt;/span&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;reamble&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The GNU General Public License is a free, copyleft license for software and other kinds of works. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The licenses for most software and other practical works are designed to take away your freedom to share and change the works. By contrast, the GNU General Public License is intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users. We, the Free Software Foundation, use the GNU General Public License for most of our software; it applies also to any other work released this way by its authors. You can apply it to your programs, too. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When we speak of free software, we are referring to freedom, not price. Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software (and charge for them if you wish), that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;To protect your rights, we need to prevent others from denying you these rights or asking you to surrender the rights. Therefore, you have certain responsibilities if you distribute copies of the software, or if you modify it: responsibilities to respect the freedom of others. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;For example, if you distribute copies of such a program, whether gratis or for a fee, you must pass on to the recipients the same freedoms that you received. You must make sure that they, too, receive or can get the source code. And you must show them these terms so they know their rights. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Developers that use the GNU GPL protect your rights with two steps: (1) assert copyright on the software, and (2) offer you this License giving you legal permission to copy, distribute and/or modify it. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;For the developers&apos; and authors&apos; protection, the GPL clearly explains that there is no warranty for this free software. For both users&apos; and authors&apos; sake, the GPL requires that modified versions be marked as changed, so that their problems will not be attributed erroneously to authors of previous versions. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Some devices are designed to deny users access to install or run modified versions of the software inside them, although the manufacturer can do so. This is fundamentally incompatible with the aim of protecting users&apos; freedom to change the software. The systematic pattern of such abuse occurs in the area of products for individuals to use, which is precisely where it is most unacceptable. Therefore, we have designed this version of the GPL to prohibit the practice for those products. If such problems arise substantially in other domains, we stand ready to extend this provision to those domains in future versions of the GPL, as needed to protect the freedom of users. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Finally, every program is threatened constantly by software patents. States should not allow patents to restrict development and use of software on general-purpose computers, but in those that do, we wish to avoid the special danger that patents applied to a free program could make it effectively proprietary. To prevent this, the GPL assures that patents cannot be used to render the program non-free. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The precise terms and conditions for copying, distribution and modification follow. &lt;/p&gt;
&lt;p style=&quot; margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;terms&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;T&lt;/span&gt;&lt;span style=&quot; font-size:large; font-weight:600;&quot;&gt;ERMS AND CONDITIONS&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section0&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;0&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Definitions.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;“This License” refers to version 3 of the GNU General Public License. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;“Copyright” also means copyright-like laws that apply to other kinds of works, such as semiconductor masks. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;“The Program” refers to any copyrightable work licensed under this License. Each licensee is addressed as “you”. “Licensees” and “recipients” may be individuals or organizations. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;To “modify” a work means to copy from or adapt all or part of the work in a fashion requiring copyright permission, other than the making of an exact copy. The resulting work is called a “modified version” of the earlier work or a work “based on” the earlier work. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A “covered work” means either the unmodified Program or a work based on the Program. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;To “propagate” a work means to do anything with it that, without permission, would make you directly or secondarily liable for infringement under applicable copyright law, except executing it on a computer or modifying a private copy. Propagation includes copying, distribution (with or without modification), making available to the public, and in some countries other activities as well. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;To “convey” a work means any kind of propagation that enables other parties to make or receive copies. Mere interaction with a user through a computer network, with no transfer of a copy, is not conveying. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;An interactive user interface displays “Appropriate Legal Notices” to the extent that it includes a convenient and prominently visible feature that (1) displays an appropriate copyright notice, and (2) tells the user that there is no warranty for the work (except to the extent that warranties are provided), that licensees may convey the work under this License, and how to view a copy of this License. If the interface presents a list of user commands or options, such as a menu, a prominent item in the list meets this criterion. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section1&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Source Code.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The “source code” for a work means the preferred form of the work for making modifications to it. “Object code” means any non-source form of a work. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A “Standard Interface” means an interface that either is an official standard defined by a recognized standards body, or, in the case of interfaces specified for a particular programming language, one that is widely used among developers working in that language. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The “System Libraries” of an executable work include anything, other than the work as a whole, that (a) is included in the normal form of packaging a Major Component, but which is not part of that Major Component, and (b) serves only to enable use of the work with that Major Component, or to implement a Standard Interface for which an implementation is available to the public in source code form. A “Major Component”, in this context, means a major essential component (kernel, window system, and so on) of the specific operating system (if any) on which the executable work runs, or a compiler used to produce the work, or an object code interpreter used to run it. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The “Corresponding Source” for a work in object code form means all the source code needed to generate, install, and (for an executable work) run the object code and to modify the work, including scripts to control those activities. However, it does not include the work&apos;s System Libraries, or general-purpose tools or generally available free programs which are used unmodified in performing those activities but which are not part of the work. For example, Corresponding Source includes interface definition files associated with source files for the work, and the source code for shared libraries and dynamically linked subprograms that the work is specifically designed to require, such as by intimate data communication or control flow between those subprograms and other parts of the work. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Corresponding Source need not include anything that users can regenerate automatically from other parts of the Corresponding Source. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Corresponding Source for a work in source code form is that same work. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section2&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;2&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Basic Permissions.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;All rights granted under this License are granted for the term of copyright on the Program, and are irrevocable provided the stated conditions are met. This License explicitly affirms your unlimited permission to run the unmodified Program. The output from running a covered work is covered by this License only if the output, given its content, constitutes a covered work. This License acknowledges your rights of fair use or other equivalent, as provided by copyright law. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may make, run and propagate covered works that you do not convey, without conditions so long as your license otherwise remains in force. You may convey covered works to others for the sole purpose of having them make modifications exclusively for you, or provide you with facilities for running those works, provided that you comply with the terms of this License in conveying all material for which you do not control copyright. Those thus making or running the covered works for you must do so exclusively on your behalf, under your direction and control, on terms that prohibit them from making any copies of your copyrighted material outside their relationship with you. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Conveying under any other circumstances is permitted solely under the conditions stated below. Sublicensing is not allowed; section 10 makes it unnecessary. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section3&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;3&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Protecting Users&apos; Legal Rights From Anti-Circumvention Law.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;No covered work shall be deemed part of an effective technological measure under any applicable law fulfilling obligations under article 11 of the WIPO copyright treaty adopted on 20 December 1996, or similar laws prohibiting or restricting circumvention of such measures. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When you convey a covered work, you waive any legal power to forbid circumvention of technological measures to the extent such circumvention is effected by exercising rights under this License with respect to the covered work, and you disclaim any intention to limit operation or modification of the work as a means of enforcing, against the work&apos;s users, your or third parties&apos; legal rights to forbid circumvention of technological measures. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section4&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;4&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Conveying Verbatim Copies.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may convey verbatim copies of the Program&apos;s source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice; keep intact all notices stating that this License and any non-permissive terms added in accord with section 7 apply to the code; keep intact all notices of the absence of any warranty; and give all recipients a copy of this License along with the Program. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may charge any price or no price for each copy that you convey, and you may offer support or warranty protection for a fee. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section5&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;5&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Conveying Modified Source Versions.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may convey a work based on the Program, or the modifications to produce it from the Program, in the form of source code under the terms of section 4, provided that you also meet all of these conditions: &lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot;&quot; style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a) The work must carry prominent notices stating that you modified it, and giving a relevant date. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;b) The work must carry prominent notices stating that it is released under this License and any conditions added under section 7. This requirement modifies the requirement in section 4 to “keep intact all notices”. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;c) You must license the entire work, as a whole, under this License to anyone who comes into possession of a copy. This License will therefore apply, along with any applicable section 7 additional terms, to the whole of the work, and all its parts, regardless of how they are packaged. This License gives no permission to license the work in any other way, but it does not invalidate such permission if you have separately received it. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;d) If the work has interactive user interfaces, each must display Appropriate Legal Notices; however, if the Program has interactive interfaces that do not display Appropriate Legal Notices, your work need not make them do so. &lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A compilation of a covered work with other separate and independent works, which are not by their nature extensions of the covered work, and which are not combined with it such as to form a larger program, in or on a volume of a storage or distribution medium, is called an “aggregate” if the compilation and its resulting copyright are not used to limit the access or legal rights of the compilation&apos;s users beyond what the individual works permit. Inclusion of a covered work in an aggregate does not cause this License to apply to the other parts of the aggregate. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section6&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;6&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Conveying Non-Source Forms.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may convey a covered work in object code form under the terms of sections 4 and 5, provided that you also convey the machine-readable Corresponding Source under the terms of this License, in one of these ways: &lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot;&quot; style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by the Corresponding Source fixed on a durable physical medium customarily used for software interchange. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;b) Convey the object code in, or embodied in, a physical product (including a physical distribution medium), accompanied by a written offer, valid for at least three years and valid for as long as you offer spare parts or customer support for that product model, to give anyone who possesses the object code either (1) a copy of the Corresponding Source for all the software in the product that is covered by this License, on a durable physical medium customarily used for software interchange, for a price no more than your reasonable cost of physically performing this conveying of source, or (2) access to copy the Corresponding Source from a network server at no charge. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;c) Convey individual copies of the object code with a copy of the written offer to provide the Corresponding Source. This alternative is allowed only occasionally and noncommercially, and only if you received the object code with such an offer, in accord with subsection 6b. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;d) Convey the object code by offering access from a designated place (gratis or for a charge), and offer equivalent access to the Corresponding Source in the same way through the same place at no further charge. You need not require recipients to copy the Corresponding Source along with the object code. If the place to copy the object code is a network server, the Corresponding Source may be on a different server (operated by you or a third party) that supports equivalent copying facilities, provided you maintain clear directions next to the object code saying where to find the Corresponding Source. Regardless of what server hosts the Corresponding Source, you remain obligated to ensure that it is available for as long as needed to satisfy these requirements. &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;e) Convey the object code using peer-to-peer transmission, provided you inform other peers where the object code and Corresponding Source of the work are being offered to the general public at no charge under subsection 6d. &lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A separable portion of the object code, whose source code is excluded from the Corresponding Source as a System Library, need not be included in conveying the object code work. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A “User Product” is either (1) a “consumer product”, which means any tangible personal property which is normally used for personal, family, or household purposes, or (2) anything designed or sold for incorporation into a dwelling. In determining whether a product is a consumer product, doubtful cases shall be resolved in favor of coverage. For a particular product received by a particular user, “normally used” refers to a typical or common use of that class of product, regardless of the status of the particular user or of the way in which the particular user actually uses, or expects or is expected to use, the product. A product is a consumer product regardless of whether the product has substantial commercial, industrial or non-consumer uses, unless such uses represent the only significant mode of use of the product. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;“Installation Information” for a User Product means any methods, procedures, authorization keys, or other information required to install and execute modified versions of a covered work in that User Product from a modified version of its Corresponding Source. The information must suffice to ensure that the continued functioning of the modified object code is in no case prevented or interfered with solely because modification has been made. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you convey an object code work under this section in, or with, or specifically for use in, a User Product, and the conveying occurs as part of a transaction in which the right of possession and use of the User Product is transferred to the recipient in perpetuity or for a fixed term (regardless of how the transaction is characterized), the Corresponding Source conveyed under this section must be accompanied by the Installation Information. But this requirement does not apply if neither you nor any third party retains the ability to install modified object code on the User Product (for example, the work has been installed in ROM). &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The requirement to provide Installation Information does not include a requirement to continue to provide support service, warranty, or updates for a work that has been modified or installed by the recipient, or for the User Product in which it has been modified or installed. Access to a network may be denied when the modification itself materially and adversely affects the operation of the network or violates the rules and protocols for communication across the network. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Corresponding Source conveyed, and Installation Information provided, in accord with this section must be in a format that is publicly documented (and with an implementation available to the public in source code form), and must require no special password or key for unpacking, reading or copying. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section7&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;7&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Additional Terms.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;“Additional permissions” are terms that supplement the terms of this License by making exceptions from one or more of its conditions. Additional permissions that are applicable to the entire Program shall be treated as though they were included in this License, to the extent that they are valid under applicable law. If additional permissions apply only to part of the Program, that part may be used separately under those permissions, but the entire Program remains governed by this License without regard to the additional permissions. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When you convey a copy of a covered work, you may at your option remove any additional permissions from that copy, or from any part of it. (Additional permissions may be written to require their own removal in certain cases when you modify the work.) You may place additional permissions on material, added by you to a covered work, for which you have or can give appropriate copyright permission. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Notwithstanding any other provision of this License, for material you add to a covered work, you may (if authorized by the copyright holders of that material) supplement the terms of this License with terms: &lt;/p&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot;&quot; style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;a) Disclaiming warranty or limiting liability differently from the terms of sections 15 and 16 of this License; or &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;b) Requiring preservation of specified reasonable legal notices or author attributions in that material or in the Appropriate Legal Notices displayed by works containing it; or &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;c) Prohibiting misrepresentation of the origin of that material, or requiring that modified versions of such material be marked in reasonable ways as different from the original version; or &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;d) Limiting the use for publicity purposes of names of licensors or authors of the material; or &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;e) Declining to grant rights under trademark law for use of some trade names, trademarks, or service marks; or &lt;/li&gt;
&lt;li style=&quot;&quot; style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;f) Requiring indemnification of licensors and authors of that material by anyone who conveys the material (or modified versions of it) with contractual assumptions of liability to the recipient, for any liability that these contractual assumptions directly impose on those licensors and authors. &lt;/li&gt;&lt;/ul&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;All other non-permissive additional terms are considered “further restrictions” within the meaning of section 10. If the Program as you received it, or any part of it, contains a notice stating that it is governed by this License along with a term that is a further restriction, you may remove that term. If a license document contains a further restriction but permits relicensing or conveying under this License, you may add to a covered work material governed by the terms of that license document, provided that the further restriction does not survive such relicensing or conveying. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you add terms to a covered work in accord with this section, you must place, in the relevant source files, a statement of the additional terms that apply to those files, or a notice indicating where to find the applicable terms. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Additional terms, permissive or non-permissive, may be stated in the form of a separately written license, or stated as exceptions; the above requirements apply either way. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section8&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;8&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Termination.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may not propagate or modify a covered work except as expressly provided under this License. Any attempt otherwise to propagate or modify it is void, and will automatically terminate your rights under this License (including any patent licenses granted under the third paragraph of section 11). &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;However, if you cease all violation of this License, then your license from a particular copyright holder is reinstated (a) provisionally, unless and until the copyright holder explicitly and finally terminates your license, and (b) permanently, if the copyright holder fails to notify you of the violation by some reasonable means prior to 60 days after the cessation. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Moreover, your license from a particular copyright holder is reinstated permanently if the copyright holder notifies you of the violation by some reasonable means, this is the first time you have received notice of violation of this License (for any work) from that copyright holder, and you cure the violation prior to 30 days after your receipt of the notice. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Termination of your rights under this section does not terminate the licenses of parties who have received copies or rights from you under this License. If your rights have been terminated and not permanently reinstated, you do not qualify to receive new licenses for the same material under section 10. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section9&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;9&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;. Acceptance Not Required for Having Copies.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You are not required to accept this License in order to receive or run a copy of the Program. Ancillary propagation of a covered work occurring solely as a consequence of using peer-to-peer transmission to receive a copy likewise does not require acceptance. However, nothing other than this License grants you permission to propagate or modify any covered work. These actions infringe copyright if you do not accept this License. Therefore, by modifying or propagating a covered work, you indicate your acceptance of this License to do so. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section10&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;0. Automatic Licensing of Downstream Recipients.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each time you convey a covered work, the recipient automatically receives a license from the original licensors, to run, modify and propagate that work, subject to this License. You are not responsible for enforcing compliance by third parties with this License. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;An “entity transaction” is a transaction transferring control of an organization, or substantially all assets of one, or subdividing an organization, or merging organizations. If propagation of a covered work results from an entity transaction, each party to that transaction who receives a copy of the work also receives whatever licenses to the work the party&apos;s predecessor in interest had or could give under the previous paragraph, plus a right to possession of the Corresponding Source of the work from the predecessor in interest, if the predecessor has it or can get it with reasonable efforts. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You may not impose any further restrictions on the exercise of the rights granted or affirmed under this License. For example, you may not impose a license fee, royalty, or other charge for exercise of rights granted under this License, and you may not initiate litigation (including a cross-claim or counterclaim in a lawsuit) alleging that any patent claim is infringed by making, using, selling, offering for sale, or importing the Program or any portion of it. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section11&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1. Patents.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A “contributor” is a copyright holder who authorizes use under this License of the Program or a work on which the Program is based. The work thus licensed is called the contributor&apos;s “contributor version”. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A contributor&apos;s “essential patent claims” are all patent claims owned or controlled by the contributor, whether already acquired or hereafter acquired, that would be infringed by some manner, permitted by this License, of making, using, or selling its contributor version, but do not include claims that would be infringed only as a consequence of further modification of the contributor version. For purposes of this definition, “control” includes the right to grant patent sublicenses in a manner consistent with the requirements of this License. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each contributor grants you a non-exclusive, worldwide, royalty-free patent license under the contributor&apos;s essential patent claims, to make, use, sell, offer for sale, import and otherwise run, modify and propagate the contents of its contributor version. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;In the following three paragraphs, a “patent license” is any express agreement or commitment, however denominated, not to enforce a patent (such as an express permission to practice a patent or covenant not to sue for patent infringement). To “grant” such a patent license to a party means to make such an agreement or commitment not to enforce a patent against the party. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If you convey a covered work, knowingly relying on a patent license, and the Corresponding Source of the work is not available for anyone to copy, free of charge and under the terms of this License, through a publicly available network server or other readily accessible means, then you must either (1) cause the Corresponding Source to be so available, or (2) arrange to deprive yourself of the benefit of the patent license for this particular work, or (3) arrange, in a manner consistent with the requirements of this License, to extend the patent license to downstream recipients. “Knowingly relying” means you have actual knowledge that, but for the patent license, your conveying the covered work in a country, or your recipient&apos;s use of the covered work in a country, would infringe one or more identifiable patents in that country that you have reason to believe are valid. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If, pursuant to or in connection with a single transaction or arrangement, you convey, or propagate by procuring conveyance of, a covered work, and grant a patent license to some of the parties receiving the covered work authorizing them to use, propagate, modify or convey a specific copy of the covered work, then the patent license you grant is automatically extended to all recipients of the covered work and works based on it. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;A patent license is “discriminatory” if it does not include within the scope of its coverage, prohibits the exercise of, or is conditioned on the non-exercise of one or more of the rights that are specifically granted under this License. You may not convey a covered work if you are a party to an arrangement with a third party that is in the business of distributing software, under which you make payment to the third party based on the extent of your activity of conveying the work, and under which the third party grants, to any of the parties who would receive the covered work from you, a discriminatory patent license (a) in connection with copies of the covered work conveyed by you (or copies made from those copies), or (b) primarily for and in connection with specific products or compilations that contain the covered work, unless you entered into that arrangement, or that patent license was granted, prior to 28 March 2007. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Nothing in this License shall be construed as excluding or limiting any implied license or other defenses to infringement that may otherwise be available to you under applicable patent law. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section12&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;2. No Surrender of Others&apos; Freedom.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License. If you cannot convey a covered work so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not convey it at all. For example, if you agree to terms that obligate you to collect a royalty for further conveying from those to whom you convey the Program, the only way you could satisfy both those terms and this License would be to refrain entirely from conveying the Program. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section13&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;3. Use with the GNU Affero General Public License.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Notwithstanding any other provision of this License, you have permission to link or combine any covered work with a work licensed under version 3 of the GNU Affero General Public License into a single combined work, and to convey the resulting work. The terms of this License will continue to apply to the part which is the covered work, but the special requirements of the GNU Affero General Public License, section 13, concerning interaction through a network will apply to the combination as such. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section14&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;4. Revised Versions of this License.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Free Software Foundation may publish revised and/or new versions of the GNU General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Each version is given a distinguishing version number. If the Program specifies that a certain numbered version of the GNU General Public License “or any later version” applies to it, you have the option of following the terms and conditions either of that numbered version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of the GNU General Public License, you may choose any version ever published by the Free Software Foundation. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If the Program specifies that a proxy can decide which future versions of the GNU General Public License can be used, that proxy&apos;s public statement of acceptance of a version permanently authorizes you to choose that version for the Program. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Later license versions may give you additional or different permissions. However, no additional obligations are imposed on any author or copyright holder as a result of your choosing to follow a later version. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section15&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;5. Disclaimer of Warranty.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section16&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;6. Limitation of Liability.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a name=&quot;section17&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;1&lt;/span&gt;&lt;span style=&quot; font-size:medium; font-weight:600;&quot;&gt;7. Interpretation of Sections 15 and 16.&lt;/span&gt; &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If the disclaimer of warranty and limitation of liability provided above cannot be given local legal effect according to their terms, reviewing courts shall apply local law that most closely approximates an absolute waiver of all civil liability in connection with the Program, unless a warranty or assumption of liability accompanies a copy of the Program in return for a fee. &lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;END OF TERMS AND CONDITIONS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="320"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="325"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Privacy policy&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="328"/>
        <source>Privacy policy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="331"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:18pt; font-weight:600;&quot;&gt;Privacy Policy&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:16pt; font-weight:600;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;ZombieTrackerGPS does not collect, sell, give away, scrape, or in any other sense traffic in any of your data.  All of your GPS data resides in files on your local disk, and will not be sent anywhere.  The entire source code for this program is available to you, so unlike with closed source software, this can be verified by you or any party of your choice.   Providing alternatives to modern data collection and monitization practices by Google et al was a key motivation for the creation of this software.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The program includes a map display based on the &lt;a href=&quot;https://en.wikipedia.org/wiki/KDE&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;KDE&lt;/span&gt;&lt;/a&gt; project&apos;s &lt;a href=&quot;https://community.kde.org/Marble&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;libMarble&lt;/span&gt;&lt;/a&gt;, which can load online maps from various online providers, including OpenStreetMap and OpenTopoMap.  This intrinsically tells that provider which map areas you download: that is the only way they can provide the data you request.  These providers are far more privacy respecting than the more common sources such as Google, but their privacy policies are outside my control.  They do not  profit from collecting information about you.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://wiki.osmfoundation.org/wiki/Privacy_Policy#Data_we_receive_automatically&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;OpenStreetMap Privacy Policy&lt;/span&gt;&lt;/a&gt; - Most of their policy is related to contributors to the map.  The section of most interest is &amp;quot;&lt;span style=&quot; font-weight:600;&quot;&gt;Data we receive automatically&amp;quot;, &lt;/span&gt; which relates to online use of the map.  If you use another map provider, that provider&apos;s own privacy policy will apply.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;The Marble map is able to use locally cached data, so you can run ZombieTrackerGPS entirely offline provided you have local map data, thus not disclosing any information to third parties.  ZombieTrackerGPS can work in that manner even without a network connection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="350"/>
        <source>Privacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="355"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;External project donation links&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="358"/>
        <source>External project donation links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="424"/>
        <source>Donate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="448"/>
        <source>Next Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="451"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Next Tab&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="454"/>
        <source>Ctrl+PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="463"/>
        <source>Prev_Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="466"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Previous tab.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/aboutdialog.ui" line="469"/>
        <source>Ctrl+PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/aboutdialog.cpp" line="76"/>
        <source>build: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ActivitySummaryPane</name>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="17"/>
        <source>Activity Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="20"/>
        <source>Acti&amp;vity Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the query string is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;458&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;as a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!~ &lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;354&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;344&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Length &amp;gt; 20 km&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags : Hike|Ski&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Begin_Date &amp;lt; 02-Mar-2017&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;524&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;514&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags =~ Hike &amp;amp; Moving_Time &amp;gt; 2 h&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;( Max_Grade &amp;gt; 10% | Max_Power &amp;gt; 200W ) &amp;amp; Name =~ Green&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="57"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lock query to a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;, so changes in the Track Pane&apos;s query text will update this chart.&lt;/p&gt;&lt;p&gt;If unlocked, the query text for this &lt;span style=&quot; font-weight:600;&quot;&gt;Activity Summary Pane&lt;/span&gt; will function independently.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="60"/>
        <source>Lock query to a Track Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="63"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="80"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the span for the summary.  Data can be summarized over weeks, months, or years.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="87"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select data to be compared over date spans from multi-selected GPS tracks in a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="123"/>
        <source>Show Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="126"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart horizontal and vertical axes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="129"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="141"/>
        <source>Show Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="144"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart legend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="147"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="162"/>
        <source>Animated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="165"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable or disable chart animations.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="174"/>
        <source>Set Bar Width...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="177"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the short dimension of the bar size, in pixels.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="189"/>
        <source>Bar Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="192"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable to set value text in bars.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="195"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="204"/>
        <source>Page Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="207"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page Left&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="210"/>
        <source>PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="219"/>
        <source>Page Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="222"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page Right&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="225"/>
        <source>PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="237"/>
        <source>Show Empty Spans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="240"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If unchecked, date spans with no data will be hidden in the chart.&lt;/p&gt;&lt;p&gt;If checked, empty spans will be displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="243"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="252"/>
        <source>Zoom to Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="255"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This action while right clicking or hovering over a bar will zoom the &lt;span style=&quot; font-weight:600;&quot;&gt;Map Pane&lt;/span&gt; to the tracks for the selected tag and date span.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="258"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="273"/>
        <source>Zero Based Y Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/activitysummarypane.ui" line="276"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the Y graph axis will start at 0.  If unchecked, it will start at the lowest value present in the data.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="118"/>
        <source>Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="119"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="120"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="432"/>
        <source>Set Bar Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="432"/>
        <source>Minimum width (px)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/activitysummarypane.cpp" line="693"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppConfig</name>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="14"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="455"/>
        <source>Backups and Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="558"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If changes are made to the GPS data (tracks imported, changes made to tracks, etc), the changes will be auto-saved every N seconds, or on exit, whichever is first.&lt;/p&gt;&lt;p&gt;If set to 0, autosaving will be disabled, but changes will still be saved on exit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="561"/>
        <source>GPS Data autosave interval.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="564"/>
        <source> sec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="461"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the number of backup files to keep for the UI (user interface) configuration. The UI is automatically saved on exit and restored on load. If you wish to restore an older version, you can explicitly load one of the numbered backups, which have filenames ending with:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;.conf.~1~&lt;br/&gt;.conf.~2~&lt;br/&gt;etc&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Setting this value to 0 will disable backups (but no existing backups will be removed).&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="464"/>
        <source>Maxinum number of UI configuration backups to keep.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="467"/>
        <location filename="../src/ui/forms/appconfig.ui" line="531"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="544"/>
        <source>GPS Save Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="525"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the number of backup files to keep for GPS data.  GPS data can be automatically saved after changes. If you wish to restore an older version, you can explicitly load one of the numbered backups, which have filenames ending with:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;.conf.~1~&lt;br/&gt;.conf.~2~&lt;br/&gt;etc&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="528"/>
        <source>Maxinum number of GPS data backups to keep.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="518"/>
        <source>UI Setting Backups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="551"/>
        <source>Autosave Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="480"/>
        <source>GPS Data Backups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="489"/>
        <location filename="../src/ui/forms/appconfig.ui" line="506"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select path under which to save GPS data.  If blank, the directory of the UI configuration will be used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="492"/>
        <source>Select GPS data save path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="509"/>
        <source>GPS data save path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="180"/>
        <source>Filters and Completion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="186"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, filter text patterns are case sensitive.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="189"/>
        <source>Case sensitive filters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="192"/>
        <source>Case Sensitive Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="199"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, sorting a table view pane (by clicking a header column) will be case sensitive.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="202"/>
        <source>Case sensitive table sorting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="205"/>
        <source>Case Sensitive Sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="212"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If &lt;span style=&quot; font-weight:600;&quot;&gt;enabled&lt;/span&gt;, query text boxes will show completions inline. This allows only a single completion to be displayed, but is less intrusive.&lt;/p&gt;&lt;p&gt;If &lt;span style=&quot; font-weight:600;&quot;&gt;disabled&lt;/span&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;, &lt;/span&gt;query completions will be shown in a popup box which can display the entire range of completion possibilities.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="215"/>
        <source>Query completion style: popup, or inline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="218"/>
        <source>Use Inline Completion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="227"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Number of items to show in the completion popup list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="230"/>
        <source>Number of items to show in completion popup list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="243"/>
        <source>Completion List Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="344"/>
        <source>UI Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These colors are used to display error, warning, notes, and other messages in the program&apos;s status bar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="365"/>
        <source>User interface colors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="274"/>
        <source>Safety</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="280"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, a warning will be presented before removing an entire tab full of panes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="283"/>
        <source>Warn on tab pane close.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="286"/>
        <source>Warn on Tab Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="296"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, a warning will be presented before removing tracks or other data.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="299"/>
        <source>Warn on data removal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="302"/>
        <source>Warn on Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="312"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, a warning will be presented before reverting the UI (session) settings from the last save.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="315"/>
        <source>Warn on session revert.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="318"/>
        <source>Warn on Revert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="328"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, a warning will be presented before the program is closed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="331"/>
        <source>Warn on application exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="334"/>
        <source>Warn on Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="133"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="722"/>
        <source>Max Undos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum number of undos that will be saved. If more changes are made, the oldest undos will be discarded.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="761"/>
        <source>The maximum number of undos to save.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="764"/>
        <source> Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="729"/>
        <source>Max Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="736"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximum size of the undo changes, in MiB.  The computed size of undos is approximate, so this is not an exact bound.  When new undos are created, the undo stack will be shrunk until it is less than this size.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="739"/>
        <source>The maximum size of all undos, in MiB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="745"/>
        <source> MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="616"/>
        <source>Max View Undos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="623"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When moving around the map, an undo point will be created after the map is still for this many seconds.  This is to help prevent very small movements from creating unnecessary undo points.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="626"/>
        <source>Create a view undo point after this many seconds of map stillness.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="629"/>
        <source> secs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="651"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum number of viewpoint undos in map panes that will be saved. If more changes are made, the oldest will be discarded.&lt;/p&gt;&lt;p&gt;You can step forward and backward through the viewpoint undo/redos with the &lt;span style=&quot; font-weight:600;&quot;&gt;[&lt;/span&gt; and &lt;span style=&quot; font-weight:600;&quot;&gt;]&lt;/span&gt; keys.&lt;/p&gt;&lt;p&gt;Undoing viewpoints does not trigger a save or mark the program&apos;s save state as dirty.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="654"/>
        <source>The maximum number of viewpoint undos to save.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="657"/>
        <source> Viewpoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="667"/>
        <source>Undo after Still</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="125"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="131"/>
        <location filename="../src/ui/forms/appconfig.ui" line="147"/>
        <source>Row Separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="138"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This string will be used to separate rows of a table view pane when it is copied to the system clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="141"/>
        <source>Clipboard row separator string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="144"/>
        <source>\n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="154"/>
        <source>Col Separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="161"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This string will be used to separate columns of a table view panewhen it is copied to the system clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="164"/>
        <source>Clipboard column separator string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="167"/>
        <source>\t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="170"/>
        <source>Column Separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="393"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size of the smoothing filter applied to track locations.  Larger numbers add more smoothing.  Many GPS sensors have noisy elevation outputs with spikes in the data.  This filter can smooth out the elevation profile.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="396"/>
        <source>Elevation smoothing filter size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="415"/>
        <source>Elevation Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1213"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Icons and icon sizes&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1216"/>
        <source>Icons and icon sizes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1535"/>
        <source>UI Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1654"/>
        <source>Note Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1548"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Icon to indicate query filter is empty.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1551"/>
        <source>Icon to show filter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1668"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be applied if the icon set for a &lt;span style=&quot; font-weight:600;&quot;&gt;Track&lt;/span&gt;, &lt;span style=&quot; font-weight:600;&quot;&gt;Tag&lt;/span&gt;, or&lt;span style=&quot; font-weight:600;&quot;&gt; View&lt;/span&gt; is not found.  This only applies upon load.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1671"/>
        <source>Icon to use if track, tag, or view icon is not available upon load.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1661"/>
        <source>Filter Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1541"/>
        <source>Filter Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1647"/>
        <source>Filter Valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1571"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Icon to indicate query filter is invalid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1574"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1620"/>
        <source>Icon to show filter is valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1577"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1623"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1594"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be displayed in the &lt;span style=&quot; font-weight:600;&quot;&gt;Notes&lt;/span&gt; column of tracks that have attached notes.  Notes can be customed with a rich text editor, and hovering the mouse cursor over the &lt;span style=&quot; font-weight:600;&quot;&gt;Notes&lt;/span&gt; column will display that text as a popup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1597"/>
        <source>Icon to use for tracks with attached notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1617"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Icon to indicate query filter is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1640"/>
        <source>Broken Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1222"/>
        <source>Icon Sizes and Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1341"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum number of icons the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Panes&lt;/span&gt; will display in the list.  More icons can be assigned to the track, but only the first N will be displayed.  This can be useful to avoid the icons becoming very small, if you have a lot of them on some tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1344"/>
        <source>Maximum icons displayed in Track Panes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1490"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked tag icons in GPS Track views such as &lt;img src=&quot;:art/ui/Road-Grey.png&quot;/&gt; will be displayed in the color assigned to the tag, for example, &lt;img src=&quot;:art/ui/Road-Bluish.png&quot;/&gt;. Otherwise, they will use the color from the icon.  This colorization only works for SVG (Scaled Vector Graphic) icons which use a global fill color.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1493"/>
        <source>Colorize tag icons with the tag&apos;s color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1334"/>
        <source>Colorize Tag Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1433"/>
        <source>Tag Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1298"/>
        <source>Filter Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1228"/>
        <source>Track Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1238"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1260"/>
        <source>Filter Pane icon sizes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1241"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1263"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1308"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1366"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1395"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1417"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1446"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2254"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2326"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2358"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2393"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2491"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2552"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4143"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4168"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4193"/>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1440"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of icons shown in &lt;span style=&quot; font-weight:600;&quot;&gt;GPS Track&lt;/span&gt; panes. For example, icons show in the Tag column, such as &lt;img src=&quot;:art/ui/Road-Grey.png&quot;/&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1443"/>
        <source>GPS Track maximum icon size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1389"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of icons shown in &lt;span style=&quot; font-weight:600;&quot;&gt;View&lt;/span&gt; panes (commonly, flag icons such as &lt;img src=&quot;:art/tags/Flags/Countries/Greece.jpg&quot; width=&quot;30&quot; height=&quot;20&quot;/&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1392"/>
        <source>Maximum size of View pane icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1483"/>
        <source>View Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1382"/>
        <source>Max Track Pane Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1360"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of icons shown in &lt;span style=&quot; font-weight:600;&quot;&gt;Tag Selector&lt;/span&gt; popups, such as &lt;img src=&quot;:art/ui/Road-Grey.png&quot;/&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1363"/>
        <source>Tag Selector icon sizes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1476"/>
        <source>Track Flag Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1411"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of flags shown in &lt;span style=&quot; font-weight:600;&quot;&gt;GPS Track&lt;/span&gt; panes.  These are displated in the &lt;span style=&quot; font-style:italic;&quot;&gt;Flags&lt;/span&gt; column.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1414"/>
        <source>GPS Track maximum flag size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1462"/>
        <source>Max Track Pane Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1279"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum number of flags the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Panes&lt;/span&gt; will display in the list. More flags can be present, but only the first N will be displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select configuration pages.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1282"/>
        <source>Maximum flags displayed in Track Panes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1721"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Map display options&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1724"/>
        <source>Map display options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2144"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2225"/>
        <source>Track Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2241"/>
        <source>Track Outline Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2345"/>
        <source>Outline Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2352"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw active (primary selection in a track pane) tracks when the map view is zoomed in.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2355"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2488"/>
        <source>Active track line width when zoomed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2485"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw active (primary selection in a track pane) tracks when the map view is zoomed out.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2478"/>
        <source>Active Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2464"/>
        <source>Unassigned Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2301"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This color is used to draw track lines which have not been assigned a color in any other way.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2304"/>
        <source>Color for track lines without their own.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2412"/>
        <source>Unselected Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2432"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw default (unselected and inactive) tracks when the map view is zoomed out. 0 is totally transparent, 255 is totally opaque.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2435"/>
        <source>Default track opacity when zoomed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2546"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw default (unselected and inactive) tracks when the map view is zoomed out.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2549"/>
        <source>Default track line width when zoomed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2276"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw active (primary selection in a track pane) tracks when the map view is zoomed out. 0 is totally transparent, 255 is totally opaque.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2279"/>
        <source>Active track opacity when zoomed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2419"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw active (primary selection in a track pane) tracks when the map view is zoomed in. 0 is totally transparent, 255 is totally opaque.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2422"/>
        <source>Active track opacity when zoomed in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2471"/>
        <source>Line Width (close)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2526"/>
        <source>Opacity (far)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2380"/>
        <source>Line Width (far)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2248"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw default (unselected and inactive) tracks when the map view is zoomed in.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2251"/>
        <source>Default track line width when zoomed in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2234"/>
        <source>Opacity (close)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2533"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the line width used to draw default (unselected and inactive) tracks when the map view is zoomed in.  0 is totally transparent, 255 is totally opaque.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2536"/>
        <source>Default track opacity when zoomed in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2387"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tracks can be outlined for easier readability against typical light colored map backgrounds. This is the width of the outline for the current (selected) track, added to the track line on both sides. If set to zero, no outline will be drawn.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2390"/>
        <source>Current (selected) track outline width.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2320"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tracks can be outlined for easier readability against typical light colored map backgrounds. This is the width of the outline for default (unselected) tracks, added to the track line on both sides. If set to zero, no outline will be drawn.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2323"/>
        <source>Outline width for default (unselected) tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1756"/>
        <source>Selected Track Legs (GPS data points)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1762"/>
        <source>Waypoint Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1982"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Waypoints using the default icon will be drawn on Map Panes in this size.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The minimum distance in screen pixels that two points must be separated by to be displayed.  Data points closer than this to the prior point will not be rendered, to avoid overwhelming the track with data point icons.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2007"/>
        <source>Threshold distance for track point display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, waypoints will be drawn on &lt;span style=&quot; font-weight:600;&quot;&gt;Map Panes&lt;/span&gt; during interactive moment or while the map is updating for some reason.&lt;/p&gt;&lt;p&gt;If unchecked, they will not, which can improve interactive performance when moving the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2124"/>
        <source>Draw Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2587"/>
        <source>Available Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2596"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is a set of tags, zero or more of which can be applied to GPS tracks by doubleclicking on the &lt;span style=&quot; font-weight:600;&quot;&gt;Tags&lt;/span&gt; column for any given track. The GPS tracks will then be drawn in the color assigned to the tag here, and the track list can be filtered based on track tags by typing their names into the filter field.&lt;/p&gt;&lt;p&gt;You can re-order tags by dragging them with the mouse. You may double click the color or the icon to edit it, add new tags, new categories to hold collections of tags, or remove old tags.  The order displayed here does not effect the tags assigned to tracks, so you may reorder them per your convenience.&lt;/p&gt;&lt;p&gt;You may optionally assign a drag coefficient * frontal area, weight, and rolling resistance.  Power will be calculated from those parameters, the athlete or passenger weight, and the altitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2742"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This area can be used to enter data about athletes or passengers, and will be used to estimate power if not available directly from a sensor.  The biometric efficiency is used to estimate energy burned during a workaround.  For most people, this efficiency is in the range of 20-25%.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1029"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When the auto-import process runs (at program start or from the &lt;span style=&quot; font-weight:600;&quot;&gt;File/Auto Import&lt;/span&gt; menu), GPS files will be imported from this directory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1113"/>
        <source>External Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1015"/>
        <source>Use Stdout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1186"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the standard output of the &lt;span style=&quot; font-weight:600;&quot;&gt;External Command&lt;/span&gt; will be read as a file. It must be in one of the supported formats.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="933"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the tags to be automatically applied to Auto-Imported tracks. You can still edit them after import from the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt; or other methods.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4072"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the color used to render the selected point range in the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Line Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4075"/>
        <source>Track Line Pane range color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4091"/>
        <source>Current Marker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4107"/>
        <source>Track Line Pane marker color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4123"/>
        <source>Chart Line Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4130"/>
        <source>Range Indicator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4137"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Line width for current item marker in &lt;span style=&quot; font-weight:600;&quot;&gt;Track Line Panes&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4162"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Line width for selected range marker in &lt;span style=&quot; font-weight:600;&quot;&gt;Track Line Panes&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4257"/>
        <source>Activity Summary Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4263"/>
        <source>Max Date Spans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4270"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;c date spans to display in the graph, with precedence to the most recent dates. This prevents graphs with a very large number of separate date spans from becoming excessively large.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4286"/>
        <source>Bar Width %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4293"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;In Act&lt;span style=&quot; font-weight:600;&quot;&gt;ivity Summary Panes&lt;/span&gt;, this is the fraction of a column width occupied by the bars.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4296"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4423"/>
        <source> %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4544"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to next tab&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4559"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to previous tab&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1887"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1941"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1966"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1988"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2010"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2051"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2096"/>
        <source> pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1769"/>
        <source>Min Proximity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1871"/>
        <source>Active Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2067"/>
        <source>Unselected Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1903"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be used to display default (unselected) sample points for the current track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1906"/>
        <source>Unselected track sample point icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1851"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1783"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be used to display selected sample points for the current track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1786"/>
        <source>Selected track sample point icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1832"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be used to display current (active) sample points for the current track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1835"/>
        <source>Current (active) track sample point icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2042"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size in pixels to display the icon for each unselected track data point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2045"/>
        <source>Unselected track sample point icon size in pixels.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1825"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1776"/>
        <source>Selected Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1957"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size in pixels to display the icon for the active (current) track data point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1881"/>
        <location filename="../src/ui/forms/appconfig.ui" line="1960"/>
        <source>Current track sample point icon size in pixels.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1878"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size in pixels to display the icon for each selected track data point.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1818"/>
        <source>GPSD Live Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of icons shown in &lt;span style=&quot; font-weight:600;&quot;&gt;Filter &lt;/span&gt;panes, such as &lt;img src=&quot;:art/ui/Road-Grey.png&quot;/&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1469"/>
        <source>New Pane Previews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1305"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the height of the pane preview shown in the &lt;span style=&quot; font-weight:600;&quot;&gt;New Pane Dialog&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1327"/>
        <source>Climb Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1257"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum size of icons shown in &lt;span style=&quot; font-weight:600;&quot;&gt;Climb &lt;/span&gt;panes, such as &lt;img src=&quot;:/art/tags/Misc/HillUp.svg&quot;/&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1802"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be used to draw the current GPSD live location from the GPS Daemon.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1935"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size in pixels to display the icon for GPSD live points.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2074"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the default icon shown with waypoints (from &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Panes&lt;/span&gt;) which do not supply an icon of their own.  Specific waypoints can be given icons with the &amp;quot;&lt;span style=&quot; font-style:italic;&quot;&gt;Set Icon...&lt;/span&gt;&amp;quot; context menu in &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Panes&lt;/span&gt;.  The &amp;quot;Guess Icon&amp;quot; menu can also be used to auto-assign icons for waypoints with certain known Symbol values.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2090"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Waypoints with specific icons set in &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Panes&lt;/span&gt; will be drawn on &lt;span style=&quot; font-weight:600;&quot;&gt;Map Panes&lt;/span&gt; in this size.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2115"/>
        <source>Interactive Movement Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, track points will be drawn in &lt;span style=&quot; font-weight:600;&quot;&gt;Map Panes &lt;/span&gt;during interactive movement.&lt;/p&gt;&lt;p&gt;If unchecked, they will not, which can improve interactive performance when moving the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2154"/>
        <source>Draw Track Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2174"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option controls the display behavior of the map during interactive movement. Since drawing many tracks can be slow, you can set this option to reduce the number of tracks drawn. Options are:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Display All Tracks&lt;/span&gt; - This will display all tracks matching the active filter.&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Display Active Track&lt;/span&gt; - This will display the single active track.&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Display No Tracks&lt;/span&gt; - This will draw only the map, but no tracks.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;After interactive movement stops, the full set of tracks matching the active filter will always be drawn.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2178"/>
        <source>Display All Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2183"/>
        <source>Display Active Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2188"/>
        <source>Display No Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2164"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the map will have inertia during interactive movement.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="387"/>
        <source>Track Data Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="610"/>
        <source>Map Viewpoints Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="716"/>
        <source>Data and UI Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="826"/>
        <source>Timeout (sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="916"/>
        <source>File pattern (shell glob) to match.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="926"/>
        <source>Move to this directory under &apos;Move&apos; mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="952"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If set, this command is run before auto-importing files. For example, if you have a custom tool to fetch files from a particular kind of device. The tool will be run with a working directory set to the &lt;span style=&quot; font-weight:600;&quot;&gt;Auto Import Directory&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;If present, &lt;span style=&quot; font-style:italic;&quot;&gt;${AutoImportDir}&lt;/span&gt; will be substituted with the &lt;span style=&quot; font-weight:600;&quot;&gt;Auto Import Directory&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;The process is run asynchronously. The tracks may not appear for several moments after using the Auto Import feature if the external process does not complete immediately. If the external tool does not finish within the &lt;span style=&quot; font-weight:600;&quot;&gt;Timeout&lt;/span&gt; value, ZTGPS will kill the process.&lt;/p&gt;&lt;p&gt;This command is not interpreted by the shell, but certain basic features such as variable substitution, tilde (~) expansion, wildcard globbing, quoting, and process substitution are available.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="955"/>
        <source>External command to run before importing files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1022"/>
        <source>Rename with this suffix under &apos;Backup&apos; mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1032"/>
        <source>Directory from which to import files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1058"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the maximum amount of time to wait for the &lt;span style=&quot; font-weight:600;&quot;&gt;External Command&lt;/span&gt; to finish before killing it.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2167"/>
        <source>Inertial Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2578"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Track tag editor&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2581"/>
        <source>Track tag editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2599"/>
        <source>Edit available track tags.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2653"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new track tag&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2656"/>
        <source>Add new track tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2659"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2784"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2883"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2670"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new track tag header.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2673"/>
        <source>Add new track tag header.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2676"/>
        <source>Add Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2687"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete track tag&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2690"/>
        <source>Delete track tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2693"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2801"/>
        <location filename="../src/ui/forms/appconfig.ui" line="2897"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2704"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sort all the tags by name.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2707"/>
        <source>Sort tags by name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2710"/>
        <source>Sort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2727"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;People editor for power and energy calculations&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2730"/>
        <source>People editor for power and energy calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2736"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="138"/>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2745"/>
        <source>People parameters, used for power and energy estimation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2778"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new person&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2781"/>
        <source>Add new person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2795"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove selected person&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2798"/>
        <source>Remove selected person</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2812"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset the selected person&apos;s birthday and maximum HR data.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2815"/>
        <source>Reset birthday and max HR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2818"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2835"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Configure training zones&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2841"/>
        <source>Training Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2847"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This area can be used to enter training zone data which is used to calculate the amount of time spent in each zone. Defaults are available through the buttons below.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2880"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new training zone entry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2894"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove selected training zone entry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2908"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset to preset training zone models.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2912"/>
        <source>Zone Model Presets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="807"/>
        <source>Auto Import Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1126"/>
        <source>Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="913"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Files matching this pattern will be auto-imported. Files which don&apos;t are ignored.&lt;/p&gt;&lt;p&gt;The pattern is a shell style glob wildcard, not a regular expression. Multiple patterns can be separated by spaces. For example:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;*.gpx *.fit&lt;/span&gt;&lt;/p&gt;&lt;p&gt;This matches only the file component of the name, not the whole path.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1099"/>
        <source>Post-Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="819"/>
        <source>Pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="906"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1133"/>
        <source>Backup Suffix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1264"/>
        <source>Auto Import Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="839"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selects the auto-import mode, from one of:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Disabled&lt;/span&gt; - Auto Import is disabled.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Menu&lt;/span&gt; - Auto Import is manually triggered from the &lt;span style=&quot; font-style:italic;&quot;&gt;File/Auto&lt;/span&gt; Import menu.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Startup&lt;/span&gt; - Auto Import happens at program start. It can still be triggered manually from the menu as well.&lt;/li&gt;&lt;/ul&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="848"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="853"/>
        <source>Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="923"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If the Post-Import Behavior is set to &lt;span style=&quot; font-weight:600;&quot;&gt;Move&lt;/span&gt;, this is the destination directory to use. Any old files of the same name will be overwritten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="843"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="968"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show directory browser to select the auto-import path.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1106"/>
        <source>Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="886"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show directory browser to select the AutoImport Backup Directory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="988"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This selects the post-import behavior, from one of:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Backup&lt;/span&gt; - Rename the files to the old name + a provided suffix.&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Move&lt;/span&gt; - Move the files to a supplied directory.&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Delete&lt;/span&gt; - Delete the files after import.&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Leave&lt;/span&gt; - Leave the files. Be careful of redundant imports.&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;The track deduplication settings will be used during auto-import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="992"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="997"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1002"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1007"/>
        <source>Leave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2931"/>
        <source>Distance and Position Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2937"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3845"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline;&quot;&gt;Pad&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3016"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3029"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3075"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3088"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3124"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3160"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3210"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3310"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3330"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3343"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3356"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3488"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3501"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3530"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3686"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3696"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Digits after decimal.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3019"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3032"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3078"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3091"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3127"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3163"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3213"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3313"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3333"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3346"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3359"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3491"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3504"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3533"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3689"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3699"/>
        <source>Digits after decimal.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3065"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for latitude and longitude display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3068"/>
        <source>Units for latitude and longitude display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2944"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="148"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="152"/>
        <source>Climb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2990"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3107"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying horizontal length or distance.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2993"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3110"/>
        <source>Units for displaying horizontal length or distance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2967"/>
        <source>Track Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3153"/>
        <source>Acceleration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3117"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3006"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying vertical length or distance.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3009"/>
        <source>Units for displaying vertical length or distance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3193"/>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3223"/>
        <source>Lat/Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2957"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for elevation display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2960"/>
        <source>Units for elevation display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3143"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3183"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for speed display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3146"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3186"/>
        <source>Units for speed display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3200"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3566"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3891"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline;&quot;&gt;Unit&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="2974"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3438"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3976"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline;&quot;&gt;Precision&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3042"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3478"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; text-decoration: underline;&quot;&gt;Format&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3170"/>
        <source>Leg Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3304"/>
        <source>Misc Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3559"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3520"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying areas.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3523"/>
        <source>Units for displaying areas.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3405"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying weight.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3408"/>
        <source>Units for displaying weight.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3471"/>
        <source>Percents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3375"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying temperatures.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3378"/>
        <source>Units for displaying temperatures.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3392"/>
        <source>Cadence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3464"/>
        <source>Slope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3421"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying slopes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3424"/>
        <source>Units for displaying slopes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3323"/>
        <source>Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3454"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3549"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3582"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying percentages.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3457"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3552"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3585"/>
        <source>Units for displaying percentages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3385"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3431"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3664"/>
        <source>Power and Energy Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3706"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="143"/>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3676"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying power.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3679"/>
        <source>Units for displaying power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3713"/>
        <source>Energy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3726"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying energy.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3729"/>
        <source>Units for displaying energy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3795"/>
        <source>Time Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3943"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying datestamps in track views (date + time of day).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3861"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3946"/>
        <source>Units for displaying track datestamps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3858"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying datestamps in point views (date + time of day).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3878"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use UTC time for track points&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3881"/>
        <source>Use UTC time for track points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3884"/>
        <location filename="../src/ui/forms/appconfig.ui" line="3992"/>
        <source>UTC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3986"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use UTC for track times&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3989"/>
        <source>Use UTC for track times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3835"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for time duration display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3838"/>
        <source>Units for time duration display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3901"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Precision for duration display&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3904"/>
        <source>Precision for duration display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3822"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3808"/>
        <source>Point Datestamps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3815"/>
        <source>Track Datestamps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3914"/>
        <source>Track Timestamps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3959"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Units for displaying timestamps in track views (time only, no date).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3962"/>
        <source>Units for displaying track timestamps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3969"/>
        <source>Time Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3927"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display format for time zones.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="3930"/>
        <source>Display format for time zones.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4400"/>
        <source>Climb Analysis Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4406"/>
        <source>Minimum Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4413"/>
        <source>Minimum Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4420"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The minimum grade that an elevation gain or loss must have to be considered in &lt;span style=&quot; font-weight:600;&quot;&gt;Climb Analysis Panes&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The minimum elevation difference that must exist to be considered in &lt;span style=&quot; font-weight:600;&quot;&gt;Climb Analysis Panes&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4442"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4471"/>
        <source> m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4461"/>
        <source>Steep Section</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4468"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Climb Analysis Panes&lt;/span&gt; can display the steepest section of a hill measured over this much distance.  This is often more interesting than the hill average grade, which includes gentle beginning and ending segments, and more interesting than the single steepest point, which is subject to sampling errors.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4045"/>
        <source>Track Line Graph Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Line width for track graph&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4140"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4165"/>
        <location filename="../src/ui/forms/appconfig.ui" line="4190"/>
        <source>Line width for track graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4104"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the color used to render the marker (position indicator) in the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Line Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4215"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Colors for graphing track point data.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4218"/>
        <source>Colors for track point graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4541"/>
        <source>Next Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4547"/>
        <source>Ctrl+PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4556"/>
        <source>Prev Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="4562"/>
        <source>Ctrl+PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="66"/>
        <source> bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="130"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="131"/>
        <source>UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="132"/>
        <source>Backups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="135"/>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="136"/>
        <source>Map Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/appconfig.ui" line="1086"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="137"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="139"/>
        <source>Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="134"/>
        <source>Auto Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="140"/>
        <source>Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="141"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="142"/>
        <source>Misc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="144"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="145"/>
        <source>Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="146"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="150"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="147"/>
        <source>Activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="149"/>
        <source>Colorizers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="151"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1045"/>
        <source>Track Line Pane marker color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1052"/>
        <source>Track Line Pane selected range color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1059"/>
        <source>Outline track color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1066"/>
        <source>Unassigned track color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1269"/>
        <source>Auto Import Backup Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1286"/>
        <source>Timeout (%1 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="809"/>
        <location filename="../src/ui/windows/appconfig.cpp" line="1279"/>
        <source>Canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1071"/>
        <source>GPS Data Save Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/appconfig.cpp" line="1087"/>
        <source>New Zone...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AreaDialog</name>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="14"/>
        <source>Area Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="25"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These buttons select which data from each track to view. The track points in the selected region with the highest or lowest value will be displayed.&lt;/p&gt;&lt;p&gt;For example, if there are 5 tracks with points in the selected area, the highest speed from each of the 5 tracks will be shown.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="28"/>
        <source>Data to display from track points in region.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="31"/>
        <source>Track Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="52"/>
        <source>Select:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display the highest value from points within the selected region for each track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="61"/>
        <source>Display highest value in selected region.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="64"/>
        <source>Highest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="74"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display the average value from points within the selected region for each track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="77"/>
        <source>A&amp;verage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="84"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display the lowest value from points within the selected region for each track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="87"/>
        <source>Display the lowest value from points within the selected region for each track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="90"/>
        <source>&amp;Lowest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="113"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom to the currently selected area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="116"/>
        <source>Zoom to currently selected area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="119"/>
        <source>Zoom To</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="132"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="139"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="152"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Typing here will filter the tracks displayed in the list below. This field supports Perl-style regular expressions.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="166"/>
        <source>Filter expression for track list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/areadialog.ui" line="169"/>
        <source>Regex...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/areadialog.cpp" line="217"/>
        <source> &lt;b&gt;Tracks with points in area:&lt;/b&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/areadialog.cpp" line="219"/>
        <source> &lt;b&gt;Selected:&lt;/b&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/areadialog.cpp" line="355"/>
        <source>No Map Pane found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClimbAnalysisPane</name>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="14"/>
        <source>Climb Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="17"/>
        <source>Cli&amp;mb Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the filter query is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="42"/>
        <source>Filter validity indicator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the pane.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;458&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;53&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;389&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;br/&gt;!~&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; as a regular expression&lt;br/&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; matching a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;360&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;352&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Speed &amp;gt; 20 km/h&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Index &amp;gt; 100&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence. Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;436&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;428&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Start &amp;lt; 10 km &amp;amp; BasePeak &amp;gt; 100 m&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;/&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="58"/>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="64"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Climb Analysiss&lt;/span&gt; pane shows data for climbs and descents in the current GPS track selected in a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;The set of data to be displayed can be adjusted with the &lt;span style=&quot; font-weight:600;&quot;&gt;Show Columns&lt;/span&gt; pulldown, or the context menus on the headers.&lt;/p&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Arrow Keys&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;Page Up/Down&lt;/span&gt; can be used to move through the list. The active point will be highlighted in the map display. If multiple climbs are selected, they selected part of the track will be highlighted in the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="126"/>
        <source>Merge Segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="129"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will merge selected segments for the currently displayed track. If no segments are selected, all segments present in the track will be merged into a single segment.&lt;/p&gt;&lt;p&gt;A GPS track is composed of multiple segments, each of which can hold many sample points. These segments are displayed in the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Points&lt;/span&gt; pane and marked &amp;quot;Seg 0&amp;quot;, &amp;quot;Seg 1&amp;quot;, etc.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="132"/>
        <source>Merge multiple track segments into single segment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="141"/>
        <source>Split Segment(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="144"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will split segments at the currently selected points. The currently selected point will become the first point in the new segment.  You may select multiple individual points, or mark a range to extract that range into a new segment of its own.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="147"/>
        <source>Split segment at selected point, or move selected range of points into new segment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="156"/>
        <source>Create Waypoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/climbanalysispane.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Creates a new waypoint at the selected point(s).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ClimbModel</name>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="162"/>
        <source>Type (climb or descent).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="164"/>
        <source>Horizontal position of start of climb.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="166"/>
        <source>Horizontal position of end of climb.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="168"/>
        <source>Steepest short section of track. Distance set in configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="170"/>
        <source>Total vertical distance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="219"/>
        <source>Climb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="219"/>
        <source>Climbs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CmdLine</name>
    <message>
        <location filename="../src/util/cmdline.cpp" line="296"/>
        <source>Provide a single feature type, such as &apos;--help-fields trk&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="335"/>
        <source>Provide a feature type and field name, such as &apos;--help-units trk Ascent&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="343"/>
        <source>Help Options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="213"/>
        <source>Filter query Language:
The batch processing query language is identical to the in-program filter syntax
which can be entered into the filter bars above Track Panes, etc.  The field names
can be combined with comparison and boolean operators. The query string can be
passed as an argument to the --filter-trk or --filter-wpt options.
   
   The following operators are supported:
      =~ or :    Perl-style regex match.  E.g: 
                    Name : Dirt.*Trail   # all matching given regex
                    Flags : Mexico       # all tracks in Mexico
      !~         Matches if regex is not found.  E.g: Name !~ Trail
      ==         Exact equality.  E.g: Max_HR == &apos;158 bpm&apos;
      !=         Inequality.
      &lt;          Less than.  E.g: Ascent &gt; 100m
      &lt;=         Less than or equal to.
      &gt;          Greater than.  E.g: Moving_Time &gt; 2h30m
      &gt;=         Greater than or equal to.

   The following boolean operators can be used to combine expressions:
      !          Unary logical negation.  E.g, !(Name : Bridge)
      &amp;          Binary logical and. E.g: Tags : Hike &amp; Ascent &gt; 1km
      ^          Binary exclusive or.
      |          Binary inclusive or.

   Parenthetical groupings are supported, but the parens must be space separated:
      ( Tags : Hike | Tags : Run ) &amp; Length &gt; 20km

   A list of field names can be obtained via:
      %s --help-fields trk|wpt

   A range of common units suffixes are supported for numeric values.  A list of
   suffixes for a given field is available via:
      %s --help-units trk|wpt FIELDNAME

   The query string must be a single command line parameter, so use shell quotes for
   queries containing spaces.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="257"/>
        <source>Batch File Processing Options:
   --input, -i FILE...    One or more GPS input files to process. &apos;-&apos; for stdin. 
   --output, -o FILE...   One per input, or omit to auto-generate names. &apos;-&apos; for stdout.
   --dir, -d DIR          Write outputs to this directory. $PWD if unset.
   --clobber              Do not complain about existing output files.
   --concat, --merge      Merge multiple input files to a single output file.
   --pretty               Generate human readable XML files.
   --indent NUM           Indent XML files with NUM spaces.
   --indent-tabs          Use tabs instead of space to indent XML files.
   --type trk|wpt|all     Write tracks, waypoints, or all.
   --filter-trk QUERY     Process tracks matching QUERY.  --help-filter for syntax.
   --filter-wpt QUERY     Process waypoints matching QUERY. --help-filter for syntax.
   --match-case           Use case sensitive queries.
   --verbose,-v           Be more verbose.
   --stat                 Print statistics about conversions.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="344"/>
        <source>   --help-filter               Describe query syntax.
   --help-batch                Describe batch processing options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="347"/>
        <source>   --help-formats-input        Describe supported input file formats.
   --help-formats-output       Describe supported output file formats.
   --help-fields trk|wpt       Available columns for queries.
   --help-units trk|wpt field  Available unit suffixes for given field.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="374"/>
        <source>No input files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="380"/>
        <source>Concatenation mode must have a single output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="386"/>
        <source>Input and output list size mismatch.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="392"/>
        <source>Output format must be provided.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="402"/>
        <source>Output directory does not exist or is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="414"/>
        <source>Missing input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="419"/>
        <source>File is not readable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="424"/>
        <source>Input is not a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="434"/>
        <source>Output file exists (see --clobber)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="439"/>
        <source>Output file exists but is not writable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="445"/>
        <source>Output directory for file is not accessable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="452"/>
        <source>Unknown input format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="458"/>
        <source>Unknown output format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="479"/>
        <source>stdin may appear only once.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="484"/>
        <source>stdout may appear only once.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="489"/>
        <source>No output files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="495"/>
        <source>Indent level must be &gt;= 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="502"/>
        <source>Unknown feature type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="509"/>
        <source>Invalid track filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="514"/>
        <source>Invalid waypoint filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="577"/>
        <source>tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="578"/>
        <source>points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="579"/>
        <source>waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="580"/>
        <source>files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="608"/>
        <source>File conversion failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="626"/>
        <source>File load failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="635"/>
        <source>File save failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/util/cmdline.cpp" line="649"/>
        <source>Internal error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DataColumnPane</name>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="76"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="95"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="112"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="120"/>
        <source>Merged Track Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="124"/>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="159"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="129"/>
        <source>bad item selection for merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="146"/>
        <source>No items selected for simplify operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/datacolumnpane.cpp" line="152"/>
        <source>Simplify</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceDialog</name>
    <message>
        <location filename="../src/ui/forms/devicedialog.ui" line="14"/>
        <source>GPS Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/devicedialog.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import tracks from selected device(s)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/devicedialog.ui" line="38"/>
        <source>Import tracks from selected device(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/devicedialog.ui" line="41"/>
        <source>&amp;Import...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/devicedialog.cpp" line="66"/>
        <source>Import Tracks from Device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DocDialog</name>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="43"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="44"/>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="45"/>
        <source>UI Basics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="46"/>
        <source>New Pane Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="47"/>
        <source>Data Pane Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="48"/>
        <source>Track Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="49"/>
        <source>Map Modes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="50"/>
        <source>Status Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="51"/>
        <source>ToolBars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="52"/>
        <source>Color Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="53"/>
        <source>Undo/Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="54"/>
        <source>Rich Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="55"/>
        <source>Importing GPS Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="56"/>
        <source>From Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="57"/>
        <source>From GPS Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="58"/>
        <source>Live Capture (GPSD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="59"/>
        <source>Auto Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="60"/>
        <source>Exporting GPS Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="61"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="62"/>
        <source>Data Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="63"/>
        <source>Area Searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="64"/>
        <source>Map Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="65"/>
        <source>Track Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="66"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="67"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="68"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="69"/>
        <source>Panes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="70"/>
        <source>Activity Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="71"/>
        <source>Climb Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="72"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="73"/>
        <source>GPS Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="74"/>
        <source>Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="75"/>
        <source>Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="76"/>
        <source>Simple View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="77"/>
        <source>Track Comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="78"/>
        <source>Track Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="79"/>
        <source>Track Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="80"/>
        <source>Training Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="81"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="82"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="83"/>
        <source>Waypoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="84"/>
        <source>Track Editing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="85"/>
        <source>Track Merging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="86"/>
        <source>Segment Split and Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="87"/>
        <source>Simplifying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="88"/>
        <source>Deleting Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="89"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="90"/>
        <source>Offline Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="91"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="92"/>
        <source>Units</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="93"/>
        <source>People</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="94"/>
        <source>AutoImport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="95"/>
        <source>Colorizers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="96"/>
        <source>Bugs &amp; Limitations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/docdialog.cpp" line="97"/>
        <source>Roadmap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="14"/>
        <source>Export Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="23"/>
        <source>Export Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="36"/>
        <source>Export Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the set of file formats which may be used to export data.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="127"/>
        <source>Select data export format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="29"/>
        <source>Write formatted output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="134"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If this is checked, data formats which allow pretty-printing (indented XML files, etc) will do so.  This makes the file large, but more human readable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="137"/>
        <source>Enable pretty-printed output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="43"/>
        <source>Indent Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the number of tabs or spaces used to indent formats which support that, such as GPX.  It only applies if formatted output is enabled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="58"/>
        <source>Set formatted output indent level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set this to use spaces in formatted output.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="80"/>
        <source>Use spaces in formatted output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="83"/>
        <source>Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="96"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set this to use tabs in formatted output.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="99"/>
        <source>Use tabs in formatted output.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="102"/>
        <source>Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="168"/>
        <source>Track Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="174"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to export all known tracks, whether or not they are selected in a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="190"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to export only selected tracks in the most recently focused &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;. The focused pane has a &lt;span style=&quot; font-weight:600;&quot;&gt;Bold Title&lt;/span&gt;. If no tracks are selected, then all tracks will be exported.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="206"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to skip track export.  For example, to export only waypoints.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="209"/>
        <source>Don&apos;t export tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="212"/>
        <source>No Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="222"/>
        <source>Waypoint Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="228"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to export all known waypoints, whether or not they are selected in a &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="231"/>
        <source>Export all waypoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="234"/>
        <source>All Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="244"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to export only selected waypoints in the most recently focused &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Pane&lt;/span&gt;. The focused pane has a &lt;span style=&quot; font-weight:600;&quot;&gt;Bold Title&lt;/span&gt;. If no waypoints are selected, then all waypoints will be exported.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="247"/>
        <source>Export selected waypoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="250"/>
        <source>Selected Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="257"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select this option to skip waypoint export. For example, to export only tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="260"/>
        <source>Export no waypoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="263"/>
        <source>No Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="177"/>
        <source>Export all tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="180"/>
        <source>All Trac&amp;ks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="193"/>
        <source>Export selected tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/exportdialog.ui" line="196"/>
        <source>Selected Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/exportdialog.cpp" line="111"/>
        <source>Export GPS Track File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterModel</name>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="85"/>
        <source>Descriptive name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="88"/>
        <source>Query string.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="117"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="117"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilterPane</name>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="14"/>
        <source>Track Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="17"/>
        <source>Trac&amp;k Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the filter query is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="42"/>
        <source>Filter validity indicator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;358&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;53&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;389&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;as a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!~ &lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;360&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;352&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Name : Canada&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;/&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;&lt;br/&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence. Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="61"/>
        <source>Filter expression (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="76"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Entries in the &lt;span style=&quot; font-weight:600;&quot;&gt;Filter Pane&lt;/span&gt; encode queries to show a particular set of GPS tracks.&lt;/p&gt;&lt;p&gt;The query language is described in the tooltip for the query text field above the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;. Those queries are stored here and can be recalled by double clicking them, rather than having to type them in again.&lt;/p&gt;&lt;p&gt;If multiple filters are selected, the &lt;span style=&quot; font-weight:600;&quot;&gt;Activate&lt;/span&gt; menu (or shift or control double clicking) can be used to show tracks matching any of the selected filters.  Any filter can be taken out of effect by using &amp;quot;&lt;span style=&quot; font-weight:600;&quot;&gt;Deactivate&lt;/span&gt;&amp;quot; menu, which will restore the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt; to showing all tracks (no active filter).&lt;/p&gt;&lt;p&gt;The filter can be updated from any modifications made to it in the Track Pane&apos;s query line by using the &amp;quot;&lt;span style=&quot; font-weight:600;&quot;&gt;Update Filter&lt;/span&gt;&amp;quot; menu.&lt;/p&gt;&lt;p&gt;An icon can be shown next to the query name by using the &amp;quot;&lt;span style=&quot; font-weight:600;&quot;&gt;Set Icon&lt;/span&gt;&amp;quot; menu.&lt;/p&gt;&lt;p&gt;Filters can be drag/dropped to reorder them or nest them under other filters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="129"/>
        <source>Create New Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="132"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a new query filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="141"/>
        <source>Edit Filter Query...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="144"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edits the query for the selected filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="153"/>
        <source>Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="156"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Activates the filter.  The most recently focused &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt; will be set to this filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="165"/>
        <source>Set Icon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sets an icon to be displayed for this filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="177"/>
        <source>Edit Filter Name...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="180"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the name of the filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="189"/>
        <source>Unset Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="192"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove any icon set for this filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="201"/>
        <source>Deactivate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="204"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clears any presently active filters from the most recently focused &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="213"/>
        <source>Update Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/filterpane.ui" line="216"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Update the query for the selected filter from the last focused &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;&apos;s query.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="152"/>
        <source>Invalid Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="157"/>
        <source>Filter Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="158"/>
        <source>Filter name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="162"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="166"/>
        <source>Add Filter: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="171"/>
        <source>Added filter: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="179"/>
        <source>No Pane found for filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="280"/>
        <source>Using filter(s): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="306"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="306"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/filterpane.cpp" line="306"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeoLoad</name>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="74"/>
        <source>Unrecognized file type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="133"/>
        <source>Error loading file: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeoLocCompleter</name>
    <message>
        <location filename="../src/ui/widgets/geoloccompleter.cpp" line="35"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeoLocModel</name>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="188"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="189"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="190"/>
        <source>TZ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="191"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="192"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="193"/>
        <source>Latitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="194"/>
        <source>Longitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="218"/>
        <source>Location name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="220"/>
        <source>Flags for this geopolitical area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="222"/>
        <source>Location&apos;s time zone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="224"/>
        <source>Location type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="226"/>
        <source>Distance from center of map.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="228"/>
        <source>Location&apos;s latitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocmodel.cpp" line="230"/>
        <source>Location&apos;s longitude.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeoSave</name>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="374"/>
        <source>Unrecognized save type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="392"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="393"/>
        <source>No track selection: export all tracks?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoio.cpp" line="407"/>
        <source>Error saving file:&lt;p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GotoLatLonDialog</name>
    <message>
        <location filename="../src/ui/forms/gotolatlondialog.ui" line="14"/>
        <source>Goto Lat/Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gotolatlondialog.ui" line="35"/>
        <source>Lat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gotolatlondialog.ui" line="45"/>
        <location filename="../src/ui/forms/gotolatlondialog.ui" line="71"/>
        <source> °</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gotolatlondialog.ui" line="61"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GpsCaptureDialog</name>
    <message>
        <location filename="../src/ui/forms/gpscapturedialog.ui" line="14"/>
        <source>GPS Live Capture</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GpsCapturePane</name>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="20"/>
        <source>GPS Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="23"/>
        <source>&amp;GPS Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This dialog will allow tracking live data from a GPS receiver connected via the Linux GPSD. There are two modes of operation:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Track &lt;/span&gt;- Plot the current location on the map, but do not collect the points into a track.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Capture&lt;/span&gt; - Capture the GPS locations into a new track. You will be prompted for a track name and tags.&lt;/p&gt;&lt;p&gt;The tool button to the right of the Position display will center the map on the current position.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="78"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The device path for the attached GPS device. If empty, any device found by the daemon will be captured. You can provide a specific device, such as:&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;monospace&apos;;&quot;&gt;&amp;nbsp;&amp;nbsp;/dev/ttyUSB0&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="85"/>
        <source>Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="92"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The hostname from which to capture data using GPSD.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="95"/>
        <source>localhost</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="102"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="109"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="116"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The TCP/IP port for data capture from GPSD. The default is 2947.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="156"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS fix longitude and latitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="159"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="214"/>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="234"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS fix altitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="217"/>
        <source>Altitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="227"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="224"/>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="244"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS fix speed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="254"/>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="264"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Timestamp of last received GPS fix.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="267"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="276"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS fix longitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="302"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS fix latitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="338"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Center fix in map pane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="352"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a waypoint (visible in &lt;span style=&quot; font-weight:600;&quot;&gt;Waypoint Panes&lt;/span&gt; and on &lt;span style=&quot; font-weight:600;&quot;&gt;Map Panes&lt;/span&gt;) at the current position.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="368"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Geopolitical region. Hover mouse over flags for details.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="371"/>
        <source>Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="378"/>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="388"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Number of satellites contributing to fix.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="381"/>
        <source>Satellites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="391"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;GPS device connection status.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="401"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="475"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Track the GPS location in a Map Pane without capturing the data into a new track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="478"/>
        <source>Track witout capture.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="481"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="498"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Capture the GPS data into a new track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="501"/>
        <source>Capture data to new track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="504"/>
        <source>Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="515"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pause GPS collection. Clicking again will resume from pause.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="518"/>
        <source>Pause capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="521"/>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="240"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="532"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stop GPS collection. This will finalize the track, in &lt;span style=&quot; font-weight:600;&quot;&gt;Capture&lt;/span&gt; mode, or simply stop tracking in &lt;span style=&quot; font-weight:600;&quot;&gt;Track&lt;/span&gt; mode.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="535"/>
        <source>Stop capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="538"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="565"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This feature requires the libgps-dev package to be installed when the application is compiled.  See the BUILDING file for details.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpscapturepane.ui" line="568"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;GPSD support was not enabled at compile time.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Live GPSD capture is not available.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="208"/>
        <source>GPSD capture started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="210"/>
        <source>GPSD capture finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="240"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="347"/>
        <source>Capture track from live data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="371"/>
        <location filename="../src/ui/panes/gpscapturepane.cpp" line="381"/>
        <source>No fix</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GpsDevicePane</name>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="17"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is a list of GPS devices currently connected to the system.&lt;/p&gt;&lt;p&gt;If a device is not directly detected, GPX, TCX, or FIT files can still be imported using the &lt;span style=&quot; font-weight:600;&quot;&gt;File/Import Tracks...&lt;/span&gt; menu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="20"/>
        <source>GPS De&amp;vices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;458&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;53&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;389&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;br/&gt;!~&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;as a regular expression&lt;br/&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expresion&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;360&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;352&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Make : Garmin&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;/&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;&lt;br/&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;436&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;428&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Name : Make : Garmin &amp;amp; Model : Dakota&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="55"/>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="58"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="96"/>
        <source>Import From Device...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="99"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import GPS data from the selected GPS units.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="108"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="111"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Refresh the list of attached GPS units.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdevicepane.ui" line="14"/>
        <source>GPS Devices</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GpsModel</name>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="246"/>
        <source>GPS device manufacturer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="248"/>
        <source>GPS device model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="250"/>
        <source>GPS device node.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="252"/>
        <source>GPS device image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="254"/>
        <source>GPS device mountpoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="279"/>
        <source>GPS Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="279"/>
        <source>GPS Devices</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GpsdVersionDialog</name>
    <message>
        <location filename="../src/ui/forms/gpsdversiondialog.ui" line="14"/>
        <source>Supported GPSD Versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/gpsdversiondialog.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are the supported versions of the GPSD library.  The versions installed on this system are indicated.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/gpsdversiondialog.cpp" line="80"/>
        <source>Library Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/gpsdversiondialog.cpp" line="81"/>
        <source>API Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/gpsdversiondialog.cpp" line="82"/>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportDialog</name>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="14"/>
        <location filename="../src/ui/forms/importdialog.ui" line="20"/>
        <source>Import Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="26"/>
        <source>Import Features:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the provided color will be assigned to each track loaded.  This color will override the color provided by the tags assigned to the track(s)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="55"/>
        <source>Check to provide track specific color overrides.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="58"/>
        <source>Override Tag Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="81"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If &amp;quot;Override Tag Colors&amp;quot; is checked, this color will be assigned to each track loaded, and overide any color from the tags assigned to the track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="84"/>
        <source>Track specific override color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, tracks which appear identical to existing ones will not be loaded.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="39"/>
        <source>Check to avoid loading duplicate tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="42"/>
        <source>Deduplicate Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the importer will collect tracks from the file into Track Panes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="112"/>
        <source>Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the importer will collect waypoints from the file into Waypoint Panes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="125"/>
        <source>Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The tags assigned to the left side group will be applied to each track imported.&lt;/p&gt;&lt;p&gt;Some formats such as FIT will attempt to find tags to match the &amp;quot;sport&amp;quot; information from the file. This is an attempt to be helpful, not a guarantee. It may fail if tags have been renamed or removed.  Tags assigned manually will override any auto-discovered tags, so you should leave the left side blank if you wish to use this feature.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="157"/>
        <source>Assign tags to tracks on import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/importdialog.ui" line="160"/>
        <source>Assign Tags to Imported Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/importdialog.cpp" line="108"/>
        <source>Import GPS Track File(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/importdialog.cpp" line="126"/>
        <source>Track Override Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="17"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="33"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="37"/>
        <source>Re&amp;cent Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="63"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="91"/>
        <source>Sho&amp;w Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="109"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="122"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="145"/>
        <source>Pa&amp;ne</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="149"/>
        <source>&amp;Add Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="158"/>
        <source>R&amp;eplace Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="167"/>
        <source>&amp;Open Pane in Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="176"/>
        <source>Add &amp;Group Sibling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="212"/>
        <source>Main Tool Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="235"/>
        <source>UI Tool Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="262"/>
        <source>Pane Tool Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="299"/>
        <source>&amp;Configure...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="302"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show program configuration options.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="305"/>
        <source>Display the Configuration dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="314"/>
        <source>&amp;About Zombie Tracker GPS...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="317"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;About Zombie Tracker GPS.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="320"/>
        <source>Display the about dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="329"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="332"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exits the program, saving the session state to be restored next time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="335"/>
        <source>Exit program, saving session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="338"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="347"/>
        <source>Show &amp;All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="350"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show All entries in pane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="353"/>
        <source>Show All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="362"/>
        <source>&amp;Expand All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="365"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Expand all levels in the active (&lt;span style=&quot; font-weight:600;&quot;&gt;bolded&lt;/span&gt;) pane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="368"/>
        <source>Expand all tree levels.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="371"/>
        <source>Ctrl+End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="380"/>
        <source>&amp;Collapse All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Collapse all levels in the active (&lt;span style=&quot; font-weight:600;&quot;&gt;bolded&lt;/span&gt;) pane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="386"/>
        <source>Collapse all tree levels.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="389"/>
        <source>Ctrl+Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="404"/>
        <source>Show &amp;Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="407"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show Filters.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="410"/>
        <source>Show or hide pane filter bars.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="419"/>
        <source>&amp;Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="422"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select All entries in pane.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="425"/>
        <source>Select all in pane.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="428"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="437"/>
        <source>Select &amp;None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="440"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Deselect all selected entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="443"/>
        <source>Un-select all in pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="446"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="455"/>
        <source>About &amp;Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="458"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show about Qt dialog.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="461"/>
        <source>Show the About Qt dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="476"/>
        <source>&amp;Tree View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This toggles between flat and tree views of containers. Containers can be listed under their containing directories. For containers which support incremental (&amp;quot;snapshot&amp;quot;) style, they will be displayed as nested in the list. For example:&lt;/p&gt;&lt;p&gt;my-root-containter&lt;/p&gt;&lt;p&gt;|-- my-sub-container1&lt;/p&gt;&lt;p&gt;\-- my-sub-container2&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="482"/>
        <source>Select tree or flat view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="485"/>
        <source>Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="494"/>
        <source>&amp;Copy Selected </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="497"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This copies a textual representation of the selected data to the system clipboard.&lt;/p&gt;&lt;p&gt;If there are multiple container views, the selection is taken from the focused one. The group box title of the focused view will be &lt;span style=&quot; font-weight:600;&quot;&gt;bold&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="500"/>
        <source>Copy selection to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="503"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="512"/>
        <source>&amp;What is...?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="515"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Obtain information about a clicked UI element.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="518"/>
        <source>Click on a UI element to obtain more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="527"/>
        <source>Size to &amp;Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="530"/>
        <source>Size to Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="533"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will resize data columns to be as wide as the widest thing they contain.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="536"/>
        <source>Resize columns to data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="539"/>
        <source>`</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="548"/>
        <source>&amp;Donate...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="551"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Support this and other worthy software&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="554"/>
        <source>Donate the this and other worthy projects.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="563"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="566"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save the current application settings to the last saved or opened file.&lt;/p&gt;&lt;p&gt;This will save preferences, tab and pane configurations, toolbar, and status bar settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="569"/>
        <source>Save program settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="572"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="581"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Save the current application settings to a selected file.&lt;/p&gt;&lt;p&gt;This will save preferences, tab and pane configurations, toolbar, and status bar settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="587"/>
        <source>Save settings as new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="590"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="599"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="602"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open application settings from a selected file.&lt;/p&gt;&lt;p&gt;This will load preferences, tab and pane configurations, toolbar, and status bar settings, and replace the current ones with the configuration loaded from the file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="605"/>
        <source>Open existing settings file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="608"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="623"/>
        <source>&amp;Show Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="626"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide the statusbar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="629"/>
        <source>Show or hide the statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="638"/>
        <source>Remove &amp;Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="641"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove the selected (&lt;span style=&quot; font-weight:600;&quot;&gt;bolded&lt;/span&gt; title) pane from the display.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="644"/>
        <source>Remove the selected (bolded title) pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="647"/>
        <source>Ctrl+Shift+W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="656"/>
        <source>Pane &amp;Left/Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="659"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Moves the focused pane before its siblings at the same level (left, or up).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="662"/>
        <source>Move the selected (bolded title) pane left or up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="665"/>
        <source>Ctrl+Shift+PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="674"/>
        <source>Pane &amp;Right/Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="677"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Moves the focused pane after its siblings at the same level (right, or down).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="680"/>
        <source>Move the selected (bolded title) pane right or down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="683"/>
        <source>Ctrl+Shift+PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="692"/>
        <source>En&amp;large Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt;&quot;&gt;Increase &lt;/span&gt;the &lt;span style=&quot; font-size:12pt;&quot;&gt;font &lt;/span&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;size.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="698"/>
        <source>Enlarge the font size by 10%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="701"/>
        <source>Ctrl+=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="710"/>
        <source>Shrink &amp;Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="713"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Decrease &lt;/span&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;the &lt;/span&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;font &lt;/span&gt;size.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="716"/>
        <source>Shrink the font size by 10%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="719"/>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="728"/>
        <source>Balance S&amp;iblings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="731"/>
        <source>Balance Siblings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="734"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Balance all sibings of this pane (panes in the same parent group). This will make them the same size, as far as is possible given size constraints.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="737"/>
        <source>Balance siblings of focused pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="746"/>
        <source>Split &amp;Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="749"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Split current pane horizontally.  The new pane can be replaced or moved.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="752"/>
        <source>Split current pane horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="761"/>
        <source>Split &amp;Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="764"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Split current pane vertically. The new pane can be replaced or moved.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="767"/>
        <source>Split current pane vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="776"/>
        <source>&amp;Revert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="779"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Revert settings from the last saved or loaded file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="782"/>
        <source>Revert settings from last save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="785"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="794"/>
        <source>Reset &amp;UI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="797"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset the UI configuration to the default.  &lt;span style=&quot; font-weight:600;&quot;&gt;Any UI customization such as pane positions, etc, will be lost.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="800"/>
        <source>Reset session to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="809"/>
        <source>Quit &amp;without Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="812"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exit program without saving the session.  Use this if you have made changes to the configuration which you don&apos;t want to preserve.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="815"/>
        <source>Exit program without saving session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="818"/>
        <source>Ctrl+Shift+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="827"/>
        <source>Reset F&amp;ont</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="830"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Resets the UI font size to the system default.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="833"/>
        <source>Reset to system font.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="836"/>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="845"/>
        <source>&amp;Import from File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="848"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import GPS track(s) from a selected file. Support formats include:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GPX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TCX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;FIT&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;KML&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;This will not clear any existing GPS tracks, but rather, append to the current set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="851"/>
        <source>Import GPS tracks from a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="854"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="863"/>
        <source>Pane Group Left/&amp;Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="866"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Moves the horizontal or vertical pane group containing the focused pane before its siblings at the same level (left, or up).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="869"/>
        <source>Move a group (row or column) of panes left or up.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="878"/>
        <source>Pane Group Right/&amp;Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="881"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Moves the horizontal or vertical pane group containing the focused pane after its siblings at the same level (right, or down).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="884"/>
        <source>Move a group (row or column) of panes right or down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="893"/>
        <source>&amp;Export Track(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="896"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exports selected track(s) to a file. If no tracks are selected, every track in the active (&lt;span style=&quot; font-weight:600;&quot;&gt;bolded&lt;/span&gt;) pane be exported. Supported formats include:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GPX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TCX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;KML&lt;/li&gt;&lt;/ul&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="899"/>
        <source>Export selected tracks to a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="902"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="911"/>
        <source>&amp;Zoom To Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="914"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom &lt;span style=&quot; font-weight:600;&quot;&gt;Map Pane&lt;/span&gt; to current selection from &lt;span style=&quot; font-weight:600;&quot;&gt;Track Panes&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;Leg Panes&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;If multiple rows are selected, the view will zoom to encompass all of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="917"/>
        <source>Zoom the map to the selected tracks or points.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="920"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="929"/>
        <source>&amp;Delete Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="932"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will delete the selected tracks in a &lt;span style=&quot; font-weight:600;&quot;&gt;GPS Track Pane&lt;/span&gt;, or points in a &lt;span style=&quot; font-weight:600;&quot;&gt;Points Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="935"/>
        <source>Delete the selected tracks or points.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="938"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="947"/>
        <source>&amp;Select Person (power est.)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="950"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a person to use for power estimations. The person selected will be used for all power and energy related estimations for all tracks.&lt;/p&gt;&lt;p&gt;The set of people and their data can be configured under &lt;span style=&quot; font-weight:600;&quot;&gt;Settings/Configure...&lt;/span&gt; under the &lt;span style=&quot; font-weight:600;&quot;&gt;People&lt;/span&gt; tab.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="953"/>
        <source>Select person for power and energy estimation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="962"/>
        <source>&amp;Show Tutorial...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="965"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show tutorial window.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="968"/>
        <source>Show program documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="977"/>
        <source>Import from &amp;Device...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="980"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Import GPS tracks from a device connected to via USB. Support formats include:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GPX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;TCX&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;FIT&lt;/li&gt;&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;KML&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;This will not clear any existing GPS tracks, but rather, append to the current set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="983"/>
        <source>Import GPS data from device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="986"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="995"/>
        <source>New &amp;Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="998"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create new UI window.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1001"/>
        <source>Create new UI window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1013"/>
        <source>&amp;Offline Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1016"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Run in offline mode: no map data will be downloaded from the network.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1019"/>
        <source>Run in offline mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1028"/>
        <source>&amp;Reload Visible Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1031"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reload the visible area of any displayed map regions from their online sources.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1034"/>
        <source>Reload visible map area from sources.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1037"/>
        <source>Ctrl+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1046"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1049"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Undo last change.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1052"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1055"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1064"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1067"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Redo last undone change.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1070"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1073"/>
        <source>Ctrl+Shift+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1082"/>
        <source>Undo &amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1085"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to previous map pane view.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1088"/>
        <source>Undo view position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1091"/>
        <source>[</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1100"/>
        <source>R&amp;edo View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move to next map pane view.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1106"/>
        <source>Redo view position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1109"/>
        <source>]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1118"/>
        <source>C&amp;lear Undo Stack</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear the contents of the undo/redo stacks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1124"/>
        <source>Clear undo history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1133"/>
        <source>&amp;Merge Selection...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Merge multiple tracks or other selections into a single item.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1139"/>
        <source>Merge selected items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1148"/>
        <source>Du&amp;plicate Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Duplicate tracks or other selected items.&lt;/p&gt;&lt;p&gt;Duplicates of tracks can later be found using &lt;span style=&quot; font-weight:600;&quot;&gt;Select All Duplicates&lt;/span&gt;, provided they have not been modified (in which case they are no longer duplicates).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1154"/>
        <source>Duplicate tracks or selected items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1163"/>
        <source>S&amp;implify Selection...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Simplify tracks or selected points by removing points close to others in distance or time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1169"/>
        <source>Simplify tracks or points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1178"/>
        <source>&amp;Live GPSD Capture...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1181"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Capture live data from a GPSD connected device.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1184"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1193"/>
        <source>Do&amp;wnload Region...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1196"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Download Region&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1199"/>
        <source>Ctrl+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1208"/>
        <source>Supported &amp;GPSD Versions...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1223"/>
        <source>Pa&amp;ne Dialog...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1241"/>
        <source>Map M&amp;ove Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1244"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move (pan) the map without adding or editing points.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1247"/>
        <source>Shift+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1259"/>
        <source>Select Poi&amp;nts Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1292"/>
        <source>Create Ne&amp;w Track...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1304"/>
        <source>Goto Lat/Lon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1307"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Center &lt;span style=&quot; font-weight:600;&quot;&gt;Map Pane&lt;/span&gt; on a provided latitude and longitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1310"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1319"/>
        <source>Visit ZombieTrackerGPS Web Site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1322"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the ZombieTrackerGPS Web Site in the default external web browser.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1330"/>
        <source>Auto Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1333"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Manually trigger auto-import process using parameters defined in the &amp;quot;Auto Import&amp;quot; section of the program configuration.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1336"/>
        <source>Alt+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1295"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Creates a new, empty track which can be used with &lt;span style=&quot; font-weight:600;&quot;&gt;Add Points Mode&lt;/span&gt; to create a track by clicking on the map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1262"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select points by clicking on the map, and drag selected points with the mouse.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="281"/>
        <source>Map Mode Tool Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1265"/>
        <source>Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1277"/>
        <source>&amp;Add Points Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1280"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add new track points by clicking on the map. Points are added after the selected point, or to the end of the track if no points are selected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1283"/>
        <source>Shift+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1211"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show supported versions of the GPSD library.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="87"/>
        <source>Settin&amp;gs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1214"/>
        <source>Show supported GPSD library versions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1226"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the &lt;span style=&quot; font-weight:600;&quot;&gt;New Pane Dialog&lt;/span&gt;, a graphical way to control UI panes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mainwindow.ui" line="1229"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="95"/>
        <source>Configure Status Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="166"/>
        <source>Loading geopolitical data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="228"/>
        <source>Creating default interface...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="487"/>
        <location filename="../src/ui/windows/mainwindow.cpp" line="490"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="490"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="511"/>
        <source>Online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="512"/>
        <source>Map Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="513"/>
        <source>Live Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="514"/>
        <source>Total Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="515"/>
        <source>Visible Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="516"/>
        <source>Selected Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="517"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="518"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="519"/>
        <source>Ascent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="520"/>
        <source>Descent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="521"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="522"/>
        <source>Geo-Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="523"/>
        <source>Geo ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="524"/>
        <source>Region Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="532"/>
        <source>Show </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="570"/>
        <source>Loading default icons...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="760"/>
        <source>Import Sample Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="761"/>
        <source>This is the first time </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="762"/>
        <source> has been run with this profile.  You can import some sample data to experiment with, or start with a clean slate.  Would you like to import the sample data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1540"/>
        <source>Map move mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1541"/>
        <source>Add points mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1542"/>
        <source>Select points mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1569"/>
        <source>AutoImport directory does not exist: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1574"/>
        <source>AutoImport directory is not readable: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1598"/>
        <source>AutoImport backup directory does not exist: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1606"/>
        <source>AutoImport backup dir cannot be created: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1612"/>
        <source>No permissions for AutoImport backup directory: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1628"/>
        <source>AutoImport disabled in configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1679"/>
        <source>Unable to delete: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1763"/>
        <source>AutoImport external command timed out, and unable to kill process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2372"/>
        <source>AutoImport command already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1586"/>
        <source>AutoImport backup suffix is empty: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1591"/>
        <source>AutoImport directory is not writable: Import canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1704"/>
        <source>No files found for AutoImport.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1654"/>
        <source>Unable to rename: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2332"/>
        <source>Create new track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2339"/>
        <source>Created Track: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2359"/>
        <source>Unable to open web site in external browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="745"/>
        <source>Unable to import sample data.  Please verify the program installation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="745"/>
        <source>Import Sample Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1100"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1112"/>
        <source>%p%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1122"/>
        <source>ERROR: %d task(s) failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1303"/>
        <source>Restoring UI configuration...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1308"/>
        <source>Loading saved data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1446"/>
        <source>Unable to create backup file: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1446"/>
        <source>Backup Save Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1452"/>
        <source>Unable to create save directory: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1452"/>
        <source>Save Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1457"/>
        <source>Save Data Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1736"/>
        <source>AutoImport external command started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1743"/>
        <source>AutoImport external command error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1765"/>
        <source>AutoImport external command timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1774"/>
        <source>AutoImport external process failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1791"/>
        <source>Failed to parse Auto Import Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1796"/>
        <source>Auto Import Command empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1992"/>
        <source>Import File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2020"/>
        <source> Skipped </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2021"/>
        <source> duplicate tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2023"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2024"/>
        <source> duplicate waypoints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2158"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2159"/>
        <source>You are about to remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2160"/>
        <source>.  Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1922"/>
        <source>Reset UI Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1922"/>
        <source>Reset UI configuration to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1927"/>
        <source>UI configuration reset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1950"/>
        <source>Track export completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1953"/>
        <source>Export Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1955"/>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1985"/>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2199"/>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2351"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2019"/>
        <source>Track import completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1963"/>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1973"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2211"/>
        <source>New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2246"/>
        <source>Undone: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="2253"/>
        <source>Redone: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapDataPane</name>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="30"/>
        <source>Edit </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="30"/>
        <source> Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="31"/>
        <source>Select </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="31"/>
        <source> Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="32"/>
        <source>Select Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mapdatapane.cpp" line="95"/>
        <source>Selected </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapDownloadDialog</name>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="14"/>
        <source>Batch Download Map Tiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="17"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This dialog allows batch downloading of map tiles for offline use.&lt;/p&gt;&lt;p&gt;Please be courteous to online services and do not fetch more than necessary.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="23"/>
        <source>Download Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="31"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Download the visible region.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="34"/>
        <source>&amp;Visible Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="57"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Download the specified area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="60"/>
        <source>Specified Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="98"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Degrees north latitude for the download area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="120"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="143"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Degrees west longitude for the download area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="165"/>
        <source>W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="218"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Degrees east longitude for the download area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="240"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="263"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Degrees south latitude for the download area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="285"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="325"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Maximum tile zoom to download.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="328"/>
        <source>Download Zoom Levels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="334"/>
        <source>Tile Level Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="341"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Minimum tile level to download.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="354"/>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="361"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Maximum tile level to download.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="374"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the currently visible tile level to the level range.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="419"/>
        <source>Tiles to Download:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mapdownloaddialog.ui" line="426"/>
        <source>0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="181"/>
        <source>Tile count error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="182"/>
        <source>Please download fewer tiles.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="187"/>
        <source>Tile count warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="188"/>
        <source>Downloading many tiles creates a high load on the service. Do you wish to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="209"/>
        <source>Excessive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="409"/>
        <source>Map download error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/mapdownloaddialog.cpp" line="410"/>
        <source>No map pane found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapPane</name>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="17"/>
        <source>Map Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="20"/>
        <source>&amp;Map Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="31"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Type here to search for geographic names. The controls to the right allow:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Region based selection for the whole world, the visible map region, the country or state near the map center, or a fixed distance from the map center.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Filtering by feature type. For example, only city names, or only the names of mountains.&lt;/li&gt;&lt;/ul&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="41"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Restrict map searches to the given area:&lt;/p&gt;&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;World&lt;/span&gt; - Return matches from anywhere in the world.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Visible Area&lt;/span&gt; - Return matches from within the visible area of the map on the screen. This is approximate.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Distance&lt;/span&gt; - Return matches that lie within a given distance from the center of the map display. The distance can be configured using the &lt;span style=&quot; font-style:italic;&quot;&gt;Distance&lt;/span&gt; box to the right.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Country&lt;/span&gt; - Return matches in the same country as the screen center.&lt;/li&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Region&lt;/span&gt; - Return matches in the same sub-country region (state, province, prefecture, oblast, etc) as the screen center.  This mode is only available in some countries.  Where not available, it will fall back to &lt;span style=&quot; font-weight:600;&quot;&gt;Country&lt;/span&gt; mode.&lt;/li&gt;&lt;/ul&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="45"/>
        <source>World</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="50"/>
        <source>Visible Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="55"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="60"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="65"/>
        <source>Region</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Restrict map searches to the selected feature categories.&lt;/p&gt;&lt;p&gt;The context menu allows setting all, none, or toggling the selection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="80"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the map theme to use for this view.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="83"/>
        <source>Select the map theme to use for this view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="86"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="106"/>
        <source>Show Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="109"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display or hide the overview map.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="121"/>
        <source>Show Scale Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="124"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display or hide the scale bar&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="136"/>
        <source>Show Compass</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="139"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display or hide the compass&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="148"/>
        <source>Orient to North</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Orient map to North&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="160"/>
        <source>Location to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Copy right-click location to clipboard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="172"/>
        <source>Add View Preset...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="175"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create preset for current map view.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="184"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select all features.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="196"/>
        <source>Select None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="199"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unselect all features.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="208"/>
        <source>Toggle Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="211"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Invert feature selection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="220"/>
        <source>Default Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="223"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Restore the default features to search within.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="232"/>
        <source>Create Waypoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/mappane.ui" line="235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a new waypoint at the selected location.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="61"/>
        <source>Feature Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="163"/>
        <source>&lt;html&gt;&lt;body&gt;The maximum distance from the center of the view to search for matches.&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="197"/>
        <source>Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="376"/>
        <source>Showing: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="536"/>
        <source>Update View: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="540"/>
        <source>View preset updated: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="745"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/mappane.cpp" line="869"/>
        <source>Location not found: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapThemeDialog</name>
    <message>
        <location filename="../src/ui/forms/mapthemedialog.ui" line="14"/>
        <source>Map Themes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewTrackDialog</name>
    <message>
        <location filename="../src/ui/dialogs/newtrackdialog.cpp" line="76"/>
        <source>Track Override Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newtrackdialog.cpp" line="92"/>
        <source>Current Track: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newtrackdialog.cpp" line="111"/>
        <source>Error adding track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newtrackdialog.cpp" line="122"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="26"/>
        <source>Track Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="36"/>
        <source>Name for captured track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="43"/>
        <source>Override Tag Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="53"/>
        <source>Check to provide track specific color overrides.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="14"/>
        <source>New GPS Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="20"/>
        <source>New Track Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="33"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This name will be applied to the new track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="50"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the provided color will be assigned to the new track. This color will override the color provided by the tags assigned to the track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="66"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If &amp;quot;Override Tag Colors&amp;quot; is checked, this color will be assigned to the new track, and overide any color from the tags assigned to the track.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="69"/>
        <source>Track specific override color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The tags assigned to the left side group will be applied to the captured track.&lt;/p&gt;&lt;p&gt;Some formats such as FIT will attempt to find tags to match the &amp;quot;sport&amp;quot; information from the file. This is an attempt to be helpful, not a guarantee. It may fail if tags have been renamed or removed. Tags assigned manually will override any auto-discovered tags, so you should leave the left side blank if you wish to use this feature.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="98"/>
        <source>Assign tags to tracks on import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newtrackdialog.ui" line="101"/>
        <source>Assign Tags to New Track</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewViewDialog</name>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="14"/>
        <location filename="../src/ui/forms/newviewdialog.ui" line="20"/>
        <source>New View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The selected flag will be automatically attached to the view.  This can be changed or removed at any time using the context menu on an item in the &lt;span style=&quot; font-weight:600;&quot;&gt;View Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="29"/>
        <source>Attach flag to view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If selected, no icon will be attached to the view.  An icon can be added later using the context menu in the &lt;span style=&quot; font-weight:600;&quot;&gt;View Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="38"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="58"/>
        <source>Select...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="71"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="96"/>
        <source>View Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newviewdialog.ui" line="103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The name for the new view. This will appear in &lt;span style=&quot; font-weight:600;&quot;&gt;View Panes&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newviewdialog.cpp" line="47"/>
        <source>My Map View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newviewdialog.cpp" line="53"/>
        <source>Add View Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newviewdialog.cpp" line="151"/>
        <source>Add View: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newviewdialog.cpp" line="155"/>
        <source>Added view preset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newviewdialog.cpp" line="162"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewWaypointDialog</name>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="14"/>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="20"/>
        <source>Create New Waypoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The Symbol value for this waypoint. This value can be exported to standard GPX files, and will be used by other software.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="36"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The name for the new waypoint.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="39"/>
        <source>Name for Waypoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This icon will be displayed on &lt;span style=&quot; font-weight:600;&quot;&gt;Map Panes&lt;/span&gt; for this waypoint.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the icon will be set from the symbol value.&lt;/p&gt;&lt;p&gt;If unchecked, the icon can be selected manually from a much larger set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="70"/>
        <source>From Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optional waypont type, such as &amp;quot;Hiking Trail&amp;quot; or &amp;quot;Post Office&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="102"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button will refresh the waypoint name from the current position. Any prior edits will be lost.  This can be used if the position is edited below.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;n</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="116"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="123"/>
        <source>Lat/Lon/Ele</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="130"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="137"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="144"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="171"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Waypoint Latitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="190"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Waypoint Longitude.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="209"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Elevation for the waypoint.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/newwaypointdialog.ui" line="238"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button resets the latitude and longitude to the values present when the dialog was opened.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newwaypointdialog.cpp" line="233"/>
        <source>Confirm waypoint creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newwaypointdialog.cpp" line="234"/>
        <source>You are creating many waypoints at once.  Proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newwaypointdialog.cpp" line="239"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newwaypointdialog.cpp" line="249"/>
        <source>Interactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/newwaypointdialog.cpp" line="260"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PersonDialog</name>
    <message>
        <location filename="../src/ui/forms/persondialog.ui" line="14"/>
        <source>Select Active Pereson</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/persondialog.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the People section of the program configuration editor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/persondialog.ui" line="38"/>
        <source>Configure...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PointModel</name>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1123"/>
        <source>Selected Points: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1128"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1129"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1129"/>
        <source>Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1129"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1712"/>
        <source>Index of point within its track segment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1714"/>
        <source>Timestamp for point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1716"/>
        <source>Elapsed time within track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1718"/>
        <source>Longitude for point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1720"/>
        <source>Latitude for point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1722"/>
        <source>Elevation at point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1724"/>
        <source>Horizontal distance to the next point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1726"/>
        <source>Distance from track start to this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1728"/>
        <source>Vertical distance to the next point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1730"/>
        <source>Grade at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1732"/>
        <source>Duration of this leg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1734"/>
        <source>Temperature.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1736"/>
        <source>Depth.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1738"/>
        <source>Speed at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1740"/>
        <source>Acceleration at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1742"/>
        <source>Heart Rate at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1744"/>
        <source>Cadence at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1746"/>
        <source>Power at this point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1748"/>
        <source>Course to next point.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1750"/>
        <source>Bearing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1815"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1815"/>
        <source>Points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PointPane</name>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="14"/>
        <source>Track Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="17"/>
        <source>Trac&amp;k Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the filter query is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="42"/>
        <source>Filter validity indicator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the pane.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;458&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;53&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;389&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;br/&gt;!~&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; as a regular expression&lt;br/&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; matching a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;360&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;352&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Speed &amp;gt; 20 km/h&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Index &amp;gt; 100&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;436&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;428&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Elaspsed &amp;lt; 20 min &amp;amp; Ele &amp;gt; 1500 m&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;( Power &amp;gt; 50w | Speed &amp;gt; 20 kph ) &amp;amp; Ele &amp;gt; 1500 m&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="58"/>
        <location filename="../src/ui/forms/pointpane.ui" line="64"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Track Points&lt;/span&gt; pane shows data for each GPS data point in the track, such as the position, duration, and distance to the next point.&lt;/p&gt;&lt;p&gt;The set of data to be displayed can be adjusted with the &lt;span style=&quot; font-weight:600;&quot;&gt;Show Columns&lt;/span&gt; pulldown, or the context menus on the headers.&lt;/p&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Arrow Keys&lt;/span&gt; or &lt;span style=&quot; font-weight:600;&quot;&gt;Page Up/Down&lt;/span&gt; can be used to move through the list. The active point will be highlighted in the map display. If multiple points are selected, they selected part of the track will be highlighted in the map.&lt;/p&gt;&lt;p&gt;A GPS track is composed of one or more discontinuous segments, each of which can contain many sample points. Segments are displayed (if &lt;span style=&quot; font-weight:600;&quot;&gt;View as Tree&lt;/span&gt; is enabled) as &amp;quot;Seg 0&amp;quot;, &amp;quot;Seg 1&amp;quot;, etc. Segments can be merged by selecting them and using the &lt;span style=&quot; font-weight:600;&quot;&gt;Merge Segments&lt;/span&gt; context menu. Segments can be split by selecting a sample point marking the split point,and using the &lt;span style=&quot; font-weight:600;&quot;&gt;Split Segments&lt;/span&gt; context menu, or selecting a range of samples to be added to a new segment.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="117"/>
        <location filename="../src/ui/panes/pointpane.cpp" line="432"/>
        <source>Merge Segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="120"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will merge selected segments for the currently displayed track. If no segments are selected, all segments present in the track will be merged into a single segment.&lt;/p&gt;&lt;p&gt;A GPS track is composed of multiple segments, each of which can hold many sample points. These segments are displayed in the &lt;span style=&quot; font-weight:600;&quot;&gt;Track Points&lt;/span&gt; pane and marked &amp;quot;Seg 0&amp;quot;, &amp;quot;Seg 1&amp;quot;, etc.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="123"/>
        <source>Merge multiple track segments into single segment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="132"/>
        <source>Split Segment(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will split segments at the currently selected points. The currently selected point will become the first point in the new segment.  You may select multiple individual points, or mark a range to extract that range into a new segment of its own.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="138"/>
        <source>Split segment at selected point, or move selected range of points into new segment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="147"/>
        <source>Create Waypoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/pointpane.ui" line="150"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Creates a new waypoint at the selected point(s).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pointpane.cpp" line="373"/>
        <source>Deleted </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pointpane.cpp" line="374"/>
        <source> segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pointpane.cpp" line="375"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pointpane.cpp" line="376"/>
        <source> points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pointpane.cpp" line="441"/>
        <source>Split Segments</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="64"/>
        <location filename="../src/core/personmodel.cpp" line="103"/>
        <location filename="../src/core/tagmodel.cpp" line="191"/>
        <location filename="../src/core/trackmodel.cpp" line="306"/>
        <location filename="../src/core/viewmodel.cpp" line="103"/>
        <location filename="../src/core/waypointmodel.cpp" line="137"/>
        <location filename="../src/core/zonemodel.cpp" line="74"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/filtermodel.cpp" line="65"/>
        <source>Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="75"/>
        <location filename="../src/core/geolocheader.cpp" line="86"/>
        <location filename="../src/core/geolocheader.cpp" line="109"/>
        <location filename="../src/core/geolocheader.cpp" line="120"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="76"/>
        <source>Regions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="77"/>
        <source>Cities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="78"/>
        <location filename="../src/core/geolocheader.cpp" line="112"/>
        <source>Parks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="79"/>
        <source>Forests</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="80"/>
        <source>Mountains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="81"/>
        <source>Water</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="82"/>
        <source>Undersea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="110"/>
        <source>Countries, states, provinces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="111"/>
        <source>Cities, towns, villiages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="113"/>
        <source>Forests, bogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="114"/>
        <source>Mountains, hills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="115"/>
        <source>Lakes, ponds, rivers, streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/geolocheader.cpp" line="116"/>
        <source>Reefs, undersea trenches, etc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="104"/>
        <location filename="../src/core/tagmodel.cpp" line="195"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="105"/>
        <source>Bio Efficiency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="106"/>
        <source>Birthdate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="107"/>
        <location filename="../src/core/trackmodel.cpp" line="350"/>
        <source>Max HR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="108"/>
        <source>FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="131"/>
        <source>&lt;i&gt;&lt;/i&gt;Person name (athlete, passenger).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="132"/>
        <source>&lt;i&gt;&lt;/i&gt;Weight, used for power estimation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="133"/>
        <source>&lt;i&gt;&lt;/i&gt;Biomechanical efficiency, used for calorie estimation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="134"/>
        <source>&lt;i&gt;&lt;/i&gt;Birth date, used for maximum heart rate and other age related factors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="135"/>
        <source>&lt;i&gt;&lt;/i&gt;Maximum heart rate in BPM for this individual. If unset, will estimate as 220 - age. Set to 0 to unset.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/personmodel.cpp" line="136"/>
        <source>&lt;i&gt;&lt;/i&gt;Functional threshold power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1238"/>
        <source>Seg %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1661"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1662"/>
        <location filename="../src/core/waypointmodel.cpp" line="143"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1663"/>
        <source>Elapsed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1664"/>
        <location filename="../src/core/waypointmodel.cpp" line="145"/>
        <source>Longitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1665"/>
        <location filename="../src/core/waypointmodel.cpp" line="144"/>
        <source>Latitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1666"/>
        <location filename="../src/core/waypointmodel.cpp" line="146"/>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="134"/>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1667"/>
        <location filename="../src/core/trackmodel.cpp" line="314"/>
        <source>Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1668"/>
        <source>Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="142"/>
        <location filename="../src/core/pointmodel.cpp" line="1669"/>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1670"/>
        <source>Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1671"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1672"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1673"/>
        <source>Depth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1674"/>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1675"/>
        <source>Accel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1676"/>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="142"/>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1677"/>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="139"/>
        <source>Cadence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1678"/>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="140"/>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1679"/>
        <source>Course</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/pointmodel.cpp" line="1680"/>
        <source>Bearing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="192"/>
        <location filename="../src/core/trackmodel.cpp" line="309"/>
        <location filename="../src/core/zonemodel.cpp" line="75"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="193"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="194"/>
        <source>CdA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="196"/>
        <source>Roll Resistance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="197"/>
        <source>Thermal Efficiency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="198"/>
        <source>% Bio-power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="199"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="200"/>
        <source>Speed Unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="222"/>
        <source>This is used for power estimation.  The value can be unset by setting it to a negative value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="227"/>
        <source>&lt;i&gt;&lt;/i&gt;Icon displayed for tracks using this tag.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="228"/>
        <source>&lt;i&gt;&lt;/i&gt;Default color for tracks using this tag.  May be overridden on a per-track basis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="230"/>
        <source>&lt;i&gt;&lt;/i&gt;Name for this tag.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="231"/>
        <source>&lt;i&gt;&lt;/i&gt;Optional drag coefficient * frontal area in m^2. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="233"/>
        <source>&lt;i&gt;&lt;/i&gt;Optional vehicle weight. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="234"/>
        <source>&lt;i&gt;&lt;/i&gt;Optional vehicle rolling resistance. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="236"/>
        <source>&lt;i&gt;&lt;/i&gt;Optional vehicle thermal efficiency, accounting for drivetrain and engine losses, etc. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="238"/>
        <source>&lt;i&gt;&lt;/i&gt;Optional percent of power that comes from human power. For a bicycle or hiking this will be 100%, and for an automobile or motorcycle it will be 0%.  An E-bike might be 10%. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="241"/>
        <source>&lt;i&gt;&lt;/i&gt;Vehicle medium. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/tagmodel.cpp" line="242"/>
        <source>&lt;i&gt;&lt;/i&gt;If set, override the default speed display unit for this tag.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackitem.cpp" line="206"/>
        <source>(track)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackitem.cpp" line="208"/>
        <source>(tag)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackitem.cpp" line="209"/>
        <source>(default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="138"/>
        <location filename="../src/core/trackmodel.cpp" line="307"/>
        <location filename="../src/core/waypointmodel.cpp" line="140"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="308"/>
        <location filename="../src/core/waypointmodel.cpp" line="138"/>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="310"/>
        <location filename="../src/core/waypointmodel.cpp" line="139"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="311"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="312"/>
        <location filename="../src/core/waypointmodel.cpp" line="142"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="313"/>
        <location filename="../src/core/waypointmodel.cpp" line="147"/>
        <source>Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="315"/>
        <source>Begin Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="316"/>
        <source>End Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="317"/>
        <source>Begin Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="318"/>
        <source>End Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="319"/>
        <source>Stopped Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="320"/>
        <source>Moving Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="321"/>
        <source>Total Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="322"/>
        <source>Min Elev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="323"/>
        <source>Avg Elev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="324"/>
        <source>Max Elev</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="325"/>
        <source>Min Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="326"/>
        <source>Overall Spd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="327"/>
        <source>Moving Spd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="328"/>
        <source>Max Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="329"/>
        <source>Min Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="359"/>
        <source>BeginToEndEle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="139"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="140"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/climbmodel.cpp" line="141"/>
        <source>Steep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="330"/>
        <source>Avg Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="331"/>
        <source>Max Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="332"/>
        <source>Min Cad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="333"/>
        <source>Moving Cad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="334"/>
        <source>Max Cad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="335"/>
        <source>Min Pow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="336"/>
        <source>Moving Pow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="337"/>
        <source>Max Pow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="338"/>
        <source>Energy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="339"/>
        <source>Ascent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="340"/>
        <source>Descent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="341"/>
        <source>Peak-Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="342"/>
        <source>Segs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="343"/>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="130"/>
        <source>Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="344"/>
        <source>Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="345"/>
        <source>Min T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="346"/>
        <source>Avg T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="347"/>
        <source>Max T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="348"/>
        <source>Min HR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="349"/>
        <source>Avg HR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="356"/>
        <source>Min HR %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="357"/>
        <source>Avg HR %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="358"/>
        <source>Max HR %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="351"/>
        <source>Lap Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="352"/>
        <source>Min Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="353"/>
        <source>Min Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="354"/>
        <source>Max Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="355"/>
        <source>Max Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="104"/>
        <source>Lat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="105"/>
        <source>Lon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="106"/>
        <source>Heading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="107"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="223"/>
        <source>Make</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="224"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="225"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="226"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/dev-io/gpsmodel.cpp" line="227"/>
        <source>Mount Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoiofit.cpp" line="804"/>
        <source>FIT export only supports one track per file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoiokml.cpp" line="106"/>
        <source>Array size mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoiokml.cpp" line="132"/>
        <source>Bad coordinate element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="89"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="90"/>
        <source>Required GPSD version not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="91"/>
        <source>Partial data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="92"/>
        <source>No GPSD found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="93"/>
        <source>Error opening GPSD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="94"/>
        <source>No GPSD device found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="95"/>
        <source>Read error from device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="96"/>
        <source>Not running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="97"/>
        <source>Already running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="98"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="99"/>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="100"/>
        <source>Acquiring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="101"/>
        <source>Running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gpsd/gpswrap.cpp" line="105"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="127"/>
        <source>Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="128"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="129"/>
        <source>Geography</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="131"/>
        <source>Extent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="132"/>
        <source>Times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="133"/>
        <source>Speeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="135"/>
        <source>High/Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="136"/>
        <source>Grades</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="137"/>
        <source>Climb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="138"/>
        <source>Exercise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="141"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/filters/trackdetailfilter.cpp" line="143"/>
        <location filename="../src/ui/panes/pane.cpp" line="83"/>
        <location filename="../src/ui/panes/pane.cpp" line="165"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="62"/>
        <source>Empty Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="63"/>
        <source>Map Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="64"/>
        <source>GPS Track Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="65"/>
        <source>View Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="66"/>
        <source>GPS Track List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="67"/>
        <source>GPS Track Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="68"/>
        <source>GPS Track Points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="69"/>
        <source>Single Track Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="70"/>
        <source>Multi Track Bar Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="71"/>
        <source>GPS Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="72"/>
        <source>GPS Capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="73"/>
        <source>Waypoint List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="74"/>
        <source>Activity Summary Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="75"/>
        <source>Climb and Descent Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="76"/>
        <source>Simple View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="77"/>
        <source>Training zone Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="78"/>
        <source>Pane Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="144"/>
        <source>Displays nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="145"/>
        <source>Primary map display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="146"/>
        <source>Query filters for GPS tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="147"/>
        <source>Map view presets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="148"/>
        <source>Display list of GPS tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="149"/>
        <source>Display detailed information about a single GPS track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="150"/>
        <source>Display data about GPS track sample points.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="151"/>
        <source>Line chart for data from a single GPS track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="152"/>
        <source>Bar chart to compare data over multiple GPS tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="153"/>
        <source>Show USB connected GPS devices for data import..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="154"/>
        <source>Live data capture from GPS daemon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="155"/>
        <source>GPS waypoint data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="156"/>
        <source>Overview of activity across weeks, months, or years.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="157"/>
        <source>Display information about climbs and descents in a GPS track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="158"/>
        <source>Simple, customizable data display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="159"/>
        <source>Horizontal or vertical group of panes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/pane.cpp" line="160"/>
        <source>Display training zone analysis for one or more tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/mapdatamodel.cpp" line="39"/>
        <source>Trk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/mapdatamodel.cpp" line="40"/>
        <source>Rte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/mapdatamodel.cpp" line="41"/>
        <source>Wpt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/mapdatamodel.cpp" line="44"/>
        <source>Unk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="141"/>
        <source>Symbol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="70"/>
        <source>AutoImport Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/windows/mainwindow.cpp" line="1473"/>
        <source>Data load error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/geo-io/geoioxml.cpp" line="47"/>
        <source>No %1 element found in file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="73"/>
        <source>Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="76"/>
        <source>MaxHR%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="77"/>
        <source>FTP%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="78"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimplePane</name>
    <message>
        <location filename="../src/ui/forms/simplepane.ui" line="14"/>
        <source>Simple View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/simplepane.ui" line="17"/>
        <source>Simple &amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/simplepane.ui" line="36"/>
        <source>Configure Display...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/simplepane.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Configure the data presented in the pane using a rich text editor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/simplepane.cpp" line="191"/>
        <source>Canceled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagRenameDialog</name>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="14"/>
        <source>Tag Update Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="20"/>
        <source>Renamed Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="38"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are tags you have renamed which are in use by your GPS tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="48"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;img src=&quot;:art/points/Triangle/Dark/Ora10Blk00.svg&quot; width=&quot;20&quot; height=&quot;20&quot;/&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;NOTE: &lt;/span&gt;&lt;/p&gt;&lt;p&gt;You have renamed or removed tags in use by your GPS tracks. You may automatically&lt;br/&gt;apply these changes to the GPS tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="55"/>
        <source>Renaming Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="61"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will rename the tags in the tracks the same way you have renamed them in the config window.  If you don&apos;t know what to pick, this is probably the option you want.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="64"/>
        <source>Update (rename) tag names in tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="74"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will leave the track tag names unchanged.  This means they won&apos;t show up as icons any more, but you may add a new tag with that name later.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="77"/>
        <source>Leave e&amp;xisting tag names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="84"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will remove any tags from tracks if they have been renamed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="87"/>
        <source>Re&amp;move renamed tags from tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="97"/>
        <source>Removal Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will remove deleted tags from the tracks.  If you don&apos;t know what to pick, this is probably the option you want.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="106"/>
        <source>Remo&amp;ve Tag from Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="116"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will leave deleted tag names in the tracks.  They won&apos;t show up as icons any more, but you might want to add a new tag with the same name later on.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="119"/>
        <source>Leave Tag in Trac&amp;ks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="139"/>
        <source>Removed Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagrenamedialog.ui" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are tags you have removed which are in use by your GPS tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="14"/>
        <source>Select Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="48"/>
        <source>Active track tags.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="111"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="171"/>
        <source>Available Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="192"/>
        <source>Available tags to apply to track(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="27"/>
        <source>Active (applied) Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="45"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are tags assigned to individual tracks or waypoints. The tags applied to a track will control the default color of the line drawn for the track (unless overriden by a track color) and allow searching and selecting based on the tags assigned to the track(s) or waypoint(s).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="88"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move tag into the active set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="108"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move tag out of the active set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move selected tag up in the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move selected tag down in the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="189"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These are available tags (set up in the &lt;span style=&quot; font-weight:600;&quot;&gt;Settings/Configure...&lt;/span&gt; window) which can be dragged or move to the left to assign zero or more of them to individual tracks or waypoints. Tags can be assigned to many items at once by multi-selecting tracks or waypoints and double clicking.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="219"/>
        <location filename="../src/ui/forms/tagselector.ui" line="225"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="222"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move available tags to the active set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove tags from the active set, sending them back to the available tags.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="234"/>
        <location filename="../src/ui/forms/tagselector.ui" line="240"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="249"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="252"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move track tags up in the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="255"/>
        <source>Shift+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="264"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="267"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Move track tags down in the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tagselector.ui" line="270"/>
        <source>Shift+Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelectorDialog</name>
    <message>
        <location filename="../src/ui/forms/tagselectordialog.ui" line="14"/>
        <source>Select Tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackCmpPane</name>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="17"/>
        <source>Track Comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="20"/>
        <source>Trac&amp;k Comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="37"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the query string is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;458&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;as a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!~ &lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;354&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;344&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Length &amp;gt; 20 km&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags : Hike|Ski&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Begin_Date &amp;lt; 02-Mar-2017&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;524&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;514&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags =~ Hike &amp;amp; Moving_Time &amp;gt; 2 h&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;( Max_Grade &amp;gt; 10% | Max_Power &amp;gt; 200W ) &amp;amp; Name =~ Green&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="50"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="60"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lock query to a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;, so changes in the Track Pane&apos;s query text will update this chart.&lt;/p&gt;&lt;p&gt;If unlocked, the query text for this &lt;span style=&quot; font-weight:600;&quot;&gt;Track Comparison Pane&lt;/span&gt; will function independently.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="63"/>
        <source>Lock query to a Track Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="89"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select data to be compared between multi-selected GPS tracks from a &lt;span style=&quot; font-weight:600;&quot;&gt;Track Pane&lt;/span&gt;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="92"/>
        <source>Compared data between multi-selected GPS tracks from a Track Pane.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="99"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Change sort direction of the bar graph.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="102"/>
        <source>Change sort direction of the bar graph.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="151"/>
        <source>Show Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart horizontal and vertical axes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="157"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="169"/>
        <source>Show Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="172"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart legend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="175"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="187"/>
        <source>Animated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="190"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable or disable chart animations.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="199"/>
        <source>Set Bar Width...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="202"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the short dimension of the bar size, in pixels.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="214"/>
        <source>Bar Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="217"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enable to set value text in bars.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="220"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="229"/>
        <source>Page Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="232"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page Up&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="235"/>
        <source>PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="244"/>
        <source>Page Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="247"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page Down&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackcmppane.ui" line="250"/>
        <source>PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackcmppane.cpp" line="465"/>
        <source>Set Bar Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackcmppane.cpp" line="465"/>
        <source>Width (px)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackDetailPane</name>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="14"/>
        <source>Track Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="17"/>
        <source>GPS Trac&amp;k Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="28"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Type here to filter the detail items shown below. If any item matches, its parent headers are also expanded.&lt;/p&gt;&lt;p&gt;You can type a &lt;a href=&quot;http://perldoc.perl.org/perlre.html#Regular-Expressions&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Perl style&lt;/span&gt;&lt;/a&gt; regular expression here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="31"/>
        <source>Filter Regex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="41"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When unlocked, the detail view will lock its view to the track selected in the focused pane. As the active selection changes (via keyboard or mouse), the details will update to that item.&lt;/p&gt;&lt;p&gt;When locked, the detail view will display its current item and ignore selection changes elsewhere.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackdetailpane.ui" line="44"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackdetailpane.cpp" line="39"/>
        <source>Track Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackdetailpane.cpp" line="41"/>
        <source>Edit Track Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackdetailpane.cpp" line="43"/>
        <source>Select Track Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackdetailpane.cpp" line="45"/>
        <source>Selelect Track Flags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackLinePane</name>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="14"/>
        <source>Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="17"/>
        <source>&amp;Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="47"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pane chart left.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="50"/>
        <source>Pan chart left.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="53"/>
        <location filename="../src/ui/forms/tracklinepane.ui" line="79"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pane chart right.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="76"/>
        <source>Pan chart right.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="99"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom the chart in.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="116"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom the chart out.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="133"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide the chart axes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="153"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide the chart legend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="224"/>
        <source>Show Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="227"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart horizontal and vertical axes.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="230"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="242"/>
        <source>Show Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="245"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart legend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="248"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="257"/>
        <source>Reset Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="260"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reset zoom to default.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="263"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="272"/>
        <source>Zoom In X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="275"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom in on X axis.  Mouse wheel also zooms.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="278"/>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="287"/>
        <source>Zoom Out X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="290"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom out on X axis.  Mouse wheel also zooms.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="293"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="302"/>
        <source>Pan Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="305"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pan chart left.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="308"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="317"/>
        <source>Pan Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="320"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pan chart right.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="323"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="332"/>
        <source>Page Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="335"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Page left.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="338"/>
        <source>PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="347"/>
        <source>Page Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="350"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pan right by a page.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="353"/>
        <source>PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="362"/>
        <source>Zoom to Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="365"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zoom view to marked range.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="368"/>
        <source>Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="376"/>
        <source>Adjust Axes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="379"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adjusts axes to round numbers, such as ending at &amp;quot;15.0 km&amp;quot; rather than &amp;quot;13.29 km&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracklinepane.ui" line="382"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/tracklinepane.cpp" line="192"/>
        <source>Chart Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackMap</name>
    <message>
        <location filename="../src/ui/widgets/trackmap.cpp" line="237"/>
        <source>Move Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/widgets/trackmap.cpp" line="276"/>
        <source>Set Map Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/widgets/trackmap.cpp" line="701"/>
        <source>Interactive add point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/widgets/trackmap.cpp" line="806"/>
        <source>Interactive drag point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackModel</name>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="436"/>
        <source>Person Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="437"/>
        <source>Requires person data to be set in preferences, and a specific person selected (see the &lt;img src=&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="440"/>
        <source>&quot;/&gt; icon or &lt;b&gt;Edit/Select Person...&lt;/b&gt; menu).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="484"/>
        <source>Total track duration (last timestamp - first timestamp).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="453"/>
        <source>Descriptive track name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="455"/>
        <source>&lt;i&gt;&lt;/i&gt;Data type: Waypoint, Route, or Track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="457"/>
        <source>Tags applied to this track.  The master list of available tags may be edited from the Settings dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="460"/>
        <source>Track display color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="462"/>
        <source>User notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="464"/>
        <source>Track keywords.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="466"/>
        <source>Import source (device or file).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="468"/>
        <source>Flags of regions containing the track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="470"/>
        <source>Total track length.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="472"/>
        <source>Earliest datestamp, including date + time of day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="474"/>
        <source>Last datestamp, including date + time of day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="476"/>
        <source>Earliest timestamp, not including date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="478"/>
        <source>Last timestamp, not including date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="480"/>
        <source>Total stopped time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="482"/>
        <source>Total moving time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="486"/>
        <source>Min (lowest) elevation contained (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="488"/>
        <source>Average elevation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="490"/>
        <source>Max (highest) elevation contained (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="492"/>
        <source>Min (slowest) speed contained (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="494"/>
        <source>Average overall speed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="496"/>
        <source>Average moving speed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="498"/>
        <source>Max (fastest) speed contained (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="500"/>
        <source>Min grade, in percent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="502"/>
        <source>Average grade, in percent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="504"/>
        <source>Max grade, in percent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="506"/>
        <source>Min cadence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="508"/>
        <source>Average moving cadence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="510"/>
        <source>Max cadence.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="512"/>
        <source>Min power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="514"/>
        <source>Average moving power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="516"/>
        <source>Max power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="518"/>
        <source>Estimated total input energy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="520"/>
        <source>Total ascent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="522"/>
        <source>Total descent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="524"/>
        <source>Peak elevation - base elevation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="526"/>
        <source>Number of separate segments in the track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="528"/>
        <source>Number of sample points in the entire track.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="530"/>
        <source>Covered area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="532"/>
        <source>Min (lowest) temperature, if avaialble.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="534"/>
        <source>Average temperature, if available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="536"/>
        <source>Max (highest) temperature, if avaialble.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="538"/>
        <source>Min (lowest) heart rate, if available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="540"/>
        <source>Average heart rate, if available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="542"/>
        <source>Max (highest) heart rate, if available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="544"/>
        <source>Min (lowest) heart rate, as % of maximum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="546"/>
        <source>Average heart rate, as % of maximum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="548"/>
        <source>Max (highest) heart rate, as % of maximum.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="550"/>
        <source>Number of laps, if available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="552"/>
        <source>Min longitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="554"/>
        <source>Min latitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="556"/>
        <source>Max longitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="558"/>
        <source>Max latitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="679"/>
        <source>Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="679"/>
        <source>Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="810"/>
        <source>Trk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="811"/>
        <source>Rte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="812"/>
        <source>Wpt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/trackmodel.cpp" line="815"/>
        <source>Unk</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackPane</name>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="17"/>
        <source>Track List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="20"/>
        <source>GPS Trac&amp;k List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="42"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the filter query is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="45"/>
        <source>Filter validity indicator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; :&lt;br/&gt;!~&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; as a regular expression&lt;br/&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;354&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;344&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Length &amp;gt; 20 km&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags : Hike|Ski&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Begin_Date &amp;lt; 02-Mar-2017&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;524&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;514&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags =~ Hike &amp;amp; Moving_Time &amp;gt; 2 h&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;( Max_Grade &amp;gt; 10% | Max_Power &amp;gt; 200W ) &amp;amp; Name =~ Green&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="61"/>
        <location filename="../src/ui/forms/trackpane.ui" line="64"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="77"/>
        <source>Select person to use for power and energy estimation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="98"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;GPS Track List&lt;/span&gt; pane shows a set of GPS tracks, along with some data about each. The columns to be displayed can be controlled with the &amp;quot;Show Columns&amp;quot; pulldown (or using context menus over the column headers).&lt;/p&gt;&lt;p&gt;The set of tracks displayed can be filtered by typing into the filter line. Full regular expressions are supported. Filtering this list will also update the map view to display only those tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="133"/>
        <source>Reset Track Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If the track(s) color has been explicitly set, this will cause it to adopt the color defaults (e.g, from any tags it has, or the global default).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="139"/>
        <source>Reset any explicit track color; track will revert to Tag color, or global default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="148"/>
        <source>Reset Track Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If track notes has been explicitly set, this will clear them for the selected tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="154"/>
        <source>Remove any note attached to the selected track(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="163"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit track data, for editable columns under mouse cusor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="169"/>
        <source>Edit track column under cursor.  Only some are editable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="178"/>
        <source>Set Icon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="181"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selects an icon to show with the track name.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="184"/>
        <source>Set a custom icon in the name column of selected tracks.  (E.g, flags).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="193"/>
        <source>Change Tags...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="196"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Change the set of tags applied to selected track(s). This may also be done by double clicking the &lt;span style=&quot; font-weight:600;&quot;&gt;Tags&lt;/span&gt; column.&lt;/p&gt;&lt;p&gt;Multiple tracks can be assigned the same set of tags at once, by multi-selecting them and using this menu or double-clicking with the shift or control key held down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="199"/>
        <source>Add or remove tags from the selected track(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="208"/>
        <source>Select Duplicates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="211"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will select all but one in each set of duplicates of the currently selected tracks. Thus, it is safe to delete the newly selected items, if any, leaving one instance of each duplicate set.&lt;/p&gt;&lt;p&gt;See also: &lt;span style=&quot; font-style:italic;&quot;&gt;Select All Duplicates&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="214"/>
        <source>Select all but one dupliate of any currently selected tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="223"/>
        <source>Select All Duplicates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="226"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will select all but one in each set of duplicates of all visible tracks. Thus, it is safe to delete the newly selected items, if any, leaving one instance of each duplicate set.&lt;/p&gt;&lt;p&gt;See also: &lt;span style=&quot; font-style:italic;&quot;&gt;Select Duplicates&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="229"/>
        <source>Select all but one duplicate of all visible tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="238"/>
        <source>Create Filter from Query...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="241"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a new named filter which will store the current query to select tracks.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="250"/>
        <source>Unset Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/trackpane.ui" line="253"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove any track name icons (does not remove tags or tag icons).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackpane.cpp" line="47"/>
        <source>Track Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/trackpane.cpp" line="134"/>
        <source>No Filter Pane found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSimplifyDialog</name>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="14"/>
        <source>Simplify Track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="17"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Often tracks have more data points than you need, increasing storage requirements and processing overhead. For example, a track made while hiking does not normally require 1-second level sampling.&lt;/p&gt;&lt;p&gt;With this dialog you can remove points which are excessively close to each other in either distance or time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="20"/>
        <source>Simplify track by distance or time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="32"/>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="54"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Simplify tracks by distance. Points closer together in distance than the given value will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="35"/>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="57"/>
        <source>Simplify tracks by distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="60"/>
        <source>B&amp;y Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adaptive filtering: removes points which contribute less to the positional accuracy of the track, while keeping points that contribute more, such as around curves.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="70"/>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="119"/>
        <source>Use adaptive point filtering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="73"/>
        <source>Adapti&amp;ve Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="86"/>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Simplify tracks by time.  Points closer together in time than the given value will be removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="89"/>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="138"/>
        <source>Simplify tracks by time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="99"/>
        <source>h&apos;h&apos; mm&apos;m&apos; ss&apos;s&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="116"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adaptive filtering: removes points which contribute less to the positional accuracy of the track, while keeping points that contribute more, such as around curves.&lt;br/&gt;&lt;br/&gt;This mode requires timestamps on the track points.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/tracksimplifydialog.ui" line="141"/>
        <source>By Ti&amp;me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/tracksimplifydialog.cpp" line="227"/>
        <source>%1 points in %2 tracks %3 %4 points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/dialogs/tracksimplifydialog.cpp" line="243"/>
        <source>Simplify</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewModel</name>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="42"/>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="55"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="128"/>
        <source>Descriptive name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="130"/>
        <source>View center latitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="132"/>
        <source>View center longitude.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="134"/>
        <source>View heading in degrees.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="136"/>
        <source>View zoom level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="170"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/viewmodel.cpp" line="170"/>
        <source>Views</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewPane</name>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="17"/>
        <source>View Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="20"/>
        <source>&amp;View Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="52"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;358&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;53&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;389&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;:&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;as a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!~ &lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;360&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;352&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Name : Canada&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Lat &amp;gt; 45.0&lt;br/&gt;Tags !~ Internet&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;&lt;br/&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence.  Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;436&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;428&quot; style=&quot; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Name : Canada &amp;amp; Lat &amp;gt; 52.0&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="55"/>
        <location filename="../src/ui/forms/viewpane.ui" line="58"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="73"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is a set of view presets for the map. Double clicking on one (or using the &lt;span style=&quot; font-weight:600;&quot;&gt;Goto&lt;/span&gt; context menu) will set the last active &lt;span style=&quot; font-weight:600;&quot;&gt;Map Pane&lt;/span&gt; to the preset view. The current view can be added as a new view preset using the &lt;span style=&quot; font-weight:600;&quot;&gt;New&lt;/span&gt; menu, or an existing view can be updated using the &lt;span style=&quot; font-weight:600;&quot;&gt;Update&lt;/span&gt; menu.&lt;/p&gt;&lt;p&gt;View presets can be rearranged by drag and drop to form a tree (if the &lt;span style=&quot; font-weight:600;&quot;&gt;View/View as Tree&lt;/span&gt; menu is enabled).  This can be useful to represent geographic hierarchies, such as countries, states, and cities.&lt;/p&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;Set Icon...&lt;/span&gt; menu can be used to display an icon next to each preset.  A large set of country flags is available, as well as state and provincial flags for some countries.  External icons can be added from files, if the internal set is not adequate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="132"/>
        <source>Goto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="135"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Goto View&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="147"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the view preset&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Update view to current display&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="171"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create new view preset&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="144"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="156"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="168"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="180"/>
        <source>Set Icon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="183"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selects an icon to show with the view name.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="192"/>
        <source>Unset Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/viewpane.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unset any icon for selected views.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaypointModel</name>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="185"/>
        <source>Descriptive waypoint name..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="187"/>
        <source>Tags applied to this waypoint. The master list of available tags may be edited from the Settings dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="190"/>
        <source>User notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="192"/>
        <source>Waypoint type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="194"/>
        <source>Waypoint symbol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="196"/>
        <source>Import source (device or file).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="198"/>
        <source>Waypoint timestamp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="200"/>
        <source>Latitude of waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="202"/>
        <source>Longitude of waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="204"/>
        <source>Elevation of waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="206"/>
        <source>Flags of regions containing the waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="244"/>
        <source>Waypoint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/waypointmodel.cpp" line="244"/>
        <source>Waypoints</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WaypointPane</name>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="14"/>
        <source>Waypoint List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="17"/>
        <source>GPS Waypoint List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="39"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Indicates whether the filter query is valid.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="42"/>
        <source>Filter validity indicator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="55"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-size:15pt; font-weight:600;&quot;&gt;Filter Query Language&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;This text area is used to enter queries which filter the data shown in the panel. The simplest use is to type some text such as &amp;quot;Canyon&amp;quot; to show all entries containing the word &amp;quot;Canyon&amp;quot; in any column.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;A query language is supported for more sophisticated queries including Perl regular expressions, comparisons for numeric, alpha, and date data, boolean operators, and parenthetical grouping. Column names can use underscores in place of spaces.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To search a given column:&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;column_name op value [unit_suffix]&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;The operator must be delimited by spaces, to avoid conflicting with regular expressions. Non-regex comparisons will use numeric or date comparisons as appropriate. The unit suffix can be provided to specify measurement units. For example, &amp;quot;Length &amp;lt; 6 mi&amp;quot; is true for a display length of &amp;quot;8 km&amp;quot;.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;=~ &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;or&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; :&lt;br/&gt;!~&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Matches &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;value&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; as a regular expression&lt;br/&gt;Matches text &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; containing a regular expression&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;==&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Exact equality&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;!=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Not equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;lt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Less than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;gt;=&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;Greater than or equal&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;354&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;344&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Elevation &amp;gt; 500 m&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Tags : Hike&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;/&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; text-decoration: underline;&quot;&gt;To combine queries with boolean operators&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;You can combine multiple queries with the &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;!&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (negation),&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;&amp;amp; &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(and), &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;^&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt; (exclusive or) and &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;| &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;(or) operators. To avoid colliding with their use in regular expressions, they must be preceded or followed by a space. These are the operators in order of decreasing precedence. Parentheses can be used to override precedence.&lt;/span&gt;&lt;br/&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;456&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;63&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600;&quot;&gt;Op&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td width=&quot;375&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-weight:600; font-style:italic;&quot;&gt;Meaning&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;!&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;&amp;amp;&lt;br/&gt;^&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Logical negation&lt;br/&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;;&quot;&gt;And&lt;br/&gt;Exclusive Or&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;|&lt;/p&gt;&lt;/td&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;Inclusive Or&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic; text-decoration: underline;&quot;&gt;Examples:&lt;/span&gt;&lt;/p&gt;&lt;table border=&quot;0&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px;&quot; width=&quot;524&quot; cellspacing=&quot;0&quot; cellpadding=&quot;4&quot;&gt;&lt;tr&gt;&lt;td width=&quot;514&quot; style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Liberation Sans, sans-serif&apos;; font-style:italic;&quot;&gt;Type =~ Overlook &amp;amp; Elevation &amp;gt; 500m&lt;/span&gt;&lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td style=&quot; vertical-align:top; padding-left:0; padding-right:0; padding-top:0; padding-bottom:0;&quot;/&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="58"/>
        <location filename="../src/ui/forms/waypointpane.ui" line="61"/>
        <source>Filter Query (see tooltip for info)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="77"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The &lt;span style=&quot; font-weight:600;&quot;&gt;GPS Waypoint List&lt;/span&gt; pane shows a set of waypoints, along with some data about each. The columns to be displayed can be controlled with the &amp;quot;Show Columns&amp;quot; pulldown (or using context menus over the column headers).&lt;/p&gt;&lt;p&gt;The set of waypoints displayed can be filtered by typing into the filter line. Full regular expressions are supported. Filtering this list will also update the map view to display only those waypoints.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="118"/>
        <source>Reset Waypoint Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If waypoint notes has been explicitly set, this will clear them for the selected waypoints.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="124"/>
        <source>Remove any note attached to the selected waypoints(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="133"/>
        <source>Edit...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit waypoint data, for editable columns under mouse cusor.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="139"/>
        <source>Edit waypoint column under cursor.  Only some are editable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="148"/>
        <source>Set Icon...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="151"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Selects an icon to show on Map Panes with the waypoint name.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="154"/>
        <source>Set a custom icon to display for the waypoint.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="163"/>
        <source>Unset Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Remove any waypoint name icons (does not remove tags or tag icons).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="175"/>
        <source>Change Tags...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="178"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Change the set of tags applied to selected waypoints(s). This may also be done by double clicking the &lt;span style=&quot; font-weight:600;&quot;&gt;Tags&lt;/span&gt; column.&lt;/p&gt;&lt;p&gt;Multiple waypoints can be assigned the same set of tags at once, by multi-selecting them and using this menu or double-clicking with the shift or control key held down.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="181"/>
        <source>Add or remove tags from the selected waypoints(s).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="190"/>
        <source>Select Duplicates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="193"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will select all but one in each set of duplicates of the currently selected waypoints. Thus, it is safe to delete the newly selected items, if any, leaving one instance of each duplicate set.&lt;/p&gt;&lt;p&gt;See also: &lt;span style=&quot; font-style:italic;&quot;&gt;Select All Duplicates&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="196"/>
        <source>Select all but one dupliate of any currently selected waypoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="205"/>
        <source>Select All Duplicates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="208"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This will select all but one in each set of duplicates of all visible waypoints. Thus, it is safe to delete the newly selected items, if any, leaving one instance of each duplicate set.&lt;/p&gt;&lt;p&gt;See also: &lt;span style=&quot; font-style:italic;&quot;&gt;Select Duplicates&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="211"/>
        <source>Select all but one duplicate of all visible waypoints.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="220"/>
        <source>Guess Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="223"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Attempts to guess an appropriate icon from waypoint symbol and type information, if present.  This will not overwrite existing icons, but icons can be cleared before hand if desired.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="232"/>
        <source>New Waypoint...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/waypointpane.ui" line="235"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Create a new waypoint.  This may also be done with the &lt;span style=&quot; font-weight:600;&quot;&gt;Map Pane&apos;s&lt;/span&gt; context menu, or adding a waypoint at a given track point using the &lt;span style=&quot; font-weight:600;&quot;&gt;Point Pane&apos;s&lt;/span&gt; context menu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZoneModel</name>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="32"/>
        <source>3 Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="33"/>
        <location filename="../src/core/zonemodel.cpp" line="40"/>
        <location filename="../src/core/zonemodel.cpp" line="49"/>
        <source>Rest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="33"/>
        <location filename="../src/core/zonemodel.cpp" line="40"/>
        <location filename="../src/core/zonemodel.cpp" line="49"/>
        <source>Resting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="34"/>
        <source>Easy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="34"/>
        <source>Low Intensity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="35"/>
        <source>Moderate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="35"/>
        <source>Moderate intensity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="36"/>
        <source>Hard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="36"/>
        <source>High intensity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="39"/>
        <source>5 Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="41"/>
        <location filename="../src/core/zonemodel.cpp" line="50"/>
        <source>Recovery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="41"/>
        <source>Assists training recovery.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="42"/>
        <location filename="../src/core/zonemodel.cpp" line="51"/>
        <source>Endurance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="42"/>
        <source>Improve endurance and fat burning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="43"/>
        <source>Aerobic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="43"/>
        <source>Improve aerobic fitness.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="44"/>
        <source>Increase maximum aerobic capacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="44"/>
        <location filename="../src/core/zonemodel.cpp" line="54"/>
        <source>VO2 Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="45"/>
        <source>Max Perf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="45"/>
        <source>Develop maximum power and speed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="48"/>
        <source>7 Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="50"/>
        <source>Increase blood flow to muscles to assist recovery.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="51"/>
        <source>Improve fat metabolism and oxygen uptake.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="52"/>
        <source>Tempo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="52"/>
        <source>Improve carb metabolism, promote slow twitch fibres.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="53"/>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="53"/>
        <source>Improve carb metabolism, develop lactate threshold.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="54"/>
        <source>Develop VO2 max, improve anaerobic energy production.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="55"/>
        <source>Anaerobic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="55"/>
        <source>Develop VO2 max, improve anaerobic power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="56"/>
        <source>Neuromuscular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="56"/>
        <source>Increase maximum muscle power.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="99"/>
        <source>Zone ID.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="101"/>
        <source>Descriptive name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="103"/>
        <source>Chart color for this zone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="105"/>
        <source>Percent of maximum heart rate, lower bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="107"/>
        <source>Percent of Functional Threshold Power, lower bound.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="109"/>
        <source>Description of zone purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="168"/>
        <source>Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/zonemodel.cpp" line="168"/>
        <source>Zones</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZonePane</name>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="206"/>
        <source>: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="334"/>
        <source> (zone </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="362"/>
        <source>No person defined for training zone analysis.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="375"/>
        <source>Invalid track selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="383"/>
        <source>No valid BPM data available for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="388"/>
        <source>No valid FTP data available for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="391"/>
        <source>Invalid calculation method.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="468"/>
        <source>No selected tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="479"/>
        <source>No training zone data defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/panes/zonepane.cpp" line="534"/>
        <source>No training zone data in selected tracks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="14"/>
        <location filename="../src/ui/forms/zonepane.ui" line="17"/>
        <source>Training Zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="52"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the method used to analyze training zone data:&lt;/p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;b&gt;% Max HR&lt;/b&gt; - This method requires heart rate data, and cannot deduce neuromuscular effort past the maximum heart rate.&lt;/li&gt;&lt;li&gt;&lt;b&gt;% FTP&lt;/b&gt; - This method uses Functional Threshold Power, which must be set per person in the program configuration. In addition, power per point must be available in the track. The % FTP method can deduce neuromuscular effort past VO2Max.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="56"/>
        <source>% Max HR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="61"/>
        <source>% FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="144"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the Training Zone page in the program configurations.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="86"/>
        <location filename="../src/ui/forms/zonepane.ui" line="93"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="127"/>
        <source>Show Legend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="130"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Show or hide chart legend.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="133"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="141"/>
        <source>Open Configuration...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="155"/>
        <source>Show Legend Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ui/forms/zonepane.ui" line="158"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Toggle display of legend details. No effect if the legend is not displayed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
