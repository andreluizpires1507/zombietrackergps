/*
    Copyright 2020-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <sys/time.h> // for timespec
#include <cassert>
#include <QObject>

#include "gpswrap.h"
#include "gpswrapv6.h"
#include "gpswrapv7.h"
#include "gpswrapv8.h"
#include "gpswrapv9.h"
#include "gpswrapv10.h"
#include "gpswrapv11.h"
#include "gpswrapv14.h"

// This is the unix date epoch.
const QDateTime GpsWrap::epochDate = QDateTime::fromString("1970-01-01T00:00:00Z", Qt::ISODate);

GpsWrap::GpsWrap(const char* lib) :
    m_openRc(-1),
    m_libHandle(dlopen(lib, RTLD_NOW))
{
}

GpsWrap::~GpsWrap()
{
    close();
}

bool GpsWrap::hasLibrary()
{
    // API versions we can handle,
    return GpsWrapV14::hasLibrary() ||
           GpsWrapV11::hasLibrary() ||
           GpsWrapV10::hasLibrary() ||
           GpsWrapV9::hasLibrary()  ||
           GpsWrapV8::hasLibrary()  ||
           GpsWrapV7::hasLibrary()  ||
           GpsWrapV6::hasLibrary();
}

GpsWrap::supportedVersions_t GpsWrap::supportedVersions()
{
#define VERDATA(LIB) { LIB::libName, LIB::staticApiVer, LIB::hasLibrary() }
    return { VERDATA(GpsWrapV14),
             VERDATA(GpsWrapV11),
             VERDATA(GpsWrapV10),
             VERDATA(GpsWrapV9),
             VERDATA(GpsWrapV8),
             VERDATA(GpsWrapV7),
             VERDATA(GpsWrapV6) };
#undef VERDATA
}

GpsWrap* GpsWrap::open(const QString& hostname, int port, const QString& device)
{
#define TRYOPEN(LIB) if (LIB::hasLibrary()) return new LIB(hostname, port, device)
    // API versions we can handle, in order from most desirable version to least
    TRYOPEN(GpsWrapV14);
    TRYOPEN(GpsWrapV11);
    TRYOPEN(GpsWrapV10);
    TRYOPEN(GpsWrapV9);
    TRYOPEN(GpsWrapV8);
    TRYOPEN(GpsWrapV7);
    TRYOPEN(GpsWrapV6);
#undef TRYOPEN

    return nullptr;
}

QString GpsWrap::statusStr(Rc rc)
{
    switch (rc) {
    case Rc::Success:        return QObject::tr("Success");
    case Rc::BadVersion:     return QObject::tr("Required GPSD version not found.");
    case Rc::Partial:        return QObject::tr("Partial data");
    case Rc::NoGpsd:         return QObject::tr("No GPSD found");
    case Rc::GpsdError:      return QObject::tr("Error opening GPSD");
    case Rc::NoDevice:       return QObject::tr("No GPSD device found");
    case Rc::ReadError:      return QObject::tr("Read error from device");
    case Rc::NotRunning:     return QObject::tr("Not running");
    case Rc::AlreadyRunning: return QObject::tr("Already running");
    case Rc::Idle:           return QObject::tr("Idle");
    case Rc::Paused:         return QObject::tr("Paused");
    case Rc::Acquiring:      return QObject::tr("Acquiring");
    case Rc::Running:        return QObject::tr("Running");
    }

    assert(0);
    return QObject::tr("Unknown");
}

void GpsWrap::close()
{
    if (isLibOpen()) {
        dlclose(m_libHandle);  // Close the shared library
        m_libHandle = nullptr; // So foundLibrary() will return false
    }
}

bool GpsWrap::hasLibrary(const char* libName)
{
   if (void* handle = dlopen(libName, RTLD_LAZY); handle != nullptr) {
       dlclose(handle);
       return true;
   }

   return false;
}

bool GpsWrap::has(Set set) const
{
    return has(uint64_t(set));
}

QDateTime GpsWrap::time(const timespec& ts)
{
    return epochDate
            .addSecs(ts.tv_sec)
            .addMSecs(ts.tv_nsec / 1000000);
}

QDateTime GpsWrap::time(double time)
{
    return epochDate.addMSecs(time * 1000.0);
}
