/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Prevent compilation warning in includes/v6/gps.h
#define register

#include "includes/v6/gps.h"
#include "gpswrapv6.h"
#include "gpswrapprivate.h"

GPSWRAPPRIVATE(GpsWrapV6)
GPSWRAPCONSTRUCT(GpsWrapV6)
GPSWRAPFNS(GpsWrapV6)
