/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSDSTUBS_H
#define GPSDSTUBS_H

#include <sys/time.h>   // for struct timespec
#include <cstdint>
#include <array>

// Stubs to allow compilation without gps.h header

#  define GPSD_API_MAJOR_VERSION 9

#  define WATCH_ENABLE   0 // ...
#  define WATCH_DEVICE   0 // ...
#  define WATCH_JSON     0 // ...

#  define MODE_2D        0 // ...
#  define MODE_3D        0 // ...
#  define PACKET_SET     0 // ...
#  define TIME_SET       0 // ...
#  define LATLON_SET     0 // ...
#  define ALTITUDE_SET   0 // ...
#  define SPEED_SET      0 // ...
#  define TRACK_SET      0 // ...
#  define SATELLITE_SET  0 // ...
#  define NAVDATA_SET    0 // ...
#  define DEVICE_SET     0 // ...
#  define DEVICEID_SET   0 // ...
#  define DEVICELIST_SET 0 // ...
#  define VERSION_SET    0 // ...

   // dummy struct to allow compilation
   struct gps_data_t {
       uint64_t set;
       int satellites_used;
       struct {
           timespec time;
           int      mode;
           double   latitude, longitude, altMSL, speed, track;
       } fix;
       struct { double air_temp, water_temp, depth; } navdata;
       struct { std::array<char, 1> path; } dev;
   };

   using gps_mask_t = uint64_t;

   // Dummy decls to allow compilation without gps.h header
   extern int gps_open(const char*, const char*, struct gps_data_t*);
   extern int gps_close(struct gps_data_t*);
   extern int gps_read(struct gps_data_t*, char* message, int message_len);
   extern bool gps_waiting(const struct gps_data_t*, int uSec);
   extern int gps_stream(struct gps_data_t*, unsigned int, void*);
   extern const char *gps_errstr(int);

#endif // GPSDSTUBS_H
