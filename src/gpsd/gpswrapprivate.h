/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Boilerplate: these are used once we have the version-specific headers from GPSD included.
// This avoids a bunch of duplicate code when dealing with different versions of the GPSD headers.
// GPSD does not have ABI compatibility between versions, so we must link in all the versions we
// want to support.

#include <limits>
#include <cassert>

// ABI specific class declaration.  The definitions cannot live here; they must be in a c++,
// since we cannot include multiple gps.h files in the same compilation unit.
#define GPSWRAPDECL(CLASS, LIB, VER) \
    class CLASS final : public GpsWrap \
    { \
    public: \
        CLASS(const QString& hostname, int port, const QString& device); \
        ~CLASS() override; \
    private: \
        class GpsPrivate;                               \
        friend class GpsWrap;                           \
        \
        static const constexpr char* libName = LIB;     \
        static const constexpr int staticApiVer = VER;  \
        int apiVersion() const override { return VER; } \
        static bool hasLibrary() { return GpsWrap::hasLibrary(libName); } \
        Rc   read() override;                           \
        QDateTime time() const override;                \
        bool has(uint64_t set) const override;          \
        bool wait(int uSec) override;                   \
        double altMSL() const override;                 \
        double lat() const override;                    \
        double lon() const override;                    \
        double track() const override;                  \
        double speed() const override;                  \
        double air_temp() const override;               \
        double water_temp() const override;             \
        int satellites_used() const override;           \
        uint32_t fix_mode() const override;             \
        \
        GpsPrivate* m_Private;                          \
    };

// New gps_read parameters showed up in ABI >= 7
#if GPSD_API_MAJOR_VERSION >= 7
#   define EXTRA_GPSD_READ_PARAMS ,nullptr,0
#else
#   define EXTRA_GPSD_READ_PARAMS
#endif

// Altitude member changed in ABI >= 9
#if GPSD_API_MAJOR_VERSION >= 9
#   define GPS_ALT_MEMBER altMSL
#else
#   define GPS_ALT_MEMBER altitude
#endif

// ABI specific class definitions.  They must be in a c++,
// since we cannot include multiple gps.h files in the same compilation unit.
#define GPSWRAPFNS(CLASS) \
    bool CLASS::has(uint64_t set) const { return (m_Private->m_gpsData.set & set) == set; } \
    bool CLASS::wait(int uSec)          { return m_Private->gps_waiting(&m_Private->m_gpsData, uSec); } \
    double CLASS::lat() const           { return m_Private->m_gpsData.fix.latitude; } \
    double CLASS::lon() const           { return m_Private->m_gpsData.fix.longitude; } \
    double CLASS::track() const         { return m_Private->m_gpsData.fix.track; } \
    double CLASS::speed() const         { return m_Private->m_gpsData.fix.speed; } \
    double CLASS::air_temp() const      { return std::numeric_limits<double>::quiet_NaN(); } \
    double CLASS::water_temp() const    { return std::numeric_limits<double>::quiet_NaN(); } \
    int CLASS::satellites_used() const  { return m_Private->m_gpsData.satellites_used; } \
    uint32_t CLASS::fix_mode() const    { return m_Private->m_gpsData.fix.mode; } \
    QDateTime CLASS::time() const       { return GpsWrap::time(m_Private->m_gpsData.fix.time); } \
    double CLASS::altMSL() const        { return m_Private->m_gpsData.fix.GPS_ALT_MEMBER; } \
    GpsWrap::Rc CLASS::read() \
    { \
        if (m_Private->gps_read(&m_Private->m_gpsData EXTRA_GPSD_READ_PARAMS) == -1) \
            return Rc::ReadError; \
    \
        return Rc::Success; \
    }

// Wrapper around ABI specific function types
#define GPSWRAPPRIVATE(CLASS) \
    class CLASS::GpsPrivate \
    { \
    public: \
        GpsPrivate() { } \
        ~GpsPrivate() { if (m_openRc == 0) gps_close(&m_gpsData); } \
        int open(const char* hostname, const char* port) { \
            return m_openRc = gps_open(hostname, port, &m_gpsData); \
        } \
    \
        std::function<decltype(::gps_open)>    gps_open;    \
        std::function<decltype(::gps_close)>   gps_close;   \
        std::function<decltype(::gps_waiting)> gps_waiting; \
        std::function<decltype(::gps_read)>    gps_read;    \
        std::function<decltype(::gps_stream)>  gps_stream;  \
        std::function<decltype(::gps_errstr)>  gps_errstr;  \
        gps_data_t m_gpsData = { }; \
        int        m_openRc = -1; \
    };

// Constructors and destructors, and static value asserts.
#define GPSWRAPCONSTRUCT(CLASS) \
    CLASS::CLASS(const QString& hostname, int port, const QString& device) : \
        GpsWrap(libName), \
        m_Private(nullptr) \
    { \
        construct(m_Private, hostname, port, device); \
        static_assert(gps_mask_t(GpsWrap::Set::Time) == TIME_SET); \
        static_assert(gps_mask_t(GpsWrap::Set::LatLon) == LATLON_SET); \
        static_assert(gps_mask_t(GpsWrap::Set::Altitude) == ALTITUDE_SET); \
        static_assert(gps_mask_t(GpsWrap::Set::Speed) == SPEED_SET); \
        static_assert(gps_mask_t(GpsWrap::Set::NavData) == NAVDATA_SET); \
        static_assert(gps_mask_t(GpsWrap::Set::Packet) == PACKET_SET); \
        static_assert(uint32_t(GpsWrap::WatchDevice) == WATCH_DEVICE); \
        static_assert(uint32_t(GpsWrap::WatchEnable) == WATCH_ENABLE); \
        static_assert(uint32_t(GpsWrap::WatchJson) == WATCH_JSON); \
    } \
    \
    CLASS::~CLASS() \
    { \
        destruct(m_Private); \
    }
