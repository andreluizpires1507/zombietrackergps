/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <functional>

#include <QDialog>
#include <QHeaderView>
#include <QByteArray>
#include <QSortFilterProxyModel>

#include <src/ui/windows/appconfigbase.h>
#include <src/ui/widgets/colordelegate.h>
#include <src/ui/widgets/comboboxdelegate.h>
#include <src/ui/widgets/doublespindelegate.h>
#include <src/ui/widgets/iconselectordelegate.h>
#include <src/ui/widgets/unitsdelegate.h>
#include <src/ui/widgets/datetimedelegate.h>
#include <src/ui/widgets/lineeditdelegate.h>
#include <src/ui/widgets/spindelegate.h>
#include <src/ui/dialogs/iconselector.h>
#include <src/ui/misc/toclist.h>

#include <src/core/colorizermodel.h>

#include "src/core/cfgdata.h"
#include "src/core/personmodel.h"
#include "src/core/zonemodel.h"

namespace Ui {
class AppConfig;
} // namespace Ui

class MainWindow;
class QAbstractButton;
class QToolButton;

// Dialog to display and edit CfgData entries.
class AppConfig :
        public AppConfigBase,
        public TOCList // for our config page TOC
{
    Q_OBJECT

public:
    explicit AppConfig(MainWindow*);
    ~AppConfig() override;

    // Configuration pages.  These must stay in order with the UI.
    enum class Page {
        GeneralUI,
        GeneralBackup,
        GeneralUndo,
        AutoImport,
        Icons,
        MapDisplay,
        Tags,
        People,
        Zones,
        UnitsDist,
        UnitsMisc,
        UnitsPower,
        UnitsTime,
        GraphsTrack,
        GraphsActivity,
        GraphsClimb,
        TrackColorizer,
        PointColorizer,
        ClimbColorizer,
        _Count,
    };

    void showPage(Page); // show given page of configuration
    static QByteArray pageIcon(Page); // icon for a given page

private slots:
    void on_action_Next_Tab_triggered();
    void on_action_Prev_Tab_triggered();
    void on_addTagHeader_clicked();
    void on_addTag_clicked();
    void on_appCfgButtons_clicked(QAbstractButton*);
    void on_autoImportBackupDirSelect_clicked();
    void on_autoImportMode_currentIndexChanged(int index);
    void on_autoImportDirSelect_clicked();
    void on_autoImportPost_currentIndexChanged(int index);
    void on_autoImportTagButton_clicked();
    void on_brokenIcon_clicked();
    void on_currentPointIcon_clicked();
    void on_dataSavePathSelect_clicked();
    void on_defaultPointIcon_clicked();
    void on_delTag_clicked();
    void on_filterEmptyIcon_clicked();
    void on_filterInvalidIcon_clicked();
    void on_filterValidIcon_clicked();
    void on_gpsdLivePointIcon_clicked();
    void on_outlineTrackColor_clicked();
    void on_peopleAdd_clicked();
    void on_peopleRemove_clicked();
    void on_peopleReset_clicked();
    void on_selectedPointIcon_clicked();
    void on_sortTags_clicked();
    void on_trackNoteIcon_clicked();
    void on_trkPtMarkerColor_clicked();
    void on_trkPtRangeColor_clicked();
    void on_unassignedTrackColor_clicked();
    void on_waypointDefaultIcon_clicked();
    void on_zonesAdd_clicked();
    void on_zonesDefaults_activated(int index);
    void on_zonesRemove_clicked();
    void on_autoImportTimeout_valueChanged(int value);

    // Resort training zone data if needed
    void zoneDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int> &roles);

    void setAutoImportEnabledState(); // enable/disable states for auto-import widgets

    // TOC page changed
    void changePage(const QModelIndex&) override; // change to page for given index

private:
    friend class TestZtgps; // test hook

    static const constexpr char* tagProperty    = "zt-tag";

    void setup() override;
    using TOCList::setupTOC;
    void setupTOC() override;
    void setupActionIcons();
    void setupTagEditor();
    void setupPeopleEditor();
    void setupZoneEditor();
    void setupTrackColorizerEditor();
    void setupPointColorizerEditor();
    void setupClimbColorizerEditor();
    void setupUnitsInputs();
    void setupSignals();
    void setupUIColors();
    void setupCompleters();
    void setupZoneDefaults();
    void setupTrkPtColors();
    void acceptInteractive() override;
    void updateUIFromCfg() override;
    void updateCfgFromUI() override;
    void saveInitial() override;
    const CfgData& prevCfgData() const override { return m_prevCfgData; }
    CfgData& prevCfgData() override { return m_prevCfgData; }

    void onShow() override;                      // possibly create UI, and save initial cfgData

    void setAutoImportTags(const QStringList& tags); // for Auto-Import pages
    QStringList getAutoImportTags() const;

    void addNewTag(bool category, const TreeItem::ItemData& = {});
    bool applyTagRenaming();

    static void getIcon(IconSelector&, QString&, QToolButton*);

    void selectDir(const QString& caption, QLineEdit*, const QString& defaultDir = QString());

    void redOnBad(QLineEdit*, const std::function<bool(const QLineEdit*)>&) const;
    void redIfNotFound(QLineEdit*) const; // if it is a file which doesn't exist
    void redIfNonPosix(QLineEdit*) const; // if it fails to parse though wordexp(3)

    CfgData               m_prevCfgData;           // initial values, for later rejection

    TagModel              tags;                  // UI doesn't store these things itself:
    PersonModel           people;                // ...
    ZoneModel             zones;                 // ...
    ColorizerModel        trackColorizer;        // ...
    ColorizerModel        pointColorizer;        // ...
    ColorizerModel        climbColorizer;        // ...
    UiColorModel          uiColor;               // ...
    TrkPtColorModel       trkPtColor;            // ...
    QString               defaultPointIcon;      // QIcon doesn't store the file path...
    QString               selectedPointIcon;     // ...
    QString               currentPointIcon;      // ...
    QString               gpsdLivePointIcon;     // ...
    QString               waypointDefaultIcon;   // ...
    QString               trackNoteIcon;         // ...
    QString               brokenIcon;            // ...
    QString               filterEmptyIconName;   // ...
    QString               filterValidIconName;   // ...
    QString               filterInvalidIconName; // ...

    ColorDelegate         colorDelegate;
    ColorDelegate         colorAlphaDelegate;
    LineEditDelegate      textDelegate;
    IconSelector*         pointIconSelector;
    IconSelector*         iconIconSelector;
    IconSelectorDelegate  tagIconSelectorDelegate;
    DoubleSpinDelegate    CdADelegate;
    DoubleSpinDelegate    weightDelegate;
    DoubleSpinDelegate    rollResistDelegate;
    DoubleSpinDelegate    efficiencyDelegate;
    DoubleSpinDelegate    bioPowerDelegate;
    DoubleSpinDelegate    zoneHrDelegate;
    DoubleSpinDelegate    zoneFtpDelegate;
    ComboBoxDelegate      mediumDelegate;
    UnitsDelegate         unitsDelegate;
    DateTimeDelegate      birthdayDelegate;
    SpinDelegate          maxHrDelegate;
    SpinDelegate          ftpDelegate;

    QHeaderView           tagHeaders;
    QHeaderView           peopleHeaders;
    QHeaderView           zoneHeaders;

    MainWindow&           mainWindow;
    Ui::AppConfig*        ui;
};

#endif // APPCONFIG_H
