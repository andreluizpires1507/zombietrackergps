/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QSet>
#include <QDir>
#include <QGuiApplication>
#include <QColorDialog>
#include <QFileDialog>
#include <QCompleter>
#include <QFileSystemModel>
#include <QSpacerItem>

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/util/ui.inl.h>
#include <src/util/posixexpander.h>

#include "src/core/app.h"
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"
#include "src/core/climbmodel.h"
#include "src/ui/dialogs/tagrenamedialog.h"
#include "src/ui/dialogs/tagselectordialog.h"
#include "src/ui/widgets/colorizereditor.h"
#include "src/ui/panes/pane.h"

#include "mainwindow.h"
#include "appconfig.h"
#include "ui_appconfig.h"

AppConfig::AppConfig(MainWindow* mainWindow) :
    AppConfigBase(mainWindow),
    trackColorizer(&app().trackModel()),
    pointColorizer(CfgData::emptyPointModel),
    climbColorizer(&app().climbModel()),
    colorDelegate(this),
    colorAlphaDelegate(this, true),
    pointIconSelector(IconSelector::iconSelector({ ":art/points"}, DataColumnPane::noColorNames)),
    iconIconSelector(IconSelector::iconSelector({":icons/hicolor"})),
    tagIconSelectorDelegate({ ":art/tags", "[US NPS Symbols]:art/us-nps-symbols" }),
    CdADelegate(this, -1.0, 100.0, 2, 0.1, "", " m^2"),
    weightDelegate(this, -1.0, 1e6, 2, 1.0),
    rollResistDelegate(this, -1.0, 10.0, 4, 0.001),
    efficiencyDelegate(this, -1.0, 100.0, 0, 1),
    bioPowerDelegate(this, -1.0, 100.0, 0, 1),
    zoneHrDelegate(this, 0.0, 1.0, 2, 0.05),
    zoneFtpDelegate(this, 0.0, 4.0, 2, 0.05),
    mediumDelegate(this, TagModel::mediumNames()),
    unitsDelegate(this, Units(Format::SpeedKPH), true),
    birthdayDelegate(this, Units(Format::DateyyyyMMdd).dateFormat()),
    maxHrDelegate(this, 0, 250, 1, "", tr(" bpm")),
    ftpDelegate(this, 0, 1000, 5,  ""),
    tagHeaders(Qt::Horizontal, this),
    peopleHeaders(Qt::Horizontal, this),
    zoneHeaders(Qt::Horizontal, this),
    mainWindow(*mainWindow),
    ui(nullptr)
{
}

AppConfig::~AppConfig()
{
    delete ui;
}

void AppConfig::setup()
{
    if (ui != nullptr)
        return;

    ui = new Ui::AppConfig;
    ui->setupUi(this);

    setupTOC();
    setupTagEditor();
    setupPeopleEditor();
    setupZoneEditor();
    setupTrackColorizerEditor();
    setupPointColorizerEditor();
    setupClimbColorizerEditor();
    setupUIColors();
    setupTrkPtColors();
    setupZoneDefaults();
    setupUnitsInputs();
    setupSignals();
    setupCompleters();
    setupActionIcons();

    setAutoImportEnabledState();

    Util::SetupWhatsThis(this);
    Util::SetFocus(ui->appCfgTabs);

    showPage(Page::GeneralUI); // start with this page

    addAction(ui->action_Next_Tab);
    addAction(ui->action_Prev_Tab);

    // This changes the label to show the numeric value
    on_autoImportTimeout_valueChanged(ui->autoImportTimeout->value());

    // Ensure UI item count matches enum.
    assert(ui->mapMoveMode->count() == int(CfgData::MapMoveMode::_Count));

    // Ensure UI tab count matches Pages enum
    assert(ui->appCfgTabs->count() == int(Page::_Count));
}

void AppConfig::setupTOC()
{
    if (TOCList::isSetup()) // avoid redundant setups
        return;

    setupTOC(ui->toc, {
        { 0, tr("General"),        -1,                        "configure" },
           { 1, tr("UI"),          int(Page::GeneralUI),      pageIcon(Page::GeneralUI) },
           { 1, tr("Backups"),     int(Page::GeneralBackup),  pageIcon(Page::GeneralBackup) },
           { 1, tr("Undo"),        int(Page::GeneralUndo),    pageIcon(Page::GeneralUndo) },
           { 1, tr("Auto Import"), int(Page::AutoImport),     pageIcon(Page::AutoImport) },
        { 0, tr("Icons"),          int(Page::Icons),          pageIcon(Page::Icons) },
        { 0, tr("Map Display"),    int(Page::MapDisplay),     pageIcon(Page::MapDisplay) },
        { 0, tr("Tags"),           int(Page::Tags),           pageIcon(Page::Tags) },
        { 0, tr("People"),         int(Page::People),         pageIcon(Page::People) },
        { 0, tr("Zones"),          int(Page::Zones),          pageIcon(Page::Zones) },
        { 0, tr("Units"),          -1,                        pageIcon(Page::UnitsMisc) },
           { 1, tr("Distance"),    int(Page::UnitsDist),      pageIcon(Page::UnitsDist) },
           { 1, tr("Misc"),        int(Page::UnitsMisc),      pageIcon(Page::UnitsMisc) },
           { 1, tr("Power"),       int(Page::UnitsPower),     pageIcon(Page::UnitsPower) },
           { 1, tr("Time"),        int(Page::UnitsTime),      pageIcon(Page::UnitsTime) },
        { 0, tr("Graphs"),         -1,                        pageIcon(Page::GraphsTrack) },
           { 1, tr("Track"),       int(Page::GraphsTrack),    pageIcon(Page::GraphsTrack) },
           { 1, tr("Activity"),    int(Page::GraphsActivity), pageIcon(Page::GraphsActivity) },
           { 1, tr("Climb"),       int(Page::GraphsClimb),    pageIcon(Page::GraphsClimb) },
        { 0, tr("Colorizers"),     -1,                        "color-profile" },
           { 1, tr("Track"),       int(Page::TrackColorizer), pageIcon(Page::TrackColorizer) },
           { 1, tr("Point"),       int(Page::PointColorizer), pageIcon(Page::PointColorizer) },
           { 1, tr("Climb"),       int(Page::ClimbColorizer), pageIcon(Page::ClimbColorizer) },
    });

    ui->tocCfgSplitter->setSizes({ ui->toc->minimumWidth() * 5 / 4,
                                   ui->toc->minimumWidth() * 5 });
}

void AppConfig::setupActionIcons()
{
    Icons::defaultIcon(ui->sortTags,                  "sort-name");
    Icons::defaultIcon(ui->action_Next_Tab,           "go-next");
    Icons::defaultIcon(ui->action_Prev_Tab,           "go-previous");
    Icons::defaultIcon(ui->autoImportTagButton,       "edit-entry");
    Icons::defaultIcon(ui->dataSavePathSelect,        "document-open-folder");
    Icons::defaultIcon(ui->autoImportBackupDirSelect, "document-open-folder");
    Icons::defaultIcon(ui->autoImportDirSelect,       "document-open-folder");
}

void AppConfig::setupTagEditor()
{
    QTreeView& view = *ui->tagTreeView;

    // Most view config is set in the form editor
    view.setModel(&tags);
    view.setHeader(&tagHeaders);

    Util::InitDelegates(view, {{ TagModel::Icon,       &tagIconSelectorDelegate },
                               { TagModel::Color,      &colorDelegate },
                               { TagModel::CdA,        &CdADelegate },
                               { TagModel::Weight,     &weightDelegate },
                               { TagModel::Efficiency, &efficiencyDelegate },
                               { TagModel::RR,         &rollResistDelegate },
                               { TagModel::BioPct,     &bioPowerDelegate },
                               { TagModel::Medium,     &mediumDelegate },
                               { TagModel::UnitSpeed,  &unitsDelegate }});

    tagHeaders.setSectionResizeMode(QHeaderView::Interactive);
    tagHeaders.setDefaultAlignment(Qt::AlignLeft);
    tagHeaders.setSectionsMovable(true);
}

void AppConfig::setupPeopleEditor()
{
    QTreeView& view = *ui->peopleTreeView;

    // Most view config is set in the form editor
    view.setModel(&people);
    view.setHeader(&peopleHeaders);

    Util::InitDelegates(view, {{ PersonModel::Weight,     &weightDelegate },
                               { PersonModel::Efficiency, &efficiencyDelegate },
                               { PersonModel::Birthdate,  &birthdayDelegate },
                               { PersonModel::MaxHR,      &maxHrDelegate },
                               { PersonModel::FTP,        &ftpDelegate }});

    peopleHeaders.setSectionResizeMode(QHeaderView::Interactive);
    peopleHeaders.setDefaultAlignment(Qt::AlignLeft);
    peopleHeaders.setSectionsMovable(true);
}

void AppConfig::setupZoneEditor()
{
    QTreeView& view = *ui->zonesTreeView;

    // Most view config is set in the form editor
    view.setModel(&zones);
    view.setHeader(&zoneHeaders);

    Util::InitDelegates(view, {{ ZoneModel::Name,        &textDelegate },
                               { ZoneModel::Color,       &colorDelegate },
                               { ZoneModel::MaxHRPct,    &zoneHrDelegate },
                               { ZoneModel::FTPPct,      &zoneFtpDelegate },
                               { ZoneModel::Description, &textDelegate }});

    zoneHeaders.setSectionResizeMode(QHeaderView::Interactive);
    zoneHeaders.setDefaultAlignment(Qt::AlignLeft);
    zoneHeaders.setSectionsMovable(true);
}

void AppConfig::setupTrackColorizerEditor()
{
    auto* trackColorizerEditor = new ColorizerEditor(trackColorizer, TrackModel::headersList<TrackModel>());

    ui->appCfgTabs->addWidget(trackColorizerEditor);
}


void AppConfig::setupPointColorizerEditor()
{
    auto* pointColorizerEditor = new ColorizerEditor(pointColorizer, PointModel::headersList<PointModel>());

    ui->appCfgTabs->addWidget(pointColorizerEditor);
}

void AppConfig::setupClimbColorizerEditor()
{
    auto* climbColorizerEditor = new ColorizerEditor(climbColorizer, ClimbModel::headersList<ClimbModel>());

    ui->appCfgTabs->addWidget(climbColorizerEditor);
}

void AppConfig::setupUnitsInputs()
{
    cfgData().unitsTrkLength.addToComboBox(ui->unitsTrkLength, Format::_DistNonAuto);
    cfgData().unitsLegLength.addToComboBox(ui->unitsLegLength, Format::_DistNonAuto);
    cfgData().unitsElevation.addToComboBox(ui->unitsElevation);
    cfgData().unitsClimb.addToComboBox(ui->unitsClimb);
    cfgData().unitsDuration.addToComboBox(ui->unitsDuration);
    cfgData().unitsTrkDate.addToComboBox(ui->unitsTrkDate);
    cfgData().unitsTrkTime.addToComboBox(ui->unitsTrkTime);
    cfgData().unitsPointDate.addToComboBox(ui->unitsPointDate);
    cfgData().unitsTz.addToComboBox(ui->unitsTz);
    cfgData().unitsLat.addToComboBox(ui->unitsLatLon);
    cfgData().unitsSpeed.addToComboBox(ui->unitsSpeed);
    cfgData().unitsAccel.addToComboBox(ui->unitsAccel);
    cfgData().unitsArea.addToComboBox(ui->unitsArea);
    cfgData().unitsTemp.addToComboBox(ui->unitsTemp);
    cfgData().unitsSlope.addToComboBox(ui->unitsSlope);
    cfgData().unitsPower.addToComboBox(ui->unitsPower);
    cfgData().unitsEnergy.addToComboBox(ui->unitsEnergy);
    cfgData().unitsWeight.addToComboBox(ui->unitsWeight);
    cfgData().unitsPct.addToComboBox(ui->unitsPct);
    cfgData().unitsCad.addToComboBox(ui->unitsCad);
    cfgData().unitsHr.addToComboBox(ui->unitsHr);
}

void AppConfig::setupSignals()
{
    if (ui == nullptr)
        return;

    // Resort training zone data if needed
    connect(&zones, &TreeModel::dataChanged, this, &AppConfig::zoneDataChanged, Qt::UniqueConnection);

    // React to changes in selected TOC page
    connect(ui->toc->selectionModel(), &QItemSelectionModel::currentChanged, this, &AppConfig::changePage);
    connect(ui->toc, &QTreeView::clicked, this, &AppConfig::changePage);

    // Auto-import command changes: enabled/disable widgets
    connect(ui->autoImportCommand, &QLineEdit::textChanged, this, &AppConfig::setAutoImportEnabledState);

    const auto colorizeBadDirs = [this](QLineEdit* lineEdit) {
        connect(lineEdit, &QLineEdit::textChanged, this, [this, lineEdit]() { redIfNotFound(lineEdit); });
    };

    // Turn some paths red if they don't exist.
    colorizeBadDirs(ui->autoImportDir);
    colorizeBadDirs(ui->autoImportBackupDir);
    colorizeBadDirs(ui->dataSavePathEdit);

    // autoImportCommand: turn red if it doesn't parse through wordexp(3)
    connect(ui->autoImportCommand, &QLineEdit::textChanged, this, [this]() {
        redIfNonPosix(ui->autoImportCommand);
    });
}

void AppConfig::setupUIColors()
{
    QTreeView& view = *ui->cfgUiColors;

    view.setModel(&uiColor);
    view.setItemDelegateForColumn(UiColorModel::Color, &colorDelegate);

    Util::ResizeViewForData(view);
}

void AppConfig::setupCompleters()
{
    auto* fsModel = new QFileSystemModel();
    fsModel->setRootPath("");
    fsModel->setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Hidden);
    ui->dataSavePathEdit->setCompleter(new QCompleter(fsModel, this));
}

void AppConfig::setupTrkPtColors()
{
    QTreeView& view = *ui->trkPtColor;

    view.setModel(&trkPtColor);
    view.setItemDelegateForColumn(TrkPtColorModel::Color, &colorAlphaDelegate);

    Util::ResizeViewForData(view);
}

void AppConfig::setupZoneDefaults()
{
    QComboBox& zoneDefaults = *ui->zonesDefaults;

    // Set icon for title entry
    zoneDefaults.setItemIcon(0, Icons::get("zones"));

    zoneDefaults.addItems(ZoneModel::presetNames());
}

// populate GUI data
void AppConfig::saveInitial()
{
    // Remember tag identifiers, so we can batch-rename or remove later if needed
    cfgDataWritable().tags.setUniqueIds();

    AppConfigBase::saveInitial();
}

void AppConfig::onShow()
{
    AppConfigBase::onShow();
    setupTOC();
}

void AppConfig::updateUIFromCfg()
{
    if (ui == nullptr)
        return;

    // Cut and paste
    ui->cfgRowSeparator->setText(cfgData().rowSeparator);
    ui->cfgColSeparator->setText(cfgData().colSeparator);

    // Filter behavior
    ui->caseSensitiveFilters->setChecked(cfgData().caseSensitiveFilters);
    ui->caseSensitiveSorting->setChecked(cfgData().caseSensitiveSorting);

    // UI stuff
    ui->cfgWarnOnClose->setChecked(cfgData().warnOnClose);
    ui->cfgWarnOnRemove->setChecked(cfgData().warnOnRemove);
    ui->cfgWarnOnRevert->setChecked(cfgData().warnOnRevert);
    ui->cfgWarnOnExit->setChecked(cfgData().warnOnExit);
    ui->inlineCompletion->setChecked(cfgData().inlineCompletion);
    ui->completionListSize->setValue(cfgData().completionListSize);
    ui->completionListSize->setDisabled(cfgData().inlineCompletion);

    // Undos
    ui->maxUndoCount->setValue(cfgData().maxUndoCount);
    ui->maxUndoSizeMiB->setValue(double(cfgData().maxUndoSizeMiB));
    ui->maxUndoCountView->setValue(cfgData().maxUndoCountView);
    ui->mapUndoStill->setValue(double(cfgData().mapUndoStill));

    // Convolution filter sizes
    ui->eleFilterSize->setValue(cfgData().eleFilterSize | 0x1);

    // Track line display
    Util::SetTBColor(ui->unassignedTrackColor, cfgData().unassignedTrackColor);
    Util::SetTBColor(ui->outlineTrackColor,    cfgData().outlineTrackColor);

    ui->defaultTrackWidthC->setValue(qreal(cfgData().defaultTrackWidthC));
    ui->defaultTrackWidthF->setValue(qreal(cfgData().defaultTrackWidthF));
    ui->defaultTrackWidthO->setValue(qreal(cfgData().defaultTrackWidthO));
    ui->currentTrackWidthC->setValue(qreal(cfgData().currentTrackWidthC));
    ui->currentTrackWidthF->setValue(qreal(cfgData().currentTrackWidthF));
    ui->currentTrackWidthO->setValue(qreal(cfgData().currentTrackWidthO));
    ui->defaultTrackAlphaC->setValue(cfgData().defaultTrackAlphaC);
    ui->defaultTrackAlphaF->setValue(cfgData().defaultTrackAlphaF);
    ui->currentTrackAlphaC->setValue(cfgData().currentTrackAlphaC);
    ui->currentTrackAlphaF->setValue(cfgData().currentTrackAlphaF);

    // Map movement options
    ui->mapMoveMode->setCurrentIndex(int(cfgData().mapMoveMode));
    ui->mapMovePoints->setChecked(cfgData().mapMovePoints);
    ui->mapMoveWaypoints->setChecked(cfgData().mapMoveWaypoints);
    ui->mapIntertialMovement->setChecked(cfgData().mapInertialMovement);

    // Track point display
    ui->defaultPointIcon->setIcon(QIcon(defaultPointIcon = cfgData().defaultPointIcon));
    ui->defaultPointIconSize->setValue(cfgData().defaultPointIconSize);
    ui->defaultPointIconProx->setValue(cfgData().defaultPointIconProx);
    ui->selectedPointIcon->setIcon(QIcon(selectedPointIcon = cfgData().selectedPointIcon));
    ui->selectedPointIconSize->setValue(cfgData().selectedPointIconSize);
    ui->currentPointIcon->setIcon(QIcon(currentPointIcon = cfgData().currentPointIcon));
    ui->currentPointIconSize->setValue(cfgData().currentPointIconSize);
    ui->gpsdLivePointIcon->setIcon(QIcon(gpsdLivePointIcon = cfgData().gpsdLivePointIcon));
    ui->gpsdLivePointIconSize->setValue(cfgData().gpsdLivePointIconSize);
    ui->waypointDefaultIcon->setIcon(QIcon(waypointDefaultIcon = cfgData().waypointDefaultIcon));
    ui->waypointDefaultIconSize->setValue(cfgData().waypointDefaultIconSize);
    ui->waypointIconSize->setValue(cfgData().waypointIconSize);

    // Icons config
    ui->trackNoteIcon->setIcon(QIcon(trackNoteIcon = cfgData().trackNoteIcon));
    ui->brokenIcon->setIcon(QIcon(brokenIcon = cfgData().brokenIcon));
    ui->filterEmptyIcon->setIcon(QIcon(filterEmptyIconName = cfgData().filterEmptyIconName));
    ui->filterValidIcon->setIcon(QIcon(filterValidIconName = cfgData().filterValidIconName));
    ui->filterInvalidIcon->setIcon(QIcon(filterInvalidIconName = cfgData().filterInvalidIconName));
    ui->colorizeTagIcons->setChecked(cfgData().colorizeTagIcons);
    ui->iconSizeTrack->setValue(cfgData().iconSizeTrack.width());
    ui->flagSizeTrack->setValue(cfgData().flagSizeTrack.width());
    ui->iconSizeView->setValue(cfgData().iconSizeView.width());
    ui->iconSizeTag->setValue(cfgData().iconSizeTag.width());
    ui->iconSizeFilter->setValue(cfgData().iconSizeFilter.width());
    ui->iconSizeClimb->setValue(cfgData().iconSizeClimb.width());
    ui->maxTrackPaneIcons->setValue(cfgData().maxTrackPaneIcons);
    ui->maxTrackPaneFlags->setValue(cfgData().maxTrackPaneFlags);
    ui->panePreviewHeight->setValue(cfgData().panePreviewHeight);

    // Autoimport settings
    ui->autoImportMode->setCurrentIndex(int(cfgData().autoImportMode));
    ui->autoImportDir->setText(cfgData().autoImportDir);
    ui->autoImportPattern->setText(cfgData().autoImportPattern);
    ui->autoImportPost->setCurrentIndex(int(cfgData().autoImportPost));
    ui->autoImportBackupSuffix->setText(cfgData().autoImportBackupSuffix);
    ui->autoImportBackupDir->setText(cfgData().autoImportBackupDir);
    ui->autoImportCommand->setText(cfgData().autoImportCommand);
    ui->autoImportStdout->setChecked(cfgData().autoImportStdout);
    ui->autoImportTimeout->setValue(cfgData().autoImportTimeout);
    setAutoImportTags(cfgData().autoImportTags);

    // Units
    ui->unitsTrkLength->setCurrentIndex(cfgData().unitsTrkLength.rangeIdx(Format::_DistNonAuto));
    ui->unitsLegLength->setCurrentIndex(cfgData().unitsLegLength.rangeIdx(Format::_DistNonAuto));
    ui->unitsDuration->setCurrentIndex(cfgData().unitsDuration.rangeIdx());
    ui->unitsTrkDate->setCurrentIndex(cfgData().unitsTrkDate.rangeIdx());
    ui->unitsTrkTime->setCurrentIndex(cfgData().unitsTrkTime.rangeIdx());
    ui->unitsPointDate->setCurrentIndex(cfgData().unitsPointDate.rangeIdx());
    ui->unitsTz->setCurrentIndex(cfgData().unitsTz.rangeIdx());
    ui->unitsElevation->setCurrentIndex(cfgData().unitsElevation.rangeIdx());
    ui->unitsLatLon->setCurrentIndex(cfgData().unitsLat.rangeIdx());
    ui->unitsSpeed->setCurrentIndex(cfgData().unitsSpeed.rangeIdx());
    ui->unitsAccel->setCurrentIndex(cfgData().unitsAccel.rangeIdx());
    ui->unitsClimb->setCurrentIndex(cfgData().unitsClimb.rangeIdx());
    ui->unitsArea->setCurrentIndex(cfgData().unitsArea.rangeIdx());
    ui->unitsTemp->setCurrentIndex(cfgData().unitsTemp.rangeIdx());
    ui->unitsSlope->setCurrentIndex(cfgData().unitsSlope.rangeIdx());
    ui->unitsPower->setCurrentIndex(cfgData().unitsPower.rangeIdx());
    ui->unitsEnergy->setCurrentIndex(cfgData().unitsEnergy.rangeIdx());
    ui->unitsWeight->setCurrentIndex(cfgData().unitsWeight.rangeIdx());
    ui->unitsPct->setCurrentIndex(cfgData().unitsPct.rangeIdx());
    ui->unitsCad->setCurrentIndex(cfgData().unitsCad.rangeIdx());
    ui->unitsHr->setCurrentIndex(cfgData().unitsHr.rangeIdx());

    ui->unitsFmtTrkLength->setValue(cfgData().unitsTrkLength.precision());
    ui->unitsFmtLegLength->setValue(cfgData().unitsLegLength.precision());
    ui->unitsFmtDuration->setValue(cfgData().unitsDuration.precision());
    ui->unitsFmtElevation->setValue(cfgData().unitsElevation.precision());
    ui->unitsFmtLatLon->setValue(cfgData().unitsLat.precision());
    ui->unitsFmtSpeed->setValue(cfgData().unitsSpeed.precision());
    ui->unitsFmtAccel->setValue(cfgData().unitsAccel.precision());
    ui->unitsFmtClimb->setValue(cfgData().unitsClimb.precision());
    ui->unitsFmtArea->setValue(cfgData().unitsArea.precision());
    ui->unitsFmtTemp->setValue(cfgData().unitsTemp.precision());
    ui->unitsFmtSlope->setValue(cfgData().unitsSlope.precision());
    ui->unitsFmtPower->setValue(cfgData().unitsPower.precision());
    ui->unitsFmtEnergy->setValue(cfgData().unitsEnergy.precision());
    ui->unitsFmtWeight->setValue(cfgData().unitsWeight.precision());
    ui->unitsFmtPct->setValue(cfgData().unitsPct.precision());
    ui->unitsFmtCad->setValue(cfgData().unitsCad.precision());
    ui->unitsFmtHr->setValue(cfgData().unitsHr.precision());

    ui->unitsPadDuration->setChecked(cfgData().unitsDuration.leadingZeros());
    ui->unitsPadLatLon->setChecked(cfgData().unitsLat.leadingZeros());

    // Graph data
    ui->trackDsUTC->setChecked(cfgData().unitsTrkDate.isUTC());
    ui->pointDsUTC->setChecked(cfgData().unitsPointDate.isUTC());

    ui->trkPtLineWidth->setValue(qreal(cfgData().trkPtLineWidth));
    ui->trkPtMarkerWidth->setValue(qreal(cfgData().trkPtMarkerWidth));
    ui->trkPtRangeWidth->setValue(qreal(cfgData().trkPtRangeWidth));
    Util::SetTBColor(ui->trkPtMarkerColor, cfgData().trkPtMarkerColor);
    Util::SetTBColor(ui->trkPtRangeColor, cfgData().trkPtRangeColor);

    ui->asMaxDateSpans->setValue(cfgData().asMaxDateSpans);
    ui->asBarWidth->setValue(cfgData().asBarWidth);

    ui->hillMinGrade->setSuffix(QString(" ") + cfgData().unitsSlope.suffix(cfgData().hillMinGrade));
    ui->hillMinGrade->setRange(cfgData().unitsSlope.toDouble(0.01),
                               cfgData().unitsSlope.toDouble(0.25));
    ui->hillMinGrade->setSingleStep(cfgData().unitsSlope.toDouble(0.01));
    ui->hillMinGrade->setDecimals(cfgData().unitsSlope.precision() + 1);
    ui->hillMinGrade->setValue(cfgData().unitsSlope.toDouble(cfgData().hillMinGrade));

    ui->hillMinHeight->setSuffix(QString(" ") + cfgData().unitsClimb.suffix(cfgData().hillMinHeight));
    ui->hillMinHeight->setRange(cfgData().unitsClimb.toDouble(25.0),
                                cfgData().unitsClimb.toDouble(1000.00));
    ui->hillMinHeight->setDecimals(cfgData().unitsClimb.precision());
    ui->hillMinHeight->setValue(cfgData().unitsClimb.toDouble(cfgData().hillMinHeight));

    ui->hillGradeLength->setSuffix(QString(" ") + cfgData().unitsLegLength.suffix(cfgData().hillGradeLength));
    ui->hillGradeLength->setRange(cfgData().unitsLegLength.toDouble(25.0),
                                  cfgData().unitsLegLength.toDouble(5000.00));
    ui->hillGradeLength->setDecimals(cfgData().unitsLegLength.precision());
    ui->hillGradeLength->setValue(cfgData().unitsLegLength.toDouble(cfgData().hillGradeLength));

    // Backups and files
    ui->backupUICount->setValue(cfgData().backupUICount);
    ui->dataAutosaveInterval->setValue(cfgData().dataAutosaveInterval);
    ui->dataSavePathEdit->setText(cfgData().dataAutosavePath);

    // Models
    tags           = cfgData().tags;
    people         = cfgData().people;
    zones          = cfgData().zones;
    trackColorizer = cfgData().trackColorizer;
    pointColorizer = cfgData().pointColorizer;
    climbColorizer = cfgData().climbColorizer;
    uiColor        = cfgData().uiColor;
    trkPtColor     = cfgData().trkPtColor;

    // Our own icon size!
    ui->tagTreeView->setIconSize(cfgData().iconSizeTag);

    // Our own delegate suffixes
    efficiencyDelegate.setSuffix(QString(" ") + cfgData().unitsPct.suffix(0.5));
    bioPowerDelegate.setSuffix(QString(" ") + cfgData().unitsPower.suffix(0.5));
    weightDelegate.setSuffix(QString(" ") + cfgData().unitsWeight.suffix(100.0_kg));
    ftpDelegate.setSuffix(QString(" ") + cfgData().unitsPower.suffix(1.0));

    cfgDataWritable().applyGlobal();

    ui->tagTreeView->expandAll();
    setAutoImportEnabledState();

    Util::ResizeViewForData(*ui->tagTreeView);
    Util::ResizeViewForData(*ui->zonesTreeView);
}

void AppConfig::updateCfgFromUI()
{
    if (ui == nullptr)
        return;

    CfgData& cfgData = cfgDataWritable();

    // Cut and paste
    cfgData.rowSeparator         = ui->cfgRowSeparator->text();
    cfgData.colSeparator         = ui->cfgColSeparator->text();

    // Filter behavior
    cfgData.caseSensitiveFilters = ui->caseSensitiveFilters->isChecked();
    cfgData.caseSensitiveSorting = ui->caseSensitiveSorting->isChecked();

    // UI stuff
    cfgData.warnOnClose          = ui->cfgWarnOnClose->isChecked();
    cfgData.warnOnRemove         = ui->cfgWarnOnRemove->isChecked();
    cfgData.warnOnRevert         = ui->cfgWarnOnRevert->isChecked();
    cfgData.warnOnExit           = ui->cfgWarnOnExit->isChecked();
    cfgData.inlineCompletion     = ui->inlineCompletion->isChecked();
    cfgData.completionListSize   = ui->completionListSize->value();

    // Undos
    cfgData.maxUndoCount         = ui->maxUndoCount->value();
    cfgData.maxUndoSizeMiB       = float(ui->maxUndoSizeMiB->value());
    cfgData.maxUndoCountView     = ui->maxUndoCountView->value();
    cfgData.mapUndoStill         = float(ui->mapUndoStill->value());

    // Convolution filter sizes
    cfgData.eleFilterSize        = ui->eleFilterSize->value();

    // Track line display.
    cfgData.unassignedTrackColor = Util::GetTBColor(ui->unassignedTrackColor);
    cfgData.outlineTrackColor    = Util::GetTBColor(ui->outlineTrackColor);

    cfgData.defaultTrackWidthC   = float(ui->defaultTrackWidthC->value());
    cfgData.defaultTrackWidthF   = float(ui->defaultTrackWidthF->value());
    cfgData.defaultTrackWidthO   = float(ui->defaultTrackWidthO->value());
    cfgData.currentTrackWidthC   = float(ui->currentTrackWidthC->value());
    cfgData.currentTrackWidthF   = float(ui->currentTrackWidthF->value());
    cfgData.currentTrackWidthO   = float(ui->currentTrackWidthO->value());
    cfgData.defaultTrackAlphaC   = ui->defaultTrackAlphaC->value();
    cfgData.defaultTrackAlphaF   = ui->defaultTrackAlphaF->value();
    cfgData.currentTrackAlphaC   = ui->currentTrackAlphaC->value();
    cfgData.currentTrackAlphaF   = ui->currentTrackAlphaF->value();

    // Map movement options
    cfgData.mapMoveMode             = CfgData::MapMoveMode(ui->mapMoveMode->currentIndex());
    cfgData.mapMovePoints           = ui->mapMovePoints->isChecked();
    cfgData.mapMoveWaypoints        = ui->mapMoveWaypoints->isChecked();
    cfgData.mapInertialMovement     = ui->mapIntertialMovement->isChecked();

    // Track point display
    cfgData.defaultPointIcon        = defaultPointIcon;
    cfgData.defaultPointIconSize    = ui->defaultPointIconSize->value();
    cfgData.defaultPointIconProx    = ui->defaultPointIconProx->value();
    cfgData.selectedPointIcon       = selectedPointIcon;
    cfgData.selectedPointIconSize   = ui->selectedPointIconSize->value();
    cfgData.currentPointIcon        = currentPointIcon;
    cfgData.currentPointIconSize    = ui->currentPointIconSize->value();
    cfgData.gpsdLivePointIcon       = gpsdLivePointIcon;
    cfgData.gpsdLivePointIconSize   = ui->gpsdLivePointIconSize->value();
    cfgData.waypointDefaultIcon     = waypointDefaultIcon;
    cfgData.waypointIconSize        = ui->waypointIconSize->value();
    cfgData.waypointDefaultIconSize = ui->waypointDefaultIconSize->value();
    cfgData.maxTrackPaneIcons       = ui->maxTrackPaneIcons->value();
    cfgData.maxTrackPaneFlags       = ui->maxTrackPaneFlags->value();
    cfgData.panePreviewHeight       = ui->panePreviewHeight->value();

    // Autoimport settings
    cfgData.autoImportMode          = CfgData::AutoImportMode(ui->autoImportMode->currentIndex());
    cfgData.autoImportDir           = ui->autoImportDir->text();
    cfgData.autoImportPattern       = ui->autoImportPattern->text();
    cfgData.autoImportPost          = CfgData::AutoImportPost(ui->autoImportPost->currentIndex());
    cfgData.autoImportBackupSuffix  = ui->autoImportBackupSuffix->text();
    cfgData.autoImportBackupDir     = ui->autoImportBackupDir->text();
    cfgData.autoImportCommand       = ui->autoImportCommand->text();
    cfgData.autoImportStdout        = ui->autoImportStdout->isChecked();
    cfgData.autoImportTimeout       = ui->autoImportTimeout->value();
    cfgData.autoImportTags          = getAutoImportTags();

    // If this state changed, we have to clear the colorization cached.
    if (cfgData.colorizeTagIcons != ui->colorizeTagIcons->isChecked())
        cfgData.svgColorizer.clear();

    // Track Icons
    cfgData.trackNoteIcon           = trackNoteIcon;
    cfgData.brokenIcon              = brokenIcon;
    cfgData.filterEmptyIconName     = filterEmptyIconName;
    cfgData.filterValidIconName     = filterValidIconName;
    cfgData.filterInvalidIconName   = filterInvalidIconName;
    cfgData.colorizeTagIcons        = ui->colorizeTagIcons->isChecked();
    cfgData.iconSizeTrack           = QSize(ui->iconSizeTrack->value(),
                                            ui->iconSizeTrack->value() * 100 / 145);
    cfgData.flagSizeTrack           = QSize(ui->flagSizeTrack->value(),
                                            ui->flagSizeTrack->value() * 100 / 145);
    cfgData.iconSizeView            = QSize(ui->iconSizeView->value(),
                                            ui->iconSizeView->value() * 100 / 145);
    cfgData.iconSizeTag             = QSize(ui->iconSizeTag->value(),
                                            ui->iconSizeTag->value() * 100 / 145);
    cfgData.iconSizeFilter          = QSize(ui->iconSizeFilter->value(),
                                            ui->iconSizeFilter->value() * 100 / 145);
    cfgData.iconSizeClimb           = QSize(ui->iconSizeClimb->value(),
                                            ui->iconSizeClimb->value() * 100 / 145);

    // Units
    cfgData.unitsTrkLength.setIdx(ui->unitsTrkLength->currentIndex(), Format::_DistNonAuto);
    cfgData.unitsLegLength.setIdx(ui->unitsLegLength->currentIndex(), Format::_DistNonAuto);
    cfgData.unitsDuration.setIdx(ui->unitsDuration->currentIndex());
    cfgData.unitsTrkDate.setIdx(ui->unitsTrkDate->currentIndex());
    cfgData.unitsTrkTime.setIdx(ui->unitsTrkTime->currentIndex());
    cfgData.unitsPointDate.setIdx(ui->unitsPointDate->currentIndex());
    cfgData.unitsTz.setIdx(ui->unitsTz->currentIndex());
    cfgData.unitsElevation.setIdx(ui->unitsElevation->currentIndex());
    cfgData.unitsLat.setIdx(ui->unitsLatLon->currentIndex());
    cfgData.unitsLon.setIdx(ui->unitsLatLon->currentIndex());
    cfgData.unitsSpeed.setIdx(ui->unitsSpeed->currentIndex());
    cfgData.unitsAccel.setIdx(ui->unitsAccel->currentIndex());
    cfgData.unitsClimb.setIdx(ui->unitsClimb->currentIndex());
    cfgData.unitsArea.setIdx(ui->unitsArea->currentIndex());
    cfgData.unitsTemp.setIdx(ui->unitsTemp->currentIndex());
    cfgData.unitsSlope.setIdx(ui->unitsSlope->currentIndex());
    cfgData.unitsPower.setIdx(ui->unitsPower->currentIndex());
    cfgData.unitsEnergy.setIdx(ui->unitsEnergy->currentIndex());
    cfgData.unitsWeight.setIdx(ui->unitsWeight->currentIndex());
    cfgData.unitsPct.setIdx(ui->unitsPct->currentIndex());
    cfgData.unitsCad.setIdx(ui->unitsCad->currentIndex());
    cfgData.unitsHr.setIdx(ui->unitsHr->currentIndex());

    cfgData.unitsTrkLength.setPrecision(ui->unitsFmtTrkLength->value());
    cfgData.unitsLegLength.setPrecision(ui->unitsFmtLegLength->value());
    cfgData.unitsDuration.setPrecision(ui->unitsFmtDuration->value());
    cfgData.unitsElevation.setPrecision(ui->unitsFmtElevation->value());
    cfgData.unitsLat.setPrecision(ui->unitsFmtLatLon->value());
    cfgData.unitsLon.setPrecision(ui->unitsFmtLatLon->value());
    cfgData.unitsSpeed.setPrecision(ui->unitsFmtSpeed->value());
    cfgData.unitsAccel.setPrecision(ui->unitsFmtAccel->value());
    cfgData.unitsClimb.setPrecision(ui->unitsFmtClimb->value());
    cfgData.unitsArea.setPrecision(ui->unitsFmtArea->value());
    cfgData.unitsTemp.setPrecision(ui->unitsFmtTemp->value());
    cfgData.unitsSlope.setPrecision(ui->unitsFmtSlope->value());
    cfgData.unitsPower.setPrecision(ui->unitsFmtPower->value());
    cfgData.unitsEnergy.setPrecision(ui->unitsFmtEnergy->value());
    cfgData.unitsWeight.setPrecision(ui->unitsFmtWeight->value());
    cfgData.unitsPct.setPrecision(ui->unitsFmtPct->value());
    cfgData.unitsCad.setPrecision(ui->unitsFmtCad->value());
    cfgData.unitsHr.setPrecision(ui->unitsFmtHr->value());

    cfgData.unitsDuration.setLeadingZeros(ui->unitsPadDuration->isChecked());
    cfgData.unitsLat.setLeadingZeros(ui->unitsPadLatLon->isChecked());
    cfgData.unitsLon.setLeadingZeros(ui->unitsPadLatLon->isChecked());

    cfgData.unitsTrkDate.setUTC(ui->trackDsUTC->isChecked());
    cfgData.unitsTrkTime.setUTC(ui->trackDsUTC->isChecked()); // TIMEstamp UTC from DATEstamp config
    cfgData.unitsPointDate.setUTC(ui->pointDsUTC->isChecked());

    cfgData.trkPtLineWidth   = float(ui->trkPtLineWidth->value());
    cfgData.trkPtMarkerWidth = float(ui->trkPtMarkerWidth->value());
    cfgData.trkPtRangeWidth  = float(ui->trkPtRangeWidth->value());
    cfgData.trkPtMarkerColor = Util::GetTBColor(ui->trkPtMarkerColor);
    cfgData.trkPtRangeColor  = Util::GetTBColor(ui->trkPtRangeColor);

    cfgData.asMaxDateSpans = ui->asMaxDateSpans->value();
    cfgData.asBarWidth     = ui->asBarWidth->value();

    // Use prevCfgData so we set with the same units the UI is configured for
    cfgData.hillMinGrade    = prevCfgData().unitsSlope.fromDouble(ui->hillMinGrade->value());
    cfgData.hillMinHeight   = prevCfgData().unitsClimb.fromDouble(ui->hillMinHeight->value());
    cfgData.hillGradeLength = prevCfgData().unitsLegLength.fromDouble(ui->hillGradeLength->value());

    // Backups and files
    cfgData.backupUICount        = ui->backupUICount->value();
    cfgData.dataAutosaveInterval = ui->dataAutosaveInterval->value();
    cfgData.dataAutosavePath     = ui->dataSavePathEdit->text();

    // Models
    cfgData.tags           = tags;
    cfgData.people         = people;
    cfgData.zones          = zones;
    cfgData.trackColorizer = trackColorizer;
    cfgData.pointColorizer = pointColorizer;
    cfgData.climbColorizer = climbColorizer;
    cfgData.uiColor        = uiColor;
    cfgData.trkPtColor     = trkPtColor;

    // Our own icon size!
    ui->tagTreeView->setIconSize(cfgData.iconSizeTag);

    cfgData.applyGlobal();
}

void AppConfig::showPage(Page page)
{
    show();

    if (int(page) >= 0 && int(page) < ui->appCfgTabs->count())
        ui->appCfgTabs->setCurrentIndex(int(page));
}

QByteArray AppConfig::pageIcon(Page page)
{
    switch (page) {
    case Page::GeneralUI:      return "interface";
    case Page::GeneralBackup:  return "backup";
    case Page::GeneralUndo:    return "edit-undo";
    case Page::Icons:          return "preferences-desktop-icons";
    case Page::MapDisplay:     return Pane::iconFile(PaneClass::Map);
    case Page::Tags:           return "tag";
    case Page::People:         return "system-users";
    case Page::Zones:          return Pane::iconFile(PaneClass::ZoneSummary);
    case Page::AutoImport:     return "document-import";
    case Page::UnitsDist:      return "ruler";
    case Page::UnitsMisc:      return "measure";
    case Page::UnitsPower:     return "utilities-energy-monitor";
    case Page::UnitsTime:      return "clock";
    case Page::GraphsTrack:    return Pane::iconFile(PaneClass::LineChart);
    case Page::GraphsActivity: return Pane::iconFile(PaneClass::ActivitySummary);
    case Page::GraphsClimb:    return Pane::iconFile(PaneClass::ClimbAnalysis);
    case Page::TrackColorizer: return Pane::iconFile(PaneClass::Track);
    case Page::PointColorizer: return Pane::iconFile(PaneClass::Points);
    case Page::ClimbColorizer: return Pane::iconFile(PaneClass::ClimbAnalysis);
    default:                   assert(0); return "";
    }
}

void AppConfig::acceptInteractive()
{
    if (!applyTagRenaming())
        return;
    AppConfigBase::acceptInteractive();
}

void AppConfig::selectDir(const QString& caption, QLineEdit* lineEdit, const QString& defaultDir)
{
    if (lineEdit == nullptr)
        return;

    const QString defaultOrRoot = defaultDir.isEmpty() ? QDir::rootPath() : defaultDir;
    const QString startDir      = lineEdit->text().isEmpty() ? defaultOrRoot : lineEdit->text();

    const QString dir = QFileDialog::getExistingDirectory(this, caption, startDir);

    if (dir.isEmpty())
        return mainWindow.statusMessage(UiType::Warning, tr("Canceled."));

    lineEdit->setText(dir);
}

void AppConfig::on_appCfgButtons_clicked(QAbstractButton* button)
{
    appCfgButtons(ui->appCfgButtons, button);
}

// Apply tag renamings & deletions to tracks
bool AppConfig::applyTagRenaming()
{
    const auto oldToNew = tags.newIdNames(m_prevCfgData.tags);

    TrackModel& tracks = app().trackModel();

    // No tracks.
    if (tracks.rowCount() == 0)
        return true;

    // See if there are tags in use, and ask what to do.

    int tracksWithRenaming = 0;
    int tracksWithDeletion = 0;
    QSet<QString> inUseDeleted;
    QSet<QString> inUseRenamed;

    Util::recurse(tracks, [&](const QModelIndex& idx) {  // For each track
        auto tags = tracks.data(TrackModel::Tags, idx, Util::RawDataRole).value<QStringList>();
        for (const auto& tag : tags) {
            if (const auto it = oldToNew.find(tag); it != oldToNew.end()) { // see if it's in rename set
                if (it.value().isNull()) {
                    inUseDeleted.insert(it.key());
                    ++tracksWithDeletion;
                } else {
                    inUseRenamed.insert(it.key());
                    ++tracksWithRenaming;
                }
            }
        }
    });

    // Nothing matters to the tracks we have.
    if (tracksWithRenaming == 0 && tracksWithDeletion == 0)
        return true;

    TagRenameDialog renameOpt(inUseRenamed, inUseDeleted, this);
    if (renameOpt.exec() != QDialog::Accepted)
        return false;

    if (renameOpt.isNoOp())
        return true;

    // Batch-rename track tags.  This is a little expensive.
    MainWindow::SaveCursor cursor(&mainWindow, Qt::WaitCursor);

    Util::recurse(tracks, [&tracks, &renameOpt, &oldToNew](const QModelIndex& idx) {  // For each track
        auto tags = tracks.data(TrackModel::Tags, idx, Util::RawDataRole).value<QStringList>();
        bool updated = false;

        for (auto tag = tags.begin(); tag != tags.end(); ) {  // for each tag in track
            bool deleted = false;

            if (const auto it = oldToNew.find(*tag); it != oldToNew.end()) { // see if it's in rename set
                if (!it.value().isNull()) {
                    if (renameOpt.tagRenameUpdate()) {
                        *tag = it.value();   // it was renamed: update the name.
                        updated = true;
                    } else if (renameOpt.tagRenameRemove()) {
                        tag = tags.erase(tag); // it was renamed: requested removal.
                        deleted = updated = true;
                    }
                } else {
                    if (renameOpt.tagRemoveRemove()) {
                        // TODO: option to apply other tag
                        tag = tags.erase(tag); // it was removed: erase it.
                        deleted = updated = true;
                    }
                }
            }

            if (!deleted)
                ++tag;
        }

        // Replace tag list, if we made any changes
        if (updated) {
            tags.removeDuplicates();
            tracks.setData(TrackModel::Tags, idx, tags, Util::RawDataRole);
        }
    });

    return true;
}

void AppConfig::addNewTag(bool category, const TreeItem::ItemData& data)
{
    const auto addAndFocus = [this, category, &data](const QModelIndex& parent) {
        tags.appendRow(category, data, parent);
        const QModelIndex newIdx = tags.index(tags.rowCount(parent) - 1, 0, parent);
        ui->tagTreeView->setCurrentIndex(newIdx);
        ui->tagTreeView->edit(newIdx); // enter edit mode
    };

    const QModelIndex currentSeletion = ui->tagTreeView->currentIndex();
    const QModelIndex currentModel = Util::MapDown(currentSeletion);

    ui->tagTreeView->setExpanded(currentSeletion, true);

    if (!currentModel.isValid() || tags.isCategory(currentModel)) {
        addAndFocus(currentModel);
        return;
    }

    addAndFocus(tags.parent(currentModel));
}

void AppConfig::on_addTag_clicked()
{
    addNewTag(false);
}

void AppConfig::on_addTagHeader_clicked()
{
    addNewTag(true, {"New Category"});
}

void AppConfig::on_delTag_clicked()
{
    if (ui->tagTreeView->selectionModel() == nullptr)
        return;

    tags.removeRows(ui->tagTreeView->selectionModel());
}

void AppConfig::getIcon(IconSelector& selector, QString& file, QToolButton* b)
{
    selector.setGeometry(Util::MapOnScreen(b, b->pos(), selector.size()));
    selector.setCurrentPath(file);

    if (selector.exec() == QDialog::Accepted) {
        b->setIcon(selector.icon());
        file = selector.iconFile();
    }
}

void AppConfig::on_defaultPointIcon_clicked()
{
    getIcon(*pointIconSelector, defaultPointIcon, ui->defaultPointIcon);
}

void AppConfig::on_selectedPointIcon_clicked()
{
    getIcon(*pointIconSelector, selectedPointIcon, ui->selectedPointIcon);
}

void AppConfig::on_currentPointIcon_clicked()
{
    getIcon(*pointIconSelector, currentPointIcon, ui->currentPointIcon);
}

void AppConfig::on_gpsdLivePointIcon_clicked()
{
    getIcon(*pointIconSelector, gpsdLivePointIcon, ui->gpsdLivePointIcon);
}

void AppConfig::on_waypointDefaultIcon_clicked()
{
    getIcon(*pointIconSelector, waypointDefaultIcon, ui->waypointDefaultIcon);
}

void AppConfig::on_action_Next_Tab_triggered()
{
    Util::NextTab(ui->appCfgTabs);
}

void AppConfig::on_action_Prev_Tab_triggered()
{
    Util::PrevTab(ui->appCfgTabs);
}

void AppConfig::on_trackNoteIcon_clicked()
{
    getIcon(*iconIconSelector, trackNoteIcon, ui->trackNoteIcon);
}

void AppConfig::on_brokenIcon_clicked()
{
    getIcon(*iconIconSelector, brokenIcon, ui->brokenIcon);
}

void AppConfig::on_peopleAdd_clicked()
{
    people.appendRow({"New Person", 60.0_kg, 0.22});
    const QModelIndex newIdx = people.index(people.rowCount() - 1, PersonModel::Name);

    ui->peopleTreeView->setCurrentIndex(newIdx);
    ui->peopleTreeView->edit(newIdx); // enter edit mode
}

void AppConfig::on_peopleRemove_clicked()
{
    if (ui->peopleTreeView->selectionModel() == nullptr)
        return;

    people.removeRows(ui->peopleTreeView->selectionModel());
}

void AppConfig::on_peopleReset_clicked()
{
    if (ui->peopleTreeView->selectionModel() == nullptr)
        return;

    people.resetRows(ui->peopleTreeView->selectionModel());
}

void AppConfig::on_filterValidIcon_clicked()
{
    getIcon(*iconIconSelector, filterValidIconName, ui->filterValidIcon);
}

void AppConfig::on_filterInvalidIcon_clicked()
{
    getIcon(*iconIconSelector, filterInvalidIconName, ui->filterInvalidIcon);
}

void AppConfig::on_filterEmptyIcon_clicked()
{
    getIcon(*iconIconSelector, filterEmptyIconName, ui->filterEmptyIcon);
}

void AppConfig::on_trkPtMarkerColor_clicked()
{
    Util::SetTBColor(ui->trkPtMarkerColor,
               QColorDialog::getColor(cfgData().trkPtMarkerColor,
                                      this, tr("Track Line Pane marker color")));
}

void AppConfig::on_trkPtRangeColor_clicked()
{
    Util::SetTBColor(ui->trkPtRangeColor,
               QColorDialog::getColor(cfgData().trkPtRangeColor,
                                      this, tr("Track Line Pane selected range color")));
}

void AppConfig::on_outlineTrackColor_clicked()
{
    Util::SetTBColor(ui->outlineTrackColor,
               QColorDialog::getColor(cfgData().outlineTrackColor,
                                      this, tr("Outline track color")));
}

void AppConfig::on_unassignedTrackColor_clicked()
{
    Util::SetTBColor(ui->unassignedTrackColor,
               QColorDialog::getColor(cfgData().unassignedTrackColor,
                                      this, tr("Unassigned track color")));
}

void AppConfig::on_dataSavePathSelect_clicked()
{
    selectDir(tr("GPS Data Save Directory"), ui->dataSavePathEdit, mainWindow.currentSettingsDirectory());
}

void AppConfig::on_sortTags_clicked()
{
    tags.sort(TagModel::Name, QModelIndex());
    // Work around an unknown bug with TreeModel::sort's dataChanged events not updating the view.
    ui->tagTreeView->collapseAll();
    ui->tagTreeView->expandAll();
}

void AppConfig::on_zonesAdd_clicked()
{
    zones.appendRow();
    const QModelIndex newIdx = zones.index(zones.rowCount() - 1, ZoneModel::Name);

    zones.setData(newIdx, tr("New Zone..."), Util::RawDataRole);
    ui->zonesTreeView->setCurrentIndex(newIdx);
    ui->zonesTreeView->edit(newIdx); // enter edit mode
}

void AppConfig::on_zonesDefaults_activated(int index)
{
    ui->zonesDefaults->setCurrentIndex(0); // re-show title

    zones.setPresetModel(index - 1); // -1 to skip the title entry

    Util::ResizeViewForData(*ui->zonesTreeView);
}

void AppConfig::on_zonesRemove_clicked()
{
    if (ui->zonesTreeView->selectionModel() == nullptr)
        return;

    zones.removeRows(ui->zonesTreeView->selectionModel());
}

void AppConfig::zoneDataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight, const QVector<int>&)
{
    // Return if percent column unchanged
    if (topLeft.column() > ZoneModel::FTPPct ||
        bottomRight.column() < ZoneModel::MaxHRPct)
        return;

    zones.sort(topLeft.column(), QModelIndex(), Qt::AscendingOrder, Util::RawDataRole);

    for (const auto column : { ZoneModel::MaxHRPct, ZoneModel::FTPPct }) {
        float priorValue = -1.0;

        // ensure all data is in order, with nothing equal to the prior entry
        Util::recurse(zones, [this, &priorValue, column](const QModelIndex& idx) {
            float value = zones.data(column, idx, Util::RawDataRole).toFloat();
            if (value <= priorValue) {
                value = priorValue;
                zones.setData(column, idx, value, Util::RawDataRole);
            }
            priorValue = value;
        });
    }
}

void AppConfig::changePage(const QModelIndex& idx)
{
    if (ui == nullptr)
        return;

    showPage(Page(tocResource(idx).toInt()));
}

void AppConfig::setAutoImportEnabledState()
{
    const auto autoImportMode = CfgData::AutoImportMode(ui->autoImportMode->currentIndex());
    const auto autoImportPost = CfgData::AutoImportPost(ui->autoImportPost->currentIndex());

    const bool isDisabled = (autoImportMode == CfgData::AutoImportMode::Disabled);
    const bool hasCommand = !ui->autoImportCommand->text().isEmpty();

    // Set disabled states appropriately.
    ui->autoImportDir->setDisabled(isDisabled);
    ui->labelAutoImportDir->setDisabled(isDisabled);

    ui->autoImportPattern->setDisabled(isDisabled);
    ui->labelAutoImportPattern->setDisabled(isDisabled);

    ui->autoImportTagButton->setDisabled(isDisabled);
    ui->labelAutoImportTags->setDisabled(isDisabled);

    ui->autoImportPost->setDisabled(isDisabled);
    ui->labelAutoImportPost->setDisabled(isDisabled);

    const bool disableBackupSuffix = isDisabled || autoImportPost != CfgData::AutoImportPost::Backup;
    ui->autoImportBackupSuffix->setDisabled(disableBackupSuffix);
    ui->labelAutoImportBackupSuffix->setDisabled(disableBackupSuffix);

    const bool disableBackupDir = isDisabled || autoImportPost != CfgData::AutoImportPost::Move;
    ui->autoImportBackupDir->setDisabled(disableBackupDir);
    ui->labelAutoImportBackupDir->setDisabled(disableBackupDir);

    ui->autoImportCommand->setDisabled(isDisabled);
    ui->labelAutoImportCommand->setDisabled(isDisabled);

    ui->autoImportStdout->setDisabled(isDisabled || !hasCommand);
    ui->labelAutoImportStdout->setDisabled(isDisabled || !hasCommand);

    ui->autoImportTimeout->setDisabled(isDisabled || !hasCommand);
    ui->labelAutoImportTimeout->setDisabled(isDisabled || !hasCommand);

    ui->autoImportDirSelect->setEnabled(ui->autoImportDir->isEnabled());
    ui->autoImportBackupDirSelect->setEnabled(ui->autoImportBackupDir->isEnabled());

    // Reset colors on these so that we stop using any active sylesheet, which may be
    // displaying red text for errors, etc.
    redIfNotFound(ui->autoImportDir);
    redIfNotFound(ui->autoImportBackupDir);
    redIfNonPosix(ui->autoImportCommand);

    // Disable the labels.
    Util::SetEnabled(ui->autoImportTagFrameHBox, !isDisabled);
}

void AppConfig::setAutoImportTags(const QStringList& tags)
{
    // Clear out old tag buttons. Keep 1 item on right (the spacer).
    Util::ClearLayout(ui->autoImportTagFrameHBox, 0, 1);

    // Add new buttons for each tag
    for (const QString& tag : tags) {
        const QIcon icon(cfgData().tags.tagIconName(tag));
        auto* label = new QLabel();
        const QSize actualSize = icon.actualSize(QSize(cfgData().iconSizeTrack));

        label->setPixmap(icon.pixmap(actualSize));
        label->setProperty(tagProperty, tag);
        label->setToolTip(tag);

        // Insert one item BEFORE the end, so annoyingly, we cannot use -1 for the position.
        ui->autoImportTagFrameHBox->insertWidget(ui->autoImportTagFrameHBox->count() - 1, label);
    }
}

QStringList AppConfig::getAutoImportTags() const
{
    QStringList tags;
    tags.reserve(ui->autoImportTagFrameHBox->count());

    // Last position = spacer, so skip that.
    for (int i = 0; i < ui->autoImportTagFrameHBox->count() - 1; ++i)
        if (const auto* w = ui->autoImportTagFrameHBox->itemAt(i)->widget(); w != nullptr)
            tags.append(w->property(tagProperty).toString());

    return tags;
}

void AppConfig::redOnBad(QLineEdit* lineEdit, const std::function<bool(const QLineEdit*)>& test) const
{
    // Reset the style sheet if lineEdit is empty, to preserve placeholder color
    if (lineEdit->text().isEmpty() || !lineEdit->isEnabled() || test(lineEdit)) {
        lineEdit->setStyleSheet(QString());
        return;
    }

    Util::SetWidgetStyle(lineEdit, uiColor[UiType::Error]);
}

void AppConfig::redIfNotFound(QLineEdit* lineEdit) const
{
    redOnBad(lineEdit, [](const QLineEdit* lineEdit) {
        return QDir(lineEdit->text()).exists();
    });
}

void AppConfig::redIfNonPosix(QLineEdit* lineEdit) const
{
    // This is hilariously heavy for a per-keystroke thing to be doing, but computers are now fast enough
    // that it works out fine.
    redOnBad(lineEdit, [](const QLineEdit* lineEdit) {
        return Util::PosixExpander(lineEdit->text()).rc() == 0;
    });
}

void AppConfig::on_autoImportMode_currentIndexChanged(int)
{
    setAutoImportEnabledState();
}

void AppConfig::on_autoImportPost_currentIndexChanged(int)
{
    setAutoImportEnabledState();
}

void AppConfig::on_autoImportDirSelect_clicked()
{
    selectDir(tr("Auto Import Directory"), ui->autoImportDir);
}

void AppConfig::on_autoImportBackupDirSelect_clicked()
{
    selectDir(tr("Auto Import Backup Directory"), ui->autoImportBackupDir);
}

void AppConfig::on_autoImportTagButton_clicked()
{
    TagSelectorDialog tagSelector(mainWindow, this);

    tagSelector.setTags(getAutoImportTags());

    if (tagSelector.exec() != QDialog::Accepted)
        return mainWindow.statusMessage(UiType::Warning, tr("Canceled."));

    setAutoImportTags(tagSelector.tags());
}

void AppConfig::on_autoImportTimeout_valueChanged(int value)
{
    ui->labelAutoImportTimeout->setText(QString(tr("Timeout (%1 sec)").arg(value)));
}
