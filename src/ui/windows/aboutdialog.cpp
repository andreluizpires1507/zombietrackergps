/*
    Copyright 2019-2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QApplication>

#include <src/util/ui.h>
#include <src/util/icons.h>

#include "src/core/app.h"
#include "src/core/builddate.h"
#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    AboutBase(parent),
    ui(nullptr)
{
}

AboutDialog::~AboutDialog()
{
    delete ui;
}

void AboutDialog::showEvent(QShowEvent* event)
{
    setup();
    AboutBase::showEvent(event);
}

void AboutDialog::setup()
{
    if (ui != nullptr)
        return;

    ui = new Ui::AboutDialog;
    ui->setupUi(this);

    setupPrevNext(ui->aboutTabs, ui->action_Next_Tab, ui->action_Prev_Tab);
    Util::SetupWhatsThis(this);
    setupActionIcons();
    setupAppTitle();
    setupZtgpsUrls();
}

void AboutDialog::setupActionIcons()
{
    Icons::defaultIcon(ui->action_Next_Tab, "go-next");
    Icons::defaultIcon(ui->action_Prev_Tab, "go-previous");
}

void AboutDialog::setupAppTitle()
{
    const QString appTitleText =
            QString("<html><head/><body><p><span style=\" font-size:18pt; font-style:italic;\">") +
            QApplication::applicationDisplayName() + " " + QApplication::applicationVersion() +
            "</span></p></body></html>";

    ui->aboutZTTitle_1->setText(appTitleText);
    ui->aboutZTTitle_2->setText(appTitleText);
    ui->buildDate->setText(tr("build: ") + BuildDate);
}

void AboutDialog::setupZtgpsUrls()
{
    QString html = ui->aboutTabSupport->toHtml();

    // replace placeholder with our URL
    ui->aboutTabSupport->setHtml(html.replace("${ZTGPSWWW}", app().ZtgpsWWW(App::WWW::Donations).toString()));;
}
