/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <src/core/settings.h>

#include "src/geo-io/geoio.h" // for GeoFormat
#include "src/geo-io/geoioparams.h"

namespace Ui {
class ExportDialog;
} // namespace Ui

enum class GeoIoFeature;

class ExportDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit ExportDialog(QWidget *parent = nullptr);
    ~ExportDialog() override;

    GeoSaveParams geoSaveParams() const;

    QString   getExportFileName(const char* filter = nullptr);

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private:
    GeoFormat    exportFormat()   const;
    GeoIoFeature features()       const;
    bool         writeFormatted() const;
    int          indentLevel()    const;
    bool         indentSpaces()   const;
    bool         exportAllTrk()   const;
    bool         exportAllWpt()   const;

    static const QString&      filterTrk();
    static const QString&      filterWpt();
    static Qt::CaseSensitivity filterCase();

    Ui::ExportDialog *ui;
    QString           defaultDir;  // which directory to show
};

#endif // EXPORTDIALOG_H
