/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DEVICEDIALOG_H
#define DEVICEDIALOG_H

#include <QDialog>

namespace Ui {
class DeviceDialog;
} // namespace Ui

class MainWindow;
class GpsDevicePane;

class DeviceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeviceDialog(MainWindow&);
    ~DeviceDialog() override;

private slots:
    void on_importButton_clicked();
    void processSelectionChanged();
    void processDoubleClick();

private:
    void setupSignals();
    void deviceImport();

    Ui::DeviceDialog* ui;
    MainWindow&       m_mainWindow;
    GpsDevicePane*    m_gpsDeviceList; // UI adopts, so we don't free it.
};

#endif // DEVICEDIALOG_H
