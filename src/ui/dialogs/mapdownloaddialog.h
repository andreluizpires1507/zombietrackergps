/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MAPDOWNLOADDIALOG_H
#define MAPDOWNLOADDIALOG_H

#include <QDialog>
#include <QTimer>
#include <QVector>

#include <src/core/settings.h>

namespace Ui {
class MapDownloadDialog;
} // namespace Ui

namespace Marble {
class GeoDataCoordinates;
class GeoDataLatLonAltBox;
class GeoDataLatLonBox;
class TileCoordsPyramid;
} // namespace Marble

class MainWindow;
class CfgData;
class MapPane;
class QPoint;
class QRect;

class MapDownloadDialog final : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit MapDownloadDialog(const MainWindow&);
    ~MapDownloadDialog() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_tileMin_valueChanged(int);
    void on_tileMax_valueChanged(int);
    void on_degN_valueChanged(double);
    void on_degS_valueChanged(double);
    void on_degW_valueChanged(double);
    void on_degE_valueChanged(double);
    void on_btnVisibleRegion_clicked();
    void on_btnSpecifiedArea_clicked();
    void on_levelRefresh_clicked();
    void on_buttonBox_clicked(QAbstractButton*);

    void updateToMapBounds();
    void setBounds(const Marble::GeoDataLatLonBox&);
    void mapVisibleAreaChanged(const Marble::GeoDataLatLonAltBox&);
    void updateTileCount();

private:
    static const constexpr double delta       = 0.0001; // minimum min/max lat/lon separation
    static const constexpr int noteTilesCount =   5000; // change color past this level
    static const constexpr int warnTilesCount =  10000; // warn user past this level
    static const constexpr int maxTilesCount  =  25000; // deny download past this level
    static const constexpr int minTileLevel   =      1; // minimum zoom level
    static const constexpr int maxTileLevel   =     18; // maximum zoom level

    void showEvent(QShowEvent*) override;
    void hideEvent(QHideEvent*) override;

    int  maxTileLevelFromMap() const;
    int  minTileLevelFromMap() const;
    void setupActionIcons();
    void setupTimers();
    void updateSignals();
    void disconnectSignals();
    void updateWidgets();
    void resetWidgetBounds();
    void updateCurrentTileZoom();
    void deferredTileCountRecalc(); // kick this off based on a timer

    static QPoint pointFromCoords(double latDeg, double lonDeg, int level); // converts to Marble's required integer coordinates
    QRect rectFromArea(int level) const;
    auto requestedRegion() const;
    static qint64 tileCount(const QVector<Marble::TileCoordsPyramid>&);
    bool download(); // download from current view

    QTimer                 m_tileCountTimer;
    const MainWindow&      m_mainWindow;
    MapPane*               m_lastMapPane;
    Ui::MapDownloadDialog *ui;
};

#endif // MAPDOWNLOADDIALOG_H
