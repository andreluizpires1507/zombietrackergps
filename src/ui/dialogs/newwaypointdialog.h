/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NEWWAYPOINTDIALOG_H
#define NEWWAYPOINTDIALOG_H

#include <marble/GeoDataCoordinates.h>

#include <QDialog>
#include <QString>

#include <src/core/settings.h>

namespace Ui {
class NewWaypointDialog;
} // namespace Ui

namespace Marble {
class GeoDataCoordinates;
} // namespace Marble

class IconSelector;
class MainWindow;

class NewWaypointDialog : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit NewWaypointDialog(MainWindow&);
    ~NewWaypointDialog() override;

    // Exec, using this latitude and longitude.  If "genName" is true, will suggest
    // a waypoint name
    int exec(double lat, double lon, double ele, bool genName = true);
    // Form taking vector of inputs
    int exec(const QVector<Marble::GeoDataCoordinates>&, bool genName = true);

    // Accesors
    QString     wptName()     const;
    QString     wptSymbol()   const;
    QString     wptIconName() const;
    QStringList wptTags()     const;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_wptIcon_clicked();
    void on_btnRefreshName_clicked();
    void on_btnRefreshPos_clicked();
    void on_wptIconFromSymbol_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void updateWptIcon();

private:
    void showEvent(QShowEvent*) override;
    void setupSymbols();
    void setupSignals();
    void setupActionIcons();
    void setIconPath(const QString&);
    void updateSuggestedName();

    QString       m_iconPath;
    double        m_origLat;
    double        m_origLon;
    double        m_origEle;
    QVector<Marble::GeoDataCoordinates> m_points;
    IconSelector* m_iconSelector;
    MainWindow&   m_mainWindow;

    Ui::NewWaypointDialog *ui;
};

#endif // NEWWAYPOINTDIALOG_H
