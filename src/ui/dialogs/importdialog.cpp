/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include <src/util/util.h>
#include <src/util/ui.h>

#include "importdialog.h"
#include "ui_importdialog.h"

#include "src/ui/windows/mainwindow.h"
#include "src/geo-io/geoio.h"    // for GeoIoFeture
#include "src/ui/widgets/tagselector.h"

ImportDialog::ImportDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::ImportDialog),
    m_tagSelector(new TagSelector(mainWindow)),
    defaultDir(QDir::home().absolutePath())
{
    ui->setupUi(this);
    ui->autoAssignTagsGroupLayout->insertWidget(0, m_tagSelector);

    Util::SetTBColor(ui->trackColor, QRgb(0x00b155));

    setupSignals();
    Util::SetupWhatsThis(this);
}

ImportDialog::~ImportDialog()
{
    delete ui;
    // m_tagSelector is adopted by the UI, so we don't delete it ourselves.
}

GeoLoadParams ImportDialog::geoLoadParams() const
{
    return GeoLoadParams(features(), tags(), trackColor(), deduplicate(), filterTrk(), filterWpt(), filterCase());
}

GeoIoFeature ImportDialog::features() const
{
    return GeoIoFeature((ui->btnTrk->isChecked() ? int(GeoIoFeature::Trk) : 0) |
                        (ui->btnWpt->isChecked() ? int(GeoIoFeature::Wpt) : 0));
}

void ImportDialog::setupSignals()
{
    connect(ui->overrideTagColors, &QCheckBox::stateChanged, ui->trackColor, &QWidget::setEnabled);

    connect(ui->trackColor, &QToolButton::clicked, this, &ImportDialog::askTrackColor);
}

QStringList ImportDialog::tags() const
{
    return m_tagSelector->tags();
}

QColor ImportDialog::trackColor() const
{
    return ui->overrideTagColors->isChecked() ? Util::GetTBColor(ui->trackColor) : QColor();
}

bool ImportDialog::deduplicate() const
{
    return ui->dedupliateTracks->isChecked();
}

const QString& ImportDialog::filterTrk()
{
    static const QString empty;
    return empty;
}

const QString& ImportDialog::filterWpt()
{
    static const QString empty;
    return empty;
}

Qt::CaseSensitivity ImportDialog::filterCase()
{
    return Qt::CaseInsensitive;
}

ImportInfoList ImportDialog::getImportFilenames(const char* filter)
{
    const QStringList trackFiles =
            QFileDialog::getOpenFileNames(parentWidget(),
                                          tr("Import GPS Track File(s)"), defaultDir,
                                          filter,  nullptr, QFileDialog::ReadOnly);

    if (!trackFiles.isEmpty())
        defaultDir = QFileInfo(trackFiles.front()).path();

    ImportInfoList importFrom;
    importFrom.reserve(trackFiles.size());

    for (const auto& file : trackFiles)
        importFrom.append(file);

    return importFrom;
}

void ImportDialog::askTrackColor()
{
    Util::SetTBColor(ui->trackColor,
                     QColorDialog::getColor(trackColor(), this, tr("Track Override Color")));
}

void ImportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui == nullptr)
        return;

    SL::Save(settings, "trackColor",  trackColor());
    SL::Save(settings, "deduplicate", ui->dedupliateTracks);
    SL::Save(settings, "importTrk",   ui->btnTrk);
    SL::Save(settings, "importWpt",   ui->btnWpt);

    // Don't save ui->overrrideTagColors; that's auto-generated on load from
    // the valid or invalid color.
}

void ImportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui != nullptr) {
        const QColor trackColor = SL::Load(settings, "trackColor", QColor());

        Util::SetTBColor(ui->trackColor, trackColor);
        ui->overrideTagColors->setChecked(trackColor.isValid());
        SL::Load(settings, "deduplicate", ui->dedupliateTracks);
        SL::Load(settings, "importTrk",   ui->btnTrk);
        SL::Load(settings, "importWpt",   ui->btnWpt);
    }
}
