/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gpsdversiondialog.h"
#include "ui_gpsdversiondialog.h"

#include <src/util/util.h>
#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"
#include "src/gpsd/gpswrap.h"

GpsdVersionDialog::GpsdVersionDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(nullptr),
    m_headerView(Qt::Horizontal, this)
{
}

GpsdVersionDialog::~GpsdVersionDialog()
{
    delete ui;
}

void GpsdVersionDialog::showEvent(QShowEvent* event)
{
    setup();
    populateList();
    QDialog::showEvent(event);
}

void GpsdVersionDialog::setup()
{
    if (ui != nullptr)
        return;

    ui = new Ui::GpsdVersionDialog;
    ui->setupUi(this);

    Util::SetupWhatsThis(this);
    setupList();
}

void GpsdVersionDialog::populateList()
{
    m_gpsdVersions.removeRows(0, m_gpsdVersions.rowCount()); // remove old contents

    for (const auto& gpsdVer : GpsWrap::supportedVersions()) {
        auto* libName = new QStandardItem(std::get<QString>(gpsdVer));
        auto* apiVer  = new QStandardItem(QString::number(std::get<int>(gpsdVer)));
        auto* isAvail = new QStandardItem();

        apiVer->setData(Qt::AlignRight, Qt::TextAlignmentRole);
        isAvail->setCheckState(std::get<bool>(gpsdVer) ? Qt::Checked : Qt::Unchecked);

        m_gpsdVersions.appendRow({ libName, apiVer, isAvail });
    }

    Util::ResizeViewForData(*ui->gpsdVersions);
}

void GpsdVersionDialog::setupList()
{
    QTreeView& view = *ui->gpsdVersions;

    m_gpsdVersions.setHorizontalHeaderLabels({ tr("Library Name"),
                                               tr("API Version"),
                                               tr("Installed") });

    view.setModel(&m_gpsdVersions);
    view.setHeader(&m_headerView);

    setFocusProxy(&view);
}
