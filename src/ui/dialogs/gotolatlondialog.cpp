/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <marble/GeoDataCoordinates.h>

#include "gotolatlondialog.h"
#include "ui_gotolatlondialog.h"

#include <src/util/ui.h>

#include "src/ui/windows/mainwindow.h"

GotoLatLonDialog::GotoLatLonDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::GotoLatLonDialog)
{
    ui->setupUi(this);

    Util::SetupWhatsThis(this);
}

GotoLatLonDialog::~GotoLatLonDialog()
{
    delete ui;
}

Marble::GeoDataCoordinates GotoLatLonDialog::location() const
{
    return { ui->longitude->value(),
             ui->latitude->value(),
             0.0, // alt
             Marble::GeoDataCoordinates::Degree };
}

void GotoLatLonDialog::setPos(Lat_t lat, Lon_t lon)
{
    ui->latitude->setValue(qreal(lat));
    ui->longitude->setValue(qreal(lon));
}

void GotoLatLonDialog::setPos(const Marble::GeoDataCoordinates& coords)
{
    setPos(Lat_t(coords.latitude(Marble::GeoDataCoordinates::Degree)),
           Lon_t(coords.longitude(Marble::GeoDataCoordinates::Degree)));
}
