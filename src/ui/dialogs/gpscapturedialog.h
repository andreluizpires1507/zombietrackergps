/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSCAPTUREDIALOG_H
#define GPSCAPTUREDIALOG_H

#include <QDialog>

#include <src/core/settings.h>

namespace Ui {
class GpsCaptureDialog;
} // namespace Ui

class MainWindow;
class GpsCapturePane;

class GpsCaptureDialog : public QDialog, public Settings
{
    Q_OBJECT

public:
    explicit GpsCaptureDialog(MainWindow& mainWindow);
    ~GpsCaptureDialog() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private:
    void setVisible(bool visible) override;

    Ui::GpsCaptureDialog *ui;
    MainWindow&          m_mainWindow;
    GpsCapturePane*      m_gpsCaptureUi; // UI adopts, so we don't free it.
};

#endif // GPSCAPTUREDIALOG_H
