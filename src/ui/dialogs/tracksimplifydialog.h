/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKSIMPLIFYDIALOG_H
#define TRACKSIMPLIFYDIALOG_H

#include <QDialog>
#include <QTimer>

#include <src/core/settings.h>

#include "src/core/simplifiablemodel.h"

namespace Ui {
class TrackSimplifyDialog;
} // namespace Ui

class QShowEvent;

class TrackSimplifyDialog : public QDialog, public Settings
{
    Q_OBJECT

public:
    using SimplifyType = SimplifiableModel::SimplifyType;
    using Params       = SimplifiableModel::Params;
    using Mode         = SimplifiableModel::Mode;

    explicit TrackSimplifyDialog(QWidget *parent = nullptr);
    ~TrackSimplifyDialog() override;

   // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    Params simplifyParams(Mode = Mode::Execute) const; // obtain simplification params

    using QDialog::exec;
    int exec(SimplifiableModel&, const QModelIndexList&);

private slots:
    void updatePreview();
    void simplify();

private:
    friend class TestZtgps;

    // Mapped-down selections using model (not filter) indexes
    void setSelections(SimplifiableModel*, const QModelIndexList*);
    void setupSignals();
    void setupSuffixes(); // set spinner suffixes from cfgData

    SimplifyType simplifyType() const;
    Dur_t  timeS() const;      // in seconds
    Dist_t distM() const;      // in meters
    Dist_t thresholdM() const; // in meters, for adaptive filtering

    void setSimplifyType(SimplifyType);
    void setTimeS(Dur_t);                   // in seconds
    void setDistM(Dist_t distM);            // in meters
    void setThresholdM(Dist_t thresholdM);  // in meters, for adaptive filtering

    void showEvent(QShowEvent*) override;

    SimplifiableModel::Count simplifiedPointCount() const;

    Ui::TrackSimplifyDialog*   ui;
    QTimer                     m_previewTimer; // update preview text
    const QModelIndexList*     m_selections;   // selections for operation
    SimplifiableModel*         m_model;        // model to simplify
};

#endif // TRACKIMPLIFYDIALOG_H
