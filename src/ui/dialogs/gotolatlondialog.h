/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GOTOLATLONDIALOG_H
#define GOTOLATLONDIALOG_H

#include <QDialog>

#include "src/fwddecl.h"

namespace Ui {
class GotoLatLonDialog;
} // namespace Ui

namespace Marble {
class GeoDataCoordinates;
} // namespace Marble

class MainWindow;

class GotoLatLonDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GotoLatLonDialog(MainWindow& mainWindow);
    ~GotoLatLonDialog() override;

    Marble::GeoDataCoordinates location() const;

    void setPos(Lat_t, Lon_t); // set displayed lat/lon
    void setPos(const Marble::GeoDataCoordinates&);

private:
    Ui::GotoLatLonDialog *ui;
};

#endif // GOTOLATLONDIALOG_H
