/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QStandardItemModel>

#include <src/util/ui.h>

#include "mapthemedialog.h"
#include "ui_mapthemedialog.h"

MapThemeDialog::MapThemeDialog(MainWindow& mainWindow, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::MapThemeDialog),
    m_mainWindow(mainWindow),
    m_mapThemeManager(this)
{
    ui->setupUi(this);

    setupView();
    setupSignals();
    Util::SetupWhatsThis(this);
}

MapThemeDialog::~MapThemeDialog()
{
    delete ui;
}

void MapThemeDialog::setupView()
{
    if (ui == nullptr || ui->mapThemeView == nullptr)
        return;

    QTreeView& view = *ui->mapThemeView;

    view.setModel(m_mapThemeManager.mapThemeModel());
    view.setAlternatingRowColors(true);
    view.setSelectionBehavior(QAbstractItemView::SelectRows);
    view.setSelectionMode(QAbstractItemView::SingleSelection);
    view.setRootIsDecorated(false);  // act like a list
    view.setEditTriggers(QAbstractItemView::NoEditTriggers);
    view.setHeaderHidden(true);
    view.setUniformRowHeights(true);

    const qreal iconSize = view.fontInfo().pointSizeF() * 8.0;
    view.setIconSize(QSize(int(iconSize), int(iconSize)));
}

void MapThemeDialog::setupSignals()
{
    QTreeView& view = *ui->mapThemeView;

    connect(&view, &QTreeView::activated, this, &MapThemeDialog::selectThemeIndex);
}

void MapThemeDialog::selectThemeIndex(const QModelIndex& index)
{
    emit themeSelected(m_mapThemeManager.mapThemeIds().at(index.row()));
}
