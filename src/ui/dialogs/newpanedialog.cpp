/*
    Copyright 2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "newpanedialog.h"

#include "src/core/app.h"
#include "src/ui/panes/pane.h"

NewPaneDialog::NewPaneDialog(MainWindowBase& mainWindow) :
    NewPaneDialogBase(mainWindow)
{
}

NewPaneDialog::~NewPaneDialog()
{
}

void NewPaneDialog::setupModel()
{
    if (ui == nullptr) // don't do this if we're not displayed yet
        return;

    NewPaneDialogBase::setupModel();

    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc))
        app().newPaneModel().appendRow(Pane::name(pc), Pane::previewFile(pc), Pane::iconFile(pc), Pane::tooltip(pc));
}
