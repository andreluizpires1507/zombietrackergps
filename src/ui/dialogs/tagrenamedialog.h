/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGRENAMEDIALOG_H
#define TAGRENAMEDIALOG_H

#include <QSet>
#include <QDialog>

namespace Ui {
class TagRenameDialog;
} // namespace Ui

class TagRenameDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TagRenameDialog(const QSet<QString>& renames, const QSet<QString>& deletions, QWidget *parent = nullptr);
    ~TagRenameDialog() override;

    bool tagRenameUpdate() const;
    bool tagRenameLeave() const;
    bool tagRenameRemove() const;
    bool tagRemoveRemove() const;
    bool tagRemoveLeave() const;

    bool isNoOp() const { return tagRenameLeave() && tagRemoveLeave(); }

private:
    void addRenames(const QSet<QString>&);
    void addRemovals(const QSet<QString>&);

    Ui::TagRenameDialog *ui;
};

#endif // TAGRENAMEDIALOG_H
