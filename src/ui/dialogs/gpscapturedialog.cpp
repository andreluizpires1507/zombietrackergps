/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gpscapturedialog.h"
#include "ui_gpscapturedialog.h"

#include <src/util/ui.h>

#include "src/ui/panes/gpscapturepane.h"
#include "src/ui/windows/mainwindow.h"

GpsCaptureDialog::GpsCaptureDialog(MainWindow& mainWindow) :
    QDialog(&mainWindow),
    ui(new Ui::GpsCaptureDialog),
    m_mainWindow(mainWindow),
    m_gpsCaptureUi(new GpsCapturePane(mainWindow, this))
{
    ui->setupUi(this);

    Util::SetupWhatsThis(this);

    ui->verticalLayout->insertWidget(0, m_gpsCaptureUi);
}

GpsCaptureDialog::~GpsCaptureDialog()
{
    delete ui;
    // m_gpsCaptureUi is adopted by the UI, so we don't delete it ourselves.
}

void GpsCaptureDialog::save(QSettings& settings) const
{
    if (m_gpsCaptureUi != nullptr)
        m_gpsCaptureUi->save(settings);
}

void GpsCaptureDialog::load(QSettings& settings)
{
    if (m_gpsCaptureUi != nullptr)
        m_gpsCaptureUi->load(settings);
}

void GpsCaptureDialog::setVisible(bool visible)
{
    QDialog::setVisible(visible);
}
