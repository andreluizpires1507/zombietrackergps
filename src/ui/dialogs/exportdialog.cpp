/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>

#include <src/util/ui.h>

#include "src/geo-io/geoio.h"    // for GeoIoFeture
#include "src/geo-io/geoiogpx.h" // just for the default name

#include "exportdialog.h"
#include "ui_exportdialog.h"

ExportDialog::ExportDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ExportDialog)
{
    ui->setupUi(this);
    Util::SetupWhatsThis(this);

    ui->exportFormat->addItems(GeoSave::formatNames());
    ui->exportFormat->setCurrentText(GeoLoadGpx::name);

    defaultDir = QDir::home().absolutePath();
}

ExportDialog::~ExportDialog()
{
    delete ui;
}

GeoSaveParams ExportDialog::geoSaveParams() const
{
    return GeoSaveParams(exportFormat(), features(), writeFormatted(), indentLevel(), indentSpaces(),
                         exportAllTrk(), exportAllWpt(), filterTrk(), filterWpt(), filterCase());
}

GeoFormat ExportDialog::exportFormat() const
{
    return GeoFormat(ui->exportFormat->currentIndex() + 1);  // +1 skips native, which we don't offer.
}

GeoIoFeature ExportDialog::features() const
{
    return GeoIoFeature((!ui->writeNoTrk->isChecked() ? int(GeoIoFeature::Trk) : 0) |
                        (!ui->writeNoWpt->isChecked() ? int(GeoIoFeature::Wpt) : 0));
}

bool ExportDialog::writeFormatted() const
{
    return ui->writeFormatted->isChecked();
}

int ExportDialog::indentLevel() const
{
    return ui->indentLevel->value();
}

bool ExportDialog::indentSpaces() const
{
    return ui->writeSpaces->isChecked();
}

bool ExportDialog::exportAllTrk() const
{
    return ui->writeAllTrk->isChecked();
}

bool ExportDialog::exportAllWpt() const
{
    return ui->writeAllWpt->isChecked();
}

const QString& ExportDialog::filterTrk()
{
    static const QString empty;
    return empty;
}

const QString& ExportDialog::filterWpt()
{
    static const QString empty;
    return empty;
}

Qt::CaseSensitivity ExportDialog::filterCase()
{
    return Qt::CaseInsensitive;
}

QString ExportDialog::getExportFileName(const char* filter)
{
    const QString trackFile =
            QFileDialog::getSaveFileName(parentWidget(),
                                         tr("Export GPS Track File"), defaultDir,
                                         filter);

    if (!trackFile.isEmpty())
        defaultDir = QFileInfo(trackFile).path();

    return trackFile;
}

void ExportDialog::save(QSettings& settings) const
{
    MemberSave(settings, defaultDir);

    if (ui == nullptr)
        return;

    SL::Save(settings, "exportFormat",   ui->exportFormat->currentText());
    SL::Save(settings, "indentLevel",    indentLevel());
    SL::Save(settings, "indentSpaces",   indentSpaces());
    SL::Save(settings, "writeFormatted", ui->writeFormatted);
    SL::Save(settings, "exportAllTrk",   ui->writeAllTrk);
    SL::Save(settings, "exportSelTrk",   ui->writeSelectedTrk);
    SL::Save(settings, "exportNoTrk",    ui->writeNoTrk);
    SL::Save(settings, "exportAllWpt",   ui->writeAllWpt);
    SL::Save(settings, "exportSelWpt",   ui->writeSelectedWpt);
    SL::Save(settings, "exportNoWpt",    ui->writeNoWpt);
}

void ExportDialog::load(QSettings& settings)
{
    MemberLoad(settings, defaultDir);

    if (ui == nullptr)
        return;

    ui->exportFormat->setCurrentText(SL::Load(settings, "exportFormat", GeoLoadGpx::name));
    ui->indentLevel->setValue(SL::Load(settings, "indentLevel", 2));
    ui->writeSpaces->setChecked(SL::Load(settings, "indentSpaces", true));
    ui->writeTabs->setChecked(!SL::Load(settings, "indentSpaces", true));

    SL::Load(settings, "writeFormatted", ui->writeFormatted);
    SL::Load(settings, "exportAllTrk",   ui->writeAllTrk);
    SL::Load(settings, "exportSelTrk",   ui->writeSelectedTrk);
    SL::Load(settings, "exportNoTrk",    ui->writeNoTrk);
    SL::Load(settings, "exportAllWpt",   ui->writeAllWpt);
    SL::Load(settings, "exportSelWpt",   ui->writeSelectedWpt);
    SL::Load(settings, "exportNoWpt",    ui->writeNoWpt);
}
