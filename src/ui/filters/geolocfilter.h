/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOLOCFILTER_H
#define GEOLOCFILTER_H

#include <deque>
#include <optional>
#include <cmath>
#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataCoordinates.h>
#include <QAbstractProxyModel>
#include <QVector>
#include <QHash>

#include <src/util/util.h>
#include "src/core/geopolregion.h"

class GeoLocModel;
class QModelIndex;
class QString;
class GeoLocEntry;

class GeoLocFilter : public QAbstractProxyModel
{
public:
    GeoLocFilter(const GeoLocModel&);

    QModelIndex mapFromSource(const QModelIndex& sourceIndex) const override;
    QModelIndex mapToSource(const QModelIndex& proxyIndex) const override;

    int rowCount(const QModelIndex& proxyIndex) const override;
    int columnCount(const QModelIndex& proxyIndex) const override;

    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &idx) const override;

    void setSourceModel(QAbstractItemModel *sourceModel) override;
    void sort(int column, Qt::SortOrder) override;
    void sort() { sort(m_currColumn, m_currOrder); }

    // If disabled, passthrough.
    void setFilterEnabled(bool);

    // Update filter.
    bool update(const GeoPolRegionVec&,                            // geopol
                const Marble::GeoDataCoordinates&, float maxDistM, // center+dist
                const Marble::GeoDataLatLonBox&,                   // hounds
                uint32_t featureMask);                             // features

    QModelIndex findClosest(const QString&) const;  // find closest index for given name

    static const uint32_t filterMaskDefault;
    static const constexpr uint32_t filterMaskAll = -1U;
    static const constexpr uint32_t filterMaskNone = 0;

private slots:
    void processModelAboutToBeReset();
    void processModelReset();

private:
    class BiMap {
    public:
        BiMap(const GeoLocFilter* flt, const QModelIndex& parent) :
            m_flt(flt),
            m_sourceParent(Util::MapDown(parent))
        { }

        int toSource(int pos) const { return m_proxyToSource.at(pos); }
        int toProxy(int pos) const {
            const auto it = m_sourceToProxy.find(pos);
            assert(it != m_sourceToProxy.end());
            return *it;
        }

        bool isEmpty() const { return m_proxyToSource.isEmpty(); }
        int rowCount() const { return m_proxyToSource.size(); }
        QModelIndex sourceParent() const { return m_sourceParent; }

        void sort();
        inline void append(int child);

    private:
        inline std::optional<bool> cmpCol(int lhs, int rhs, int column, Qt::SortOrder) const;

        const GeoLocFilter*   m_flt;           // parent filter
        QVector<int>          m_proxyToSource; // map proxy row to source row
        QHash<int, int>       m_sourceToProxy; // reverse map
        QModelIndex           m_sourceParent;  // parent index in the source model
    };

    friend class BiMap;

    const GeoLocModel& model() const { return m_model; }

    inline QModelIndex fromSource(const QModelIndex& idx) const;
    inline QModelIndex toSource(const QModelIndex& idx) const;
    void               clear();
    void               sort(BiMap&);

    const GeoLocModel&             m_model;
    mutable QHash<quintptr, BiMap> m_map; // map from the Node*, so we can use for src->proxy or proxy->src
    mutable std::deque<quintptr>   m_order;

    static const constexpr int maxBuckets = 16;

    decltype(m_map)::const_iterator map(const QModelIndex& idx) const;

    struct FilterParams {
        FilterParams(const GeoPolRegionVec& geoPol = GeoPolRegionVec(),
                     const Marble::GeoDataCoordinates& center = Marble::GeoDataCoordinates(),
                     const Marble::GeoDataLatLonBox& bounds = Marble::GeoDataLatLonBox(),
                     float maxDistM = filterMaskDefault,
                     uint32_t featureMask = -1U) :
            m_geoPol(geoPol), m_center(center), m_bounds(bounds), m_maxDistM(maxDistM), m_featureMask(featureMask)
        { }

        bool nonLocChange(const FilterParams&) const;
        bool accepts(const GeoLocEntry&) const;

        GeoPolRegionVec            m_geoPol;      // geopol filtering
        Marble::GeoDataCoordinates m_center;      // for distance from center point
        Marble::GeoDataLatLonBox   m_bounds;      // items within bounds (empty = everything)
        float                      m_maxDistM;    // 0 = accept all distances
        uint32_t                   m_featureMask; // one bit per GeoLocEntry::Feature value
    };

    FilterParams               m_currFilter;  // current filter params
    FilterParams               m_prevFilter;  // current filter params
    bool                       m_enabled;     // if disabled, match everything.
    int                        m_currColumn;  // current sort column
    Qt::SortOrder              m_currOrder;   // previous sort order
    int                        m_prevColumn;  // previous sort column
    Qt::SortOrder              m_prevOrder;   // previous sort order
};

#endif // GEOLOCFILTER_H
