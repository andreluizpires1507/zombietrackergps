/*
    Copyright 2019-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "trackdetailfilter.h"

// This is the layout we'll display.
const TrackDetailFilter::Line TrackDetailFilter::rootItem = {
    Header(_Count), {
        { Header(Meta), {
              TrackModel::Name,
              TrackModel::Type,
              TrackModel::Tags,
              TrackModel::Flags,
              TrackModel::Color,
              TrackModel::Keywords,
              TrackModel::Source,
        } },

        { Header(Track), {
             { Header(Geography), {
                   TrackModel::Length,
                   TrackModel::Area,
             } },
             { Header(Points), {
                   TrackModel::Segments,
                   TrackModel::Points,
             } },
             { Header(Extent), {
                   TrackModel::MinLon,
                   TrackModel::MinLat,
                   TrackModel::MaxLon,
                   TrackModel::MaxLat,
             } },
        } },

        { Header(Times), {
              TrackModel::BeginDate,
              TrackModel::BeginTime,
              TrackModel::EndDate,
              TrackModel::EndTime,
              TrackModel::StoppedTime,
              TrackModel::MovingTime,
              TrackModel::TotalTime,
        } },

        { Header(Speeds), {
              TrackModel::MinSpeed,
              TrackModel::AvgOvrSpeed,
              TrackModel::AvgMovSpeed,
              TrackModel::MaxSpeed,
        } },

        { Header(Elevation), {
             { Header(HighLow), {
                   TrackModel::MinElevation,
                   TrackModel::AvgElevation,
                   TrackModel::MaxElevation,
             } },
             { Header(Grade), {
                   TrackModel::MinGrade,
                   TrackModel::AvgGrade,
                   TrackModel::MaxGrade,
             } },
             { Header(Climb), {
                   TrackModel::Ascent,
                   TrackModel::Descent,
                   TrackModel::BasePeak,
             } },
        } },

        { Header(Exercise), {
             { Header(HeartRate), {
                   TrackModel::MinHR,
                   TrackModel::AvgHR,
                   TrackModel::MaxHR,
                   TrackModel::MinHRPct,
                   TrackModel::AvgHRPct,
                   TrackModel::MaxHRPct,
             } },
              { Header(Cadence), {
                    TrackModel::MinCad,
                    TrackModel::AvgMovCad,
                    TrackModel::MaxCad,
              } },
             { Header(Power), {
                   TrackModel::MinPower,
                   TrackModel::AvgMovPower,
                   TrackModel::MaxPower,
                   TrackModel::Energy,
             } },
             { Header(Temperature), {
                   TrackModel::MinTemp,
                   TrackModel::AvgTemp,
                   TrackModel::MaxTemp,
             } },
             TrackModel::Laps,
        } },

        TrackModel::Notes,
    }
};

TrackDetailFilter::TrackDetailFilter(QObject *parent) :
    DetailFilter(rootItem, TrackModel::_First, TrackModel::_Count, parent)
{
}

QString TrackDetailFilter::text(DetailFilter::Header header) const
{
    // These names must be unique, for the detail pane save/load to work right
    if (int(header) >= _First) {
        switch (int(header)) {
        case Meta:         return QObject::tr("Metadata");
        case Track:        return QObject::tr("Track");
        case Geography:    return QObject::tr("Geography");
        case Points:       return QObject::tr("Points");
        case Extent:       return QObject::tr("Extent");
        case Times:        return QObject::tr("Times");
        case Speeds:       return QObject::tr("Speeds");
        case Elevation:    return QObject::tr("Elevation");
        case HighLow:      return QObject::tr("High/Low");
        case Grade:        return QObject::tr("Grades");
        case Climb:        return QObject::tr("Climb");
        case Exercise:     return QObject::tr("Exercise");
        case Cadence:      return QObject::tr("Cadence");
        case Power:        return QObject::tr("Power");
        case Temperature:  return QObject::tr("Temperature");
        case HeartRate:    return QObject::tr("Heart Rate");
        default:           return QObject::tr("n/a");
        }
    } else {
        return TrackModel::mdName(int(header));
    }
}
