/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOLOCCOMPLETER_H
#define GEOLOCCOMPLETER_H

#include <QCompleter>
#include <QString>

#include <src/core/settings.h>
#include "src/ui/widgets/flagdelegate.h"

class QTableView;
class GeoLocModel;

class GeoLocCompleter final : public QCompleter, public Settings
{
    Q_OBJECT

public:
    GeoLocCompleter(QAbstractItemModel* model, QObject* parent = nullptr);
    ~GeoLocCompleter() override;

    QStringList splitPath(const QString& path) const override;
    QString pathFromIndex(const QModelIndex&) const override;

    void fontResized() { setupView(); }

    QModelIndex lastSelectedIndex() const { return m_lastIndex; }
    void resetLastSelectedIndex() { m_lastIndex = QModelIndex(); }

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private:
    GeoLocModel* model() { return reinterpret_cast<GeoLocModel*>(QCompleter::model()); }
    const GeoLocModel* model() const { return reinterpret_cast<const GeoLocModel*>(QCompleter::model()); }

    void setupView();
    void setupDelegates();

    FlagDelegate        m_flagDelegate;
    QTableView*         m_completionView;
    mutable QModelIndex m_lastIndex;
};

#endif // GEOLOCCOMPLETER_H
