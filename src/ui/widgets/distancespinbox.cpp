/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cmath>
#include <numeric>
#include "distancespinbox.h"

DistanceSpinBox::DistanceSpinBox()
{
    setRange(1.0, 10001.0);
//  setSpecialValueText(tr("World"));
    setDecimals(2);
    setValue(100.0);
}

namespace {
   // Log10, but return 0 at pole
    double log10Pole(double x) { return x < std::numeric_limits<double>::min() ? 0.0 : log10(x); }
} // anonymous namespace

// Step by 10/25/50/75/100/250/500/750/etc (for single step),
// or 10/100/1000/etc (for large step)
void DistanceSpinBox::stepBy(int steps)
{
    static const int stepsPerPow10 = 4; // 4 steps per power of 10
    static const double pad = 0.00001;

    if (steps == 0)
        return;

    double newValue;

    if (value() < 1.0) {
        newValue = 1.0;
    } else if ((value() >= (maximum() - 1.0) && steps > 0) || (value() <= 1.0 && steps < 0)) {
        newValue = 0.0;
    } else {
        double intPart;
        double fracPart = modf(log10(value()), &intPart);
        double newFrac = 0.0;

        if (steps > 1) {
            ++intPart;
        } else if (steps < -1) {
            --intPart;
        } else if (steps > 0) {
            newFrac = -1.0;
            for (int step = 1; step <= stepsPerPow10 && newFrac < 0.0; ++step)
                if (double fracStep = log10Pole(step * (10.0 / stepsPerPow10)); fracPart < (fracStep - pad))
                    newFrac = fracStep;
        } else {
            newFrac = -1.0;
            for (int step = stepsPerPow10 - 1; step >= 0 && newFrac < 0.0; --step)
                if (double fracStep = log10Pole(step * (10.0 / stepsPerPow10)); fracPart > (fracStep + pad))
                    newFrac = fracStep;

            if (newFrac < 0) {
                --intPart;
                newFrac = log10Pole((stepsPerPow10-1) * (10.0 / stepsPerPow10));
            }
        }

        newValue = pow(10.0, intPart + newFrac);
    }

    if (newValue < minimum() || newValue > maximum())
        return;

    setValue(newValue);
}
