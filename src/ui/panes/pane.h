/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PANE_H
#define PANE_H

#include <src/ui/panes/panebase.h>
#include "panegroup.h"

class QModelIndex;
class MainWindow;
class QSplitter;

// Update this when adding more pane types.
// These values are saved in config files, so do not reorder them.
enum class PaneClass {
    _First = 0,
    Empty     = _First,    // empty (help/info) pane
    Map,                   // map pane
    Filter,                // track categories
    View,                  // view presets
    Track,                 // GPS track pane
    TrackDetail,           // GPS track detail pane
    Points,                // GPS points pane
    LineChart,             // GPS track line graph for single track
    CmpChart,              // GPS track data comparisons
    GpsDevice,             // GPS device pane
    GpsCapture,            // GPSD (linux GPS daemon) capture pane
    Waypoint,              // waypoint pane
    ActivitySummary,       // activity summary pane
    ClimbAnalysis,         // climbs and descents
    SimpleView,            // simple HTML view
    ZoneSummary,           // training zone analysis
    _Count,

    Group = 65536,         // avoid namespace range used by the main pane classes
};

Q_DECLARE_METATYPE(PaneClass)
QDataStream& operator<<(QDataStream&, const PaneClass&);
QDataStream& operator>>(QDataStream&, PaneClass&);

// Class for operations provided by UI panels
class Pane : public PaneBase
{
    Q_OBJECT

public:
    using Container = PaneGroup;

    Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent);

    template <typename T>
    [[nodiscard]] static T* factory(PaneClass pc, MainWindowBase& mainWindow) {
        return dynamic_cast<T*>(widgetFactory(pc, mainWindow));
    }

    [[nodiscard]] QString name() const override { return name(PaneClass(int(paneClass()))); }

    // Static things we can ask about a pane action or class.
    [[nodiscard]] static QString     name(PaneClass pc);          // human readable class name
    [[nodiscard]] static PaneClass   findClass(const QString&);   // name to class
    [[nodiscard]] static QString     tooltip(PaneClass);          // tooltip text for the pane description
    [[nodiscard]] static QString     whatsthis(PaneClass);        // ...
    [[nodiscard]] static QString     statusTip(PaneClass);        // ...
    [[nodiscard]] static const char* iconFile(PaneClass);         // small icon for menus, etc
    [[nodiscard]] static const char* previewFile(PaneClass);      // sample preview image for pane selector dialog

protected:
    friend class PaneGroupBase;
    [[nodiscard]] const MainWindow& mainWindow() const;
    [[nodiscard]] MainWindow& mainWindow();

    [[nodiscard]] static QWidget* widgetFactory(PaneClass pc, MainWindowBase& mainWindow);

private:
    [[nodiscard]] static Container* newContainer(MainWindowBase& mainWindow);

    friend class PaneGroup;

    Pane(const Pane&) = delete;
    Pane& operator=(const Pane&) = delete;
};

#endif // PANE_H
