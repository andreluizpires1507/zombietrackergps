/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/core/mapdatamodel.h"
#include "src/core/app.h"

#include "mapdatapane.h"

MapDataPane::MapDataPane(MainWindow& mainWindow, PaneClass paneClass, const QString& itemName, QWidget *parent) :
    DataColumnPane(mainWindow, paneClass, parent, false),
    m_textDelegate(this),
    m_noteDelegate(app().getModel(App::Model::Track), this, false, tr("Edit ") + itemName + tr(" Note")),
    m_tagDelegate(mainWindow, tr("Select ") + itemName + tr(" Tags"), true, cfgData().tags.keyRole()),
    m_flagDelegate(this, tr("Select Flags"), true, Util::RawDataRole),
    m_mapUpdateTimer(this),
    m_mostRecentFocus(false)
{
}

MapDataPane::~MapDataPane()
{
}

void MapDataPane::setupTimers()
{
    // Wait a little bit after the typing to update the map visibility.
    m_mapUpdateTimer.setSingleShot(true);
    connect(&m_mapUpdateTimer, &QTimer::timeout, this, &MapDataPane::updateVisibility);
}

void MapDataPane::zoomToSelection()
{
    gotoSelection(getSelections());
}

void MapDataPane::gotoSelection(const QModelIndexList& selection) const
{
    auto* mapPane = mainWindow().findPane<MapPane>();

    if (m_treeView == nullptr || mapPane == nullptr || selection.isEmpty())
        return;

    mapPane->zoomTo(modelAs<MapDataModel>()->boundsBox(selection));
}

void MapDataPane::selectDuplicates(bool all)
{
    const MapDataModel* model = modelAs<MapDataModel>();
    const auto hashes = model->hashes(); // for deduplication

    QSet<uint> selectedHash;

    if (!all)
        for (const auto& idx : getSelections())
            selectedHash.insert(model->hash(idx));

    selectionModel()->clearSelection();
    selectionModel()->clearCurrentIndex();

    int dupeCount = 0;

    for (const auto& key : hashes.uniqueKeys()) {
        if (!all && !selectedHash.contains(key))
            continue;

        const auto keyRange = hashes.equal_range(key);
        for (auto rangeIt = keyRange.first; rangeIt != keyRange.second; ++rangeIt) {
            if (rangeIt == keyRange.first ||                  // skip first of identical set
                !model->isDuplicate(*keyRange.first, *rangeIt)) // skip if doesn't compare equal
                continue;

            if (select(*rangeIt, QItemSelectionModel::Select | QItemSelectionModel::Rows))
                ++dupeCount;
        }
    }

    mainWindow().statusMessage(UiType::Info, tr("Selected ") + QString::number(dupeCount) +
                               " duplicate " + std::get<0>(getItemName()) + ".");
}

void MapDataPane::focusIn()
{
    DataColumnPane::focusIn();

    // Reset other same class panes' flag, and remember this pane as the most recently focused
    // pane of this type.
    mainWindow().runOnPanels<MapDataPane>([this](MapDataPane* tp) {
        if (paneClass() == tp->paneClass())
            tp->m_mostRecentFocus = false;
    });

    m_mostRecentFocus = true;

    updateVisibility(); // update visibility for the new focus
}

void MapDataPane::resetDataAction(ModelType mt, const QAction* action)
{
    const QModelIndexList selections = getSelections();

    // Nicer undo name than the default one.
    const QString undoName = UndoMgr::genNameX(action->text(), selections.size());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    for (auto idx : selections)
        model()->setData(mt, idx, QVariant(), Util::RawDataRole);

    mainWindow().statusMessage(UiType::Success, undoName);
}

void MapDataPane::doubleClicked(const QModelIndex& idx)
{
    // If it's an editable column edit, rather than view the selection
    if (modelAs<MapDataModel>()->isEditable(idx.column()))
        return;

    gotoSelection(getSelections());
}

void MapDataPane::postLoadHook()
{
    DataColumnPane::postLoadHook();
    updateVisibility(); // only runs for most recently focused pane
}

void MapDataPane::save(QSettings& settings) const
{
    DataColumnPane::save(settings);

    MemberSave(settings, m_mostRecentFocus);
    SL::Save(settings, "noteDelegate", m_noteDelegate);
}

void MapDataPane::load(QSettings& settings)
{
    DataColumnPane::load(settings);

    MemberLoad(settings, m_mostRecentFocus);
    SL::Load(settings, "noteDelegate", m_noteDelegate);
}
