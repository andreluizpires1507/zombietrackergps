/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSCAPTUREPANE_H
#define GPSCAPTUREPANE_H

#include <QWidget>
#include <QPixmap>
#include <QPersistentModelIndex>

#include "pane.h"
#include "src/core/pointitem.h"
#include "src/gpsd/gpsd.h"

namespace Ui {
class GpsCapturePane;
} // namespace Ui

class QIcon;
class PointModel;

class GpsCapturePane final : public Pane
{
    Q_OBJECT

public:
    explicit GpsCapturePane(MainWindow&, QWidget *parent = nullptr);
    ~GpsCapturePane() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

private slots:
    void on_btnTrack_clicked();
    void on_btnCapture_clicked();
    void on_btnPause_clicked();
    void on_btnStop_clicked();
    void on_btnGoto_clicked();
    void on_btnWaypoint_clicked();
    void on_GpsCapturePane_toggled(bool checked);
    void runStatusChanged(bool);
    void pauseStatusChanged(bool);                   // pause/unpause
    void receiveData(const Gpsd::GpsData&);   // receive GPS data
    void statusMsg(Gpsd::Rc);

private:
    enum class Mode {
        Track,
        Capture,
    };

    void setupActionIcons();                   // default icons
    void setupSignals();                       // signals from Gpsd class
    void setupLabelStyle();                    // text styles
    void setupGpsdSupported();                 // replace UI if we don't have GPSD support

    void updateUi(const Gpsd::GpsData&);       // update UI from GPS data
    void clearUi() { updateUi(Gpsd::GpsData()); } // clear UI
    void startCollection(Mode);                // trigger start of data collection
    void registerGpsdPoint();                  // register ourselves with maps
    void setGpsdPoint(const Gpsd::GpsData&);   // set point on maps
    bool appendToTrack(const Gpsd::GpsData&);  // append point to track
    void unregisterGpsdPoint();                // unregister with maps
    void setRunStatus(bool running, bool msg); // enable/disable widgets

    PointModel* capturePointModel();           // for the track we're capturing to

    static QIcon statusIcon(Gpsd::Rc);         // icon for GPSD status

    template <typename FN> void forwardToMap(const FN&) const;

    // GPS daemon support
    Gpsd                  m_gpsd;       // before data below due to destructor order
    QIcon                 m_offline;    // offline bitmap
    PointItem             m_lastFix;    // last received fix
    Mode                  m_mode;       // track-only, or capture
    bool                  m_firstPoint; // true for first captured point (for zoomTo)
    QPersistentModelIndex m_appendTo;   // track to append to

    Ui::GpsCapturePane *ui;
};

#endif // GPSCAPTUREPANE_H
