/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKCMPPANE_H
#define TRACKCMPPANE_H

#include "barchartbase.h"

namespace Ui {
class TrackCmpPane;
} // namespace Ui

class TrackCmpPane final : public BarChartBase
{
    Q_OBJECT

public:
    explicit TrackCmpPane(MainWindow&, QWidget *parent = nullptr);
    ~TrackCmpPane() override;

    // *** begin Settings API
    void save(QSettings&) const override;
    void load(QSettings&) override;
    // *** end Settings API

    void setAxesShown(bool shown) const override;
    void setLegendShown(bool /*shown*/) const override { }
    void setBarValuesShown(bool show);
    bool axesShown() const override;
    bool legendShown() const override { return false; }

private slots:
    void on_TrackCmpPane_toggled(bool checked) { paneToggled(checked); }
    void currentTrackChanged(const QModelIndex&);
    void hovered(bool status, int index, QtCharts::QBarSet*);
    void clicked(int index, QtCharts::QBarSet*);
    void on_lockToQuery_toggled(bool checked);
    void on_sortDirection_toggled(bool checked);
    void on_action_Set_Bar_Width_triggered();
    void on_action_Page_Up_triggered();
    void on_action_Page_Down_triggered();
    void on_action_Show_Bar_Values_triggered(bool shown);
    void on_action_Show_Axes_triggered(bool shown) const;

private:
    void setupActionIcons();
    void setupTimers();
    void setupChart();
    void setupMenus();
    void setupSignals();
    void updateChart() override;
    void refreshChart(int delayMs = 500) override; // 0 to avoid deferring
    void clearChart() override;
    void updateActions() override;
    void resizeEvent(QResizeEvent*) override;
    void setSortDirection(bool state);
    void highlightCurrent();

    Ui::TrackCmpPane*               ui;
    QTimer                          m_currentTimer;      // because setting bar color repaints whole graph!

    QtCharts::QBarCategoryAxis*     m_axisY;             // ...
    QtCharts::QValueAxis*           m_axisX;             // ...

    QVector<int>                    m_chartToRow;        // chart index to model row map
    QVector<int>                    m_rowToChart;        // model row to chart index map
    int                             m_currentRow;        // for current item highlighting, in model space
    int                             m_prevRow;           // ...

    static const constexpr char* rowProperty    = "zt-row";
};

#endif // TRACKCMPPANE_H
