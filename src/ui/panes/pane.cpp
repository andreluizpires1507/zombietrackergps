/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>
#include <QMenu>
#include <QWidget>

#include "pane.h"
#include <src/util/util.h>
#include "src/core/cfgdata.h"
#include "src/ui/windows/mainwindow.h"
#include <src/ui/panes/emptypane.h>
#include "src/ui/panes/filterpane.h"
#include "src/ui/panes/pointpane.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/panes/viewpane.h"
#include "src/ui/panes/trackpane.h"
#include "src/ui/panes/trackdetailpane.h"
#include "src/ui/panes/tracklinepane.h"
#include "src/ui/panes/trackcmppane.h"
#include "src/ui/panes/gpsdevicepane.h"
#include "src/ui/panes/gpscapturepane.h"
#include "src/ui/panes/waypointpane.h"
#include "src/ui/panes/activitysummarypane.h"
#include "src/ui/panes/climbanalysispane.h"
#include "src/ui/panes/simplepane.h"
#include "src/ui/panes/zonepane.h"

Pane::Pane(MainWindow& mainWindow, PaneClass paneClass, QWidget* parent) :
    PaneBase(mainWindow, PaneClass_t(int(paneClass)), parent)
{
}

const MainWindow& Pane::mainWindow() const
{
    return static_cast<const MainWindow&>(PaneBase::mainWindow());
}

MainWindow& Pane::mainWindow()
{
    return static_cast<MainWindow&>(PaneBase::mainWindow());
}

QString Pane::name(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:           return QObject::tr("Empty Pane");
    case PaneClass::Map:             return QObject::tr("Map Display");
    case PaneClass::Filter:          return QObject::tr("GPS Track Filter");
    case PaneClass::View:            return QObject::tr("View Presets");
    case PaneClass::Track:           return QObject::tr("GPS Track List");
    case PaneClass::TrackDetail:     return QObject::tr("GPS Track Detail");
    case PaneClass::Points:          return QObject::tr("GPS Track Points");
    case PaneClass::LineChart:       return QObject::tr("Single Track Line Chart");
    case PaneClass::CmpChart:        return QObject::tr("Multi Track Bar Chart");
    case PaneClass::GpsDevice:       return QObject::tr("GPS Devices");
    case PaneClass::GpsCapture:      return QObject::tr("GPS Capture");
    case PaneClass::Waypoint:        return QObject::tr("Waypoint List");
    case PaneClass::ActivitySummary: return QObject::tr("Activity Summary Chart");
    case PaneClass::ClimbAnalysis:   return QObject::tr("Climb and Descent Analysis");
    case PaneClass::SimpleView:      return QObject::tr("Simple View");
    case PaneClass::ZoneSummary:     return QObject::tr("Training zone Analysis");
    case PaneClass::Group:           return QObject::tr("Pane Group");
    case PaneClass::_Count:          break;
    }

    assert(0);
    return QObject::tr("n/a");
}

PaneClass Pane::findClass(const QString& className)
{
    for (PaneClass pc = PaneClass::_First; pc < PaneClass::_Count; Util::inc(pc))
        if (name(pc) == className)
            return pc;

    return PaneClass::Empty; // failed to find it
}

QWidget* Pane::widgetFactory(PaneClass pc, MainWindowBase& mainWindow)
{
    // This can't be done in the pane constructor, since the obj is not fully baked yet.
    const auto setClass = [](PaneBase* pane) {
        pane->setObjectName(pane->metaObject()->className());
        return pane;
    };

    auto& mw = static_cast<MainWindow&>(mainWindow);

    switch (pc) {
    case PaneClass::Empty:           return setClass(new EmptyPane(mw, PaneClass_t(int(PaneClass::Empty))));
    case PaneClass::Map:             return setClass(new MapPane(mw));
    case PaneClass::Filter:          return setClass(new FilterPane(mw));
    case PaneClass::View:            return setClass(new ViewPane(mw));
    case PaneClass::Track:           return setClass(new TrackPane(mw));
    case PaneClass::TrackDetail:     return setClass(new TrackDetailPane(mw));
    case PaneClass::Points:          return setClass(new PointPane(mw));
    case PaneClass::LineChart:       return setClass(new TrackLinePane(mw));
    case PaneClass::CmpChart:        return setClass(new TrackCmpPane(mw));
    case PaneClass::GpsDevice:       return setClass(new GpsDevicePane(mw));
    case PaneClass::GpsCapture:      return setClass(new GpsCapturePane(mw));
    case PaneClass::ActivitySummary: return setClass(new ActivitySummaryPane(mw));
    case PaneClass::Waypoint:        return setClass(new WaypointPane(mw));
    case PaneClass::ClimbAnalysis:   return setClass(new ClimbAnalysisPane(mw));
    case PaneClass::SimpleView:      return setClass(new SimplePane(mw));
    case PaneClass::ZoneSummary:     return setClass(new ZonePane(mw));
    case PaneClass::Group:           return newContainer(mainWindow);
    case PaneClass::_Count:          break;
    }

    assert(0);
    return nullptr;
}

// create, set up, and return new pane container
Pane::Container* Pane::newContainer(MainWindowBase& mainWindow)
{
    auto* splitter = new Pane::Container(mainWindow);

    splitter->setChildrenCollapsible(false); // we allow removing children, but not collapsing.
    splitter->setObjectName(containerName);

    return splitter;
}

QString Pane::tooltip(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:           return QObject::tr("Displays nothing.");
    case PaneClass::Map:             return QObject::tr("Primary map display.");
    case PaneClass::Filter:          return QObject::tr("Query filters for GPS tracks.");
    case PaneClass::View:            return QObject::tr("Map view presets.");
    case PaneClass::Track:           return QObject::tr("Display list of GPS tracks.");
    case PaneClass::TrackDetail:     return QObject::tr("Display detailed information about a single GPS track.");
    case PaneClass::Points:          return QObject::tr("Display data about GPS track sample points.");
    case PaneClass::LineChart:       return QObject::tr("Line chart for data from a single GPS track.");
    case PaneClass::CmpChart:        return QObject::tr("Bar chart to compare data over multiple GPS tracks.");
    case PaneClass::GpsDevice:       return QObject::tr("Show USB connected GPS devices for data import..");
    case PaneClass::GpsCapture:      return QObject::tr("Live data capture from GPS daemon.");
    case PaneClass::Waypoint:        return QObject::tr("GPS waypoint data.");
    case PaneClass::ActivitySummary: return QObject::tr("Overview of activity across weeks, months, or years.");
    case PaneClass::ClimbAnalysis:   return QObject::tr("Display information about climbs and descents in a GPS track.");
    case PaneClass::SimpleView:      return QObject::tr("Simple, customizable data display.");
    case PaneClass::Group:           return QObject::tr("Horizontal or vertical group of panes.");
    case PaneClass::ZoneSummary:     return QObject::tr("Display training zone analysis for one or more tracks.");
    case PaneClass::_Count:          break;
    }

    assert(0);
    return QObject::tr("n/a");
}

const char* Pane::iconFile(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:           return "window-new";
    case PaneClass::Map:             return "application-x-marble";
    case PaneClass::Filter:          return "view-filter";
    case PaneClass::View:            return "map-flat";
    case PaneClass::Track:           return "view-list-tree";
    case PaneClass::TrackDetail:     return "view-list-details";
    case PaneClass::Points:          return "view-list-details";
    case PaneClass::LineChart:       return "office-chart-line";
    case PaneClass::CmpChart:        return "office-chart-bar";
    case PaneClass::GpsDevice:       return "kstars_satellites";
    case PaneClass::GpsCapture:      return "kstars_satellites";
    case PaneClass::Waypoint:        return "routeplanning";
    case PaneClass::ActivitySummary: return "office-chart-bar";
    case PaneClass::ClimbAnalysis:   return "labplot-xy-curve-segments";
    case PaneClass::SimpleView:      return "labplot-editgrid";
    case PaneClass::Group:           return "window-new";
    case PaneClass::ZoneSummary:     return "zones";
    case PaneClass::_Count:          break;
    }

    assert(0);
    return "window-new";
}

const char* Pane::previewFile(PaneClass pc)
{
    switch (pc) {
    case PaneClass::Empty:           return "EmptyPane.png";
    case PaneClass::Map:             return "MapPaneWaypoints.jpg";
    case PaneClass::Filter:          return "FilterPane.png";
    case PaneClass::View:            return "ViewPane.png";
    case PaneClass::Track:           return "TrackPane.png";
    case PaneClass::TrackDetail:     return "TrackDetailPane.png";
    case PaneClass::Points:          return "PointPane.png";
    case PaneClass::LineChart:       return "LinePane.png";
    case PaneClass::CmpChart:        return "TrackCmpPane.png";
    case PaneClass::GpsDevice:       return "DeviceDialog.png";
    case PaneClass::GpsCapture:      return "GpsCapture.png";
    case PaneClass::Waypoint:        return "WaypointPane.png";
    case PaneClass::ActivitySummary: return "ActivitySummaryPane.png";
    case PaneClass::ClimbAnalysis:   return "ClimbAnalysis.png";
    case PaneClass::SimpleView:      return "SimpleViewPane.png";
    case PaneClass::ZoneSummary:     return "ZonePane.png";
    case PaneClass::Group:           break;
    case PaneClass::_Count:          break;
    }

    assert(0);
    return "";
}

QString Pane::whatsthis(PaneClass pc)
{
    return tooltip(pc);
}

QString Pane::statusTip(PaneClass pc)
{
    return tooltip(pc);
}

QDataStream& operator<<(QDataStream& out, const PaneClass& pc)
{
    return out << std::underlying_type_t<PaneClass>(pc);
}

QDataStream& operator>>(QDataStream& in, PaneClass& pc)
{
    return in >> reinterpret_cast<std::underlying_type_t<PaneClass>&>(pc);
}
