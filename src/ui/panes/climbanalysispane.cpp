/*
    Copyright 2021-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <src/util/util.h>
#include <src/util/icons.h>
#include <src/util/ui.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/windows/mainwindow.h"
#include "src/ui/panes/mappane.h"
#include "src/core/app.h"
#include "src/core/climbmodel.h"
#include "src/core/trackmodel.h"
#include "src/core/pointmodel.h"

#include "climbanalysispane.h"
#include "ui_climbanalysispane.h"

ClimbAnalysisPane::ClimbAnalysisPane(MainWindow& mainWindow, QWidget *parent) :
    DataColumnPane(mainWindow, PaneClass::ClimbAnalysis, parent),
    PointSelectPane(mainWindow),
    NamedItem(ClimbModel::getItemNameStatic()),
    ui(new Ui::ClimbAnalysisPane)
{
    ui->setupUi(this);

    setupView(ui->climbView, &app().climbModel());
    setWidgets<ClimbModel>(defColumnView(),
                           ui->filterClimbs, nullptr, ui->showColumns, ui->filterCtrl, ui->filterIsValid);

    // Default sort by start of climb
    ui->climbView->sortByColumn(ClimbModel::Start, Qt::AscendingOrder);

    setupActionIcons();
    setupContextMenus();
    setupSignals();
    newConfig();

    // Initialize this in case we appear after any change signals.
    PointSelectPane::currentTrackChanged(app().climbModel().currentTrack());

    Util::SetupWhatsThis(this);
}

ClimbAnalysisPane::~ClimbAnalysisPane()
{
    deleteUI(ui);
}

void ClimbAnalysisPane::setupActionIcons()
{
}

void ClimbAnalysisPane::setupContextMenus()
{
    paneMenu.addAction(mainWindow().getMainAction(MainAction::ZoomToSelection));

    setupActionContextMenu(paneMenu);
    setContextMenuPolicy(Qt::CustomContextMenu);

    connect(this, &ClimbAnalysisPane::customContextMenuRequested, this, &ClimbAnalysisPane::showContextMenu);
}

void ClimbAnalysisPane::setupSignals()
{
    DataColumnPane::setupSignals();

    // Selection
    connect(ui->climbView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &ClimbAnalysisPane::selectionChanged);

    connect(&mainWindow(), &MainWindow::currentTrackChanged, this, &ClimbAnalysisPane::currentTrackChanged);

    // Row deletion from model
    connect(&app().trackModel(), &TrackModel::rowsAboutToBeRemoved, this, &ClimbAnalysisPane::processRowsAboutToBeRemoved);
    // Model reset
    connect(&app().trackModel(), &TrackModel::modelAboutToBeReset, this, &ClimbAnalysisPane::processModelAboutToBeReset);

    // Double click to zoom-to
    connect(ui->climbView, &QTreeView::doubleClicked, this, &ClimbAnalysisPane::doubleClicked);
}

TreeModel* ClimbAnalysisPane::model()
{
    return &app().climbModel();
}

const TreeModel* ClimbAnalysisPane::model() const
{
    return &app().climbModel();
}

const ClimbAnalysisPane::DefColumns& ClimbAnalysisPane::defColumnView() const
{
    static const DefColumns cols = {
        { ClimbModel::Type,         true  },
        { ClimbModel::Start,        true  },
        { ClimbModel::End,          false },
        { ClimbModel::_unused1,     false },
        { ClimbModel::_unused2,     false },
        { ClimbModel::_unused3,     false },
        { ClimbModel::Length,       true  },
        { ClimbModel::Vertical,     true  },
        { ClimbModel::Steep,        true  },
        { ClimbModel::MinGrade,     false },
        { ClimbModel::AvgGrade,     false },
        { ClimbModel::MaxGrade,     false },
        { ClimbModel::BeginDate,    false },
        { ClimbModel::EndDate,      false },
        { ClimbModel::BeginTime,    false },
        { ClimbModel::EndTime,      false },
        { ClimbModel::StoppedTime,  false },
        { ClimbModel::MovingTime,   false },
        { ClimbModel::TotalTime,    false },
        { ClimbModel::MinElevation, false },
        { ClimbModel::AvgElevation, false },
        { ClimbModel::MaxElevation, false },
        { ClimbModel::MinSpeed,     false },
        { ClimbModel::AvgOvrSpeed,  false },
        { ClimbModel::AvgMovSpeed,  true  },
        { ClimbModel::MaxSpeed,     false },
        { ClimbModel::MinCad,       false },
        { ClimbModel::AvgMovCad,    false },
        { ClimbModel::MaxCad,       false },
        { ClimbModel::MinPower,     false },
        { ClimbModel::AvgMovPower,  true  },
        { ClimbModel::MaxPower,     false },
        { ClimbModel::Energy,       false },
        { ClimbModel::Ascent,       false },
        { ClimbModel::Descent,      false },
        { ClimbModel::Segments,     false },
        { ClimbModel::Points,       false },
        { ClimbModel::Area,         false },
        { ClimbModel::MinTemp,      false },
        { ClimbModel::AvgTemp,      false },
        { ClimbModel::MaxTemp,      false },
        { ClimbModel::MinHR,        false },
        { ClimbModel::AvgHR,        false },
        { ClimbModel::MaxHR,        false },
        { ClimbModel::Laps,         false },
        { ClimbModel::MinLon,       false },
        { ClimbModel::MinLat,       false },
        { ClimbModel::MaxLon,       false },
        { ClimbModel::MaxLat,       false },
        { ClimbModel::Flags,        false },
        { ClimbModel::MinHRPct,     false },
        { ClimbModel::AvgHRPct,     false },
        { ClimbModel::MaxHRPct,     false },
    };

    // Verify we added everything.  It's not critical, but better practice.
    for (ModelType md = ClimbModel::_First; md < ClimbModel::_Count; ++md)
        assert(cols.findData(md) >= 0);

    return cols;
}

void ClimbAnalysisPane::newConfig()
{
    ui->climbView->setIconSize(cfgData().iconSizeClimb);
}

void ClimbAnalysisPane::filterTextChanged(const QString& regex)
{
    DataColumnPane::filterTextChanged(regex);
}

void ClimbAnalysisPane::selectionChanged()
{
    assert(PointSelectPane::currentTrack() == app().climbModel().currentTrack());

    PointModel* geoPoints = getGeoPoints();
    if (geoPoints == nullptr)
        return;

    // For performance, we must add everything to a QItemSelection and select it all at once
    QItemSelection selected;

    // For each climb, select the point ranges within it
    for (const auto& idx : getSelections()) {
        const PointItem::Dist_t startDist = app().climbModel().data(ClimbModel::Start, idx, Util::RawDataRole).toDouble();
        const PointItem::Dist_t endDist = app().climbModel().data(ClimbModel::End, idx, Util::RawDataRole).toDouble();

        const QModelIndex startIdx = geoPoints->closestPoint(startDist);
        const QModelIndex endIdx   = geoPoints->closestPoint(endDist);

        if (startIdx.isValid() && endIdx.isValid()) {
            for (int segId = startIdx.parent().row(); segId <= endIdx.parent().row(); ++segId) {
                const QModelIndex segIdx = geoPoints->index(segId, 0);

                const int firstPtId = (segId == startIdx.parent().row()) ? startIdx.row() : 0;
                const int lastPtId = (segId == endIdx.parent().row()) ? endIdx.row() : geoPoints->rowCount(segIdx) - 1;

                const QModelIndex firstPtIdx = PointSelectPane::mapUp(geoPoints->index(firstPtId, 0, segIdx));
                const QModelIndex lastPtIdx = PointSelectPane::mapUp(geoPoints->index(lastPtId, 0, segIdx));

                selected.select(firstPtIdx, lastPtIdx);
            }
        }
    }

    PointSelectPane::select(selected);
}

void ClimbAnalysisPane::showContextMenu(const QPoint& pos)
{
    paneMenu.exec(mapToGlobal(pos));
}

void ClimbAnalysisPane::gotoSelection(const QModelIndexList& selection) const
{
    auto* mapPane = mainWindow().findPane<MapPane>();

    if (ui == nullptr || ui->climbView == nullptr || mapPane == nullptr || selection.isEmpty())
        return;

    const PointModel* pointModel = app().trackModel().geoPoints(app().climbModel().currentTrack());
    if (pointModel == nullptr)
        return;

    mapPane->zoomTo(pointModel->boundsBox(PointItem::Flags::Select));
}

bool ClimbAnalysisPane::hasAction(MainAction action) const
{
    switch (action)
    {
    case MainAction::ZoomToSelection:    return true;
    default:                             return false;
    }
}

void ClimbAnalysisPane::doubleClicked(const QModelIndex&)
{
    gotoSelection(getSelections());
}

void ClimbAnalysisPane::zoomToSelection()
{
    gotoSelection(getSelections());
}

void ClimbAnalysisPane::currentTrackChanged(const QModelIndex& current)
{
    PointSelectPane::currentTrackChanged(current);
}

void ClimbAnalysisPane::processModelAboutToBeReset()
{
    PointSelectPane::processModelAboutToBeReset();
}

void ClimbAnalysisPane::processRowsAboutToBeRemoved(const QModelIndex& parent, int first, int last)
{
    PointSelectPane::processRowsAboutToBeRemoved(parent, first, last);
}

void ClimbAnalysisPane::focusIn()
{
    PointSelectPane::focusIn();
}

