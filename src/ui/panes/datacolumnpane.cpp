/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QInputDialog>

#include <src/util/roles.h>
#include <src/util/util.h>
#include "src/ui/windows/mainwindow.h"
#include "src/core/mergeablemodel.h"
#include "src/core/undomgr.h"
#include "src/core/app.h"
#include "src/core/simplifiablemodel.h"
#include "datacolumnpane.h"

DataColumnPane::DataColumnPane(MainWindow& mainWindow, PaneClass paneClass, QWidget *parent, bool useFlattener) :
    DataColumnPaneBase(mainWindow, PaneClass_t(int(paneClass)), parent, useFlattener)
{
}

// This is needed to make the MOC hapy
DataColumnPane::~DataColumnPane()
{
}

const MainWindow& DataColumnPane::mainWindow() const
{
    return static_cast<const MainWindow&>(DataColumnPaneBase::mainWindow());
}

MainWindow& DataColumnPane::mainWindow()
{
    return static_cast<MainWindow&>(DataColumnPaneBase::mainWindow());
}

// Reject names that match a color-name pattern.  Pass all others.
bool DataColumnPane::noColorNames(const QString& name)
{
    static const QRegularExpression nameRe(PointColorNameRe ".*");

    return !nameRe.match(name).hasMatch();
}

IconSelector* DataColumnPane::iconSelectorFactory() const
{
    return new IconSelector(iconSelectorPaths(), nullptr, noColorNames);
}

// Path for icon selector
const QStringList& DataColumnPane::iconSelectorPaths() const
{
    static const QStringList paths = {":art/tags", "[Points]:art/points", "[US NPS Symbols]:art/us-nps-symbols"};
    return paths;
}

void DataColumnPane::duplicateSelection()
{
    const QModelIndexList selections = getSelections();

    if (selections.isEmpty() || model() == nullptr)
        return; // nothing to do

    const QString undoName = UndoMgr::genName(tr("Duplicate"), selections.size(), getItemName());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    for (const auto& idx : selections)
        model()->insertRow(*model(), idx, model()->rowCount());

    sort();  // resort items in view

    mainWindow().statusMessage(UiType::Success, undoName);
}

void DataColumnPane::deleteSelection()
{
    if (selectionModel() == nullptr || model() == nullptr)
        return;

    const int count = selectionModel()->selectedRows().size();

    // Friendly undo name
    const QString undoName = UndoMgr::genName(tr("Delete"), count, getItemName());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    model()->removeRows(selectionModel(), &topFilter());

    mainWindow().statusMessage(UiType::Success, undoName);
}

void DataColumnPane::mergeSelection()
{
    const QModelIndexList selections = getSelections();

    auto* mergeModel = modelAs<MergeableModel>();

    if (selections.size() <= 1 || mergeModel == nullptr)
        return; // nothing to do; we need at least 2 items to merge.

    const QString undoName = UndoMgr::genName(tr("Merge"), selections.size(), getItemName());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    bool ok;
    QString newName = candidateMergedName(selections);

    // Query user for name, supplying candidate.  The appended tabs are a cheesy way to
    // force a larger width for the dialog.
    newName = QInputDialog::getText(this, undoName, tr("Merged Track Name") + QString(5, '\t'),
                                    QLineEdit::Normal, newName, &ok);

    if (!ok)
        return mainWindow().statusMessage(UiType::Warning, tr("Canceled"));

    const QModelIndex newItem = mergeModel->merge(selections, newName);

    if (!newItem.isValid()) {
        mainWindow().statusMessage(UiType::Error, tr("bad item selection for merge"));
        return;
    }

    sort();  // resort per active criteria
    select(newItem, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);

    mainWindow().statusMessage(UiType::Success, undoName);
}

void DataColumnPane::simplifySelection()
{    
    const QItemSelectionModel* selections = selectionModel();

    auto* simplifyModel = modelAs<SimplifiableModel>();

    if (selections == nullptr || !selections->hasSelection() || simplifyModel == nullptr) {
        mainWindow().statusMessage(UiType::Warning, tr("No items selected for simplify operation"));
        return; // nothing to do
    }

    const QModelIndexList mappedSelections(Util::MapDown(selections->selectedRows(0)));

    const QString undoName = UndoMgr::genName(tr("Simplify"), getSelections().size(), getItemName());
    const UndoMgr::ScopedUndo undoSet(app().undoMgr(), undoName);

    if (mainWindow().getTrackSimplifyDialog().exec(*simplifyModel, mappedSelections) == QDialog::Accepted) {
        mainWindow().expandPointPanes();
        mainWindow().statusMessage(UiType::Success, undoName);
    } else {
        mainWindow().statusMessage(UiType::Warning, tr("Canceled"));
    }
}
