/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef DATACOLUMNPANE_H
#define DATACOLUMNPANE_H

#include <src/util/nameditem.h>
#include <src/ui/panes/datacolumnpanebase.h>

#include <tuple>

#include "pane.h"

// Intermediate class for panes which show columns of data.  Provides methods to
// hide/sort/etc columns.  Has no UI of its own.

class MainWindow;
class TreeModel;
class IconSelector;
enum class MainAction;

class DataColumnPane : public DataColumnPaneBase, virtual public NamedItemInterface
{
    Q_OBJECT

public:
    DataColumnPane(MainWindow&, PaneClass, QWidget *parent = nullptr, bool useFlattener = true);
    ~DataColumnPane() override;

    // Editing features
    virtual void zoomToSelection() { }
    virtual void deleteSelection();
    virtual void duplicateSelection();
    virtual void mergeSelection();
    virtual void simplifySelection();
    virtual bool hasAction(MainAction) const = 0;
    [[nodiscard]] QString name() const override { return Pane::name(PaneClass(int(paneClass()))); }

    static bool noColorNames(const QString&);

protected:
    const MainWindow& mainWindow() const;
    MainWindow& mainWindow();

    // Candidate name for data merging
    virtual QString candidateMergedName(const QModelIndexList&) const { return { }; }

    // Return model associated with displayed data
    virtual TreeModel* model() = 0;
    virtual const TreeModel* model() const = 0;

    const QStringList& iconSelectorPaths()   const override;
    IconSelector*      iconSelectorFactory() const override;

    // Obtain model as a given static type
    template <class T> T* modelAs() { return dynamic_cast<T*>(model()); }
    template <class T> const T* modelAs() const { return dynamic_cast<const T*>(model()); }
};

#endif // DATACOLUMNPANE_H
