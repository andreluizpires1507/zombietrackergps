/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "importinfo.h"

ImportInfoList::ImportInfoList(const QList<QUrl>& urls)
{
    reserve(urls.size());

    for (const auto& url : urls)
        append(url.toLocalFile());
}

ImportInfoList::ImportInfoList(const QFileInfoList& files)
{
    reserve(files.size());

    for (const auto& file : files)
        append(ImportInfo(file.baseName(), file.absoluteFilePath()));
}
