/*
    Copyright 2019-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GPSDEVICE_H
#define GPSDEVICE_H

#include <QString>
#include <QStringList>

// Encapsulate operations for a type of GPS device (copy data to/from device, find device name & mountpoint, etc).
// This is a base class for vendor or device specific classes to derive from.
class GpsDevice
{
public:
    GpsDevice(const QString& mount);
    virtual ~GpsDevice();

    // Types of data we can copy to or from the device
    enum Data {
        GPS,
        Photos,
        POI,
        MapsBase,
        MapsCustom,
    };

    // Transfer direction
    enum Transfer {
        Input,
        Output,
        InputOutput,
    };

    // Derived classes should implement a static method as follows, used in GpsModel::getDevice() to determine
    // which subclass should handle the given root.
    //    static bool is(const QString& root);

    [[nodiscard]] virtual QString     mountPoint() const;              // return filesystem mountpoint of device
    [[nodiscard]] virtual QString     device() const;                  // return block device node
    [[nodiscard]] virtual QString     make() const = 0;                // return device make
    [[nodiscard]] virtual QString     model() const = 0;               // return model name
    [[nodiscard]] virtual QString     image() const;                   // return model image, if available
    [[nodiscard]] virtual QStringList files(Data, Transfer) const = 0; // device filenames

protected:
    // Make a guess about the image filename, and set it in m_image if found.
    // This is invoked from the constructor, so should be called explicitly from derived classes.
    virtual void guessGpsImage();

    // This is intrinsically somewhat fuzzy due to naming differences
    [[nodiscard]] virtual QString tryToMatchGpsImage(const QString&) const;

    QString m_mount;  // location of the mount for this device
    QString m_image;  // location of image for this device, if available
};

#endif // GPSDEVICE_H
