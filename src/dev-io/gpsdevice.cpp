/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "gpsdevice.h"

#include <QStorageInfo>
#include <QDirIterator>
#include <QRegularExpression>

GpsDevice::GpsDevice(const QString& mount) :
    m_mount(mount)
{
}

GpsDevice::~GpsDevice()
{
}

QString GpsDevice::mountPoint() const
{
    return m_mount;
}

QString GpsDevice::device() const
{
    return QStorageInfo(mountPoint()).device();
}

QString GpsDevice::image() const
{
    return m_image;
}

QString GpsDevice::tryToMatchGpsImage(const QString& modelString) const
{
    static const char* imageTree = ":gps-images";

    QString matchString = modelString + ".*[.]png";
    matchString.replace(" ", "-");

    const QRegularExpression regex(matchString, QRegularExpression::CaseInsensitiveOption);

    QDirIterator it(imageTree, QDirIterator::Subdirectories);

    while (it.hasNext()) {
        const QString& file = it.next();

        if (file.contains(regex))
            return file;
    }

    return { }; // not found
}

void GpsDevice::guessGpsImage()
{
    if (make().isEmpty() || model().isEmpty())
        return;

    // We will make a half-hearted attempt to find an image matching this GPS model.
    // First, replace spaces in the model strings with dashes to try to directly match a file.
    if (m_image = tryToMatchGpsImage(make() + " " + model()); !m_image.isEmpty())
        return;

    // If that didn't work, try matching everything up to the second space, in case we have
    // a model match but not a sub-model, such as "Garmin Dakota 10" vs "Garmin Dakota 20".
    const QString modelString = make() + " " + model();

    int spacePos = 0;
    spacePos = modelString.indexOf(' ', spacePos);
    if (spacePos >= 0)
        spacePos = modelString.indexOf(' ', spacePos + 1);
    if (spacePos <= 0)
        return;

    m_image = tryToMatchGpsImage(make() + " " + model().mid(spacePos));
}
