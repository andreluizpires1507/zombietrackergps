/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIO_H
#define GEOIO_H

#include <type_traits>
#include <functional>
#include <QObject>
#include <QModelIndex>
#include <QString>
#include <QColor>
#include <QVector>
#include <QMultiHash>

#include <src/core/query.h>

#include "src/dev-io/importinfo.h"
#include "geoiodecls.h"
#include "geoioparams.h"

class ImportDialog;
class ExportDialog;
class TrackModel;
class WaypointModel;
class PointModel;
class MainWindow;
class GeoLoadBase;
class GeoSaveBase;
class QAbstractItemModel;
class QIODevice;

class GeoSaveLoadBase
{
public:
    GeoSaveLoadBase(MainWindow*,
                    const QAbstractItemModel* modelTrk,
                    const QAbstractItemModel* modelWpt);

    static GeoIoFeature parseFeature(const QString&);

    MainWindow*     m_mainWindow; // for status bar updates

    [[nodiscard]] static const QString& formatName(GeoFormat);

protected:
    // Set optional query strings
    void setQueries(const QString& filterTrk, const QString& filterWpt, Qt::CaseSensitivity);

    // Terminate recursion of tryFormat and return the default
    template <typename SL, typename RC, typename PRED, typename EXEC>
    [[nodiscard]] RC tryFormat(const PRED&, const EXEC&, SL&, const RC& def) { return def; }

    // Try a single format: if predicate succeeds, execute exec fn.
    template <typename SL, typename RC, typename PRED, typename EXEC, typename FMT, typename... REST>
    [[nodiscard]] RC tryFormat(const PRED&, const EXEC&, SL&, const RC& def);

    Query::Context           m_queryCtxTrk; // query context for tracks
    Query::Context           m_queryCtxWpt; // query context for waypoints
    Query::query_uniqueptr_t m_queryTrk;    // query tree root
    Query::query_uniqueptr_t m_queryWpt;    // query tree root
    bool                     m_hasQueryTrk; // for performance
    bool                     m_hasQueryWpt; // for performance
};

Q_DECLARE_METATYPE(GeoFormat)
QDataStream& operator<<(QDataStream&, const GeoFormat&);
QDataStream& operator>>(QDataStream&, GeoFormat&);

// Interface to reading GPX/KML/etc formats.  Selects loader based on the file type.
class GeoLoad : public QObject, public GeoSaveLoadBase
{
    Q_OBJECT

public:
    GeoLoad(MainWindow*, TrackModel&, WaypointModel&, const GeoLoadParams& = GeoLoadParams());

    // Try predicate on each supported format in turn, and execute exec fn on first that succeeds.
    // def is the default return if no formats match.
    template <typename RC, typename PRED, typename EXEC = std::function<bool(const GeoLoadBase&)>>
    [[nodiscard]] RC execIf(const PRED&, const EXEC& = [](const GeoLoadBase&) {return true;}, const RC& rc = RC());

    [[nodiscard]] bool load(const QString& path); // load zero or more tracks, appending.  returns success.
    [[nodiscard]] QVector<bool> load(const GeoLoadParams&, const ImportInfoList&); // import given files.
    [[nodiscard]] QVector<bool> load(ImportDialog&, const ImportInfoList&); // import given files.

    // Look at vector of return codes, return true if any failed.
    [[nodiscard]] static bool anyFailed(const QVector<bool>&);

    [[nodiscard]] GeoFormat format(QIODevice&) const; // format of this device, or Unknown
    [[nodiscard]] GeoFormat format(const QString& path) const; // format of this file, or Unknown

    [[nodiscard]] const QString& errorString() const { return m_errorString; }
    [[nodiscard]] const QModelIndexList& importedTrk() const { return m_importedTrk; }
    [[nodiscard]] const QModelIndexList& importedWpt() const { return m_importedWpt; }

    [[nodiscard]] static const QStringList& formatNames(); // return names for supported save formats.

    [[nodiscard]] TrackModel&    trkModel() { return m_trkModel; }
    [[nodiscard]] WaypointModel& wptModel() { return m_wptModel; }

    // Loaders MUST use this to insert into the model.  TODO: hide the model otherwise,
    // to enforce that architecturally, but provide a few needed things such as rowCount().
    void appendTrack(const QString& name, const QString& notes,
                     const QStringList& tags,
                     const QString& keywords,
                     PointModel& newPoints);

    // Loaders MUST use this to insert into the model.  TODO: hide the model otherwise,
    // to enforce that architecturally, but provide a few needed things such as rowCount().
    void appendWaypoint(const QString& name, const QString& notes,
                        const QStringList& tags,
                        const QString& symbol,
                        const QString& type,
                        const QDateTime& time,
                        double lat, double lon, double ele);

    // Set stdin device, for use with "-"
    void setStdin(QIODevice& dev) { m_stdin = &dev; }

    // For progress reporting.
    void reportRead(qint64 b);
    [[nodiscard]] int duplicateTrkCount() const { return m_duplicateTrk; }
    [[nodiscard]] int duplicateWptCount() const { return m_duplicateWpt; }

    static const constexpr char* trackFileFilter = "GPS (*.fit *.gpx *.kml *.tcx);;All (*)";

    QString        m_errorString;

    const GeoLoadParams& operator()() { return m_param; }

private:
    void setParams(const GeoLoadParams&);

    static qint64 totalFileSize(const ImportInfoList&);
    void preLoadSetup();
    bool loadInternal(const ImportInfo&);

    bool allNative(const ImportInfoList&);
    static QString htmlize(const QString&);

    TrackModel&     m_trkModel;           // track model to load into
    WaypointModel&  m_wptModel;           // waypoint model to load into
    GeoLoadParams   m_param;              // loader parameters
    QString         m_importSource;       // current import source
    qint64          m_totalSize;          // for progress reporting
    qint64          m_readSize;           // amount read so far
    int             m_duplicateTrk;       // rejected duplicate count
    int             m_duplicateWpt;       // rejected duplicate count
    QIODevice*      m_stdin;              // stdin seekable device

    QModelIndexList m_importedTrk;        // indexes to our imported tracks
    QModelIndexList m_importedWpt;        // indexes to our imported tracks

    QMultiHash<uint, QModelIndex> m_trkHashes; // for deduplication
    QMultiHash<uint, QModelIndex> m_wptHashes; // for deduplication

    GeoLoad(const GeoLoad&)            = delete;
    GeoLoad& operator=(const GeoLoad&) = delete;
};

// Interface to reading GPX/KML/etc formats.  This is a dispatcher to select
// a loader based on the file type.
class GeoSave : public QObject, public GeoSaveLoadBase
{
    Q_OBJECT

public:
    GeoSave(MainWindow*, TrackModel&, WaypointModel&, const GeoSaveParams& = GeoSaveParams());

    // Try predicate on each supported format in turn, and execute exec fn on first that succeeds.
    // def is the default return if no formats match.
    template <typename RC, typename PRED, typename EXEC = std::function<bool(const GeoSaveBase&)>>
    [[nodiscard]] RC execIf(const PRED&, const EXEC& = [](const GeoSaveBase&) {return true;}, const RC& rc = RC());

    [[nodiscard]] bool save(const QString& path, bool allTrk, bool allWpt); // save from given index list
    [[nodiscard]] bool save(ExportDialog&);       // UI form: present file selector etc

    [[nodiscard]] GeoFormat formatForExt(const QString& ext) const;

    const QString& errorString() const { return m_errorString; }

    const GeoSaveParams& operator()() { return m_param; }

    QString                m_errorString;    // last error string, if any

    static const QStringList& formatNames(); // return names for supported load formats.

    static const constexpr char* trackFileFilter = GeoLoad::trackFileFilter;

    // Set stdin device, for use with "-"
    void setStdout(QIODevice& dev) { m_stdout = &dev; }

    const TrackModel& trkModel()    { return m_trkModel; }
    const WaypointModel& wptModel() { return m_wptModel; }

    // For progress reporting.
    void reportWrite(qint64 items);

private:
    void setParams(const GeoSaveParams&);

    GeoSaveParams          m_param;          // save parameters
    const TrackModel&      m_trkModel;       // model to save data from
    const WaypointModel&   m_wptModel;       // waypoint model to save data from
    qint64                 m_totalCount = 0; // for progress reporting
    qint64                 m_writeCount = 0; // ...
    QIODevice*             m_stdout;         // stdout seekable device

    GeoSave(const GeoSave&)            = delete;
    GeoSave& operator=(const GeoSave&) = delete;
};

inline GeoIoFeature& operator|=(GeoIoFeature& lhs, const GeoIoFeature& rhs)
{
    return lhs = GeoIoFeature(std::underlying_type_t<GeoIoFeature>(lhs) | std::underlying_type_t<GeoIoFeature>(rhs));
}

#endif // GEOIO_H
