/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOIODECLS_H
#define GEOIODECLS_H

// Currently supported I/O formats
enum class GeoFormat {
    Native = 0,  // Our own binary format for super-fast save/load.  Yay!
    Gpx,         // https://en.wikipedia.org/wiki/GPS_Exchange_Format
    Tcx,         // https://en.wikipedia.org/wiki/Training_Center_XML
    Kml,         // https://en.wikipedia.org/wiki/Keyhole_Markup_Language
    Fit,         // (no public spec yet discovered)
    Unknown,     // no known format
};

// Features we can import (e.g, tracks, or waypoints)
enum class GeoIoFeature {
    None = 0,
    Trk  = (1<<0), // tracks
    Wpt  = (1<<1), // waypoints
    Rte  = (1<<2), // routes
    Pnt  = (1<<3), // points, for queries etc
    Unk  = (1<<4), // unknown

    All  = (Trk|Wpt|Rte),  // primary types
};


#endif // GEOIODECLS_H
