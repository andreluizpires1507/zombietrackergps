/*
    Copyright 2019 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "geoio.h"
#include "geoioxml.h"

bool GeoLoadXml::is(QIODevice& io, const char* startTag)
{
    if (!openReader(io))
        return false;

    while (!done())
        if (const auto token = xml.readNext(); isStart(token))
            return xml.name() == startTag;

    return false;
}

bool GeoLoadXml::load(QIODevice& io)
{
    if (!openReader(io))
        return false;

    m_foundTopTag = false;

    parseKeys([this]() {
        if (xml.name() == topTag()) parseXml();
        else xml.skipCurrentElement();
    });

    if (xml.error() == QXmlStreamReader::NoError) {
        if (!m_foundTopTag)
            xml.raiseError(QObject::tr("No %1 element found in file").arg(topTag()));
    }

    if (xml.hasError()) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoLoadXml::openReader(QIODevice& io)
{
    geoLoad.m_errorString.clear();

    if (!io.isOpen() && !io.open(openMode()))
        return false;

    if (!io.isSequential())
        io.seek(0); // start at beginnings

    xml.setDevice(&io);

    if (xml.error() != QXmlStreamReader::NoError) {
        geoLoad.m_errorString = xml.errorString();
        return false;
    }

    return true;
}

bool GeoSaveXml::save(QIODevice& io)
{
    if (!openWriter(io))
        return false;

    xml.writeStartDocument();
    saveXml();
    xml.writeEndDocument();

    return !xml.hasError();
}

bool GeoSaveXml::openWriter(QIODevice& io)
{
    geoSave.m_errorString.clear();

    if (!io.isOpen() && !io.open(openMode() | QIODevice::Truncate))
        return false;
    
    xml.setDevice(&io);

    if (xml.hasError()) {
        geoSave.m_errorString = io.errorString();
        return false;
    }

    // Set formatting
    xml.setAutoFormatting(geoSave().m_writeFormatted);
    xml.setAutoFormattingIndent(geoSave().m_indentLevel *
                                      (geoSave().m_indentSpaces ? 1 : -1));

    return true;
}
