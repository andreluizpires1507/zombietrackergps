/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "geoioparams.h"

GeoParamsBase::GeoParamsBase(GeoIoFeature feature,
                             const QString& filterTrk,
                             const QString& filterWpt,
                             Qt::CaseSensitivity filterCase) :
    m_feature(feature),
    m_filterTrk(filterTrk),
    m_filterWpt(filterWpt),
    m_filterCase(filterCase)
{
}

GeoLoadParams::GeoLoadParams(GeoIoFeature features,
                             const QStringList& tags,
                             const QColor& trackColor,
                             bool deduplicate,
                             const QString& filterTrk,
                             const QString& filterWpt,
                             const Qt::CaseSensitivity filterCase) :
    GeoParamsBase(features, filterTrk, filterWpt, filterCase),
    m_tags(tags),
    m_trackColor(trackColor),
    m_deduplicate(deduplicate)
{
}

GeoSaveParams::GeoSaveParams(GeoFormat format,
                             GeoIoFeature features,
                             bool writeFormatted,
                             int indentLevel,
                             bool indentSpaces,
                             bool exportAllTrk,
                             bool exportAllWpt,
                             const QString& filterTrk,
                             const QString& filterWpt,
                             Qt::CaseSensitivity filterCase) :
    GeoParamsBase(features, filterTrk, filterWpt, filterCase),
    m_format(format),
    m_writeFormatted(writeFormatted),
    m_indentLevel(indentLevel),
    m_indentSpaces(indentSpaces),
    m_exportAllTrk(exportAllTrk),
    m_exportAllWpt(exportAllWpt)
{
}

GeoSaveParams::GeoSaveParams(const QModelIndexList& trkSel, const QModelIndexList& wptSel) :
    GeoSaveParams()
{
    m_trkSelection = trkSel;
    m_wptSelection = wptSel;
}

bool HasFeature(GeoIoFeature f0, GeoIoFeature f1)
{
    return bool(std::underlying_type_t<GeoIoFeature>(f0) & std::underlying_type_t<GeoIoFeature>(f1));
}
