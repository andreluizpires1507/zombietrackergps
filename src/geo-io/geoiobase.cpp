/*
    Copyright 2019-2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QString>
#include <QVariant>

#include <src/util/util.h>

#include "src/core/trackmodel.h"
#include "src/core/waypointmodel.h"
#include "geoiobase.h"

GeoLoadBase::GeoLoadBase(GeoLoad& geoLoad) :
    geoLoad(geoLoad),
    lastPos(0)
{
}

bool GeoLoadBase::load(const QString& path)
{
    QFile file(path);
    return load(file);
}

bool GeoLoadBase::is(const QString& path) const
{
    QFile file(path);
    return is(file);
}

GeoSaveBase::GeoSaveBase(GeoSave& geoSave) :
    geoSave(geoSave)
{
}

bool GeoSaveBase::save(const QString& path)
{
    QFile file(path);
    return save(file);
}

void GeoLoadBase::ParseTrk::append(GeoLoad& geoLoad)
{
    geoLoad.appendTrack(name, desc, tags, keywords, geoPoint);
}

void GeoLoadBase::ParseTrk::newSegment()
{
    geoPoint.push_back(PointModel::value_type());
    geoPoint.back().reserve(64);
}

void GeoLoadBase::ParseWpt::append(GeoLoad& geoLoad)
{
    geoLoad.appendWaypoint(name, desc, tags, symbol, type, time, lat, lon, ele);
}

// We only ever use these specializations.  We could put the unspecialized template in the
// header, but that annoyingly requires including trackmodel.h, so we put the two specializations
// here to avoid that inclusion in the header.
template <> double GeoSaveBase::trkGet<double>(int mt, const QModelIndex& idx) const
{
    return geoSave.trkModel().data(mt, idx, Util::RawDataRole).value<double>();
}

template <> TrackType GeoSaveBase::trkGet<TrackType>(int mt, const QModelIndex& idx) const
{
    return geoSave.trkModel().data(mt, idx, Util::RawDataRole).value<TrackType>();
}

template <> QString GeoSaveBase::trkGet<QString>(int mt, const QModelIndex& idx) const
{
    return geoSave.trkModel().data(mt, idx, Util::RawDataRole).value<QString>();
}

template <> QVariant GeoSaveBase::trkGet<QVariant>(int mt, const QModelIndex& idx) const
{
    return geoSave.trkModel().data(mt, idx, Util::RawDataRole);
}

template <> double GeoSaveBase::wptGet<double>(int mt, const QModelIndex& idx) const
{
    return geoSave.wptModel().data(mt, idx, Util::RawDataRole).value<double>();
}

template <> QString GeoSaveBase::wptGet<QString>(int mt, const QModelIndex& idx) const
{
    return geoSave.wptModel().data(mt, idx, Util::RawDataRole).value<QString>();
}

template <> QVariant GeoSaveBase::wptGet<QVariant>(int mt, const QModelIndex& idx) const
{
    return geoSave.wptModel().data(mt, idx, Util::RawDataRole);
}
