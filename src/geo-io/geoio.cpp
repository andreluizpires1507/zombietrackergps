/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <type_traits>

#include <QFileInfo>
#include <QMessageBox>
#include <QBuffer>

#include <src/util/util.h>
#include <src/util/limitedseekstream.h>
#include <src/ui/panes/datacolumnpanebase.inl.h>

#include "src/ui/dialogs/importdialog.h"
#include "src/ui/dialogs/exportdialog.h"
#include "src/ui/windows/mainwindow.h"
#include "src/core/waypointitem.h"
#include "src/ui/panes/trackpane.h" // for the sort disabler
#include "geoio.inl.h"
#include "geoioquery.h"

GeoLoad::GeoLoad(MainWindow* mainWindow, TrackModel& modelTrk, WaypointModel& modelWpt, const GeoLoadParams& loadParams) :
    GeoSaveLoadBase(mainWindow, &modelTrk, &modelWpt),
    m_trkModel(modelTrk),
    m_wptModel(modelWpt),
    m_totalSize(0),
    m_readSize(0),
    m_duplicateTrk(0),
    m_duplicateWpt(0),
    m_stdin(nullptr)
{
    setParams(loadParams);
}

bool GeoLoad::allNative(const ImportInfoList& importFrom)
{
    for (const auto& import : importFrom)
        if (!GeoLoadNative(*this).is(import.m_file))
            return false;

    return true;
}

bool GeoLoad::loadInternal(const ImportInfo& import)
{
    const QString& path = import.m_file;
    m_importSource = import.m_source;

    QFile file(path);

    QIODevice* dev = (path == "-") ? m_stdin : &file;
    if (dev == nullptr)
        return false;

    if (execIf<bool>([&dev](const GeoLoadBase& l) { return l.is(*dev); },
                     [&dev](GeoLoadBase& l) { return l.load(*dev); }))
        return true;

    m_errorString = tr("Unrecognized file type.");
    return false;
}

void GeoLoad::preLoadSetup()
{
    m_errorString.clear();
    m_importedTrk.clear();
    m_duplicateTrk = 0;
    m_duplicateWpt = 0;

    m_trkHashes = trkModel().hashes(); // for deduplication
    m_wptHashes = wptModel().hashes(); // ...
}

bool GeoLoad::load(const QString& path)
{
    preLoadSetup();

    if (m_totalSize == 0)
        m_totalSize = QFileInfo(path).size();

    MainWindow::ProgressHandler progress(m_mainWindow, int(m_totalSize));
    MainWindow::SaveCursor cursor(m_mainWindow, Qt::WaitCursor);

    // disable sorting for track panes until stack is popped.
    // Do not delete this, no matter what clang-tidy thinks!
    const auto disableSort = TrackPane::DisableSort<TrackPane>(*this);

    return loadInternal(path);
}

QVector<bool> GeoLoad::load(const GeoLoadParams& loadParams, const ImportInfoList& importFrom)
{
    preLoadSetup();

    QVector<bool> rc; // return one bool per input file

    if (importFrom.isEmpty())
        return rc;

    rc.reserve(importFrom.size());

    setParams(loadParams);

    MainWindow::ProgressHandler progress(m_mainWindow, int(m_totalSize));
    const MainWindow::SaveCursor cursor(m_mainWindow, Qt::WaitCursor);

    // disable sorting for track panes until stack is popped.
    // Do not delete this, no matter what clang-tidy thinks!
    const auto disableSort = TrackPane::DisableSort<TrackPane>(*this);

    QString tentativeError;

    for (const auto& import : importFrom) {
        if (loadInternal(import)) {
            rc.append(true);
        } else {
            if (tentativeError.isEmpty())
                tentativeError = tr("Error loading file: ") + import.m_file + ": " + errorString();
            rc.append(false);
        }
    }

    m_errorString = tentativeError;

    // Attempt to guess waypoint icons from symbol value
    wptModel().guessIcons(m_importedWpt, false);

    assert(importFrom.size() == rc.size());

    return rc;
}

QVector<bool> GeoLoad::load(ImportDialog& importOpts, const ImportInfoList& importFrom)
{
    preLoadSetup();

    if (importFrom.isEmpty())
        return { };

    // Skip import dialog for native formats
    if (!allNative(importFrom)) {
        if (importOpts.exec() != QDialog::Accepted) {
            m_errorString.clear();  // no error message on cancel
            return { };
        }

        m_totalSize   = totalFileSize(importFrom);
        m_readSize    = 0;
    }

    return load(importOpts.geoLoadParams(), importFrom);
}

bool GeoLoad::anyFailed(const QVector<bool>& rc)
{
    return std::any_of(rc.begin(), rc.end(), [](bool b) { return !b; });
}

GeoFormat GeoLoad::format(QIODevice& io) const
{
    return const_cast<GeoLoad&>(*this).
            execIf<GeoFormat>([&io](const GeoLoadBase& l) { return l.is(io); },
                             [](const GeoLoadBase& l) { return l.format(); },
                             GeoFormat::Unknown);
}

GeoFormat GeoLoad::format(const QString& path) const
{
    if (path == "-" && m_stdin != nullptr)
        return format(*m_stdin);

    QFile file(path);
    return format(file);
}

void GeoLoad::reportRead(qint64 b)
{
    m_readSize += b;

    if (m_mainWindow != nullptr)
        m_mainWindow->updateProgress(int(m_readSize));
}

void GeoLoad::setParams(const GeoLoadParams& loadParams)
{
    m_param = loadParams;
    setQueries(m_param.m_filterTrk, m_param.m_filterWpt, m_param.m_filterCase);
}

const QStringList& GeoLoad::formatNames()
{
    // Must be in GeoFormat enum order.
    static const QStringList supported = {
        GeoLoadGpx::name,
        GeoLoadTcx::name,
        GeoLoadKml::name,
        GeoLoadFit::name,
    };

    return supported;
}

QString GeoLoad::htmlize(const QString& s)
{
    // Make imported notes HTML if they're not already.
    if (!s.isEmpty() && !s.contains("<html>"))
        return QString("<html><head></head><body>") + s + "</body></html>";

    return s;
}

// Loaders MUST use this to insert into the model.  TODO: hide the model otherwise,
// to enforce that architecturally, but provide a few needed things such as rowCount().
void GeoLoad::appendTrack(const QString &name, const QString& notes,
                          const QStringList& tags,
                          const QString& keywords,
                          PointModel& newPoints)
{
    // Discard if not asked to load tracks
    if (!m_param.hasFeature(GeoIoFeature::Trk))
        return;

    newPoints.removeEmptySegments(); // remove any empty segments

    if (newPoints.isEmpty())
        return;

    uint newHash = -1U;

    // reject duplicates if asked
    if (m_param.m_deduplicate) {
        newHash = qHash(newPoints);
        for (const auto& idx : m_trkHashes.values(newHash)) {
            if (trkModel().isDuplicate(idx, newPoints)) {
                ++m_duplicateTrk;
                return;
            }
        }
    }

    // Explicitly set tags override auto-detected tags
    const QStringList& finalTags = (m_param.m_tags.isEmpty() ? tags : m_param.m_tags);
    const QModelIndex newIdx = trkModel().appendRow(name, TrackType::Trk, finalTags, m_param.m_trackColor, htmlize(notes), keywords, m_importSource, newPoints);

    // If we have a query and it doesn't match, remove it.  It would be better not to add it to begin with,
    // but that's not very easy since the query operates on the model.
    if (m_hasQueryTrk && !m_queryCtxTrk.match(m_queryTrk, newIdx)) {
        trkModel().removeRow(newIdx.row());
        return;
    }

    // Add new hash, so we can reject further duplicates.
    if (m_param.m_deduplicate)
        m_trkHashes.insert(newHash, newIdx);

    m_importedTrk.append(newIdx);
}

void GeoLoad::appendWaypoint(const QString& name, const QString& notes,
                             const QStringList& tags, const QString& symbol,
                             const QString& type, const QDateTime& time, double lat, double lon, double ele)
{
    // Discard if not asked to load tracks
    if (!m_param.hasFeature(GeoIoFeature::Wpt))
        return;

    uint newHash = -1U;

    // reject duplicates if asked
    if (m_param.m_deduplicate) {
        newHash = WaypointItem::hash(lat, lon, ele, true, time);
        for (const auto& idx : m_wptHashes.values(newHash)) {
            if (wptModel().isDuplicate(idx, lat, lon, ele, time)) {
                ++m_duplicateWpt;
                return;
            }
        }
    }

    // Explicitly set tags override auto-detected tags
    const QStringList& finalTags = (m_param.m_tags.isEmpty() ? tags : m_param.m_tags);
    const QModelIndex newIdx = wptModel().appendRow(name, lat, lon, ele, finalTags, htmlize(notes), type, symbol, m_importSource, time);

    // If we have a query and it doesn't match, remove it.  It would be better not to add it to begin with,
    // but that's not very easy since the query operates on the model.
    if (m_hasQueryWpt && !m_queryCtxWpt.match(m_queryWpt, newIdx)) {
        wptModel().removeRow(newIdx.row());
        return;
    }

    // Add new hash, so we can reject further duplicates.
    if (m_param.m_deduplicate)
        m_wptHashes.insert(newHash, newIdx);

    m_importedWpt.append(newIdx);
}

const QStringList& GeoSave::formatNames()
{
    // Must be in GeoFormat enum order.
    static const QStringList supported = {
        GeoLoadGpx::name,
        GeoLoadTcx::name,
        GeoLoadKml::name,
        GeoLoadFit::name,
    };

    return supported;
}

qint64 GeoLoad::totalFileSize(const ImportInfoList& importFrom)
{
    qint64 total = 0;

    for (const auto& import : importFrom)
        total += QFileInfo(import.m_file).size();

    return total;
}

GeoSave::GeoSave(MainWindow* mainWindow, TrackModel& modelTrk, WaypointModel& modelWpt,
                 const GeoSaveParams& saveParams) :
    GeoSaveLoadBase(mainWindow, &modelTrk, &modelWpt),
    m_trkModel(modelTrk),
    m_wptModel(modelWpt),
    m_stdout(nullptr)
{
    setParams(saveParams);
}

bool GeoSave::save(const QString& path, bool allTrk, bool allWpt)
{
    if (allTrk) {
        m_param.m_trkSelection.clear();
        Util::recurse(trkModel(), [this](const QModelIndex& idx) { m_param.m_trkSelection.append(idx); });
    }

    if (allWpt) {
        m_param.m_wptSelection.clear();
        Util::recurse(wptModel(), [this](const QModelIndex& idx) { m_param.m_wptSelection.append(idx); });
    }

    m_writeCount = 0;
    m_totalCount = m_param.m_trkSelection.size() + m_param.m_wptSelection.size();

    MainWindow::ProgressHandler progress(m_mainWindow, int(m_totalCount));
    const MainWindow::SaveCursor cursor(m_mainWindow, Qt::WaitCursor);

    QFile file(path);

    QIODevice* dev = (path == "-") ? m_stdout : &file;
    if (dev == nullptr)
        return false;

    if (execIf<bool>([this](const GeoSaveBase& s) { return s.format() == m_param.m_format; },
                     [&dev](GeoSaveBase& s) { return s.save(*dev); }))
        return true;

    m_errorString = tr("Unrecognized save type.");
    return false;
}

bool GeoSave::save(ExportDialog& exportOpts)
{
    m_errorString.clear();

    if (exportOpts.exec() != QDialog::Accepted)
        return false;

    setParams(exportOpts.geoSaveParams());

    bool allTrk = m_param.m_exportAllTrk;
    bool allWpt = m_param.m_exportAllWpt;

    if (!allTrk && m_param.m_trkSelection.empty()) {
        const int ret = QMessageBox::question(exportOpts.parentWidget(),
                                              tr("Selection"),
                                              tr("No track selection: export all tracks?"),
                                              QMessageBox::Yes | QMessageBox::No,
                                              QMessageBox::Yes);

        if (ret != QMessageBox::Yes)
            return false;
        allTrk = true;
    }

    const QString saveFile = exportOpts.getExportFileName(trackFileFilter);
    if (saveFile.isEmpty())
        return false;

    if (!save(saveFile, allTrk, allWpt)) {
        m_errorString = tr("Error saving file:<p>") + saveFile + "<p>" + errorString();
        return false;
    }

    return true;
}

GeoFormat GeoSave::formatForExt(const QString& ext) const
{
    if (ext.isEmpty())
        return GeoFormat::Unknown;

    return const_cast<GeoSave&>(*this).
            execIf<GeoFormat>(
                [&ext](const GeoSaveBase& s) { return s.hasExt(ext); },
                [](const GeoSaveBase& s) { return s.format(); },
                GeoFormat::Unknown);
}

void GeoSave::reportWrite(qint64 i)
{
    m_writeCount += i;
    if (m_mainWindow != nullptr)
        m_mainWindow->updateProgress(int(m_writeCount));
}

void GeoSave::setParams(const GeoSaveParams& saveParams)
{
    // Save thes in case saveParams doesn't supply any
    const QModelIndexList trkSelection = m_param.m_trkSelection;
    const QModelIndexList wptSelection = m_param.m_wptSelection;

    m_param = saveParams;

    if (m_param.m_trkSelection.isEmpty())
        m_param.m_trkSelection = trkSelection;

    if (m_param.m_wptSelection.isEmpty())
        m_param.m_wptSelection = wptSelection;

    setQueries(m_param.m_filterTrk, m_param.m_filterWpt, m_param.m_filterCase);
}

QDataStream& operator<<(QDataStream& out, const GeoFormat& pc)
{
    return out << std::underlying_type_t<GeoFormat>(pc);
}

QDataStream& operator>>(QDataStream& in, GeoFormat& pc)
{
    return in >> reinterpret_cast<std::underlying_type_t<GeoFormat>&>(pc);
}

GeoSaveLoadBase::GeoSaveLoadBase(MainWindow* mainWindow,
                                 const QAbstractItemModel* modelTrk,
                                 const QAbstractItemModel* modelWpt) :
    m_mainWindow(mainWindow),
    m_queryCtxTrk(modelTrk),
    m_queryCtxWpt(modelWpt),
    m_hasQueryTrk(false),
    m_hasQueryWpt(false)
{
}

void GeoSaveLoadBase::setQueries(const QString& filterTrk, const QString& filterWpt, Qt::CaseSensitivity filterCase)
{
    m_queryCtxTrk.setCaseSensitivity(filterCase);  // must do before parsing below
    m_queryCtxWpt.setCaseSensitivity(filterCase);

    m_queryTrk    = m_queryCtxTrk.parse(filterTrk);
    m_queryWpt    = m_queryCtxWpt.parse(filterWpt);
    m_hasQueryTrk = !m_queryTrk->is<Query::All>();
    m_hasQueryWpt = !m_queryWpt->is<Query::All>();
}

GeoIoFeature GeoSaveLoadBase::parseFeature(const QString& name)
{
    const QString lname = name.toLower();

    if (lname == "none")
        return GeoIoFeature::None;
    if (lname == "track" || lname == "tracks" || lname == "trk")
        return GeoIoFeature::Trk;
    if (lname == "waypoint" || lname == "waypoints" || lname == "wpt")
        return GeoIoFeature::Wpt;
    if (lname == "route" || lname == "routes" || lname == "rte")
        return GeoIoFeature::Rte;
    if (lname == "all" || lname == "*")
        return GeoIoFeature::All;

    return GeoIoFeature::Unk;
}

const QString& GeoSaveLoadBase::formatName(GeoFormat format)
{
    static const QString unknown = "N/A";

    switch (format) {
    case GeoFormat::Native:  return GeoLoadNative::name;
    case GeoFormat::Gpx:     return GeoLoadGpx::name;
    case GeoFormat::Tcx:     return GeoLoadTcx::name;
    case GeoFormat::Kml:     return GeoLoadKml::name;
    case GeoFormat::Fit:     return GeoLoadFit::name;
    default: return  unknown;
    }
}
