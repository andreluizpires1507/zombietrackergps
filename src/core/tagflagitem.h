/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TAGFLAGITEM_H
#define TAGFLAGITEM_H

#include <src/core/modelmetadata.h>
#include <src/util/roles.h>
#include <src/core/treeitem.h>

#include "geopolmgr.h"
#include "app.h"

class QString;
class QVariant;

class TagFlagItem
{
protected:
    explicit TagFlagItem();

    // Generate tooltips
    [[nodiscard]] static QString flagTooltip(const QString& name, const QVariant& rawData);
    [[nodiscard]] static QString tagTooltip(const QString& name, const QVariant& rawData);

    template <ModelType FLAG, class ITEM>
    void updateFlags(ITEM&, bool force);  // update Flags column on location change
};

template <ModelType FLAG, class ITEM>
void TagFlagItem::updateFlags(ITEM& item, bool force)
{
    if (!force && !item.data(FLAG, Util::RawDataRole).isNull())
        return;

    // Auto-generate set of flags for political regions this track intersects
    const auto regions = app().geoPolMgr().intersectionHierarchicalNames(item);
    item.setData(FLAG, regions, Util::RawDataRole);
}

#endif // TAGFLAGITEM_H
