/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/roles.h>
#include <src/util/units.h>

#include "climbitem.h"
#include "climbmodel.h"

ClimbItem::ClimbItem(TreeItem* parent) :
    TreeItem(parent)
{
}

ClimbItem::ClimbItem(const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(data, parent, Util::RawDataRole)
{
}

QVariant ClimbItem::data(int column, int role) const
{
    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Qt::TextAlignmentRole:
        return { ClimbModel::mdAlignment(column) };

    case Qt::EditRole:   [[fallthrough]];
    case Util::PlotRole:
        return rawData;

    case Qt::DisplayRole: [[fallthrough]];
    case Util::CopyRole:
        // Treat type column specially: replace with unicode arrow
        if (column == ClimbModel::Type) {
            if (const QVariant beginToEndEle = TreeItem::data(TrackModel::BeginToEndEle, Util::RawDataRole); beginToEndEle.isValid())
                return (beginToEndEle.toFloat() > 0.0f) ? QChar(0x25B2) : QChar(0x25BC);
        }

        if (rawData.isValid())
            return ClimbModel::mdUnits(column)(rawData);
        break;
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool ClimbItem::setData(int column, const QVariant& value, int role, bool &changed)
{
    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    return TreeItem::setData(column, value, role, changed);
}

int ClimbItem::columnCount() const
{
    return ClimbModel::_Count;
}

ClimbItem* ClimbItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<ClimbItem*>(parent) != nullptr);

    return new ClimbItem(data, parent);
}
