/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef ZONEMODEL_H
#define ZONEMODEL_H

#include <tuple>

#include <QModelIndex>
#include <QString>
#include <QColor>
#include <QVector>

#include <src/util/nameditem.h>
#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>

class ZoneItem;

class ZoneModel final :
        public TreeModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        Zone,
        Name,
        Color,
        MaxHRPct,
        FTPPct,
        Description,
        _Count,
    };

    ZoneModel(QObject* parent = nullptr);
    ~ZoneModel() override;

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static bool           mdIsChartable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static int            mdDataRole(ModelType);
    static const Units&   mdUnits(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

    // *** begin QAbstractItemModel API
    [[nodiscard]] QVariant headerData(int section, Qt::Orientation orientation,
                                      int role = Qt::DisplayRole) const override;

    using TreeModel::data;
    [[nodiscard]] QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;

    [[nodiscard]]int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }
    // [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;
    // *** end QAbstractItemModel API

    void setPresetModel(int);         // configure a preset default model
    static QStringList presetNames(); // obtain names of presets

private:
    // Default training zone data: name of zone and vector of default entries for that zone
    static const QVector<std::tuple<QString, QVector<std::tuple<QString, QColor, float, float, QString>>>> m_defaultZones;

    ZoneItem* getItem(const QModelIndex&) const;

    Qt::DropActions supportedDropActions() const override { return Qt::MoveAction; }
    Qt::ItemFlags flags(const QModelIndex&) const override;
};

#endif // ZONEMODEL_H
