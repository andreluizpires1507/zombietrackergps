/*
    Copyright 2019-2020 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MERGEABLEMODEL_H
#define MERGEABLEMODEL_H

#include <src/core/changetrackingmodel.h>

class UndoMgr;
class QModelIndex;
class QModelIndexListb;
class QVariant;

// Class for models whose items can be merged.
class MergeableModel : public ChangeTrackingModel
{
public:
    using ChangeTrackingModel::ChangeTrackingModel;

    // Merge entire index list into given index.  The userData variant
    // can be used to convey arbitrary data to the protected methods below.
    virtual QModelIndex merge(const QModelIndexList&, const QVariant& userData = QVariant());

protected:
    virtual QModelIndex beginMerge(const QModelIndexList&, const QVariant& userData) = 0;
    virtual void merge(const QModelIndex& to, const QModelIndex& from, const QVariant& userData) = 0;
    virtual void endMerge(const QModelIndex&, const QVariant& /*userData*/) { }
};

#endif // MERGEABLEMODEL_H
