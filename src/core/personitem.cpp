/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QFileInfo>

#include <src/util/roles.h>
#include "personmodel.h"
#include "personitem.h"
#include "app.h"

PersonItem::PersonItem(TreeItem* parent) :
    TreeItem(parent)
{
    init();
}

PersonItem::PersonItem(const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(parent)
{
    init(data);
}

void PersonItem::init(const TreeItem::ItemData& data)
{
    for (int t = 0; t < PersonModel::_Count; ++t)
        if (data.size() > t && data[t].isValid())
            setData(t, data[t], PersonModel::mdDataRole(t));
}

QVariant PersonItem::data(int column, int role) const
{
    if (parent() == nullptr)
        return TreeItem::data(column, role);

    QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    // If no MaxHR available, use calculated value,
    if (!rawData.isValid() && column == PersonModel::MaxHR && role != Util::RawDataRole)
        if (const int maxHr = maxBpm(); maxHr >= 60)
            rawData = maxHr;

    if (!rawData.isValid())
        return rawData;

    switch (role) {
    case Qt::TextAlignmentRole:
        return { PersonModel::mdAlignment(column) };

    case Qt::DisplayRole:
    case Util::CopyRole:
        return PersonModel::mdUnits(column)(rawData);

    case Qt::EditRole:
        return PersonModel::mdUnits(column).to(rawData);
    }

    return TreeItem::data(column, role);
}

bool PersonItem::setData(int column, const QVariant& value, int role, bool &changed)
{
    if (role == Qt::EditRole) {
        role = PersonModel::mdDataRole(column);
        QVariant unitVal = PersonModel::mdUnits(column).from(value);

        // Allow resetting MaxHR to auto-computed value
        if (column == PersonModel::MaxHR && unitVal.toUInt() <= 60)
            unitVal.clear();

        // Allow resetting FTP
        if (column == PersonModel::FTP && unitVal.toUInt() <= 10)
            unitVal.clear();

        return TreeItem::setData(column, unitVal, role, changed);
    }

    return TreeItem::setData(column, value, role, changed);
}

int PersonItem::columnCount() const
{
    return PersonModel::_Count;
}

int PersonItem::ageInDays(const QDate& relativeTo) const
{
    if (const QDate birthdate = data(PersonModel::Birthdate, Util::RawDataRole).toDate(); birthdate.isValid())
        relativeTo.daysTo(birthdate);

    return -1;
}

int PersonItem::maxBpm(const QDate& relativeTo) const
{
    if (const QVariant value = data(PersonModel::MaxHR, Util::RawDataRole); value.isValid())
        return value.toInt();

    if (const QDate birthdate = data(PersonModel::Birthdate, Util::RawDataRole).toDate(); birthdate.isValid()) {
        const QDate relativeDateInBirthYear(birthdate.year(), relativeTo.month(), relativeTo.day());
        const bool pastBirthday = birthdate >= relativeDateInBirthYear;

        return 220 - ((relativeTo.year() - birthdate.year()) + (pastBirthday ? 1 : 0));
    }

    return -1; // no data available;
}

Power_t PersonItem::ftp(const QDate&) const
{
    if (const QVariant value = data(PersonModel::FTP, Util::RawDataRole); value.isValid())
        return value.toFloat();

    return { 0 };
}

PersonItem* PersonItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<PersonItem*>(parent) != nullptr);

    return new PersonItem(data, parent);
}
