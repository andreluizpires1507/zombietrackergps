/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "undomap.h"
#include "src/ui/panes/mappane.h"
#include "src/ui/widgets/trackmap.h"
#include "src/ui/windows/mainwindow.h"

UndoMapBase::UndoMapBase(TrackMap& trackMap) :
    m_mainWindow(trackMap.mainWindow()),
    m_mapPaneId(trackMap.mapPane().paneId())
{ }

UndoMapBase::~UndoMapBase() { }

TrackMap* UndoMapBase::findMap() const
{
    auto* mapPane = m_mainWindow.findPane<MapPane>(m_mapPaneId);

    return (mapPane == nullptr) ? nullptr : mapPane->m_mapWidget;
}

UndoMapView::UndoMapView(TrackMap& trackMap, const ViewParams& before, const ViewParams& after) :
    UndoMapBase(trackMap), m_before(before), m_after(after)
{
}

bool UndoMapView::apply(const ViewParams& view) const
{
    if (TrackMap* trackMap = findMap(); trackMap != nullptr) {
        trackMap->gotoView(view, false);
        return true;
    }

    return false;
}

bool UndoMapTheme::apply(const QString& themeId) const
{
    if (TrackMap* trackMap = findMap(); trackMap != nullptr) {
        trackMap->setMapThemeId(themeId);
        return true;
    }

    return false;
}

size_t UndoMapTheme::size() const
{
    return sizeof(*this) + size(m_before) + size(m_after);
}
