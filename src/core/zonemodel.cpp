/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>

#include <QModelIndex>

#include <src/util/roles.h>
#include <src/util/units.h>
#include <src/core/modelmetadata.inl.h>

#include "zonemodel.h"
#include "zoneitem.h"

decltype(ZoneModel::m_defaultZones) ZoneModel::m_defaultZones = {
    // Model Name
       // Zone name           Color              %MaxHR %FTP  Description
    { tr("3 Zone"), {
       { tr("Rest"),          QColor(0xffafabab), 0.00, 0.00, tr("Resting.") },
       { tr("Easy"),          QColor(0xff01b0f1), 0.60, 0.10, tr("Low Intensity.") },
       { tr("Moderate"),      QColor(0xff85cf54), 0.75, 0.60, tr("Moderate intensity.") },
       { tr("Hard"),          QColor(0xffc10001), 0.88, 1.10, tr("High intensity.") },
    } },

    { tr("5 Zone"), {
       { tr("Rest"),          QColor(0xffafabab), 0.00, 0.00, tr("Resting.") },
       { tr("Recovery"),      QColor(0xff33aaff), 0.50, 0.05, tr("Assists training recovery.") },
       { tr("Endurance"),     QColor(0xff0cbe56), 0.60, 0.50, tr("Improve endurance and fat burning.") },
       { tr("Aerobic"),       QColor(0xffc3d400), 0.70, 0.75, tr("Improve aerobic fitness.") },
       { tr("VO2 Max")  ,     QColor(0xfffba917), 0.80, 1.00, tr("Increase maximum aerobic capacity.") },
       { tr("Max Perf"),      QColor(0xffd72c0e), 0.90, 1.25, tr("Develop maximum power and speed.") },
    } },

    { tr("7 Zone"), {
       { tr("Rest"),          QColor(0xffafabab), 0.00, 0.00, tr("Resting.") },
       { tr("Recovery"),      QColor(0xff99cdff), 0.60, 0.05, tr("Increase blood flow to muscles to assist recovery.") },
       { tr("Endurance"),     QColor(0xff95ff97), 0.70, 0.55, tr("Improve fat metabolism and oxygen uptake.") },
       { tr("Tempo"),         QColor(0xfffeff78), 0.80, 0.75, tr("Improve carb metabolism, promote slow twitch fibres.") },
       { tr("Threshold"),     QColor(0xffffcc00), 0.90, 0.90, tr("Improve carb metabolism, develop lactate threshold.") },
       { tr("VO2 Max"),       QColor(0xfffe9900), 1.00, 1.05, tr("Develop VO2 max, improve anaerobic energy production.") },
       { tr("Anaerobic"),     QColor(0xffff6600), 1.00, 1.20, tr("Develop VO2 max, improve anaerobic power.") },
       { tr("Neuromuscular"), QColor(0xfff40000), 1.00, 1.50, tr("Increase maximum muscle power.") },
    } },
};

ZoneModel::ZoneModel(QObject* parent) :
    TreeModel(new ZoneItem(), parent),
    NamedItem(getItemNameStatic())
{
}

ZoneModel::~ZoneModel()
{
}

QString ZoneModel::mdName(ModelType mt)
{
    switch (mt) {
    case ZoneModel::Zone:        return QObject::tr("Zone");
    case ZoneModel::Name:        return QObject::tr("Name");
    case ZoneModel::Color:       return QObject::tr("Color");
    case ZoneModel::MaxHRPct:    return QObject::tr("MaxHR%");
    case ZoneModel::FTPPct:      return QObject::tr("FTP%");
    case ZoneModel::Description: return QObject::tr("Description");

    case ZoneModel::_Count:      break;
    }

    assert(0 && "Unknown ZoneModel value");
    return "";
}

bool ZoneModel::mdIsEditable(ModelType mt)
{
    return mt != ZoneModel::Zone;
}

// tooltip for container column header
QString ZoneModel::mdTooltip(ModelType mt)
{
    const bool editable = mdIsEditable(mt);

    switch (mt) {
    case ZoneModel::Zone:
        return makeTooltip(tr("Zone ID."), editable);
    case ZoneModel::Name:
        return makeTooltip(tr("Descriptive name."), editable);
    case ZoneModel::Color:
        return makeTooltip(tr("Chart color for this zone."), editable);
    case ZoneModel::MaxHRPct:
        return makeTooltip(tr("Percent of maximum heart rate, lower bound."), editable);
    case ZoneModel::FTPPct: 
        return makeTooltip(tr("Percent of Functional Threshold Power, lower bound."), editable);
    case ZoneModel::Description:
        return makeTooltip(tr("Description of zone purpose."), editable);
    default:
        assert(0 && "Unknown ZoneModel column");
        return { };
    }
}

// tooltip for container column header
QString ZoneModel::mdWhatsthis(ModelType mt)
{
    return mdTooltip(mt);  // just pass through the tooltip
}

Qt::Alignment ZoneModel::mdAlignment(ModelType mt)
{
    switch (mt) {
    case ZoneModel::Name:        [[fallthrough]];
    case ZoneModel::Description: return Qt::AlignLeft    | Qt::AlignVCenter;
    default:                     return Qt::AlignRight   | Qt::AlignVCenter;
    }
}

const Units& ZoneModel::mdUnits(ModelType mt)
{
    static const Units unitsPercent(Format::Percent);
    static const Units unitsInt(Format::Int);
    static const Units rawString(Format::String);
    static const Units unitsInvalid(Format::_Invalid);

    switch (mt) {
    case ZoneModel::Zone:        return unitsInt;
    case ZoneModel::Name:        [[fallthrough]];
    case ZoneModel::Color:       return rawString;
    case ZoneModel::MaxHRPct:    [[fallthrough]];
    case ZoneModel::FTPPct:      return unitsPercent;
    case ZoneModel::Description: return rawString;
    }

    return unitsInvalid;
}

int ZoneModel::mdDataRole(ModelType mt)
{
    switch (mt) {
    case ZoneModel::Zone:        [[fallthrough]];
    case ZoneModel::Name:        return Util::RawDataRole;
    case ZoneModel::Color:       return Qt::ForegroundRole;
    case ZoneModel::MaxHRPct:    [[fallthrough]];
    case ZoneModel::FTPPct:      [[fallthrough]];
    case ZoneModel::Description: return Util::RawDataRole;
    case ZoneModel::_Count:      break;
    }

    assert(0 && "Unknown ZoneModel value");
    return Util::RawDataRole;
}

ZoneModel::name_t ZoneModel::getItemNameStatic()
{
    return { tr("Zone"), tr("Zones") };
}

QVariant ZoneModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (const QVariant val = ModelMetaData::headerData<ZoneModel>(section, orientation, role); val.isValid())
        return val;

    if (role == Qt::DisplayRole)
        return mdName(section);

    return TreeModel::headerData(section, orientation, role);
}

QVariant ZoneModel::data(const QModelIndex& idx, int role) const
{
    assert(!idx.parent().isValid()); // we don't handle hierarchies

    // calculate zone from row
    if ((role == Qt::DisplayRole || role == Util::RawDataRole) && idx.column() == ZoneModel::Zone)
        return idx.row();

    return TreeModel::data(idx, role);
}

void ZoneModel::setPresetModel(int index)
{
    if (index < 0 || index >= m_defaultZones.size())
        return; // out of bounds - nothing to do.

    clear(); // remove old cruft

    const auto& model = std::get<1>(m_defaultZones.at(index));

    for (const auto& entry : model) {
        appendRow({ QVariant(),           // don't set the zone number
                    std::get<0>(entry),   // ZoneModel::Zone
                    std::get<1>(entry),   // ZoneModel::Color
                    std::get<2>(entry),   // ZoneModel::MaxHRPct
                    std::get<3>(entry),   // ZoneModel::FTPPct
                    std::get<4>(entry)}); // ZoneModel::Description
    }
}

QStringList ZoneModel::presetNames()
{
    QStringList presetNames;

    presetNames.reserve(m_defaultZones.size());

    for (const auto& model : m_defaultZones)
        presetNames.append(std::get<QString>(model));

    return presetNames;
}

Qt::ItemFlags ZoneModel::flags(const QModelIndex& idx) const
{
    Qt::ItemFlags flags = TreeModel::flags(idx) | ModelMetaData::flags<ZoneModel>(idx) | Qt::ItemIsDragEnabled;

    if (!idx.isValid())
        flags |= Qt::ItemIsDropEnabled;

    return flags;
}
