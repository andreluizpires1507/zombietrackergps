/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <QString>
#include <QStringList>
#include <QVariant>

#include "app.h"
#include "tagflagitem.h"
#include "geopolmgr.h"
#include "cfgdata.h"

TagFlagItem::TagFlagItem()
{
}

QString TagFlagItem::tagTooltip(const QString& name, const QVariant& rawData)
{
    if (rawData.type() != QVariant::StringList)
        return { "" };

    const QStringList& tags = rawData.toStringList();

    QString tooltip = QString("<p><b><u><nobr><big>") + name +
            "</big></nobr></u></b></p>" +
            "<table border=0.5 cellspacing=0 cellpadding=2>";

    for (const auto& tag : tags) {
        const QString colorizedIcon = cfgData().tags.tagIconName(tag);
        // display svg in size from config times a small multipler so it's easier to see.
        if (!tag.isNull() && !colorizedIcon.isNull()) {
            tooltip += "<tr><td valign=middle>";
            if (!colorizedIcon.isEmpty())
                tooltip += "<img src=" + colorizedIcon + " height=" +
                           QString::number(cfgData().iconSizeTag.height() * 3 / 2) + "></img>";
            tooltip += "</td><td valign=middle>" + tag + "</td></tr>";
        }
    }

    tooltip += "</table>";
    return tooltip;
}

QString TagFlagItem::flagTooltip(const QString& name, const QVariant& rawData)
{
    if (rawData.type() != QVariant::StringList)
        return { "" };

    const QStringList& regionNames = rawData.toStringList();

    return app().geoPolMgr().flagToolTip(name, regionNames);
}

