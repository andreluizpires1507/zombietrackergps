/*
    Copyright 2021-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CLIMBMODEL_H
#define CLIMBMODEL_H

#include <QTimer>
#include <QPersistentModelIndex>

#include <src/util/nameditem.h>
#include <src/core/modelmetadata.h>
#include <src/core/treemodel.h>

#include "trackmodel.h"  // we share most of the column enums

class QVariant;
class TreeItem;
class PointModel;
class ClimbItem;

class ClimbModel final :
        public TreeModel,
        public NamedItem,
        public ModelMetaData
{
    Q_OBJECT

public:
    enum {
        _First        = TrackModel::_First,
        Type          = _First,
        Start,            // horizontal beginning of climb
        End,              // horizontal end of climb
        Steep,            // Steepest section of N meters (from cfgData.hilllGradeLength)
        _unused1,         // unused...
        _unused2,         // ...
        _unused3,         // ...
        Length        = TrackModel::Length,
        BeginDate     = TrackModel::BeginDate,
        EndDate       = TrackModel::EndDate,
        BeginTime     = TrackModel::BeginTime,
        EndTime       = TrackModel::EndTime,
        StoppedTime   = TrackModel::StoppedTime,
        MovingTime    = TrackModel::MovingTime,
        TotalTime     = TrackModel::TotalTime,
        MinElevation  = TrackModel::MinElevation,
        AvgElevation  = TrackModel::AvgElevation,
        MaxElevation  = TrackModel::MaxElevation,
        MinSpeed      = TrackModel::MinSpeed,
        AvgOvrSpeed   = TrackModel::AvgOvrSpeed,
        AvgMovSpeed   = TrackModel::AvgMovSpeed,
        MaxSpeed      = TrackModel::MaxSpeed,
        MinGrade      = TrackModel::MinGrade,
        AvgGrade      = TrackModel::AvgGrade,
        MaxGrade      = TrackModel::MaxGrade,
        MinCad        = TrackModel::MinCad,
        AvgMovCad     = TrackModel::AvgMovCad,
        MaxCad        = TrackModel::MaxCad,
        MinPower      = TrackModel::MinPower,
        AvgMovPower   = TrackModel::AvgMovPower,
        MaxPower      = TrackModel::MaxPower,
        Energy        = TrackModel::Energy,
        Ascent        = TrackModel::Ascent,
        Descent       = TrackModel::Descent,
        Vertical      = TrackModel::BasePeak,   // repurpose peak-base as vertical distance
        Segments      = TrackModel::Segments,
        Points        = TrackModel::Points,
        Area          = TrackModel::Area,
        MinTemp       = TrackModel::MinTemp,
        AvgTemp       = TrackModel::AvgTemp,
        MaxTemp       = TrackModel::MaxTemp,
        MinHR         = TrackModel::MinHR,
        AvgHR         = TrackModel::AvgHR,
        MaxHR         = TrackModel::MaxHR,
        Laps          = TrackModel::Laps,
        MinLon        = TrackModel::MinLon,
        MinLat        = TrackModel::MinLat,
        MaxLon        = TrackModel::MaxLon,
        MaxLat        = TrackModel::MaxLat,
        Flags         = TrackModel::Flags,
        MinHRPct      = TrackModel::MinHRPct,
        AvgHRPct      = TrackModel::AvgHRPct,
        MaxHRPct      = TrackModel::MaxHRPct,
        _Count,

        // Internal use, not displayed by interface
        BeginToEndEle = TrackModel::BeginToEndEle,
    };

    static_assert(int(ClimbModel::_unused3) == int(TrackModel::_LastSavedColumn));
    static_assert(int(ClimbModel::_Count)   == int(TrackModel::_Count));

    ClimbModel(QObject *parent = nullptr);
    ~ClimbModel() override;

    void update(const PointModel*); // update climbs from the given PointModel

    [[nodiscard]] const Units& units(const QModelIndex& idx) const override;

    // *** begin Stream Save API
    quint32 streamMagic() const override;
    quint32 streamVersionMin() const override;
    quint32 streamVersionCurrent() const override;
    // *** end Stream Save API

    // *** begin ModelMetaData API
    static QString        mdName(ModelType);
    static bool           mdIsEditable(ModelType);
    static QString        mdTooltip(ModelType);
    static QString        mdWhatsthis(ModelType);
    static Qt::Alignment  mdAlignment(ModelType);
    static const Units&   mdUnits(ModelType);
    static bool           mdIgnore(ModelType);
    // *** end ModelMetaData API

    // *** begin NamedItem API
    static name_t getItemNameStatic();
    // *** end NamedItem API

    // *** begin QAbstractItemModel API
    [[nodiscard]] QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    using TreeModel::data;
    [[nodiscard]] QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;
    [[nodiscard]] QModelIndex parent(const QModelIndex& idx) const override;

    [[nodiscard]]int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const override { return _Count; }

    [[nodiscard]] Qt::ItemFlags flags(const QModelIndex&) const override;
    // *** end QAbstractItemModel API

    void deferredUpdate(const QModelIndex&);
    void deferredUpdate();
    void update();    // update climb data from current track
    void newConfig();
    [[nodiscard]] QModelIndex currentTrack() const;

private slots:
    void currentTrackChanged(const QModelIndex&);
    void processRowsAboutToBeRemoved(const QModelIndex&, int first, int last);
    void processModelAboutToBeReset();

private:
    void setupTimers();
    void setupSignals() const;
    void unsetIndex(QPersistentModelIndex&, const QModelIndex&, int first, int last);

    [[nodiscard]] const ClimbItem* getItem(const QModelIndex& idx) const;

    const QModelIndex& verify(const QModelIndex& idx) const {
        assert(idx.model() == nullptr || idx.model() == this);
        return idx;
    }

    QPersistentModelIndex  m_currentTrackIdx;   // index of track whose climbs we log
    QPersistentModelIndex  m_nextTrackIdx;      // for deferred updating
    QTimer                 m_updateTimer;       // for deferred update with track change
};

#endif // CLIMBMODEL_H
