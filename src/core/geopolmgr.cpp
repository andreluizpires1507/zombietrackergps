/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <type_traits>

#include <QFile>
#include <QDataStream>
#include <QtConcurrent>
#include <QDirIterator>
#include <QFileInfo>
#include <QDir>

#include <marble/GeoDataCoordinates.h>
#include <marble/GeoDataPolygon.h>
#include <marble/GeoDataMultiGeometry.h>
#include <marble/GeoDataLatLonAltBox.h>

#include <src/util/resources.h>
#include "app.h"
#include "geopolmgr.h"

GeoPolMgr::GeoPolMgr() :
    m_world("World", "", "", -1, ":art/tags/Flags/Organizations/Political/United_Nations.jpg"),
    m_asyncLoad(false),
    m_abortLoad(false)
{
    asyncLoad(App::geoPolFile);
}

GeoPolMgr::~GeoPolMgr()
{
    abortLoad();  // stop the load thread if it's running.
}

// Return one region given its hierarchical name
const GeoPolRegion* GeoPolMgr::operator[](const QString& name) const
{
    const GeoPolRegion* region = &m_world;

    const int sepCount = name.count(QDir::separator());

    for (int p = 0; p <= sepCount && region != nullptr; ++p)
        if (const QString part = name.section(QDir::separator(), p, p); !part.isEmpty())
            if (const auto it = region->m_nameToRegionMap.find(part); it != region->m_nameToRegionMap.end())
                region = *it;

    return region;
}

// Return vector of regions given vector of hierarchical names
GeoPolRegionVec GeoPolMgr::operator[](const QStringList& names) const
{
    GeoPolRegionVec regions;

    for (const auto& name : names)
        if (const GeoPolRegion* region = (*this)[name]; region != nullptr)
            regions.append(region);

    return regions;
}

QStringList GeoPolMgr::hierarchicalNames(const GeoPolRegionVec& regions)
{
    QStringList names;

    for (const auto& region : regions)
        names.append(region->hierarchicalName());

    return names;
}

QStringList GeoPolMgr::flagIconNames(const QStringList& hierarchicalNames) const
{
    QStringList flagIcons;

    for (const auto& geoPolName : hierarchicalNames)
        if (const GeoPolRegion* region = (*this)[geoPolName]; region != nullptr)
            flagIcons.append(region->flag());

    return flagIcons;
}

QString GeoPolMgr::flagToolTip(const QString& name, const QStringList& regionNames) const
{
    QString tooltip = QString("<p><b><u><nobr><big>") +
            name +
            "</big></nobr></u></b></p>" +
            "<table border=0.5 cellspacing=0 cellpadding=2>";

    for (const auto* region : (*this)[regionNames])
        tooltip += region->tooltipTableRow();

    tooltip += "</table>";

    return tooltip;
}

bool GeoPolMgr::checkEndMarker(QDataStream& in)
{
    std::remove_const<decltype(ztgpsArrayEndMarker)>::type endMarker;
    in >> endMarker;
    return endMarker == ztgpsArrayEndMarker;
}

uint32_t GeoPolMgr::readLoopCount(QDataStream& in, uint32_t maxCount)
{
    uint32_t count;
    in >> count;

    return (count <= maxCount) ? count : uint32_t(-1);
}

// Generate mapping of canonical flag names to the names under art/tags/flags
auto GeoPolMgr::canonicalNameToFlagNameMap(const QString& root)
{
    QMap<QString, QString> map;

    for (auto it = QDirIterator(root, QDirIterator::Subdirectories); it.hasNext(); it.next())
        if (it.fileInfo().isFile())
            map.insert(canonicalizeName(it.filePath()), it.filePath());

    return map;
}

void GeoPolMgr::setupFlags(GeoPolRegion& region, const QMap<QString, QString>& flagMap)
{
    static const QChar sep = QDir::separator();
    static const QString ext = ".jpg";

    QString path;

    // Build up path.  Unfortunately, country and region level are in different subdirs.
    if (!region.name().isEmpty()) {
        if (region.level() == 0) {
            path = QString(flagRoot) + sep + "Countries" + sep + region.name() + ext;
        } else if (region.level() == 1) {
            path = QString(flagRoot) + sep + "Regions" + sep + region.parentName() + sep + region.name() + ext;
        } else if (region.level() > 1) {
            assert(0 && "Currently a two level region hierarchy is assumed.");
        }
    }

    if (const auto it = flagMap.find(canonicalizeName(path)); it != flagMap.end()) {
        region.m_flag = *it;
    } else {
        // qDebug() << "Not found: " << path;
    }

    // Recurse for sub-regions.
    for (auto* it : qAsConst(region.m_nameToRegionMap))
        setupFlags(*it, flagMap);
}

void GeoPolMgr::abortLoad()
{
    m_abortLoad = true;
    m_loaded.waitForFinished();
}

// Load array of data as produced by geojson-to-dat
bool GeoPolMgr::load(const QString& filename)
{
    // This is only for initialization
    assert(m_world.size() == 0 && m_world.m_nameToRegionMap.isEmpty());

    QFile geoPolFile(filename); // will be closed on stack pop

    if (!geoPolFile.open(QFile::ReadOnly))
        return false;

    QDataStream in(&geoPolFile);

    std::remove_const<decltype(ztgpsBeginMagic)>::type beginMagic;
    in >> beginMagic;
    if (beginMagic != ztgpsBeginMagic)
        return false;

    const uint32_t worldSize = readLoopCount(in, 4096);
    if (worldSize == uint32_t(-1)) // sanity
        return false;

    for (uint32_t worldIdx = 0; worldIdx < worldSize; ++worldIdx) {
        if (m_abortLoad.load(std::memory_order_relaxed))
            return false;

        const uint32_t featureSize = readLoopCount(in, 4096);
        if (featureSize == uint32_t(-1)) // sanity
            return false;

        for (uint32_t featureIdx = 0; featureIdx < featureSize; ++featureIdx) {
            QStringList names;
            QString     isoA2;
            QString     type;
            int         level;

            in >> names >> isoA2 >> type >> level;

            if (names.size() != (level + 1))
                return false;

            if (!m_world.load(in, names, isoA2, type, level))
                return false;
        }

        if (!checkEndMarker(in))
            return false;
    }

    if (!checkEndMarker(in))
        return false;

    std::remove_const<decltype(ztgpsBeginMagic)>::type endMagic;
    in >> endMagic;
    if (endMagic != ztgpsEndMagic)
        return false;

    m_world.updateBounds(); // refresh all the bounds at once
    setupFlags(m_world,  GeoPolMgr::canonicalNameToFlagNameMap(flagRoot));

    return true;
}

bool GeoPolMgr::asyncLoadStatic(GeoPolMgr* geoPolData, const QString& filename)
{
    return geoPolData->load(filename);
}

void GeoPolMgr::asyncLoad(const QString& filename)
{
    m_loaded = QtConcurrent::run(asyncLoadStatic, this, filename);
    m_asyncLoad = true;
}

bool GeoPolMgr::finishLoad() const
{
    if (!m_loaded.isFinished()) {
        qApp->setOverrideCursor(Qt::WaitCursor);

        // Make sure we're finished loading.
        if (!m_loaded.result()) {
            qCritical("%s: Unable to load geopolitical data %s",
                      qUtf8Printable(QApplication::applicationDisplayName()),
                      App::geoPolFile);
            throw Exit(5);
        }

        qApp->restoreOverrideCursor();
    }

    return true;
}

