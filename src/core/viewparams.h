/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VIEWPARAMS_H
#define VIEWPARAMS_H

#include <marble/GeoDataLatLonBox.h>
#include <marble/GeoDataCoordinates.h>

class ViewParams
{
public:
    ViewParams(qreal clat = 0.0, qreal clon = 0.0, qreal heading = -1000.0, int zoom = -1) :
        m_centerLat(clat), m_centerLon(clon), m_heading(heading), m_zoom(zoom) { }

    ViewParams(const Marble::GeoDataCoordinates& coords, qreal heading = -1000.0, int zoom = -1) :
        ViewParams(coords.latitude(Marble::GeoDataCoordinates::Degree),
                   coords.longitude(Marble::GeoDataCoordinates::Degree), heading, zoom) { }

    bool isValid() const { return m_zoom > 0; }

    // Count close points as equal
    bool operator==(const ViewParams& rhs) const;
    bool operator!=(const ViewParams& rhs) const { return !(*this == rhs); }

    qreal centerLat() const { return m_centerLat; }
    qreal centerLon() const { return m_centerLon; }
    qreal heading() const { return m_heading; }
    int zoom() const { return m_zoom; }
    const Marble::GeoDataLatLonBox& bounds() const { return m_bounds; }
    Marble::GeoDataCoordinates center() const {
        return { centerLon(), centerLat(), 0, Marble::GeoDataCoordinates::Degree };
    }

    bool hasHeading() const { return m_heading >= -500.0; }
    bool hasZoom() const { return m_zoom >= 0; }
    bool hasBounds() const { return !m_bounds.isEmpty(); }

    ViewParams& setBounds(const Marble::GeoDataLatLonBox& b) { m_bounds = b; return *this; }

private:
    qreal   m_centerLat;
    qreal   m_centerLon;
    qreal   m_heading;
    int     m_zoom;
    Marble::GeoDataLatLonBox m_bounds;
};

#endif // VIEWPARAMS_H
