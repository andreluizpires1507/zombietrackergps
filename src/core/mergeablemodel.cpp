/*
    Copyright 2019-2022 Loopdawg Software

    ldutils is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mergeablemodel.h"

QModelIndex MergeableModel::merge(const QModelIndexList& selection, const QVariant& userData)
{
    // Step 0: get model to merge into.  Use persistent index so it lives
    // across the row deletion step below.
    const QPersistentModelIndex to = beginMerge(selection, userData);

    if (!to.isValid())
        return { to };

    // Step 1: Merge everything
    for (const auto& idx : selection)
        if (idx != to)
            merge(to, idx, userData);

    // Step 2: delete now-unused items, but not the one we merged into.
    removeRows([&to, &selection](const QModelIndex& idx) {
        return selection.contains(idx) && idx != to;
    });

    endMerge(to, userData);

    return { to };
}
