/*
    Copyright 2020-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef GEOPOLREGION_H
#define GEOPOLREGION_H

#include <utility>
#include <marble/GeoDataPolygon.h>
#include <marble/GeoDataMultiGeometry.h>
#include <marble/GeoDataLatLonAltBox.h>

#include <QString>
#include <QMap>
#include <QMutex>
#include <QVector>
#include <QIcon>

class QDataStream;
class TrackItem;
class WaypointItem;
class GeoPolRegion;
class GeoLocEntry;
class ViewParams;

using GeoPolRegionVec = QVector<const GeoPolRegion*>;

// Political region: may contain nested boundaries (e.g, country/state)
class GeoPolRegion final : public Marble::GeoDataMultiGeometry
{
public:
    GeoPolRegion() : m_lastHitLat(-1000.0), m_lastHitLon(-1000.0), m_parent(nullptr), m_level(0) { }

    enum WorldOpt {
        IncludeNever,     // always add the world level
        IncludeAlways,    // never add the world level
        IncludeIfOther,   // include only if there are others
        IncludeIfNoOther, // only add if otherwise no intersection
    };

    [[nodiscard]] const QString&      name()  const    { return m_name; }
    [[nodiscard]] QString             hierarchicalName() const;
    [[nodiscard]] const QString&      isoA2() const    { return m_isoA2; }
    [[nodiscard]] const QString&      type()  const    { return m_type; }
    [[nodiscard]] const QString&      flag()  const    { return m_flag; }
    [[nodiscard]] QIcon               flagIcon() const { return QIcon(flag()); }
    [[nodiscard]] QPixmap             flagPixmap(int height) const { return flagIcon().pixmap(flagSize(height)); }
    [[nodiscard]] int                 level() const    { return m_level; }
    [[nodiscard]] const GeoPolRegion* parent() const   { return m_parent; }
    [[nodiscard]] const QString&      parentName() const;
    [[nodiscard]] QString             tooltipTableRow() const;

    // Return true if the given point or track intersects this region.
    [[nodiscard]] bool intersects(const TrackItem&) const;  // intersection test with a TrackItem (begin/end only)
    [[nodiscard]] bool intersects(const WaypointItem&) const;  // intersection test with a WaypointItem
    [[nodiscard]] bool intersects(const Marble::GeoDataCoordinates&) const; // ... with a single coordinate

    // Flag size computation from bounding height with aspect ratio
    [[nodiscard]] static QSize flagSize(int flagHeight, const QSize& margin = QSize(0,0));

private:
    friend class GeoPolMgr;

    GeoPolRegion(const QString& name, const QString& isoA2, const QString& type, int level,
                 const QString& flag = "") :
        m_parent(nullptr),
        m_name(name), m_isoA2(isoA2), m_type(type), m_flag(flag), m_level(level)
    { }

    [[nodiscard]] bool loadPolygon(QDataStream&);

    // Find GeoPolRegion suitable for adding the named item to.
    [[nodiscard]] GeoPolRegion* findParent(const QStringList& names, int level = 0);

    [[nodiscard]] bool loadRegion(QDataStream&);  // load region from coordinate array

    // Load new sub-region, and connect to proper place by name match
    // QStringList is a set of names up the hierarchy: 0 = top level, etc.
    [[nodiscard]] bool load(QDataStream&, const QStringList& names,
                            const QString& isoA2, const QString& type, int level);

    [[nodiscard]] const Marble::GeoDataLatLonAltBox& bounds() const { return m_bounds; }
    Marble::GeoDataLatLonAltBox updateBounds(); // deferred, so we don't do it on every addition

    // Find all intersections recursively, finding names.  Used by GeoPolData::intersections()
    bool intersections(const TrackItem&, GeoPolRegionVec&, WorldOpt = IncludeNever) const;
    bool intersections(const WaypointItem&, GeoPolRegionVec&, WorldOpt = IncludeNever) const;
    bool intersections(const Marble::GeoDataCoordinates&, GeoPolRegionVec&, WorldOpt = IncludeNever) const;
    bool intersections(const GeoLocEntry&, GeoPolRegionVec&, WorldOpt = IncludeNever) const;
    bool intersections(const std::pair<double, double>&, GeoPolRegionVec&, WorldOpt = IncludeNever) const; // lon/lat
    bool intersections(const ViewParams&, GeoPolRegionVec&, WorldOpt = IncludeNever) const; // lon/lat

    // Add world level, if asked to, and there was no intersection found.
    [[nodiscard]] bool appendWorld(GeoPolRegionVec&, int initSize, WorldOpt) const;

    [[nodiscard]] GeoPolRegion* copy() const override;  // so the GeoDataMultiGeometry can copy us
    void append(const QString& name, GeoPolRegion*); // track name to region mapping

    // Because these methods are not virtual in the GeoDataMultiGeometry, we cannot support erasure
    // while keeping the m_nameToRegionMap in sync.
    QVector<Marble::GeoDataGeometry*>::iterator erase(QVector<GeoDataGeometry*>::iterator) = delete;

    QVector<GeoDataGeometry*>::iterator erase(QVector<GeoDataGeometry*>::iterator,
                                              QVector< GeoDataGeometry*>::Iterator) = delete;


    // Marble's GeoDataLatLon(Alt)Box unions are broken.  We implement our own here.
    static inline qreal abslondiff(qreal lon0, qreal lon1); // absolute longitude difference
    static inline void extend(Marble::GeoDataLatLonAltBox& lhs, qreal lon);
    static void extend(Marble::GeoDataLatLonAltBox& lhs, const Marble::GeoDataLatLonAltBox& rhs);

    static const int  nameCacheSize = 4;                // number of cached children
    static const bool disableIntersectionCache = false; // true to disable cache

    Marble::GeoDataLatLonAltBox  m_bounds;             // cache bounds, since the GetoDataMultiGeometry doesn't
    QMap<QString, GeoPolRegion*> m_nameToRegionMap;    // map names to pointers (no update on erase!)
    mutable QMutex               m_cacheMutex;         // protect the name hit cache
    mutable QStringList          m_lastHitNameCache;   // for intersection efficiency
    mutable double               m_lastHitLat = 0.0;   // cache for intersects()
    mutable double               m_lastHitLon = 0.0;   // cache for intersects()
    const GeoPolRegion*          m_parent;             // parent region

    QString m_name;   // human readable name
    QString m_isoA2;  // Two-character-per-level ISO abbreviation
    QString m_type;   // region type
    QString m_flag;   // flag path
    int     m_level;  // level in hierarchy
};

#endif // GEOPOLREGION_H
