/*
    Copyright 2020-2021 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "mapdatamodel.h"
#include <marble/GeoDataLatLonBox.h>

MapDataModel::MapDataModel()
{
}

Marble::GeoDataLatLonBox MapDataModel::boundsBox(const QVector<QPersistentModelIndex>& srcIndexes) const
{
    QModelIndexList indexes;
    indexes.reserve(srcIndexes.size());

    for (const auto& idx : srcIndexes) // convert to normal index list for zoomTo
        indexes.append(idx);

    return boundsBox(indexes);
}

QString MapDataModel::trackTypeName(TrackType tt)
{
    switch (tt) {
    case TrackType::Trk: return QObject::tr("Trk");
    case TrackType::Rte: return QObject::tr("Rte");
    case TrackType::Wpt: return QObject::tr("Wpt");
    }

    return QObject::tr("Unk");
}
