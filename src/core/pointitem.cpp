/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <QDataStream>
#include <src/util/roles.h>
#include <src/util/math.h>
#include <src/util/units.h>
#include <src/util/versionedstream.h>
#include <src/core/modelmetadata.h>

#include "pointitem.h"
#include "pointitem.inl.h"
#include "pointmodel.h"
#include "app.h"

const QVariant PointItem::m_empty;
decltype(PointItem::m_sinAtanTable) PointItem::m_sinAtanTable; // fixed point sin(atan(x)) tables
bool PointItem::m_sinAtanTableInit = false;

void PointItem::clear()
{
    *this = PointItem();
}

bool PointItem::hasExtData() const
{
    return hasAtemp()         ||
           hasWtemp()         ||
           hasDepth()         ||
           hasSpeed(Measured) ||
           hasHr()            ||
           hasCad()           ||
           hasPower(Measured) ||
           hasCourse()        ||
           hasBearing();
}

PointItem PointItem::interpolate(const PointItem& p0, const PointItem& p1, const QDateTime& ts)
{
    PointItem interp;

    // % of distance between p0 and p1.  If >1, it's extrapolation.
    const qreal pct = qreal(p0.time().msecsTo(ts)) / qreal(p0.time().msecsTo(p1.time()));

    interp.m_flags  = Flags(uint8_t(p0.m_flags) | uint8_t(p1.m_flags));
    interp.setTime(ts);

    if (p0.hasLoc() && p1.hasLoc()) {
        interp.setLon(Util::Lerp(p0.lon(), p1.lon(), pct));
        interp.setLat(Util::Lerp(p0.lat(), p1.lat(), pct));
    }

    if (p0.hasEle() && p1.hasEle())
        interp.setEle(Util::Lerp(p0.ele(false), p1.ele(false), pct));

    if (p0.hasFltEle() && p1.hasFltEle())
        interp.m_fltEle = Util::Lerp(p0.ele(true), p1.ele(true), pct);

    if (p0.hasDistance() && p1.hasDistance())
        interp.setDistance(Util::Lerp(p0.distance(), p1.distance(), pct));

    if (p0.hasGrade() && p1.hasGrade())
        interp.setGrade(Util::Lerp(p0.grade(), p1.grade(), pct));

    if (p0.hasAtemp() && p1.hasAtemp())
        interp.setATemp(Util::Lerp(p0.atemp(), p1.atemp(), pct));

    if (p0.hasWtemp() && p1.hasWtemp())
        interp.setWTemp(Util::Lerp(p0.wtemp(), p1.wtemp(), pct));

    if (p0.hasDepth() && p1.hasDepth())
        interp.setDepth(Util::Lerp(p0.depth(), p1.depth(), pct));

    if (p0.hasAccel() && p1.hasAccel())
        interp.setAccel(Util::Lerp(p0.accel(), p1.accel(), pct));

    if (p0.hasHr() && p1.hasHr())
        interp.setHr(Util::Lerp(float(p0.hr()), float(p1.hr()), pct));

    if (p0.hasCad() && p1.hasCad())
        interp.setCad(Util::Lerp(float(p0.cad()), float(p1.cad()), pct));

    if (p0.hasPower(Measured) && p1.hasPower(Measured))
        interp.setPower(Util::Lerp(p0.power(), p1.power(), pct));

    if (p0.hasCourse() && p1.hasCourse())
        interp.setCourse(Util::Lerp(p0.course(), p1.course(), pct));

    if (p0.hasBearing() && p1.hasBearing())
        interp.setBearing(Util::Lerp(p0.bearing(), p1.bearing(), pct));

    return interp;
}

PointItem PointItem::interpolate(const PointItem& next, const QDateTime& ts) const
{
    return interpolate(*this, next, ts);
}

template <>
QVariant PointItem::data<QVariant>(ModelType mt, int row, int role, bool flt,
                                   const PointItem* startPt,
                                   const PointItem* nextPt,
                                   const PowerData& pd) const
{
    static const constexpr qint64 mStonS = 1000000;  // mS to nS

    if (!hasData(mt, nextPt))
        return m_empty;

    QVariant value;

    // The uint8_t (for size) x/min rates must be converted to the Unit's base measurement of x/sec.
    static const double rateConv = (1.0 / 60.0);

    switch (mt) {
    case PointModel::Index:     value = row;                         break;
    case PointModel::Time:      value = time();                      break;
    case PointModel::Elapsed:
        value = (startPt == nullptr) ? m_empty : Elaps_t::base_type(mStonS * elapsed(startPt->time()));
        break;
    case PointModel::Lon:       value = Lon_t::base_type(lon());     break;
    case PointModel::Lat:       value = Lat_t::base_type(lat());     break;
    case PointModel::Ele:       value = Ele_t::base_type(ele(flt));  break;
    case PointModel::Length:
        value = (nextPt == nullptr) ? m_empty : qreal(nextPt->distance() - distance());
        break;
    case PointModel::Distance:  value = qreal(distance());           break;
    case PointModel::Vert:
        value = (nextPt != nullptr) ? Ele_t::base_type(vert(*nextPt)) : m_empty;
        break;
    case PointModel::Grade:     value = Grade_t::base_type(grade()); break;
    case PointModel::Duration:
        value = (nextPt == nullptr) ? m_empty : Dur_t::base_type(mStonS * duration(*nextPt));
        break;
    case PointModel::Temp:      value = Temp_t::base_type(atemp());  break;
    case PointModel::Depth:     value = depth();                     break;
    case PointModel::Speed:     value = Speed_t::base_type(speed(nextPt)); break;
    case PointModel::Accel:     value = Accel_t::base_type(accel()); break;
    case PointModel::Hr:        value = double(hr()) * rateConv;     break;
    case PointModel::Cad:       value = double(cad()) * rateConv;    break;
    case PointModel::Power:     value = Power_t::base_type(power(pd, *nextPt)); break;
    case PointModel::Course:    value = course();                    break;
    case PointModel::Bearing:   value = bearing();                   break;
    default: assert(0); break;
    }

    if (role == Qt::EditRole)
        return PointModel::mdUnits(mt).to(value);

    return value;
}

template <>
qreal PointItem::data<qreal>(ModelType mt, int row, int role, bool flt,
                             const PointItem* startPt,
                             const PointItem* nextPt,
                             const PowerData& pd) const
{
    return data<QVariant>(mt, row, role, flt, startPt, nextPt, pd).toDouble();
}

void PointItem::clearData(ModelType mt)
{
    switch (mt) {
    case PointModel::Temp:  setATemp(badATempI);  return;
    case PointModel::Depth: setDepth(badDepthI);  return;
    case PointModel::Hr:    setHr(badHr);         return;
    case PointModel::Cad:   setCad(badCad);       return;
    default: return;
    }
}

bool PointItem::setData(ModelType mt, QVariant value, int role, bool& changed)
{
    changed = true; // TODO: we could be smarter about this.

    if (role == Qt::EditRole) {
        value = PointModel::mdUnits(mt).from(value);

        // Unset certain columns if set to zero.  (OK to check ints as doubles, for convenience)
        switch (mt) {
        case PointModel::Temp:  [[fallthrough]];
        case PointModel::Depth: [[fallthrough]];
        case PointModel::Hr:    [[fallthrough]];
        case PointModel::Cad:
            if (value.toDouble() == 0.0) {
                clearData(mt);
                return true;
            }
            break;
        default:
            break;
        }
    }

    // The uint8_t (for size) x/min rates must be converted from the Unit's base measurement of x/sec.
    const double rateConv = 60.0;

    switch (mt) {
    case PointModel::Time:     setTime(value.toDateTime());      break;
    case PointModel::Lon:      setLon(value.toDouble());         break;
    case PointModel::Lat:      setLat(value.toDouble());         break;
    case PointModel::Ele:      setEle(value.toDouble());         break;
    case PointModel::Temp:     setATemp(value.toFloat());        break;
    case PointModel::Depth:    setDepth(value.toFloat());        break;
    case PointModel::Speed:    setSpeed(value.toFloat());        break;
    case PointModel::Hr:       setHr(Hr_t(value.toDouble() * rateConv)); break;
    case PointModel::Cad:      setCad(Cad_t(value.toDouble() * rateConv)); break;
    case PointModel::Power:    setPower(value.toFloat());        break;
    case PointModel::Course:   setCourse(value.toFloat());       break;
    case PointModel::Bearing:  setBearing(value.toFloat());      break;
    default:
        assert(0);
        return false;
    }

    return true;
}

bool PointItem::hasData(ModelType mt, const PointItem* nextPt) const
{
    switch (mt) {
    case PointModel::Index:     return true;
    case PointModel::Time:      return hasTime();
    case PointModel::Elapsed:   return hasElapsed();
    case PointModel::Lon:       return hasLoc();
    case PointModel::Lat:       return hasLoc();
    case PointModel::Ele:       return hasEle();
    case PointModel::Length:    return hasLength(nextPt);
    case PointModel::Distance:  return hasDistance();
    case PointModel::Vert:      return hasVert(nextPt);
    case PointModel::Grade:     return hasGrade();
    case PointModel::Duration:  return hasDuration(nextPt);
    case PointModel::Temp:      return hasAtemp();
    case PointModel::Depth:     return hasDepth();
    case PointModel::Speed:     return hasSpeed(Either, nextPt);
    case PointModel::Accel:     return hasAccel();
    case PointModel::Hr:        return hasHr();
    case PointModel::Cad:       return hasCad();
    case PointModel::Power:     return hasPower(Either, nextPt);
    case PointModel::Course:    return hasCourse();
    case PointModel::Bearing:   return hasBearing();
    }

    assert(0);
    return false;
}

bool PointItem::operator==(const PointItem& other) const
{
    // Do not compare the filtered data!  We can change that per config settings.
    // No need to compare derived (calculated) values such as distance and acceleration.
    return
        hasTime()          == other.hasTime()          &&
        hasLoc()           == other.hasLoc()           &&
        hasEle()           == other.hasEle()           &&
        hasSpeed(Measured) == other.hasSpeed(Measured) &&
        hasPower(Measured) == other.hasPower(Measured) &&

        (!hasTime()      || time() == other.time()) &&
        (!hasLoc()       || Math::almost_equal(m_lon , other.m_lon)) &&
        (!hasLoc()       || Math::almost_equal(m_lat , other.m_lat)) &&
        (!hasEle()       || Math::almost_equal(m_ele , other.m_ele)) &&
        m_atemp == other.m_atemp    &&
        m_wtemp == other.m_wtemp    &&
        m_depth == other.m_depth    &&
        (!hasSpeed(Measured) || Math::almost_equal(m_speed, other.m_speed)) &&
        m_hr      == other.m_hr     &&  // int types can use exact comparison
        m_cad     == other.m_cad    &&  // ...
        (!hasPower(Measured) || Math::almost_equal(m_power, other.m_power)) &&
        m_course  == other.m_course &&
        m_bearing == other.m_bearing;
}

// Create table of sin(atan(x)) values
void PointItem::calcSinAtans()
{
    static QMutex mutex;

    const QMutexLocker lock(&mutex);

    if (m_sinAtanTableInit)
        return;

    for (int i = 0; i < int(m_sinAtanTable.size()); ++i) {
        const float grade = (1.0 / (badGradeI-1)) * (i + int(std::numeric_limits<decltype(m_grade)>::min()));
        m_sinAtanTable[i] = std::sin(std::atan(grade)) * (badGradeI-1);
    }

    m_sinAtanTableInit = true;
}

QDataStream& operator<<(QDataStream& stream, const PointItem& pt)
{
    // Accel, Distance, etc are computed from other values: no need to save/load
    return stream << pt.m_time
                  << pt.m_lon
                  << pt.m_lat
                  << pt.m_ele
                  << pt.m_speed
                  << pt.m_power
                  << pt.m_depth
                  << pt.m_atemp
                  << pt.m_wtemp
                  << pt.m_course
                  << pt.m_bearing
                  << pt.m_hr
                  << pt.m_cad;
}

QDataStream& operator>>(QDataStream& stream, PointItem& pt)
{
    // Accel, Distance, etc are computed from other values: no need to save/load
    return stream >> pt.m_time
                  >> pt.m_lon
                  >> pt.m_lat
                  >> pt.m_ele
                  >> pt.m_speed
                  >> pt.m_power
                  >> pt.m_depth
                  >> pt.m_atemp
                  >> pt.m_wtemp
                  >> pt.m_course
                  >> pt.m_bearing
                  >> pt.m_hr
                  >> pt.m_cad;
}

uint qHash(const PointItem& pt, uint seed)
{
    // Accel, Distance, etc are computed from other values: no need to hash
    if (pt.hasTime())                     seed = qHash(std::make_pair(seed, pt.m_time));
    if (pt.hasLoc()) {
        seed = qHash(std::make_pair(seed, pt.m_lon));
        seed = qHash(std::make_pair(seed, pt.m_lat));
    }

    if (pt.hasEle())                      seed = qHash(std::make_pair(seed, pt.m_ele));
    if (pt.hasAtemp())                    seed = qHash(std::make_pair(seed, pt.m_atemp)); // hash raw data
    if (pt.hasWtemp())                    seed = qHash(std::make_pair(seed, pt.m_wtemp)); // hash raw data
    if (pt.hasDepth())                    seed = qHash(std::make_pair(seed, pt.m_depth));
    if (pt.hasSpeed(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.m_ele));
    if (pt.hasHr())                       seed = qHash(std::make_pair(seed, pt.m_hr));
    if (pt.hasCad())                      seed = qHash(std::make_pair(seed, pt.m_cad));
    if (pt.hasPower(PointItem::Measured)) seed = qHash(std::make_pair(seed, pt.m_power));
    if (pt.hasCourse())                   seed = qHash(std::make_pair(seed, pt.m_course)); // hash raw data
    if (pt.hasBearing())                  seed = qHash(std::make_pair(seed, pt.m_bearing)); // hash raw data

    return seed;
}
