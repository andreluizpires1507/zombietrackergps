/*
    Copyright 2019-2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TRACKITEM_H
#define TRACKITEM_H

#include <QFont>

#include "src/core/pointmodel.h"
#include "src/core/treeitem.h"
#include "src/core/geotypes.h"
#include "src/core/tagflagitem.h"

class QDataStream;
class QString;
class QVariant;
class TrackModel;
class GeoPolMgr;

namespace Marble {
class GeoDataLatLonBox;
} // namespace Marble

// Derive TreeItem, and add our privateData, which will only be created for the
// first column of each row.  It's merely handy to have it here, so it can get
// destroyed as the TrackItem is.
class TrackItem final : public TreeItem, public TagFlagItem
{
public:
    template <class T> void update(const T&);   // updates both track metadata and placemarks

    static void setupSmallFont();

    const Units& units(ModelType mt) const;
    const TrackSegLines& trackSegLines() const { return m_trackLines; }

private:
    friend class TagFlagItem;

    explicit TrackItem(TreeItem *parent = nullptr);
    explicit TrackItem(const TreeItem::ItemData&, TreeItem *parent = nullptr);
    ~TrackItem() override;

    // *** begin Stream Save API
    QDataStream& save(QDataStream&, const TreeModel&) const override;
    QDataStream& load(QDataStream&, TreeModel&) override;
    // *** end Stream Save API

    friend uint qHash(const TrackItem& trk, uint seed);

    QVariant data(ModelType column, int role) const override;

    using TreeItem::setData;
    bool setData(ModelType column, const QVariant &value, int role, bool& changed, const QModelIndex& personIdx);
    
    int  columnCount() const override;
    bool saveRole(int role) const override;

    TrackItem* factory(const TrackItem::ItemData& data, TreeItem* parent) override;

    void shallowCopy(const TreeItem* src) override; // copy data, but not structure

    friend class TrackModel;

    // Append segments from geoPoints.  NOTE: for efficiency, this is destructive
    // to the input list: it uses std::swap.
    void append(TrackModel& model, PointModel& newPoints);
    void append(const PointModel& newPoints); // non-destuctive form

    void refresh(TrackModel&);            // refresh placemarks from base GeoPoint data
    void update(bool force = false);      // updates metadata, placemarks, and flags
    void threadedUpdate(TrackModel&);     // threaded update
    void updatePowerData();               // update only power (cheaper than UpdateTrackData)
    void updateHrPct(const QModelIndex&); // update heart rate percentages
    void updateTrackData();               // recalculate track metadata from underlying GeoPoints
    void updateTrackLines(bool force);    // add any missing GeoPoint data to trackLines

    void applyValueMap(const PointModel::ValueMap_t&);

    void setPersonIdx(const QModelIndex& personIdx);

    QVariant nthTagData(int n, ModelType column, int role) const;  // get tag
    QVariant trackColor(const QVariant& rawData) const;

    Marble::GeoDataLatLonBox bounds() const;   // get bounds in Marble GeoDataLatLonBox form

    // performance cache for the TrackMap
    void setVisible(bool v)     { m_isVisible = v; }
    bool isVisible() const      { return m_isVisible; }
    void setAreaSelect(bool v)  { m_areaSelected = v; }
    bool isAreaSelected() const { return m_areaSelected; }

    void selectPointsWithin(const Marble::GeoDataLatLonBox&);

    bool pointEqual(const TrackItem& other) const;
    bool pointEqual(const PointModel& other) const;

    // Generate tooltips
    using TagFlagItem::flagTooltip;
    using TagFlagItem::tagTooltip;
    QString flagTooltip(const QVariant& rawData) const;
    QString tagTooltip(const QVariant& rawData) const;

    // Find first tag for this track that has power data
    QModelIndex firstPowerTag() const;

    // This is all the data we track about each geo point.
    PointModel       m_geoPoints;    // raw geodata
    TrackSegLines    m_trackLines;   // for drawing the tracks on maps
    bool             m_isVisible;    // performance opt for TrackMap display
    bool             m_areaSelected; // true if it's in the last selected area

    static const constexpr qint64 mStonS = 1000000;  // mS to nS

    static QFont     m_smallerFont;  // for color source

    TrackItem(const TrackItem&) = delete;
    TrackItem& operator=(const TrackItem&) = delete;
};

// Hashing
uint qHash(const TrackItem&, uint seed = 0);

#endif // TRACKITEM_H
