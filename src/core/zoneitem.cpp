/*
    Copyright 2022 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/roles.h>
#include <src/util/units.h>

#include "zoneitem.h"
#include "zonemodel.h"

ZoneItem::ZoneItem(TreeItem* parent) :
    TreeItem(parent)
{
}

ZoneItem::ZoneItem(const TreeItem::ItemData& data, TreeItem* parent) :
    TreeItem(data, parent, Util::RawDataRole)
{
}

QVariant ZoneItem::data(int column, int role) const
{
    const QVariant rawData = TreeItem::data(column, Util::RawDataRole);

    switch (role) {
    case Qt::TextAlignmentRole:
        return { ZoneModel::mdAlignment(column) };

    case Qt::EditRole:   [[fallthrough]];
    case Util::PlotRole:
        return rawData;

    case Qt::BackgroundRole:
        if (column == ZoneModel::Color) // don't display color names - just use a swatch
            role = Util::RawDataRole;
        break;

    case Qt::DisplayRole: [[fallthrough]];
    case Util::CopyRole:
        if (column == ZoneModel::Color) // don't display color names - just use a swatch
            return { };

        if (rawData.isValid())
            return ZoneModel::mdUnits(column)(rawData);
        break;

    case Util::RawDataRole:
        return rawData;
    }

    // We didn't handle anything specially for the copy: get the display value instead
    if (role == Util::CopyRole)
        role = Qt::DisplayRole;

    return TreeItem::data(column, role);
}

bool ZoneItem::setData(int column, const QVariant& value, int role, bool &changed)
{
    if (role == Qt::EditRole)
        role = Util::RawDataRole;

    if (role == Qt::BackgroundRole || column == ZoneModel::Color)
        role = Util::RawDataRole;

    return TreeItem::setData(column, value, role, changed);
}

int ZoneItem::columnCount() const
{
    return ZoneModel::_Count;
}

ZoneItem* ZoneItem::factory(const TreeItem::ItemData& data, TreeItem* parent)
{
    assert(dynamic_cast<ZoneItem*>(parent) != nullptr);

    return new ZoneItem(data, parent);
}
