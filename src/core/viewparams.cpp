/*
    Copyright 2020 Loopdawg Software

    ZombieTrackerGPS is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <src/util/units.h>
#include <src/util/math.h>
#include "viewparams.h"

bool ViewParams::operator==(const ViewParams& rhs) const
{
    return Math::almost_equal(m_centerLat, rhs.m_centerLat) &&
            Math::almost_equal(m_centerLon, rhs.m_centerLon) &&
            Math::almost_equal(m_heading, rhs.m_heading) &&
            m_bounds == rhs.m_bounds &&
            m_zoom == rhs.m_zoom;
}
