#-----------------------------------------------------------------------
# Copyright 2020 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

APPNAME     = zombietrackergps
TEMPLATE    = aux
RCC_DIR     = build/rcc

# Build art resources
artlist = art art-light art-dark icons-light icons-dark geopol us-nps-symbols gps-images

rcc.input = ART
rcc.output = $${OUT_PWD}/$${RCC_DIR}/${QMAKE_FILE_IN_BASE}.rcc
rcc.commands = env LANG=C.UTF-8 rcc --compress 6 --threshold 8 --binary ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
rcc.name = rcc
QMAKE_EXTRA_COMPILERS += rcc

INSTALL_ROOT=$$(INSTALL_ROOT) # use env variable if set
isEmpty(INSTALL_ROOT) {
    INSTALL_ROOT = /usr/local
}

for (art, artlist) {
    ART += $${PWD}/art/$${art}.qrc
    artrcc.path = $${INSTALL_ROOT}/share/$${APPNAME}
    artrcc.files += $${OUT_PWD}/$${RCC_DIR}/$${art}.rcc
}

INSTALLS += artrcc

unix {
    MAKEFILE="Makefile.$$basename(_PRO_FILE_)"
    MAKEFILE=$$replace(MAKEFILE, ".pro", "")

    system("which dpkg-architecture >/dev/null 2>&1") {
        deb.target   = deb
        deb.commands = INSTALL_ROOT=$${OUT_PWD}/debbuild/usr qmake -o $${MAKEFILE} $${_PRO_FILE_}; \
                       make -f $${MAKEFILE} install;

        QMAKE_EXTRA_TARGETS += deb
    }

    # for "make rpm"
    system("which rpmbuild >/dev/null 2>&1") {
        rpm.target = rpm
        rpm.commands = INSTALL_ROOT=$${OUT_PWD}/rpmbuild/usr qmake -o $${MAKEFILE} $${_PRO_FILE_}; \
                       make -f $${MAKEFILE} install;

        QMAKE_EXTRA_TARGETS += rpm
    }
}
