#-----------------------------------------------------------------------
# Copyright 2019-2022 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

TEMPLATE    = subdirs
TARGET      = zombietrackergps
PKGVERSION  = $$cat(VERSION)    # package version
PKGVERSION  = $$replace(PKGVERSION, "\"", "")
PACKAGEDIR  = $${OUT_PWD}/build/package

SUBDIRS += art ztgps tests

art.file = art.pro

ztgps.file = ztgps.pro
ztgps.depends = art

tests.file = tests.pro
tests.depends = ztgps

# for "make tar"
tar.target   = tar
tar.commands = mkdir -p $${PACKAGEDIR}; \
               tar --transform="s:^[.]/:./$${TARGET}-$${PKGVERSION}/:" -C "$$PWD" --exclude '*.json' --exclude '*.pro.user' --exclude-vcs --exclude-backups --owner loopdawg --group loopdawg --xz -cvf "$${PACKAGEDIR}/$${TARGET}-$${PKGVERSION}.tar.xz" .

QMAKE_EXTRA_TARGETS += tar

system("which dpkg-architecture >/dev/null 2>&1") {
    DEB_TARGET_ARCH = $$system(dpkg-architecture -q DEB_TARGET_ARCH)

    deb.target   = deb
    deb.commands = rm -rf $${OUT_PWD}/debbuild; \
                   mkdir -p $${PACKAGEDIR}; \
                   make -f Makefile.art deb; \
                   make -f Makefile.ztgps deb; \
                   mkdir -p $${OUT_PWD}/debbuild/DEBIAN; \
                   sed -e "\"s/Architecture:.*/Architecture: $${DEB_TARGET_ARCH}/\"" $${PWD}/DEBIAN/control > $${OUT_PWD}/debbuild/DEBIAN/control; \
                   dpkg-deb --root-owner-group --build debbuild $${PACKAGEDIR}/$${TARGET}-$${PKGVERSION}_$${DEB_TARGET_ARCH}.deb; \
                   rm -rf $${OUT_PWD}/debbuild

   QMAKE_EXTRA_TARGETS += deb
}

system("which rpmbuild >/dev/null 2>&1") {
   rpm.target   = rpm
   rpm.commands = rm -rf $${OUT_PWD}/rpmbuild; \
                  mkdir -p $${PACKAGEDIR}; \
                  make -f Makefile.art rpm; \
                  make -f Makefile.ztgps rpm; \
                  sed -e "\"s/Version:.*/Version: $${PKGVERSION}/\"" $${PWD}/rpmbuild/SPECS > $${OUT_PWD}/build/SPECS; \
                  find $${OUT_PWD}/rpmbuild/usr -type f | sed "s:$${OUT_PWD}/rpmbuild::" >> $${OUT_PWD}/build/SPECS; \
                  rpmbuild -bb --buildroot $${OUT_PWD}/rpmbuild $${OUT_PWD}/build/SPECS; \
                  mv $(HOME)/rpmbuild/RPMS/*/$${TARGET}-$${PKGVERSION}*.rpm $${PACKAGEDIR}; \
                  rm -rf $${OUT_PWD}/rpmbuild;

   QMAKE_EXTRA_TARGETS += rpm
}

