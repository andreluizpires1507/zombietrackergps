#-----------------------------------------------------------------------
# Copyright 2020 Loopdawg Software
# 
# ZombieTrackerGPS is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#-----------------------------------------------------------------------

QT           += core widgets gui charts Marble svg xml concurrent testlib
CONFIG       += console
TEMPLATE      = app
TARGET        = testztgps
MOC_DIR       = build/moc
OBJECTS_DIR   = build/obj
UI_DIR        = build/ui

# Input
HEADERS      += $$files(src/*.h, true)
SOURCES      += $$files(src/*.cpp, true)
SOURCES      -= $$files(main.cpp, true)  # avoid conflicting main
HEADERS      += $$files(tests/*.h, true)
SOURCES      += $$files(tests/*.cpp, true)
FORMS        += $$files(src/*.ui, true)

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

unix {
    # disable -Wno-odr (One Definition Rule) for link, otherwise qmetatype.h has warnings under gcc 12.x
    QMAKE_LFLAGS="-Wno-odr"
    QMAKE_LFLAGS_RELEASE='-Wl,--strip-all,--dynamic-list-cpp-typeinfo'
    QMAKE_LFLAGS_DEBUG='-Wl,--dynamic-list-cpp-typeinfo'
    QMAKE_CXXFLAGS += -std=c++17 -Wall -pedantic
    QMAKE_CXXFLAGS_RELEASE += -DNDEBUG

    TARGET_MULTIARCH = $$system(gcc -print-multiarch)

    INCLUDEPATH += $$clean_path($$_PRO_FILE_PWD_/../libldutils)
    INCLUDEPATH += /usr/local/include/libldutils
    INCLUDEPATH += /usr/include/libldutils

    LIBS += -L$$clean_path($$_PRO_FILE_PWD_/../libldutils/lib/$${TARGET_MULTIARCH})
    LIBS += -L/usr/lib/$${TARGET_MULTIARCH}
    LIBS += -L/usr/local/lib
    LIBS += -ldl

    CONFIG(debug, debug|release) {
        LIBS += -lldutilsd
    } else {
        LIBS += -lldutils
    }

    # This should already be covered by the Qt += Marble entry near the top, but for some reason that
    # isn't working on Arch Linux.  This adds that library in explicitly, and should be harmless on
    # other distros.
    LIBS += -lmarblewidget-qt5
}

INSTALLS = # don't install anything for tests
